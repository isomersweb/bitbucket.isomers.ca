# Moneris DirectPost for PrestaShop
A PrestaShop module that allows you to accept payments directly from your website using your Moneris eSelectPlus Account.

## Usage

Testing

To use Test mode, first select Test in the configuration.
Go to https://esqa.moneris.com/mpg/index.php and login using the information on that page.
Click on "Admin", click on "directpost config", click on "Generate a New Configuration".
Copy the ps_store_id and hpp_key and paste them in the corresponding fields.
Change Response Method to "Displayed as key/value pairs on our server". Leave the approved/declined fields blank.
Click on Configure Security and check "Enable Transaction Verification" and paste the following url in the box starting with either http//: OR https:// trivium.ca/presta/modules/monerisdirect/validation.php

Live Mode

When you are ready to accept real payments, select Production in the configuration.
Go to https://www3.moneris.com/mpg/index.php and login using your eSelect Plus login.
Click on "Admin", click on "directpost config", click on "Generate a New Configuration".
Copy the ps_store_id and hpp_key and paste them in the corresponding fields.
Change Response Method to "Displayed as key/value pairs on our server". Leave the approved/declined fields blank.
Click on Configure Security and check "Enable Transaction Verification" and paste the following url in the box https://trivium.ca/presta/modules/monerisdirect/validation.php

Receipts

As per Visa and MasterCard's requirements, you must configure email receipts.
Click "Return to main configuration" and click "Configure Email Receipts".
Leave Merchant email address blank if you don't want receipts to be emailed to yourself, Check "Send email to Cardholder on approval". Emails will be sent to the email that the customer signed up with on your website.
Scroll down and fill in the color fields, "000000" for Black, "FFFFFF" for White and "CCCCCC" for Grey.

## Features
* Users can enter their payment information directly on your checkout page without having to leave your website.
* Transaction Validation is performed for each order to make sure that responses sent to your page are not "spoofed" and that you are only receiving the responses once.
* Billing, Shipping and Product information is stored in your Moneris records for each transaction.
* Interac payments, Verified By Visa and MasterCard SecureCode are supported, but need to be enabled on your Moneris Account for a fee.
