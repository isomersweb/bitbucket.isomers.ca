<?php

$displayName = 'Moneris Online Payment';

session_start();
if ($_SESSION['payment']['payment_method'] != 'cc_payment') return false;

include(dirname(__FILE__). '/../../config/config.inc.php');
include(dirname(__FILE__). '/../../init.php');
include(dirname(__FILE__). '/monerishosted.php');


$monerishosted = new MonerisHosted();

$cart = Context::getContext()->cart;

if (!Validate::isLoadedObject($cart))
{
	Logger::addLog('Cart loading failed for cart '.(int)$_POST['order_id'], 4);
	die('Fatal error with the cart '.(int)$_POST['order_id']);
}

require "./mpgClasses.php";

/************************ Request Variables ***************************/

$store_id=Configuration::get('MH_STORE_ID');
$api_token=Configuration::get('MH_HPP_KEY');


/********************* Transactional Variables ************************/

$store_abbr = strtolower(substr(Configuration::get('PS_SHOP_NAME'), 0, 3)) . "-";
$store_abbr = str_replace(" ", "", $store_abbr);
$summary = $cart->getSummaryDetails();
if (strlen($store_abbr) < 3) {
	$store_abbr = "ord-";
}
/* Create random transaction id with store abbreviation prefix */
$order_id = $store_abbr . $cart->id . "-" .substr(number_format(time() * rand(),0,'',''),0,10);

$type='purchase';
//$order_id='ord-'.date("dmy-G:i:s");
//$cust_id='my cust id';
$amount=number_format((float)$cart->getOrderTotal(true, 3), 2, '.', '');
$pan=$_SESSION['payment']['cc_num'];
$expiry_date=$_SESSION['payment']['cc_exp'];		
//$pan='4242424242424242'; 
//$expiry_date='1111';
$crypt='7';

/******************* Customer Information Variables ********************/

$first_name = $summary['delivery']->firstname;
$last_name = $summary['delivery']->lastname;
$company_name = $summary['delivery']->company;
$address = $summary['delivery']->address1;
$city = $summary['delivery']->city;
$province = $summary['delivery_state'];
$postal_code = $summary['delivery']->postcode;
$country = $summary['delivery']->country;
$phone_number = $summary['delivery']->phone;
//$fax = '453-989-9877';
$tax1 = $summary['total_tax'];
//$tax2 = '1.02';
//$tax3 = '1.03';
$shipping_cost = $summary['total_shipping'];
$email =$summary['delivery']->email;
//$instructions =$summary->message;

$inv_first_name = $summary['invoice']->firstname;
$inv_last_name = $summary['invoice']->lastname;
$inv_company_name = $summary['invoice']->company;
$inv_address = $summary['invoice']->address1;
$inv_city = $summary['invoice']->city;
$inv_province = $summary['invoice_state'];
$inv_postal_code = $summary['invoice']->postcode;
$inv_country = $summary['invoice']->country;
$inv_phone_number = $summary['invoice']->phone;

/******************** Customer Information Object *********************/

$mpgCustInfo = new mpgCustInfo();

/********************** Set Customer Information **********************/

$billing = array(
				 'first_name' => $inv_first_name,
                 'last_name' => $inv_last_name,
                 'company_name' => $inv_company_name,
                 'address' => $inv_address,
                 'city' => $inv_city,
                 'province' => $inv_province,
                 'postal_code' => $inv_postal_code,
                 'country' => $inv_country,
                 'phone_number' => $inv_phone_number,
                 //'fax' => $fax,
                 'tax1' => $tax1,
//                 'tax2' => $tax2,
//                 'tax3' => $tax3,
                 'shipping_cost' => $shipping_cost
                 );

$mpgCustInfo->setBilling($billing);

$shipping = array(
				 'first_name' => $first_name,
                 'last_name' => $last_name,
                 'company_name' => $company_name,
                 'address' => $address,
                 'city' => $city,
                 'province' => $province,
                 'postal_code' => $postal_code,
                 'country' => $country,
                 'phone_number' => $phone_number,
                 'tax1' => $tax1,
                 'shipping_cost' => $shipping_cost
                 );

$mpgCustInfo->setShipping($shipping);

$mpgCustInfo->setEmail($email);
//$mpgCustInfo->setInstructions($instructions);

/*********************** Set Line Item Information *********************/

$i = 0;
foreach ($summary['products'] as $product) {
    $item[$i] = array(
	   'name'=>$product['name'],
       'quantity'=>$product['cart_quantity'],
       'product_code'=>$product['reference'],
       'extended_amount'=>$product['total_wt']
       );
    $mpgCustInfo->setItems($item[$i]);    
    if (!empty($product['recurring']) && $product['recurring'] > 0) {
        $recurUnit = 'day'; 
        $startDate = date('Y/m/d'); 
        $numRecurs = '99'; 
        $recurInterval = $product['recurring']; 
        $recurAmount = $product['total_wt']; 
        $startNow = 'false'; 
        
        $recurArray[$i] = array('recur_unit'=>$recurUnit, // (day | week | month) 
             'start_date'=>$startDate, //yyyy/mm/dd 
             'num_recurs'=>$numRecurs, 
             'start_now'=>$startNow, 
             'period' => $recurInterval, 
             'recur_amount'=> $recurAmount 
             ); 
    }
    $i++;    
}

/***************** Transactional Associative Array ********************/

$txnArray=array(
				'type'=>$type,
		        'order_id'=>$order_id,
		        'cust_id'=>$cust_id,
		        'amount'=>$amount,
		        'pan'=>$pan,
		        'expdate'=>$expiry_date,
		        'crypt_type'=>$crypt
	           );

/************************** AVS Variables *****************************/

//$avs_street_number = '201';
$avs_street_name = $inv_address;
$avs_zipcode = $inv_postal_code;
$avs_email = $email;
$avs_hostname = '';
$avs_browser = $_SERVER['HTTP_USER_AGENT'];
$avs_shiptocountry = $country;
//$avs_merchprodsku = '123456';
$avs_custip = $_SERVER['REMOTE_ADDR'];
$avs_custphone = $inv_phone_number;

/************************** CVD Variables *****************************/

$cvd_indicator = '1';
$cvd_value = $_SESSION['payment']['cc_cvd'];

/********************** AVS Associative Array *************************/

$avsTemplate = array(
					 'avs_street_number'=>$avs_street_number,
                     'avs_street_name' =>$avs_street_name,
                     'avs_zipcode' => $avs_zipcode,
                     'avs_hostname'=>$avs_hostname,
					 'avs_browser' =>$avs_browser,
					 'avs_shiptocountry' => $avs_shiptocountry,
//					 'avs_merchprodsku' => $avs_merchprodsku,
					 'avs_custip'=>$avs_custip,
					 'avs_custphone' => $avs_custphone
                    );

/********************** CVD Associative Array *************************/

$cvdTemplate = array(
					 'cvd_indicator' => $cvd_indicator,
                     'cvd_value' => $cvd_value
                    );

/************************** AVS Object ********************************/

$mpgAvsInfo = new mpgAvsInfo ($avsTemplate);

/************************** CVD Object ********************************/

$mpgCvdInfo = new mpgCvdInfo ($cvdTemplate);

/********************** Transaction Object ****************************/

$mpgTxn = new mpgTransaction($txnArray);

/******************** Set Customer Information ************************/

$mpgTxn->setCustInfo($mpgCustInfo);

/************************* Request Object *****************************/

$mpgRequest = new mpgRequest($mpgTxn);

/************************ HTTPS Post Object ***************************/

$mpgHttpPost  =new mpgHttpsPost($store_id,$api_token,$mpgRequest);



foreach ($recurArray as $val) {
    $mpgRecur = new mpgRecur($val); 
    $mpgTxn->setRecur($val);       
}


/****************8********** Response *********************************/

//echo '<pre>';print_r($customer);echo '</pre>';die;

//unset($_SESSION['payment']);
$mpgResponse=$mpgHttpPost->getMpgResponse();

/*echo '<pre>';
print("\nCardType = " . $mpgResponse->getCardType());
print("\nTransAmount = " . $mpgResponse->getTransAmount());
print("\nTxnNumber = " . $mpgResponse->getTxnNumber());
print("\nReceiptId = " . $mpgResponse->getReceiptId());
print("\nTransType = " . $mpgResponse->getTransType());
print("\nReferenceNum = " . $mpgResponse->getReferenceNum());
print("\nResponseCode = " . $mpgResponse->getResponseCode());
print("\nISO = " . $mpgResponse->getISO());
print("\nMessage = " . $mpgResponse->getMessage());
print("\nIsVisaDebit = " . $mpgResponse->getIsVisaDebit());
print("\nAuthCode = " . $mpgResponse->getAuthCode());
print("\nComplete = " . $mpgResponse->getComplete());
print("\nTransDate = " . $mpgResponse->getTransDate());
print("\nTransTime = " . $mpgResponse->getTransTime());
print("\nTicket = " . $mpgResponse->getTicket());
print("\nTimedOut = " . $mpgResponse->getTimedOut());
echo '</pre>';*/

$response_code = $mpgResponse->getResponseCode();
$message = $mpgResponse->getMessage();
if (isset($response_code)) {
	if ($response_code < 50 && $response_code != "null")
	{
		PaymentModule::validateOrder((int)$cart->id, Configuration::get('PS_OS_PAYMENT'), $_POST['charge_total'], $displayName, $message, array(), NULL, false,	$cart->secure_key);

		$url = 'index.php?controller=order-confirmation&';
		if (_PS_VERSION_ < '1.5')
			$url = 'order-confirmation.php?';

		Tools::redirect($url.'id_module='.(int)$monerishosted->id.'&id_cart='.(int)$cart->id.'&key='.$customer->secure_key);
	}

	if ($response_code >= 50 || $response_code == "null")
	{

		$error_message = $message;

		$checkout_type = Configuration::get('PS_ORDER_PROCESS_TYPE') ?
			'order-opc' : 'order';
		$url = _PS_VERSION_ >= '1.5' ?
			'index.php?controller='.$checkout_type.'&' : $checkout_type.'.php?';
		$url .= 'step=4&cgv=1&monerror=1&message='.$error_message;

		if (!isset($_SERVER['HTTP_REFERER']) || strstr($_SERVER['HTTP_REFERER'], 'order'))
			Tools::redirect($url);
		else if (strstr($_SERVER['HTTP_REFERER'], '?'))
			Tools::redirect($_SERVER['HTTP_REFERER'].'&monerror=1&message='.$error_message, '');
		else
			Tools::redirect($_SERVER['HTTP_REFERER'].'?monerror=1&message='.$error_message, '');

		exit;

	}
}

?>

