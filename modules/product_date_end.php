<?php
/*
Select All enabled products with End date and disabled them
*/

define('PRESTASHOP_INTEGRATION_VERSION', true);
define ('PS_DIR', __DIR__ . '/../');
//define('_PS_MODE_DEV_', true);
require_once PS_DIR .'/config/config.inc.php';

if (!defined('_PS_VERSION_'))
	exit;
    
$products = Db::getInstance()->ExecuteS('SELECT id_product, active, date_end
    FROM `'._DB_PREFIX_.'product` p
    WHERE active > 0
    AND date_end < NOW()
    AND date_end > 0');
//d($products);

foreach ($products AS $product) {
    $pd = new Product((int)$product['id_product']);
    $pd->active = 0;
    $pd->update();   
    echo "{$pd->id} <br>";          
}
d('');
