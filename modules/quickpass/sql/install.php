<?php
/**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

$sqls = array();
$sqls[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'quickpass`(
    `id_attempt` INT(10) unsigned NOT NULL AUTO_INCREMENT,
    `id_customer` INT(10) unsigned NOT NULL,
    `id_shop` INT(10) DEFAULT NULL,
    `passkey` VARCHAR(32) DEFAULT NULL,
    `passkey_generated` DATETIME,
    `is_verified` TINYINT(1) unsigned NOT NULL DEFAULT 0,
    `ip` VARCHAR(15) DEFAULT NULL,
    `passkey_verified` DATETIME,
    PRIMARY KEY (`id_attempt`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
';
foreach ($sqls as $sql) {
    if (!Db::getInstance()->execute($sql)) {
        return false;
    }
}
