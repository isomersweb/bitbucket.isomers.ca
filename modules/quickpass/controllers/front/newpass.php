<?php
/**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

class QuickPassNewpassModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public function __construct()
    {
        parent::__construct();
    }
    public function initContent()
    {
        parent::initContent();
        $token = Tools::getValue('token');
        $record = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'quickpass` WHERE `passkey` = \''
        .pSQL($token).'\' AND `is_verified` = 0 AND `id_shop` = '.(int)$this->context->shop->id);
        $this->context->smarty->assign(array(
            'token' => $token,
            'record' => $record,
            'loader' => _MODULE_DIR_.'quickpass/views/img/indicator_bar.gif'
        ));
        $this->setTemplate('newpass.tpl');
    }
    public function setMedia()
    {
        parent::setMedia();
        /*$this->addjQuery();
        $this->addCSS(_MODULE_DIR_.'quickpass/views/css/quickpass.fo.css');
        $this->addJS(_MODULE_DIR_.'quickpass/views/js/quickpass.fo.js');*/
    }
}
