{**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 *}
