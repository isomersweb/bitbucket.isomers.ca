/**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */
 
 $(document).ready(function(){
	$('.lost_password.form-group a').attr('href', newpasslink);
	$('#resltmsg').removeClass('alert-info alert-danger alert-success').html('');	
	$('#qp_passreco').click(function(){
		$('.overlay').removeClass('forcehide');
		$.ajax({
			url:qp_ajax,
			method:'POST',
			data:{
				action:'passreco',
				email:$('#qp_email').val(),
                token:static_token
			},
			context:$(this),
			success:function(data){
				var json = $.parseJSON(data);
				if(json.flag == 1){
					$('#resltmsg').addClass('alert-success').removeClass('alert-danger').html(json.msg);
					$('#qp_email').val('');
				}
				else{
					$('#resltmsg').addClass('alert-danger').removeClass('alert-success').html(json.msg);
				}
				$('.overlay').addClass('forcehide');
			},
			error:function(data){
				$('.overlay').addClass('forcehide');
			}
		});
	});
	$('#resetqp').click(function(){
		$('.passbox .overlay').removeClass('forcehide');
		$.ajax({
			url:qp_ajax,
			method:'POST',
			data:{
				pass:$('#qp_newpass').val(),
				cpass:$('#qp_cnewpass').val(),
				action:'confirmpass',
				qptoken:$('#qptoken').val(),
                token:static_token
			},
			context:$(this),
			success:function(data){
				var json = $.parseJSON(data);
				if(json.flag == 1)
				{
					$('#resetmsg').addClass('alert-success').removeClass('alert-danger').html(json.msg);
				}
				else
				{
					$('#resetmsg').addClass('alert-danger').removeClass('alert-success').html(json.msg);
				}
				$('.passbox .overlay').addClass('forcehide');
			},
			error:function(data){
				$('.passbox .overlay').addClass('forcehide');
			}
		});
	});
});