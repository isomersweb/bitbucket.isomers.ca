Hello {customer_name}

We would like to inform you, that your order #{order_src_id} was merged with your previous order no #{order_dst_id}

Best regards,
{shop_name}