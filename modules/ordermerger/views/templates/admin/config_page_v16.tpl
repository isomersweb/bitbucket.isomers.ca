{*
 * OrderMerger Prestashop module
 *
 * @author    Wiktor Koźmiński
 * @copyright 2017-2017 Wiktor Koźmiński
*}
<style type="text/css">
    .table_order_stats tbody > tr > td {
        padding: 5px;
    }

    .list-action-enable i {
        font-size: 18px;
    }
</style>
<form action="" method="post" class="form-horizontal">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {l s='Module settings' mod='ordermerger'}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="alert alert-info col-md-6">
                {l s='Please select which order statuses should be able to merge (mergeable).' mod='ordermerger'}<br/>
                {l s='Orders such as sent, canceled, delivered should not be able to merge. We recommend enable statuses like "Wait for payment".' mod='ordermerger'}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table_order_stats">
                        <thead>
                            <tr>
                                <th><span class="title_box">{l s='Id' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Status name' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Able to merge?' mod='ordermerger'}</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $order_states as $os}
                            <tr>
                                <td>{$os['id_order_state']|escape:'htmlall':'UTF-8'}</td>
                                <td><span class="label color_field" style="background-color:{$os['color']|escape:'htmlall':'UTF-8'};">{$os['name']|escape:'htmlall':'UTF-8'}</span></td>
                                <td>
                                  <input 
                                    type="checkbox" 
                                    name="config_mergeableStatuses[]" 
                                    value="{$os['id_order_state']|escape:'htmlall':'UTF-8'}"
                                    {if $os['id_order_state']|in_array:$config->mergeableStatuses}checked="checked"{/if}
                                    >
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>

            </div>
            <hr style="border-top: 1px solid #ccc;" />

            <div class="row">
                <div class="alert alert-info col-md-6">
                {l s='After merge operation status of source order will change automatically.' mod='ordermerger'}
                {l s='Please select which status should be set.' mod='ordermerger'}
                ( {l s='We recommend newly created status: "Order merged".' mod='ordermerger'} )<br/>
                {l s='If you want email to be sent to customer after merge orders, go to the status edit panel and select appropriate email template.' mod='ordermerger'}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>{l s='After merge operation status of source order will change to:' mod='ordermerger'}
                    </label>
                    <select name="config_mergedOrderStateId">
                        {foreach $order_states as $os}
                        <option value="{$os['id_order_state']|escape:'htmlall':'UTF-8'}" {if $os['id_order_state'] == $config->mergedOrderStateId}selected="selected"{/if}>{$os['name']|escape:'htmlall':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
        
        <div class="panel-footer">
            <input type="hidden" name="action" value="updateConfig"/>
            <button type="submit" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='ordermerger'}
            </button>
        </div>
    </div>
</form>
