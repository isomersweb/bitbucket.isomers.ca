{*
 * OrderMerger Prestashop module
 *
 * @author    Wiktor Koźmiński
 * @copyright 2017-2017 Wiktor Koźmiński
*}
<div class="refernce-to-dst-order col-lg-6 col-lg-offset-3">
    <div class="panel">
        <h4>{l s='Order' mod='ordermerger'} #{$idOrder|escape:'htmlall':'UTF-8'} {l s='was joined to the order' mod='ordermerger'}
            <a href="{$orderDetailsUrl|escape:'htmlall':'UTF-8'}&id_order={$idWithWhatMerge|escape:'htmlall':'UTF-8'}">
                #{$idWithWhatMerge|escape:'htmlall':'UTF-8'}
            </a>
        </h4><br/>
        <p>{l s='If you want to modify / edit this order click on the link below. We also inform you that modifying joined orders is not recommended.' mod='ordermerger'}
        </p>
        <a href="#" class="share-modification-merge-order">
            {l s='Make available modification' mod='ordermerger'}
        </a>
    </div>
</div>
{if $similarOrders}
<div id="order_merge">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
               <div class="panel-heading">
                    <span><i class="icon-eye"></i> {l s='Other merge-able orders from this client' mod='ordermerger'}</span>
                    <a href="#" id="merge_show_btn" class="btn btn-default dropdown-toggle pull-right"><span class="caret"></span></a>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box">{l s='Order' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Date' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Products quantity' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Sum' mod='ordermerger'}</span></th>
                                <th><span class="title_box">{l s='Status' mod='ordermerger'}</span></th>
                                <th><span class="title_box"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$similarOrders key=k item=order}
                            <tr>
                                <td>
                                    <a href="{$orderDetailsUrl|escape:'htmlall':'UTF-8'}&id_order={$order['order']->id|escape:'htmlall':'UTF-8'}">
                                        #{$order['order']->id|escape:'htmlall':'UTF-8'}
                                    </a>
                                </td>
                                <td>{$order['order']->date_add|escape:'htmlall':'UTF-8'}</td>
                                <td>{$order['products']|count|escape:'htmlall':'UTF-8'}</td>
                                <td>{$order['order']->total_paid|number_format:2:".":","|escape:'htmlall':'UTF-8'}</td>
                                <td>
                                <span class="label color_field" 
                                    style="background-color: {$order['state']->color|escape:'htmlall':'UTF-8'}; color: white;">
                                    {$order['state']->name[{$id_lang|escape:'htmlall':'UTF-8'}]}
                                </span>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary show_window_similitude" data-id-order="{$order['order']->id|escape:'htmlall':'UTF-8'}">
                                        {l s='Merge' mod='ordermerger'}
                                    </button>
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </div>
    <div class="row window_similitude" style="display: none;">

       <div class="save_merge" style="display: none;">
            <div class="col-md-7 col-md-offset-5">
                <div class="panel">
                    <div class="panel-body">
                    <img src="../img/loader.gif" class="img-responsive" style="float: left; margin-right: 10px;">
                        {l s='Saving changes...' mod='ordermerger'}
                        <br/>
                        {l s='Please wait.' mod='ordermerger'}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="unchecked_box box_position" style="display: none;">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel">
                    <div class="panel-body">
                    {l s='Some fields are not checked. Please select which fields should be used in original order.' mod='ordermerger'}
                    </div>
                    <div class="panel-footer my-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary unchecked-ok" style="float: right;">{l s='ok' mod='ordermerger'}</button>
                        </div>
                    </div>    
                    </div>
                </div>
            </div>
        </div>
        <div class="cofirm_window box_position" style="display: none;">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel">
                    <div class="panel-body">
                    {l s='Are you sure? This operation can not be undone!' mod='ordermerger'}
                    </div>
                    <div class="panel-footer my-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger confirm-no" style="float: left;">
                            {l s='Cancel' mod='ordermerger'}
                            </button>
                            <button type="button" class="btn btn-primary confirm-yes" data-srcOrderId="" style="float: right;">
                            {l s='Merge' mod='ordermerger'}
                            </button>
                        </div>
                    </div>    
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-6 col-md-offset-3 similitude">
            <div class="overlay" style="display: none;"></div>
            <div class="panel" >
                <div class="col-md-12 info-similitude">
                    {l s='Below is a list of differences between selected orders. Please select which box should be used in original order.' mod='ordermerger'}
                </div>
                <hr/>
                <div class="panel-heading">
                    <div class="col-md-6">
                        <span class="badge first_order_header"></span>
                        <p style="color: #00aff0;">{l s='ORIGINAL' mod='ordermerger'}</p>
                    </div>
                    <div class="col-md-6">
                        <span class="badge second_order_header"></span>
                        <p style="color: #00aff0;">{l s='JOINED' mod='ordermerger'}</p>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="no-differences" style="display: none;">
                        {l s='There are no differences between these orders.' mod='ordermerger'}
                    </div>
                    <div class="dst_content col-md-6">
                        <button type="button" class="btn btn-default single_diff_order addressFrom" 
                        data-diff-type="addressFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-truck"></i><b> {l s='SHIPPING ADDRESS' mod='ordermerger'}</b>
                            <div class="address_delivery_dst">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order adress_invoice" 
                        data-diff-type="adress_invoice" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-file-text"></i><b> {l s='INVOICE ADDRESS' mod='ordermerger'}</b>
                            <div class="adress_invoice_dst">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order carrierFrom" 
                        data-diff-type="carrierFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-truck"></i><b> {l s='DELIVERY METHOD' mod='ordermerger'}</b>
                            <div class="delivery_method_dst">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order paymentFrom" 
                        data-diff-type="paymentFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-money"></i><b> {l s='PAYMENT' mod='ordermerger'}</b>
                            <div class="payment_method_dst">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order statusFrom" 
                        data-diff-type="statusFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-eye-close"></i><b> {l s='STATUS' mod='ordermerger'}</b>
                            <div class="status_dst" style="white-space: normal!important;">
                            </div>
                        </button>
                    </div>
                    <div class="src_content col-md-6">
                        <button type="button" class="btn btn-default single_diff_order addressFrom" 
                        data-diff-type="addressFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-truck"></i><b> {l s='SHIPPING ADDRESS' mod='ordermerger'}</b>
                            <div class="address_delivery_src">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order adress_invoice" 
                        data-diff-type="adress_invoice" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-file-text"></i><b> {l s='INVOICE ADDRESS' mod='ordermerger'}</b>
                            <div class="adress_invoice_src">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order carrierFrom" 
                        data-diff-type="carrierFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-truck"></i><b> {l s='DELIVERY METHOD' mod='ordermerger'}</b>
                            <div class="delivery_method_src">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order paymentFrom" 
                        data-diff-type="paymentFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-money"></i><b> {l s='PAYMENT' mod='ordermerger'}</b>
                            <div class="payment_method_src">
                            </div>
                        </button>
                        <button type="button" class="btn btn-default single_diff_order statusFrom" 
                        data-diff-type="statusFrom" style="display: none;">
                            <div class="icon_check" style="display: none;"><i class="icon-check" style="float:right;"></i></div>
                            <i class="icon-eye-close"></i><b> {l s='STATUS' mod='ordermerger'}</b>
                            <div class="status_src" style="white-space: normal!important;">
                            </div>
                        </button>
                    </div>
                </div>
                <div class="panel-footer my-footer">
                    <div class="row">
                        <div class="checkbox col-md-6">
                            {* <label>
                                <input type="checkbox" value="" class="sendEmail">
                                Po połaczeniu, wyślij mail'a do klienta.
                            </label> *}
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger back-to-order" style="float: left;">
                            {l s='Cancel' mod='ordermerger'}
                            </button>
                            <button type="button" class="btn btn-primary merge-select-order" style="float: right;">
                            {l s='Merge' mod='ordermerger'}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
{/if}
<script type="text/javascript">
  var ddstOrderId = '{$idOrder|escape:'javascript':'UTF-8'}';
  var uurlOrderMergerControler = '{$restUrl|escape:'javascript':'UTF-8'}';
  var iisMerge = '{$isMerge|escape:'javascript':'UTF-8'}';
</script>
