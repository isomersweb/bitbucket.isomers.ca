<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Merger;

use Ordermerger\Merger\MergeOptions;
use Ordermerger\Common\Config;

/**
 * Allows to merge two orders into one based on given options
 */
class OrderMerger
{
    /** @var Config configuration */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function merge(\Order $srcOrder, \Order $dstOrder, MergeOptions $options)
    {
        if ($srcOrder->id_currency != $dstOrder->id_currency) {
            throw new \Exception("Cant merge orders with different currency");
        }

        // 1. przepisz adres jesli ma zostac uzyty z src
        if ($options->getAddressFrom() == MergeOptions::SRC_ORDER) {
            $dstOrder->id_address_delivery = $srcOrder->id_address_delivery;
        }

        // 2. przepis stats jesli ma zostac uzyty z src
        if ($options->getStatusFrom() == MergeOptions::SRC_ORDER) {
            $history = new \OrderHistory();
            $history->id_order = $dstOrder->id;
            $history->changeIdOrderState($srcOrder->current_state, $dstOrder->id);
            $history->add(true);
            $dstOrder->current_state = $srcOrder->current_state;
        }

        // 3. przepisz przewoznika jesli ma zostac uzyty z src
        if ($options->getCarrierFrom() == MergeOptions::SRC_ORDER) {
            $dstOrder->id_carrier = $srcOrder->id_carrier;
        }

        // 4. kopiowanie / przenoszenie produktow...
        $this->duplicateProducts($srcOrder, $dstOrder);
        $this->summarizeOrderTotals($srcOrder, $dstOrder);
        
        // 5. Zmien status łączonego zam.
        $history = new \OrderHistory();
        $history->id_order = $srcOrder->id;
        $history->changeIdOrderState($this->config->mergedOrderStateId, $srcOrder->id);
        $history->addWithemail();
        $srcOrder->current_state = (int)$this->config->mergedOrderStateId;
        $srcOrder->save();

        $dstOrder->save();
    }

    private function duplicateProducts(\Order $srcOrder, \Order $dstOrder)
    {
        $productsSQL = $srcOrder->getProductsDetail();

        foreach ($productsSQL as $p) {
            $tmpOrderDetail = new \OrderDetail($p['id_order_detail']);
            $duplicated = $tmpOrderDetail->duplicateObject();
            $duplicated->id_order = $dstOrder->id;
            $duplicated->update();
        }
    }

    private function summarizeOrderTotals(\Order $srcOrder, \Order $dstOrder)
    {
        $fieldsToSummarize = array('total_paid', 'total_paid_tax_incl', 'total_paid_tax_excl', 'total_products', 'total_products_wt');
        foreach ($fieldsToSummarize as $field) {
            $dstOrder->$field = (float) $dstOrder->$field + (float) $srcOrder->$field;
        }
    }
}
