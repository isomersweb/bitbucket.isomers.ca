<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Common;

/**
* Configuration class, store all config in one DB record in JSON
*/
class Config
{

    const CONFIG_COLUMN_NAME = 'wk_ordermerger_config';

    /** Config data */
    public $mergeableStatuses = array();
    public $mergedOrderStateId = 0;
    /** --- */
    
    public function __construct($initialLoad = true)
    {
        if ($initialLoad) {
            $this->load();
        }
    }

    /**
     * Purges (removes) current configuration from presta db
     * @return [type] [description]
     */
    public static function purge()
    {
        \Configuration::deleteByName(self::CONFIG_COLUMN_NAME);
    }

    /**
     * Saves current config into DB.
     * @param  boolean $getPostValues if set to true, get post values with 'config_' prefix
     * @return void
     */
    public function update($getPostValues = true)
    {
        if ($getPostValues) {
            $this->assignPostValues();
        }
        $json = \Tools::jsonEncode($this);
        \Configuration::updateValue(self::CONFIG_COLUMN_NAME, $json);
    }

    /**
     * Loads config from DB.
     * @return boolean false if values couldn't been loaded
     */
    public function load()
    {
        $json = \Configuration::get(self::CONFIG_COLUMN_NAME);
        if ($json === false) {
            return false;
        }
        
        $values = \Tools::jsonDecode($json, true);

        if (is_array($values)) {
            foreach ($values as $key => $v) {
                if (property_exists($this, $key)) {
                    $this->$key = $v;
                }
            }
        }

        return true;
    }

    /**
     * Assign post values with prefix 'config_' to class vars.
     * @return void
     */
    private function assignPostValues()
    {
        foreach (get_object_vars($this) as $key => $value) {
            $v = \Tools::getValue('config_'.$key);
            if ($v !== false) {
                $this->$key = $v;
            }
        }
    }
}
