<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Common;

class ModuleMailer
{
    public static function notifyAboutMerge(\Order $srcOrder, \Order $dstOrder)
    {
        $customer = new \Customer($srcOrder->id_customer);

        $params = array(
            '{order_src_id}' => $srcOrder->id,
            '{order_dst_id}' => $dstOrder->id,
            '{customer_name}' => $customer->lastname . ' ' . $customer->firstname,
            '{customer_email}' => $customer->email,
        );

        \Mail::Send(
            (int)$srcOrder->id_lang,
            'orders_merged',
            'Your orders was merged into one',
            $params,
            $customer->email,
            $customer->firstname.' '.$customer->lastname,
            null,
            null,
            null,
            null,
            dirname(__FILE__) . '/../../mails/',
            false,
            (int)$srcOrder->id_shop
        );
    }
}
