{*
* Admin - List of country/currency
* 
*}
<fieldset class="width">
    <input type='hidden' name='banned_submit' value='1' />
	<table class='table table-striped table-bordered' id='tblSB'>
		<thead>
			<tr><th>{l s='Shop' mod='currencybycountry'}</th><th>{l s='Country' mod='currencybycountry'}</th><th>{l s='Banned Product' mod='currencybycountry'} 
			
			<a href="javascript:;" onclick="$('.cbc_product_country').prop('checked', true).attr('checked', 'checked')">{l s='Select all' mod='currencybycountry'}</a> /
			<a href="javascript:;" onclick="$('.cbc_product_country').prop('checked', false).removeAttr('checked')">{l s='Unselect all' mod='currencybycountry'}</a>
			
			</th></tr>
		</thead>
		<tbody>
			{foreach from=$countries key=k2 item=country name="countries"}
				{foreach from=$shops key=k1 item=shop name="shops"}
					<tr>
						<td>{$shop.name|escape:'htmlall':'UTF-8'}</td>
						<td>{$country.name|escape:'htmlall':'UTF-8'}</td>
						<td><input class="cbc_product_country" {if $actual_product && $actual_product[$shop.id_shop] && $actual_product[$shop.id_shop][$country.id_country]}checked="checked"{/if} type='checkbox' name='banned[{$shop.id_shop|escape:'htmlall':'UTF-8'}][{$country.id_country|escape:'htmlall':'UTF-8'}]' value='1' /></td>
					</tr>
				{/foreach}
			{/foreach}
		</tbody>
	</table>
</fieldset>
<div class="panel-footer">
    <a href="{$link->getAdminLink('AdminProducts')|escape:'htmlall':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel' mod='currencybycountry'}</a>
    <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='currencybycountry'}</button>
    <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay' mod='currencybycountry'}</button>
</div>