<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{currencybycountry}prestashop>currencybycountry_54af26efb66a5813bb14b565d3db2ca6'] = 'Configuración GeoIP';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_19cbd04c3657ecd2d2a615a7d3561083'] = 'Cambia la moneda de la tienda de forma automática y bloquea productos por país utilizando Geoip.';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_fdea3303ac75dc8b92ff62814270f615'] = 'Este producto no se puede comprar en su país.';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_4b6a3de80ba022e733335f3966678a71'] = 'Fallo la creación de tablas en la base de datos.';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Todos';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_9f82518d468b9fee614fcc92f76bb163'] = 'Tienda';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_59716c97497eb9694541f7c3d37b1a4d'] = 'Pais';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_9992b87e9c9e3c1e72977cf9023dd7e3'] = 'Moneda por Defecto';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_bf94ba2051e7c38b925ae5c2ab3a897c'] = 'Forzar Moneda';
$_MODULE['<{currencybycountry}prestashop>currencybycountry_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{currencybycountry}prestashop>product_extra_9f82518d468b9fee614fcc92f76bb163'] = 'Tienda';
$_MODULE['<{currencybycountry}prestashop>product_extra_59716c97497eb9694541f7c3d37b1a4d'] = 'Pais';
$_MODULE['<{currencybycountry}prestashop>product_extra_4d305367d60ee26b328f2047ac44a680'] = 'Bloquear Producto';
$_MODULE['<{currencybycountry}prestashop>product_extra_4c41e0bd957698b58100a5c687d757d9'] = 'Seleccionar Todo';
$_MODULE['<{currencybycountry}prestashop>product_extra_237c7b6874386141a095e321c9fdfd38'] = 'Deseleccionar Todo';
$_MODULE['<{currencybycountry}prestashop>product_extra_ea4788705e6873b424c65e91c2846b19'] = 'Cancelar';
$_MODULE['<{currencybycountry}prestashop>product_extra_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{currencybycountry}prestashop>product_extra_9ea67be453eaccf020697b4654fc021a'] = 'Guardar y permanecer';
