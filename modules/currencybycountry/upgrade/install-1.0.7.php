<?php
/**
* Upgrade de MercadoPago
*
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_0_7($instance)
{
    if (!$instance || $instance->name != 'currencybycountry') {
        $instance = Module::getInstanceByName('currencybycountry');
    }
    return $instance->upgradeOverride();
}
