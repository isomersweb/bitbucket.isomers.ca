<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StatsTeam extends ModuleGrid
{
	private $html;
	private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statsteam';
		$this->tab = 'analytics_stats';
		$this->version = '1.0.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

		parent::__construct();

				$this->default_sort_column = 'lastname';
		$this->default_sort_direction = 'DESC';
		$this->empty_message = $this->l('Empty recordset returned');
		$this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');

		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		/*$this->columns = array(
			array(
				'id' => 'lastname',
				'header' => $this->l('Last Name'),
				'dataIndex' => 'lastname',
				'align' => 'center'
			),
			array(
				'id' => 'firstname',
				'header' => $this->l('First Name'),
				'dataIndex' => 'firstname',
				'align' => 'center'
			),
			array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'center'
			),
			array(
				'id' => 'totalBPs',
				'header' => $this->l('Visits'),
				'dataIndex' => 'totalBPs',
				'align' => 'center'
			),
			array(
				'id' => 'totalCredits',
				'header' => $this->l('Credits'),
				'dataIndex' => 'totalCredits',
				'align' => 'center'
			),
			array(
				'id' => 'totalCommissions',
				'header' => $this->l('Commissions'),
				'dataIndex' => 'totalCommissions',
				'align' => 'center'
			),
		);*/

		$this->displayName = $this->l('Team Report by Regions');
//		$this->description = $this->l('Adds a Report based on Beauty Points to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		return (parent::install() && $this->registerHook('AdminStatsModules'));
	}

	public function hookAdminStatsModules($params)
	{
/*		$engine_params = array(
			'id' => 'id_customer',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMessage' => $this->empty_message,
			'pagingMessage' => $this->paging_message
		);*/

        $this->getData();  		    
        
		if (Tools::getValue('export')) {
		  if (empty($id_customer)) {
		      $this->_csv = 
                $this->l('Month').';'
                .$this->l('Group').';'
                .$this->l('Country').';'
                .$this->l('Quantity').';';
		  }
          else {}
          $this->_csv .= "\n";
          
	      foreach ($this->_values as $val) {
            $this->_csv .= $val['cl_date'].';';
            $this->_csv .= $val['cl_group'].';';
            $this->_csv .= $val['cl_country'].';';
            $this->_csv .= $val['qty'].';';
            $this->_csv .= "\n";
          }
          $this->_displayCsv();
		}
		$this->html = '
		<div class="panel-heading">
			'.$this->displayName.'
		</div>';
        $dates = array_column($this->_values, 'cl_date', 'date_upd');
        $dates = array_unique($dates);
        $dates_array = array();
        ksort($dates);
        $this->html .= '<div><table class="table"><thead><tr><th></th>';
        foreach ($dates as $key => $val) {
            $this->html .= '<th class="center"><h4></h4><span class="title_box active">'.$val.'</span></th>';
            $dates_array[strtotime($val)]= $val;
        }
        $this->html .= '</tr></thead><tbody>';
        $gr_prev = $cl_date = '';
        $da_date = 0;
        $cols_count = count($dates_array);
        foreach ($this->_values as $val) {
            if ($gr_prev != $val['cl_group']) {
                if (!empty($gr_prev)) {
                    $this->group_total($dates_array, $gr_total);
                    unset($gr_total);
                    
                }
                $this->html .= '<tr><td class="left panel-heading" colspan="'.($cols_count+1).'">'.$val['cl_group'].'</td></tr>';
                $gr_prev = $val['cl_group'];
                $cl_country = '';
                $gr_total = array();
            }
            if ($cl_country != $val['cl_country']) {
                if (!empty($cl_country)) {
                    end($dates_array);
                    end($gr_total);
                    if (key($gr_total) < key($dates_array)) $this->html .= '<td colspan="99"></td>';
                    $this->html .= '</tr><tr>';                     
                }
                else {
                    $this->html .= '<tr>'; 
                }
                $cl_country = $val['cl_country']; 
                $da_date = 0;
                $this->html .= '<td class="left" style="padding-left: 20px;">'.$val['cl_country'].'</td>'; 
            }
            
            $cdate = strtotime($val['cl_date']);                
            foreach ($dates_array as $dkey => $dval) {
                if ($dkey > $cdate) {
                    $this->html .= '<td></td>'; 
                }
                else if ($dkey < $cdate) {
                    if ($da_date < $dkey) {
                        $this->html .= '<td></td>'; 
                    }
                    continue;
                }
                else {
                    $this->html .= '<td class="center">'.$val['qty'].'</td>';
                    $da_date = $dkey;
                    $gr_total[$dkey] += $val['qty'];  
                    break;
                }
            }
        }
        //d();
        $this->group_total($dates_array, $gr_total);
        unset($gr_total);
        
		$this->html .= '</tbody></table></div>';
        $this->html .= '<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=').'1">
			<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
		</a>';
		return $this->html;	

	}    
    
	public function getData()
	{
        $lang = (int)$this->context->language->id;
        $this->query = '
        SELECT DATE_FORMAT(log.`date_upd`, \'%b %Y\') AS cl_date, 
            log.`date_upd` AS date_upd,
            gr.`name` as cl_group,
            COUNT(log.`date_upd`) AS qty,
            cl.`name` AS cl_country
        FROM `'._DB_PREFIX_.'customer_group_log` log
        LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.id_customer = log.id_customer
        LEFT JOIN `'._DB_PREFIX_.'address` ad ON ad.id_address = (
            SELECT MIN(ad2.`id_address`) FROM  is_address ad2 
            WHERE ad2.`id_customer` = log.`id_customer`)
        LEFT JOIN `'._DB_PREFIX_.'group_lang` gr ON gr.id_group = log.id_group
        LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON ad.id_country = cl.id_country
        WHERE log.`date_upd` BETWEEN '.$this->getDate()
        	.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c')
            .' AND cl.`id_lang` = ' .$lang
            .' AND gr.`id_lang` = ' .$lang
            .' AND log.`date_upd` IN (
                SELECT MAX(log2.`date_upd`) 
                FROM `'._DB_PREFIX_.'customer_group_log` log2
                WHERE log2.`date_upd` BETWEEN '.$this->getDate().'
                GROUP BY YEAR(log2.date_upd), MONTH(log2.date_upd),log2.id_customer
        )
        GROUP BY cl_group, cl_country, cl_date
        ORDER BY cl_group ASC, cl_country ASC, log.`date_upd` ASC';
                
        if (($this->_start === 0 || Validate::IsUnsignedInt($this->_start)) && Validate::IsUnsignedInt($this->_limit))
        	$this->query .= ' LIMIT '.(int)$this->_start.', '.(int)$this->_limit;
        $this->_values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
        //$this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
        //echo $this->query; p($this->_values); d();
	}
    private function group_total($dates_array, $gr_total) {
        end($dates_array);
        end($gr_total);
        if (key($gr_total) < key($dates_array)) $this->html .= '<td colspan="99"></td>';
        $this->html .= '</tr>';
        $this->html .= '<tr><td><h4>'.$this->l('Total by Group:').'</h4></td>';
        foreach ($dates_array as $key => $val) {
            if (array_key_exists($key, $gr_total)) {                            
                $this->html .= '<td class="center"><h4>'.$gr_total[$key].'</h4></td>';                        
            }
            else {
                 $this->html .= '<td></td>';
            }
        }
        $this->html .= '</tr>';   
    }
}
