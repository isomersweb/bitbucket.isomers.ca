<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StatsBestCustomers extends ModuleGrid
{
	private $html;
	private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statsbestcustomers';
		$this->tab = 'analytics_stats';
		$this->version = '1.5.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

		parent::__construct();

		$this->default_sort_column = 'totalMoneySpent';
		$this->default_sort_direction = 'DESC';
		$this->empty_message = $this->l('Empty recordset returned');
		$this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');

		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$this->columns = array(
			array(
				'id' => 'lastname',
				'header' => $this->l('Last Name'),
				'dataIndex' => 'lastname',
				'align' => 'center'
			),
			array(
				'id' => 'firstname',
				'header' => $this->l('First Name'),
				'dataIndex' => 'firstname',
				'align' => 'center'
			),
			array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'center'
			),
			array(
				'id' => 'totalVisits',
				'header' => $this->l('Visits'),
				'dataIndex' => 'totalVisits',
				'align' => 'center'
			),
			array(
				'id' => 'totalValidOrders',
				'header' => $this->l('Valid orders'),
				'dataIndex' => 'totalValidOrders',
				'align' => 'center'
			),
			array(
				'id' => 'totalBeautyPoints',
				'header' => $this->l('Beauty Points '),
				'dataIndex' => 'totalBeautyPoints',
				'align' => 'center'
			),
			array(
				'id' => 'totalMoneySpent',
				'header' => $this->l('Money spent').' ('.Tools::safeOutput($currency->iso_code).')',
				'dataIndex' => 'totalMoneySpent',
				'align' => 'center'
			)
		);

		$this->displayName = $this->l('Best customers');
		$this->description = $this->l('Adds a list of the best customers to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		return (parent::install() && $this->registerHook('AdminStatsModules'));
	}

	public function hookAdminStatsModules($params)
	{
		$engine_params = array(
			'id' => 'id_customer',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMessage' => $this->empty_message,
			'pagingMessage' => $this->paging_message
		);
        $currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

        $this->getData();  		    
        $id_customer = Tools::getValue('id_customer');
        
		if (Tools::getValue('export')) {
		  if (empty($id_customer)) {
		      $this->_csv = $this->l('id_customer').';'
                .$this->l('Last Name').';'
                .$this->l('First Name').';'
                .$this->l('Email').';'
                .$this->l('City').';'
                .$this->l('State').';'
                .$this->l('Country').';'
                .$this->l('Visits').';'
                .$this->l('Valid orders').';'
                .$this->l('Money spent').' ('.Tools::safeOutput($currency->iso_code).')'.';'
                .$this->l('Beauty Points').';'
                .$this->l('Last Order Date').';';							
		  }
          else {
		      $this->_csv = $this->l('id_customer').';'
                .$this->l('Last Name').';'
                .$this->l('First Name').';'
                .$this->l('Order Number').';'
                .$this->l('Order Date').';'
                .$this->l('Shipping Date').';'
                .$this->l('Shipping Cost').';'							
                .$this->l('Order Total').';'
                .$this->l('Tax').';'
                .$this->l('Products Qty').';'
                .$this->l('BP Used').';'
                .$this->l('BP Accrued').';'
                .$this->l('Refund').';'
                .$this->l('Source').';'
                .$this->l('Customer Group').';';
          }
          $this->_csv .= "\n";
          
	      foreach ($this->_values as $val) {
            foreach ($val as $val1) {
                $this->_csv .= $val1;
                $this->_csv .= ';';
            }
            $this->_csv .= "\n";
          }
          $this->_displayCsv();
		}
			

		$this->html = '
		<div class="panel-heading">
			'.$this->displayName.'
		</div>
		<h4>'.$this->l('Guide').'</h4>
			<div class="alert alert-warning">
				<h4>'.$this->l('Develop clients\' loyalty').'</h4>
				<div>
					'.$this->l('Keeping a client can be more profitable than gaining a new one. That is one of the many reasons it is necessary to cultivate customer loyalty.').' <br />
					'.$this->l('Word of mouth is also a means for getting new, satisfied clients. A dissatisfied customer can hurt your e-reputation and obstruct future sales goals.').'<br />
					'.$this->l('In order to achieve this goal, you can organize:').'
					<ul>
						<li>'.$this->l('Punctual operations: commercial rewards (personalized special offers, product or service offered), non commercial rewards (priority handling of an order or a product), pecuniary rewards (bonds, discount coupons, payback).').'</li>
						<li>'.$this->l('Sustainable operations: loyalty points or cards, which not only justify communication between merchant and client, but also offer advantages to clients (private offers, discounts).').'</li>
					</ul>
					'.$this->l('These operations encourage clients to buy products and visit your online store more regularly.').'
				</div>
			</div>';
        //echo '<pre>';
        if (empty($id_customer)) {
            $this->html .= '
				<div>
					<table class="table"><thead><tr>
    						<th>
								<span class="title_box  active">'.$this->l('Last Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('First Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Email').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('City').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('State').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Country').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Visits').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Valid orders').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Money spent').' ('.Tools::safeOutput($currency->iso_code).')'.'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Beauty Points').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Last Order Date').'</span>
							</th>
                    </tr></thead><tbody>';
            
            foreach ($this->_values as $val) {
                $this->html .= '<tr>';
                $this->html .= '<td class="left"><a href="?controller=AdminStats&module=statsbestcustomers&id_customer='.$val['id_customer'].'&token='.Tools::getValue('token').'">'.$val['lastname'].'</a></td>';        
                $this->html .= '<td class="left"><a href="?controller=AdminStats&module=statsbestcustomers&id_customer='.$val['id_customer'].'&token='.Tools::getValue('token').'">'.$val['firstname'].'</a></td>';        
                $this->html .= '<td class="left">'.$val['email'].'</td>';        
                $this->html .= '<td class="left">'.$val['city'].'</td>';        
                $this->html .= '<td class="left">'.$val['state'].'</td>';        
                $this->html .= '<td class="left">'.$val['country'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalVisits'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalValidOrders'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalMoneySpent'].'</td>';  
                $this->html .= '<td class="center">'.$val['totalBeautyPoints'].'</td>';  
                $this->html .= '<td class="center">'.$val['last_order_date'].'</td>';        
                $this->html .= '</tr>';                
            }
    		$this->html .= '</tbody></table>';
            $this->html .= '<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=').'1">
    			<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
    		</a>';
    		//echo 'done';die;
    		return $this->html;	
        }
        else {
             $this->html .= '
				<div>
					<table class="table"><thead><tr>
    						<th>
								<span class="title_box  active">'.$this->l('Last Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('First Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Order Num').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Products Qty').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Order Date').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Shipping Date').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Tax').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Order Total').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Shipping Cost').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('BP Converted').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('BP Accrued').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Source').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Account').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Refunds').'</span>
							</th>
                    </tr></thead><tbody>';
            $token_order = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$this->context->employee->id);
            $token_customer = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$this->context->employee->id);
            foreach ($this->_values as $val) {
                $this->html .= '<tr>';
                $this->html .= '<td class="left"><a href="?tab=AdminCustomers&id_customer='.$val['id_customer'].'&viewcustomer&token='.$token_customer.'">'.$val['lastname'].'</a></td>';        
                $this->html .= '<td class="left"><a href="?tab=AdminCustomers&id_customer='.$val['id_customer'].'&viewcustomer&token='.$token_customer.'">'.$val['firstname'].'</a></td>';        
                $this->html .= '<td class="left"><a href="?tab=AdminOrders&id_order='.$val['id_order'].'&vieworder&token='.$token_order.'">'.$val['id_order'].'</a></td>';        
                $this->html .= '<td class="left">'.$val['order_qty'].'</td>';        
                $this->html .= '<td class="left">'.$val['date_add'].'</td>';        
                $this->html .= '<td class="left">'.$val['shipping_date'].'</td>';        
                $this->html .= '<td class="center">'.Tools::displayprice($val['tax'], $currency).'</td>';        
                $this->html .= '<td class="center">'.Tools::displayprice($val['total_paid'], $currency).'</td>';  
                $this->html .= '<td class="center">'.Tools::displayprice($val['total_shipping'], $currency).'</td>';  
                $this->html .= '<td class="center">'.$val['bp_converted'].'</td>';  
                $this->html .= '<td class="center">'.$val['bp_added'].'</td>';  
                $this->html .= '<td class="center">'.$val['source'].'</td>';  
                $this->html .= '<td class="center">'.$val['customer_group'].'</td>';        
                $this->html .= '<td class="center">'.Tools::displayprice($val['refund'], $currency).'</td>';        
                $this->html .= '</tr>';                
            }
    		$this->html .= '</tbody></table>';
            $this->html .= '<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=').'1">
    			<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
    		</a>';
    		//echo 'done';die;
    		return $this->html;	                            
        }	
	}    
    
	public function getData()
	{
	   $id_customer = Tools::getValue('id_customer');
       if (empty($id_customer)) {
    		$this->query = '
    		SELECT SQL_CALC_FOUND_ROWS c.`id_customer`, c.`lastname`, c.`firstname`, c.`email`, ad.city, st.name as state, cl.name as country,
    			COUNT(co.`id_connections`) as totalVisits,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(op.`amount`, 0) / cu.conversion_rate), 2)
    				FROM `'._DB_PREFIX_.'orders` o
    				LEFT JOIN `'._DB_PREFIX_.'order_payment` op ON o.reference = op.order_reference
    				LEFT JOIN `'._DB_PREFIX_.'currency` cu ON o.id_currency = cu.id_currency
    				WHERE o.id_customer = c.id_customer
    				AND o.invoice_date BETWEEN '.$this->getDate().'
    				AND o.valid
    			), 0) as totalMoneySpent,
    			IFNULL((
    				SELECT COUNT(*)
    				FROM `'._DB_PREFIX_.'orders` o
    				WHERE o.id_customer = c.id_customer
    				AND o.invoice_date BETWEEN '.$this->getDate().'
    				AND o.valid
    			), 0) as totalValidOrders,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0))* '.(int)Myconf::get('REWARDS_VIRTUAL_VALUE_1', null, 2).', 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.id_customer = c.id_customer
    				AND r.date_add BETWEEN '.$this->getDate().'
                    AND r.id_template = 8
    				AND r.virt = 1
    			), 0) as totalBeautyPoints,
    			IFNULL((
    				SELECT DATE_FORMAT(date_add, \'%d-%m-%Y\')
    				FROM `'._DB_PREFIX_.'orders` o
    				WHERE o.id_customer = c.id_customer
    				AND o.invoice_date BETWEEN '.$this->getDate().'
    				AND o.valid
                    ORDER BY date_add DESC
                    LIMIT 1
    			), 0) as last_order_date
     		FROM `'._DB_PREFIX_.'customer` c
    		LEFT JOIN `'._DB_PREFIX_.'guest` g ON c.`id_customer` = g.`id_customer`
    		LEFT JOIN `'._DB_PREFIX_.'connections` co ON g.`id_guest` = co.`id_guest`
            LEFT JOIN `'._DB_PREFIX_.'address` ad ON c.id_customer = ad.id_customer
    		LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON ad.id_country = cl.id_country
    		LEFT JOIN `'._DB_PREFIX_.'state` st ON ad.id_state = st.id_state		
    		WHERE co.date_add BETWEEN '.$this->getDate()
    			.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c').
    			'GROUP BY c.`id_customer`, c.`lastname`, c.`firstname`, c.`email`';
    
    		if (Validate::IsName($this->_sort))
    		{
    		      if (empty($this->_sort)) $this->_sort = 'totalMoneySpent';
    			$this->query .= ' ORDER BY `'.bqSQL($this->_sort).'` DESC';
    			if (isset($this->_direction) && Validate::isSortDirection($this->_direction))
    				$this->query .= ' '.$this->_direction;
    		}
    //echo $this->query; die;
    		if (($this->_start === 0 || Validate::IsUnsignedInt($this->_start)) && Validate::IsUnsignedInt($this->_limit))
    			$this->query .= ' LIMIT '.(int)$this->_start.', '.(int)$this->_limit;
            $this->_values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
    		$this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
        }
        else {
            $this->query = '
    		SELECT SQL_CALC_FOUND_ROWS c.`id_customer`, c.`lastname`, c.`firstname`, o.`id_order`, o.`date_add`, DATE_FORMAT(oc.`date_add`, \'%d-%m-%Y\') as shipping_date, o.`total_shipping`, o.`total_paid`, (o.`total_paid_tax_incl` - o.`total_paid_tax_excl`) as tax, 
    			IFNULL((
    				SELECT COUNT(*)
    				FROM `'._DB_PREFIX_.'order_detail` od
    				WHERE o.id_order = od.id_order
    			), 0) as order_qty,
    			IFNULL((
    				SELECT SUM(ocr.`value_tax_excl`)/'. Configuration::get('PS_LOYALTY_POINT_VALUE') .'
    				FROM `'._DB_PREFIX_.'order_cart_rule` ocr
                    WHERE ocr.id_order = o.id_order
    				AND ocr.`name` = \''. Configuration::get('PS_LOYALTY_VOUCHER_DETAILS', $this->context->language->id) .'\'                    
    			), 0) as bp_used,
    			IFNULL((
    				SELECT SUM(lo.`points`)
    				FROM `'._DB_PREFIX_.'loyalty` lo
    				WHERE lo.id_order = o.id_order
    			), 0) as bp_added,
    			IFNULL((
    				SELECT SUM(osl.`amount`)
    				FROM `'._DB_PREFIX_.'order_slip` osl
    				WHERE osl.id_order = o.id_order
    			), 0) as refund, src.`name` as source,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0))* '.(int)Myconf::get('REWARDS_VIRTUAL_VALUE_1', null, 2).', 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.date_add BETWEEN '.$this->getDate().'
                    AND r.id_order = o.id_order
                    AND r.id_template = 8
    				AND r.virt = 1
    			), 0) as bp_added,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0))* '.(int)Myconf::get('REWARDS_VIRTUAL_VALUE_1', null, 2).', 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.date_add BETWEEN '.$this->getDate().'
                    AND r.id_order = o.id_order
                    AND r.id_template = 8
    				AND r.virt = 1
                    AND r.id_reward_state=4
    			), 0) as bp_converted,
    			(
    				SELECT gl.`name`
    				FROM `'._DB_PREFIX_.'group_lang` gl
                    LEFT JOIN `'._DB_PREFIX_.'customer` cg ON cg.`id_default_group` = gl.`id_group`
    				WHERE cg.id_customer = '. $id_customer .'
    				AND gl.id_lang = '. $this->context->language->id .'
    			) as customer_group
     		FROM `'._DB_PREFIX_.'orders` o
    		LEFT JOIN `'._DB_PREFIX_.'customer` c ON o.`id_customer` = c.`id_customer`
    		LEFT JOIN `'._DB_PREFIX_.'order_carrier` oc ON o.`id_order` = oc.`id_order`
    		LEFT JOIN `'._DB_PREFIX_.'order_source` src ON o.`source` = src.`id_source`
            WHERE c.id_customer = '. $id_customer .'
                AND o.date_add BETWEEN '.$this->getDate()
    			.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c').
    			'GROUP BY c.`id_customer`, o.`id_order` ORDER BY o.`id_order` DESC';
    
    		/*if (Validate::IsName($this->_sort))
    		{
    		      if (empty($this->_sort)) $this->_sort = 'totalMoneySpent';
    			$this->query .= ' ORDER BY `'.bqSQL($this->_sort).'` DESC';
    			if (isset($this->_direction) && Validate::isSortDirection($this->_direction))
    				$this->query .= ' '.$this->_direction;
    		}*/
            //echo $this->query; die;
            
            $this->_values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
        }
	}
}
