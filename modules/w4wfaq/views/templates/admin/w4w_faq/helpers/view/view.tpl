<!-- Admin Config Page Template -->
<style>
	.bootstrap .table tbody>tr.row_hover>td {
	 font-size: 18px;
	 font-weight: 700;
	}
	label {
	 float: none;

	}
	.margin-form {
		padding: 0;
	}
</style>
{$html}
{literal}
<script>
	$(function(){
		//If page has a font awesome picker
		if($("input.fapicker").size()>0)
		{
			//if category/entry has icon, show it
			if($("input.fapicker").val()!='')
			{
				$("i.picker-target").get(0).className = 'picker-target fa '+$("input.fapicker").val();
			}

			//Create iconpicker (autohide, add UI for a "none" button)
			$("input.fapicker").iconpicker({
				hideOnSelect:true,
				templates:{
					search: '<input type="search" class="form-control iconpicker-search" placeholder="{/literal}{l s='Type to Filter' mod='w4wfaq'}{literal}" />',
					iconpicker: '<div><button class="btn no-icon">{/literal}{l s='No Icon' mod='w4wfaq'}{literal}</button></div><div class="iconpicker"><div class="iconpicker-items"></div></div>'
				}
        	});

        	//Display the icon when selected
			$("input.fapicker").on("iconpickerSelected",function(e){
				$('.picker-target').get(0).className = 'picker-target fa ' +
                            e.iconpickerInstance.options.iconBaseClass + ' ' +
                            e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
			});

			//logic for "none" button
			$("body").on("click","button.no-icon",function(){

				$("input.fapicker").val("");
				$('input.fapicker').data('iconpicker').hide();
				$("i.picker-target").get(0).className = 'picker-target fa';
				return false;
			});
		}


	});
</script>
{/literal}