<?php

Class CmsController extends CmsControllerCore
{
	//this override comes with w4wfaq module. it allows to append content from the module after the regular content for a given (in module) CMS entry
	public function initContent()
	{
		parent::initContent();
		if(Module::isInstalled('w4wfaq')&&Module::isEnabled('w4wfaq'))
		{
			$mod = Module::getInstanceByName('w4wfaq');
			$extra_content = $mod->getCmsContent($this->cms->id,$this->context->language->id);
			$this->cms->content.=$extra_content;
			$this->context->smarty->assign(array('cms'=>$this->cms));
		}
	}
}