<?php

Class W4wFaqUpgrader
{
	public static function checkUpgrades()
	{
		$upgrades = array();

	    //1.2 : Adding font-awesome icons to categories
	    $sql_fa_cat = "SHOW COLUMNS FROM `"._DB_PREFIX_."w4wfaq_category` LIKE '%fa_icon%'";
	    $results = Db::getInstance()->executeS($sql_fa_cat);
	    if(count($results)==0)
	    {
	      $upgrades[] = 'font-awesome-cat';
	    }

	    //1.2 : Adding font-awesome icons to entries
	    $sql_fa_cat = "SHOW COLUMNS FROM `"._DB_PREFIX_."w4wfaq_entry` LIKE '%fa_icon%'";
	    $results = Db::getInstance()->executeS($sql_fa_cat);
	    if(count($results)==0)
	    {
	      $upgrades[] = 'font-awesome-entry';
	    }

	    return $upgrades;
	}

	public static function doUpgrades()
	{
		$upgrades = self::checkUpgrades();
		if(count($upgrades)>0)
		{
			foreach($upgrades as $upgrade)
			{
				if($upgrade == 'font-awesome-cat')
				{
					$sql = "ALTER TABLE `"._DB_PREFIX_."w4wfaq_category` ADD fa_icon_category varchar(50) NOT NULL DEFAULT '';";
					Db::getInstance()->execute($sql);
				}

				if($upgrade == 'font-awesome-entry')
				{
					$sql = "ALTER TABLE `"._DB_PREFIX_."w4wfaq_entry` ADD fa_icon_entry varchar(50) NOT NULL DEFAULT '';";
					Db::getInstance()->execute($sql);
				}
			}
		}
	}
}