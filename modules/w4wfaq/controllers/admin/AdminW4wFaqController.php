<?php

Class AdminW4wFaqController extends ModuleAdminController
{
	//this controller is set to be the go-to controller of everything faq-module related
	public $bootstrap = true;
	public function __construct()
	{
		if(!Module::isInstalled('w4wfaq')||!Module::isEnabled('w4wfaq'))
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
		$this->display = 'view';
		$this->meta_title = $this->l('Faq Administration');
		parent::__construct();
		if(!$this->module->active)
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
	}

	public function setMedia()
	{
		parent::setMedia();
		//Font Awesome
		$this->addCSS($this->module->getPath().'css/w4wfaq.font-awesome.min.css');
		//Font Awesome Picker (https://github.com/mjolnic/fontawesome-iconpicker)
		$this->addCSS($this->module->getPath().'css/fontawesome-iconpicker.min.css');
		$this->addJS($this->module->getPath().'js/fontawesome-iconpicker.min.js');
		$this->addCSS($this->module->getPath().'css/w4wfaq-admin.css');
	}

	public function renderView()
	{
		//$html = '<h3>'.$this->meta_title.'</h3>';	
		$html = '';

		//Changes in global module config
		if(Tools::isSubmit('submitparams'))
		{
			$cms_id = (int)Tools::getValue('selected_cms');
			$show_phone = Tools::getValue('show_phone');
			$faq_mode = Tools::getValue('faq_mode');
			if($show_phone=="on")
				$show_phone = 1;
			else
				$show_phone = 0;

			$bootstrap_js = Tools::getValue('bootstrap_js');
			if($bootstrap_js=="on")
				$bootstrap_js = 1;
			else
				$bootstrap_js = 0;

			$bootstrap_css = Tools::getValue('bootstrap_css');
			if($bootstrap_css=="on")
				$bootstrap_css = 1;
			else
				$bootstrap_css = 0;

			$phone_number = Tools::getValue('phone_number');
			Configuration::updateValue('W4WFAQ_CMS_HOOK_ID',$cms_id);
 			Configuration::updateValue('W4WFAQ_SHOW_PHONE',$show_phone);
    		Configuration::updateValue('W4WFAQ_PHONE_NUMBER',$phone_number);
    		Configuration::updateValue('W4WFAQ_INCLUDE_BOOTSTRAP_CSS',$bootstrap_css);
    		Configuration::updateValue('W4WFAQ_INCLUDE_BOOTSTRAP_JS',$bootstrap_js);
    		Configuration::updateValue('W4WFAQ_FAQ_MODE',$faq_mode);
    		$html .= $this->module->displayConfirmation($this->l('Configuration Saved'));
		}

		//Category creation/edition
		if(Tools::isSubmit('submitAddCat'))
  		{
  			$newcat = new FaqCategory();
  			//get posted data
  			$this->context->controller->copyFromPost($newcat,'w4wfaq_category');
  			//if id is in the parameters : edit instead of creating
  			if($var = Tools::getValue('id_category'))
  			{
  				$oldcat = new FaqCategory($var);
  				$newcat->id = $var;
  				$newcat->position = $oldcat->position;
  			}
  			//save it
  			$newcat->save();
  			//redirect to the controller
  			Tools::redirectAdmin($this->context->link->getAdminLink('AdminW4wFaq'));
  		}

  		//Entry creation/edition
  		if(Tools::isSubmit('submitAddEntry'))
  		{
  			$entry = new FaqEntry();
  			//get posted data
  			$this->context->controller->copyFromPost($entry,'w4wfaq_entry');
  			//if id is in the parameters : edit instead of creating
  			if($var = Tools::getValue('id_entry'))
  			{
  				$oldentry = new FaqEntry($var);
  				$entry->id = $var;
  				$entry->position = $oldentry->position;
  			}
  			//save it
  			$entry->save();
  			//redirect to the controller
  			Tools::redirectAdmin($this->context->link->getAdminLink('AdminW4wFaq'));
  		}

  		//by default show categories / entries list
  		$show_list = true;

  		//controller url (for various forms)
  		$baseurl = $this->context->link->getAdminLink('AdminW4wFaq');
  		
  		//current language (back office)
  		$id_lang = Context::getContext()->language->id;

  		//Creating/editing entry/category
  		if(Tools::getIsset("newcat")||Tools::getIsset("newentry")||Tools::getIsset('editcat')||Tools::getIsset("editentry"))
  		{

	        // Form fields for category creation / edition
	        if(Tools::getIsset("newcat")||Tools::getIsset("editcat"))
	        {

		        $fields_form[0]['form'] = array(
		        	'tinymce' => false,
		        	'legend'=> array(
		        		'title'=> $this->l('FAQ Category Creation')
		        	),
		            'input' => array(
		                array(
		                    'type' => 'text',
		                    'label' => $this->l('Category Name:'),
		                    'name' => 'name',
		                    'required' => true,
		                    'lang'=>true,
		                    'size' => 50
		                ),
		                array(
		                    'type' => 'text',
		                    'label' => $this->l('Category Icon:').' <i class="picker-target fa"></i> ',
		                    'desc' => $this->l('See all icons available on').' : <a href="http://fontawesome.io/icons/" target="_blank">Font Awesome</a>',
		                    'name' => 'fa_icon_category',
		                    'required' => false,
		                    'lang'=>false,
		                    'size' => 50,
		                    'class' => 'fapicker'
		                )
		            ),
		            'submit' => array(
		                'title' => $this->l('Save'),
		                'name' => 'submitAddCat',
		                'class' => 'button'
		            )
		        );

		        //if we're editing (not creating), sneak in the id of the category
		        if($var = Tools::getValue('editcat'))
		        {
		        	$fields_form[0]['form']['input'][]=array(
		        		'type'=>'hidden',
		        		'name'=>'id_category'
		        	);
		        }
		        $helper = $this->module->getFormHelper();
		        $helper->submit_action = 'submitAddCat';
		    // Form fields for entry creation / edition
	        }else{
	        	//Category list for binding the entry to them
	        	$cats = array();
	        	$cates = FaqCategory::getCategories();
	        	foreach($cates as $category)
	        	{
	        		$cats[]=array(
	        			'id'=>(int)$category->id,
	        			'name'=>$category->name[Context::getContext()->language->id]
	        		);
	        	}
	        	$options = array(
	        		'query'=>$cats,
	        		'id'=>'id',
	        		'name'=>'name'
	        	);

	        	//Form in itself
	        	$fields_form[0]['form'] = array(
		        	'tinymce' => true,
		        	'legend'=> array(
		        		'title'=> $this->l('FAQ Entry Creation')
		        	),
		            'input' => array(
		                array(
		                    'type' => 'text',
		                    'label' => $this->l('Question:'),
		                    'name' => 'title',
		                    'required' => true,
		                    'lang'=>true,
		                    'size' => 50
		                ),
		                array(
		                    'type' => 'text',
		                    'label' => $this->l('Entry Icon:').' <i class="picker-target fa"></i> ',
		                    'desc' => $this->l('See all icons available on').' : <a href="http://fontawesome.io/icons/" target="_blank">Font Awesome</a>',
		                    'name' => 'fa_icon_entry',
		                    'required' => false,
		                    'lang'=>false,
		                    'size' => 50,
		                    'class' => 'fapicker'
		                ),
		                array(
							'type' => 'textarea',
							'label' => $this->l('Answer:'),
							'name' => 'content',
							'autoload_rte' => true,
							'lang' => true,
							'rows' => 5,
							'cols' => 40,
							'hint' => $this->l('Invalid characters:').' <>;=#{}'
						),
		                array(
		                    'type' => 'radio',
		                    'label' => $this->l('Question answered button ?'),
		                    'name' => 'has_controls',
		                    'required' => true,
		                    'class' => 't',
		                    'is_bool' => true,
		                    'values'=>array(
		                    	array(
			                    	'id'=>'has_controls_on',
			                    	'value'=>1,
			                    	'label'=>$this->l('Yes')
		                    	),
		                    	array(
			                    	'id'=>'has_controls_off',
			                    	'value'=>0,
			                    	'label'=>$this->l('No')
		                    	)
		                    )
		                ),
		                array(
		                	'type'=>'select',
		                	'label' => $this->l('Category'),
		                    'name' => 'id_w4wfaq_category',
		                    'required' => true,
		                    'options'=>$options
		                )
		            ),
		            'submit' => array(
		                'title' => $this->l('Save'),
		                'name' => 'submitAddEntry',
		                'class' => 'button'
		            )
		        );

				//if we're editing (not creating), sneak in the category ID
				if($var = Tools::getValue('editentry'))
		        {
		        	$fields_form[0]['form']['input'][]=array(
		        		'type'=>'hidden',
		        		'name'=>'id_entry'
		        	);
		        }
		        $helper = $this->module->getFormHelper();
		        $helper->submit_action = 'submitAddEntry';

	        }
	        $languages = Language::getLanguages();
	        
	        //Filling form with existing values (or blank if appropriate)
	        if(Tools::getIsset('editcat'))
			{
				//case of edition : get current category state
				$current_values = new FaqCategory(Tools::getValue('editcat'));
			}elseif(Tools::getIsset('editentry'))
			{
				//case of edition : get current entry state
				$current_values = new FaqEntry(Tools::getValue('editentry'));
			}else{
				$current_values = false;
			}

			//Scan thru fields of the form and init them
	        foreach($fields_form[0]['form']['input'] as $imp)
	        {
	        	//always true test in case we later need to exclude some fields of this process
	        	if((1==1)||$imp['type']=="text"||$imp['type']=="textarea")
	        	{
	        		//if category id (editing category case), skip to next field
	        		if($imp['name']=='id_category')
	        		{
	        			$helper->fields_value[$imp['name']]=Tools::getValue('editcat');
	        			continue;
	        		}
	        		//if entry id (editing entry case), skip to next field
	        		if($imp['name']=='id_entry')
	        		{
	        			$helper->fields_value[$imp['name']]=Tools::getValue('editentry');
	        			continue;
	        		}
	        		//id of the parent category (case of an entry)
	        		if($imp['name']=="id_w4wfaq_category")
	        		{
	        			//if we're creating, we're creating an entry, so we put the parent category we get from parameters
	        			if($current_values==false)
	        				$helper->fields_value[$imp['name']] = Tools::getValue("newentry");
	        			//else we put the already existing value
	        			else
	        				$helper->fields_value[$imp['name']] = $current_values->{$imp['name']};
	        			continue;
	        		}
	        		//if field is translated, set each of the translations to the existing, or blank it.
	        		if(isset($imp['lang'])&&$imp['lang'])
	        		{
	        			foreach($languages as $language)
	        			{
	        				if($current_values==false||!isset($current_values->{$imp['name']}))
		        				$helper->fields_value[$imp['name']][$language['id_lang']] = "";
		        			else
		        				$helper->fields_value[$imp['name']] = $current_values->{$imp['name']};
	        			}
	        		//if it's a regular field, set to existing or blank it.
	        		}else{
	        			if($current_values==false||!isset($current_values->{$imp['name']}))
	        				$helper->fields_value[$imp['name']][$language['id_lang']] = "";
	        			else
	        				$helper->fields_value[$imp['name']] = $current_values->{$imp['name']};
	        		}
	        	}
	        }
	        //get html of the form.
	       	$html .= $helper->generateForm($fields_form);
	       	//in case of category/entry creation/edition, don't show the rest of the UI (config + cat/entry list)
	       	$show_list = false;
	    //move category up
  		}elseif($catid = Tools::getValue("catup")){
  			$html .= "";
  			$cat = new FaqCategory($catid);
  			$cat->moveUp();
  		//move category down();
  		}elseif($catid = Tools::getValue("catdown")){
  			$html .= "";
  			$cat = new FaqCategory($catid);
  			$cat->moveDown();
  		//delete category
  		}elseif($catid = Tools::getValue("delcat")){
  			$html .= "";
  			$cat = new FaqCategory($catid);
  			$cat->delete();
  		//delete entry
  		}elseif($entrid = Tools::getValue("delentry")){
  			$html .= '';
  			$entry = new FaqEntry($entrid);
  			$entry->delete();
  		//move entry up
  		}elseif($entrid = Tools::getValue("entryup")){
  			$html .= "";
  			$entry = new FaqEntry($entrid);
  			$entry->moveUp();
  		//move entry down
  		}elseif($entrid = Tools::getValue("entrydown")){
  			$html .= "";
  			$entry = new FaqEntry($entrid);
  			$entry->moveDown();
  		}

  		//if we are to display UI, do it (code in module file)
  		if($show_list)
		{
			$html .= $this->module->getCategoriesHtml($baseurl);
		}

		//assign content to the template (module_folder/views/templates/admin/w4w_faq/helpers/view/view.tpl)
		$this->tpl_view_vars=array('html'=>$html);
		
		//Rest is prestashop magic
		return parent::renderView();
	}
}