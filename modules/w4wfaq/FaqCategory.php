<?php
//Object model for a FAQ Category
Class FaqCategory extends ObjectModel
{
	public $name;
	public $position;
	public $fa_icon_category='';

	public static $definition = array(
		'table' => 'w4wfaq_category',
		'primary' => 'id_w4wfaq_category',
		'multilang' => true,
		'fields' => array(
			'position' 			=> array('type' => self::TYPE_INT),
			'fa_icon_category' 	=> array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString', 'size' => 50, 'default' => ''),

			//Lang Fields
			'name'				=> array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 255),
		),
	);

	//When we add a category, put it at the bottom of the list
	public function add($autodate = true, $null_values = false)
	{
		$this->position = FaqCategory::getLastPosition();
		return parent::add($autodate, $null_values);
	}

	//when we update a category, update all positions (except if we're doing up/down reordering)
	public function update($null_values = false,$redopos = true)
	{
		if (parent::update($null_values))
		{
			if($redopos)
				return $this->cleanPositions();
			else
				return true;
		}
		return false;
	}

	//when deleting a category, delete all associated entries and redo positions
	public function delete()
	{
	 	FaqEntry::deleteByCategory($this->id);
	 	if (parent::delete())
	 	{
			return $this->cleanPositions();
	 	}
		return false;
	}

	//get number of categories
	public static function getCount()
	{
		$sql = "SELECT count(*) FROM  `"._DB_PREFIX_."w4wfaq_category`";
		return (Db::getInstance()->getValue($sql));
	}

	//get the position where to insert a new category
	public static function getLastPosition()
	{
		$sql = "SELECT MAX(position) + 1 FROM `"._DB_PREFIX_."w4wfaq_category`";
		return (Db::getInstance()->getValue($sql));
	}

	//move a category up the list
	public function moveUp()
	{
		$oldpos = $this->position;
		$newpos = $oldpos - 1;
		//if it wasn't already first
		if($newpos>=0)
		{
			//get category above current
			$cat_to_switch_id = FaqCategory::getCatAtPosition($newpos);
			if($cat_to_switch_id!==false)
			{
				//load its data
				$cat_to_switch = new FaqCategory($cat_to_switch_id);
				//set its position to what current is actually
				$cat_to_switch->position = $oldpos;
				//update database without reordering
				$cat_to_switch->update(false,false);
			}
			//set current category to the (now free) position above
			$this->position = $newpos;
			//update database without reordering
			$this->update(false,false);
		}
		//ensure order is okay
		FaqCategory::cleanPositions();
	}

	//move a category down
	public function moveDown()
	{
		$oldpos = $this->position;
		$newpos = $oldpos + 1;
		//if new position is correct
		if($newpos>=0)
		{
			//get category below current
			$cat_to_switch_id = FaqCategory::getCatAtPosition($newpos);
			if($cat_to_switch_id!==false)
			{
				//load its data
				$cat_to_switch = new FaqCategory($cat_to_switch_id);
				//set its position to what current is actually
				$cat_to_switch->position = $oldpos;
				//update database without reordering
				$cat_to_switch->update(false,false);
			}
			//set current category to the (now free) position below
			$this->position = $newpos;
			//update database without reordering
			$this->update(false,false);
		}
		//ensure order is okay
		FaqCategory::cleanPositions();
	}

	//get the id of a category occupying the position in parameter (for up/down switching purpose)
	public static function getCatAtPosition($position)
	{
		$sql ="SELECT id_w4wfaq_category FROM `"._DB_PREFIX_."w4wfaq_category` WHERE position = ".(int)$position;
		return Db::getInstance()->getValue($sql);
	}

	//ensure the "positions" of categories is good (0 to n-1 without gaps) after deletion/update
	public static function cleanPositions()
	{
		$sql = "
		SELECT `id_w4wfaq_category`
		FROM `"._DB_PREFIX_."w4wfaq_category` 
		ORDER BY `position`";

		$result = Db::getInstance()->executeS($sql);

		for ($i = 0, $total = count($result); $i < $total; ++$i)
		{
			$sql = "UPDATE `"._DB_PREFIX_."w4wfaq_category`
					SET `position` = ".(int)$i."
					WHERE `id_w4wfaq_category` = ".(int)$result[$i]['id_w4wfaq_category'];
			Db::getInstance()->execute($sql);
		}
		return true;
	}

	//get all categories (as objects)
	public static function getCategories($id_lang = null)
	{
		$results = array();
		$sql = "SELECT id_w4wfaq_category FROM `"._DB_PREFIX_."w4wfaq_category` ORDER by position ASC";
		$res = Db::getInstance()->executeS($sql);
		foreach($res as $re)
		{
			$results[] = new FaqCategory($re['id_w4wfaq_category'],$id_lang);
		}
		//var_dump($results);die();
		return $results;
	}

	//get all entries of current category
	public function getEntries($id_lang = null)
	{
		return self::getEntriesS($this->id,$id_lang);
	}

	//static version of the above.
	public static function getEntriesS($id_category,$id_lang=null)
	{
		$results = array();
		$sql = "SELECT id_w4wfaq_entry FROM `"._DB_PREFIX_."w4wfaq_entry` WHERE id_w4wfaq_category = ".(int)$id_category." ORDER by position ASC";
		$res =  Db::getInstance()->executeS($sql);
		foreach($res as $re)
		{
			$results[]=new FaqEntry($re['id_w4wfaq_entry'],$id_lang);
		}
		return $results;

	}
}