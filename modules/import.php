<?php
die();
global $smarty;
global $cookie;
define('PRESTASHOP_INTEGRATION_VERSION', true);
define ('PS_DIR', __DIR__ . '/../');
define('_PS_MODE_DEV_', true);
require_once PS_DIR .'/config/config.inc.php';
require_once PS_DIR .'/modules/allinone_rewards/allinone_rewards.php';

if (!defined('_PS_VERSION_'))
	exit;

$servername = "localhost";
$username = _DB_USER_;
$password = _DB_PASSWD_;
$database = "isomersdev";
$mysqli = new mysqli($servername, $username, $password, $database);
$mysqli->set_charset('utf8');

if ($mysqli->connect_errno) {
    return ("Connection failed: " . $mysqli->connect_errno);
} 

//$selected = mysql_select_db($database, $mysqli)
//  or die("Could not select $database");

/*
$sql = "SELECT DISTINCT usr.*, usr.DateCreated AS usrDateCreated, sur.*, ui.AccountType AS usr_group,  
    news.*, news.Subscribed+0 AS Subscribed, news.DateCreated AS newsDateCreated
    FROM user_emails usr 
    INNER JOIN newsletter_surveys AS sur ON sur.UserID = usr.UserID 
    INNER JOIN user_incentives AS ui ON ui.UserID = usr.UserID 
    INNER JOIN newsletter_subscriptions AS news ON news.UserID = usr.UserID 
    WHERE news.SubscriptionID = (SELECT MAX(SubscriptionID) from newsletter_subscriptions WHERE UserID = usr.UserID) OR news.SubscriptionID IS NULL 
    GROUP BY usr.UserID
    LIMIT 3;";
*/
$sql = "SELECT DISTINCT usr.*, usr.UserID AS UsrID, usr.DateCreated AS usrDateCreated, sur.*, 
    ui.AccountType AS usr_group, ui.ReferrerUserID AS ReferrerUserID, ui.CurrentBeautyPoints AS CurrentBeautyPoints, ui.CurrentCredit AS CurrentCredit, ui.CurrentCashBack AS CurrentCashBack,
    news.*, news.Subscribed+0 AS Subscribed, news.DateCreated AS newsDateCreated
    FROM user_emails usr 
    LEFT JOIN newsletter_surveys AS sur ON sur.UserID = usr.UserID 
    LEFT JOIN newsletter_subscriptions AS news ON news.UserID = usr.UserID 
    LEFT JOIN user_incentives AS ui ON ui.UserID = usr.UserID 
    WHERE (news.SubscriptionID = (SELECT MAX(SubscriptionID) from newsletter_subscriptions WHERE UserID = usr.UserID) OR news.SubscriptionID IS NULL)
        AND (usr.email NOT LIKE '%@chriscom.com' AND usr.email NOT LIKE '%@isomers.ca' AND usr.UserID > 100 AND usr.email != '')
        AND usr.UserID NOT IN(1849, 13600, 19632, 27879)  
    GROUP BY usr.UserID;";
//echo "$sql<br>";
if ($result = $mysqli->query($sql)) {
    $is_customer = array();
    $lang = (int)Configuration::get('PS_LANG_DEFAULT');
    while ($row = $result->fetch_assoc()) {
        //p($row);
        $country = $state = '';
        $country = Db::getInstance()->getValue('SELECT `id_country` FROM `'._DB_PREFIX_.'country` WHERE `iso_code` = \''.$row["Country"].'\'');
        if (!empty($row["Region"]) && !empty($country)) {
            $state = Db::getInstance()->getValue('SELECT `id_state` FROM `'._DB_PREFIX_.'state` WHERE `iso_code` = \''.$row["Region"].'\' AND id_country ='.$country);        
        }
        if ($row["Sex"] == 'Male') $sex = 1; else $sex = 2;
        switch ($row["usr_group"]) {
            case 'Sales team':
                $cgroup = 5;
                break;
            case 'Refer a friend':
                $cgroup = 7;
                break;
            case 'House account':
                $cgroup = 3;
                break;
            default:
                $cgroup = 3;
        }
        $address1= $address2 = $note = '';
        if (!empty($row["BestBeautyZone"])) $note .= "Best Beauty Zone: " . $row["BestBeautyZone"] ."\n";
        if (!empty($row["AreaOfConcern"])) $note .= "Area Of Concern: " . $row["AreaOfConcern"] ."\n";
        if (!empty($row["Age"])) $note .= "Age Range: " . $row["Age"] ."\n";
        if (!empty($row["Comments"])) $note .= "Comments:" ."\n" . $row["Comments"];
        if (strlen($address1 = $row["Address1"] . " " . $row["Address2"]) >=128) {
            $address1 = substr($row["Address1"], 0, 128);
            $address2 = substr($row["Address2"], 0, 128);
        }
        else {
            $address1 = $row["Address1"] . " " . $row["Address2"];
        }
        
        $address1 = preg_replace("/[^a-zA-Z0-9 .,:;]+/", "", $address1);
        $address2 = preg_replace("/[^a-zA-Z0-9 .,:;]+/", "", $address2);
        $postal = preg_replace("/[^a-zA-Z0-9 ]+/", "", $row["Postal"]);
        
        $city = preg_replace("/[^a-zA-Z ]+/", "", $row["City"]);
        $state_name = preg_replace("/[^a-zA-Z ]+/", "", $row["Region"]);
        $firstname = preg_replace("/[^a-zA-Z ]+/", "", $row["FirstName"]);
        $lastname = preg_replace("/[^a-zA-Z ]+/", "", $row["LastName"]);
        $firstname = substr($firstname, 0, 32);
        $lastname = substr($lastname, 0, 32);

        $is_customer[] = array(
            "UserID" => $row["UsrID"],
            "Email" => filter_var( $row["Email"], FILTER_SANITIZE_EMAIL),
            "FirstName" => !empty($firstname) ? $firstname : ' ',
            "LastName" => !empty($lastname) ? $lastname : ' ',
            "Address1" => $address1,
            "Address2" => $address2,
            "City" => $city,
            "id_country" => $country,
            "id_state" => $state,
            "Country" => $row["Country"],
            "State" => $state_name,
            "Postal" =>  substr($postal, 0, 12),
            "Phone" => $row["Phone"],
            "date_add" => $row["usrDateCreated"],
            "Age" => $row["Age"],
            "id_gender" => $sex,
            "Sex" => $row["Sex"],
            "BestBeautyZone" => $row["BestBeautyZone"],
            "AreaOfConcern" => $row["AreaOfConcern"],
            "Comments" => $row["Comments"],
            "newsletter" => (int)$row["Subscribed"],
            "newsletter_date_add" => $row["newsDateCreated"],
            "newsletter_date_del" => $row["UnsubscribedDate"],
            "SubscribeMethod" => $row["SubscribeMethod"],
            "group" => $row["usr_group"],
            "id_group" => $cgroup,
            "note" => $note,
            "ReferrerUserID" => $row["ReferrerUserID"],
            "CurrentBeautyPoints" => (int) $row["CurrentBeautyPoints"],
            "CurrentCredit" => (int) $row["CurrentCredit"],
            "CurrentCashBack" => (int) $row["CurrentCashBack"],
        );
    }
    $result->free();
}



foreach ($is_customer as $val) {
    $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'customer` WHERE `UserID` = \''.$val["UserID"].'\'');
//p($val);
    $customer = new Customer((int)$id_customer);
    $customer->firstname = $val["FirstName"];
    $customer->lastname = $val["LastName"];
    if (!empty($val["id_gender"])) $customer->id_gender = $val["id_gender"];
    $customer->id_default_group = $val["id_group"];
    $customer->id_lang = $lang;
    $customer->email = $val["Email"];
    if  (!empty($val["newsletter"])) {
        $customer->newsletter = $val["newsletter"];
        $customer->newsletter_date_add = $val["newsletter_date_add"];
    }
    $customer->date_add = $val["date_add"];
    $customer->date_upd = $val["date_add"];
    $customer->note = $val["note"];
    $customer->UserID = $val["UserID"];
    $customer->active = 1;
    $customer->passwd = Tools::encrypt($val["Email"]);
    $customer->secure_key = md5(uniqid(rand(), true));
    //p($val);
    if ($customer->id && $customer->customerExists($customer->email)) {
        if ($customer->update()) {
            echo "The User UserID = ".$val["UserID"]." id_customer = ".$customer->id." is updated <br>";
        }
        else {
            echo Tools::displayError("An error occurred while updating your account. UserID = ".$val["UserID"]."<br>");
        }
        //echo "The User UserID = ".$val["UserID"]." id_customer = ".$customer->id." is updated <br>";
    }
    else  {
        if ($customer->add(false)) {
            echo "New User UserID = ".$val["UserID"]." id_customer = ".$customer->id." is added <br>";
            
            Db::getInstance()->execute('
				INSERT INTO '._DB_PREFIX_.'customer_group_log (id_customer, id_group, date_upd)
				VALUES ('.$customer->id.','.$val["id_group"].',\''.$val["date_add"].'\')', false);   
                
                  
            if (empty($val["ReferrerUserID"])) {
                if ($val["id_group"] == 5) {
                    RewardsTemplateModel::addCustomer(2, (int) $customer->id);
                    RewardsTemplateModel::addCustomer(3, (int) $customer->id);
                    RewardsTemplateModel::addCustomer(5, (int) $customer->id);
                }
                elseif ($val["id_group"] == 7) {
                    RewardsTemplateModel::addCustomer(2, (int) $customer->id);
                    RewardsTemplateModel::addCustomer(4, (int) $customer->id);
                    RewardsTemplateModel::addCustomer(6, (int) $customer->id);
                }
            }
        }
        else {
            echo Tools::displayError("An error occurred while creating your account UserID = ".$val["UserID"].".<br>");
        }            
    }
        
    addAddress($customer, $val);
    addTicket($customer, $mysqli);
    addReward($customer, $val);
}
//p($is_customer);


function addReward($customer, $val) {
    $id_referral = Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'customer` WHERE `UserID` = \''.$val["UserID"].'\'');
    if ($val["CurrentBeautyPoints"] > 0) {
        $id_reward = Db::getInstance()->getValue('SELECT id_reward FROM `'._DB_PREFIX_.'rewards` WHERE `id_customer` = \''.$id_referral.'\' AND id_order = 0 AND virt = 1 AND id_template = 8 AND plugin = \'loyalty\'');
        $reward = new RewardsModel($id_reward);
		$reward->virt = 1;
        $reward->id_reward_state = RewardsStateModel::getValidationId(); 
        $reward->id_template = 8;
        $sponsor_reward = $val["CurrentBeautyPoints"]/10;
		$reward->plugin = 'loyalty';
        $reward->id_customer = (int)$id_referral;
		$reward->credits = (float)$sponsor_reward;
		$reward->save();
 	}
    if ($val["CurrentCredit"] > 0) {
        $id_reward = Db::getInstance()->getValue('SELECT id_reward FROM `'._DB_PREFIX_.'rewards` WHERE `id_customer` = \''.$id_referral.'\' AND id_order = 0 AND virt = 1 AND id_template = 4 AND plugin = \'loyalty\'');    
        $reward = new RewardsModel($id_reward);
		$reward->virt = 1;
        $reward->id_template = 4;
        $reward->id_reward_state = RewardsStateModel::getValidationId(); 
        $sponsor_reward = $val["CurrentCredit"]/10;
		$reward->plugin = 'loyalty';
        $reward->id_customer = (int)$id_referral;
		$reward->credits = (float)$sponsor_reward;
		$reward->save();
	}
    if ($val["CurrentCashBack"] > 0) {
        $id_reward = Db::getInstance()->getValue('SELECT id_reward FROM `'._DB_PREFIX_.'rewards` WHERE `id_customer` = \''.$id_referral.'\' AND id_order = 0 AND virt = 0 AND plugin = \'loyalty\'');
        $reward = new RewardsModel($id_reward);
		$reward->virt = 0;
        $reward->id_template = 3;
        $reward->id_reward_state = RewardsStateModel::getDefaultId();         
        $sponsor_reward = $val["CurrentCashBack"];
		$reward->plugin = 'loyalty';
        $reward->id_customer = (int)$id_referral;
		$reward->credits = (float)$sponsor_reward;
		$reward->save();
	}
    echo "Points for  UserID = ".$val["UserID"]." id_customer = ".$customer->id." is added <br>";
    //echo "ReferrerUserID = {$val["ReferrerUserID"]} <br>";
    if (!empty($val["ReferrerUserID"])) {
        $id_sponsor = Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'customer` WHERE `UserID` = \''.$val["ReferrerUserID"].'\'');
        if (!empty($id_sponsor) && !empty($id_referral)) {
            if ($id_sponsorship = RewardsSponsorshipModel::isSponsorised((int)$id_referral, true)) {
                $sponsorship = new RewardsSponsorshipModel((int)$id_sponsorship);
            }
            else {
                $sponsorship = new RewardsSponsorshipModel();
            }            
			$sponsorship->id_sponsor = $id_sponsor;
			$sponsorship->email = $val["Email"];
			$sponsorship->id_customer = $id_referral;
			$sponsorship->firstname = $val["FirstName"];
			$sponsorship->lastname = $val["LastName"];
			$sponsorship->channel = 1;	
            $sponsorship->save();
        }
        echo "The ReferrerUserID for  UserID = ".$val["UserID"]." id_customer = ".$customer->id." is added <br>";
    }
}

$mysqli->close();


function addAddress($customer, $val) {
    if(trim($val["Address1"]) == '') {
        echo "Address for UserID = ".$val["UserID"]." is absent <br>";
        return;
    }
    
    $address = new Address($customer->id);                
    $address->id_customer = $customer->id;                
    $address->id_country = $val["id_country"];                
    if (!empty($val["id_state"])) $address->id_state = $val["id_state"];                
    $address->alias = "Default Address";                
    $address->firstname = $val["FirstName"];                
    $address->lastname = $val["LastName"];                
    $address->address1 = $val["Address1"];                
    $address->address2 = $val["Address2"];                
    $address->postcode = $val["Postal"];                
    $address->city = !empty($val["City"])? $val["City"] : ' ';                
    $address->phone = (Validate::isPhoneNumber($val["Phone"]))? $val["Phone"]: '';                
    $address->date_add = $val["date_add"];                
    $address->active = 1; 
    if ($address->id && $address->addressExists($address->id)) {
        if ($address->update()) {
            echo "Address for UserID = ".$val["UserID"]." is updated <br>";
        }
        else {
            echo Tools::displayError("An error occurred while updating your address UserID = ".$val["UserID"].".<br>");
        }
    }
    else {   
        if ($address->add(false)) {
            echo "New Address for UserID = ".$val["UserID"]." is added <br>";
        }
        else {
            echo Tools::displayError("An error occurred while creating your address UserID = ".$val["UserID"].".<br>");
        }                             
    }
}

function addTicket($customer, $mysqli) {
    $sql = "SELECT * FROM user_tickets WHERE UserID =".$customer->UserID;
    if ($result = $mysqli->query($sql)) {
    while ($row = $result->fetch_assoc()) {   
        if (empty($row["Message"])) continue;
        $id_customer_thread = Db::getInstance()->getValue('
    		SELECT cm.id_customer_thread
    		FROM '._DB_PREFIX_.'customer_thread cm
    		WHERE id_customer = '.(int)$customer->id . ' AND date_add = \''.$row['DateCreated'].'\'');
        if ((int)$id_customer_thread) {
            $ct = new CustomerThread($id_customer_thread);
            /*$ct->status = 'closed';
            $ct->id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
            $ct->update();*/
        } else {
            $ct = new CustomerThread();
            if (isset($customer->id)) {
                $ct->id_customer = (int)$customer->id;
            }
            $ct->id_shop = 1;
            $ct->id_order = 0;
            $ct->id_product = 0;
            $ct->id_contact = 0;
            $ct->id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
            $ct->email = $customer->email;
            $ct->status = 'closed';
            $ct->token = Tools::passwdGen(12);
            $ct->date_add = $row['DateCreated'];
            $ct->add(false);
        }
        if ($ct->id) {
            $id_customer_message = Db::getInstance()->getValue('
        		SELECT cm.id_customer_message
        		FROM '._DB_PREFIX_.'customer_message cm
        		WHERE id_customer_thread = '.(int)$ct->id . ' AND date_add = \''.$row['DateCreated'].'\'');
           if (!(int)$id_customer_message) {
                $cm = new CustomerMessage();
                $cm->id_customer_thread = $ct->id;
                $cm->message = $row["Message"];
                $cm->date_add = $row['DateCreated'];
                if (!$cm->add(false)) {
                    $this->errors[] = Tools::displayError('An error occurred while sending the message.<br>');
                }
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while sending the message.<br>');
        }
    }
    $result->free();           
    }
}
d('IMPORT ENDED');
?>