<?php 
class htmlboxpro extends Module {
	function __construct(){
	    ini_set("display_errors", 0);
        error_reporting(0); //E_ALL  
		$this->name = 'htmlboxpro';
		$this->tab = 'front_office_features';
        $this->author = 'MyPresta.eu';
		$this->version = '2.6.0.2';
        $this->dir = '/modules/htmlboxpro/';
		parent::__construct();
        $this->trusted();
		$this->displayName = $this->l('HTML Box Pro');
		$this->description = $this->l('With this module you can put the HTML/JavaScript/CSS code anywhere you want');
        $this->allhooks = array (
        'header',
        'top',
        'displayTopColumn',
        'displayNav',
        'leftColumn',
        'rightColumn',
        'footer',
        'home',
        'extraLeft',
        'extraRight',
        'productActions',
        'productOutOfStock',
        'productfooter',
        'productTab',
        'productTabContent',
        'shoppingCart',
        'shoppingCartExtra',
        'createAccountTop',
        'createAccountForm',
        'customerAccount',
        'myAccountBlock',
        'payment',
        'extraCarrier',
        'orderConfirmation',
        'paymentReturn',
        'paymentTop',
        ); 
        
        
        
        $this->mkey="nlc";       
        if (@file_exists('../modules/'.$this->name.'/key.php'))
            @require_once ('../modules/'.$this->name.'/key.php');
        else if (@file_exists(dirname(__FILE__) . $this->name.'/key.php'))
            @require_once (dirname(__FILE__) . $this->name.'/key.php');
        else if (@file_exists('modules/'.$this->name.'/key.php'))
            @require_once ('modules/'.$this->name.'/key.php');                        
        $this->checkforupdates();
	}
    
    function checkforupdates(){
            if (isset($_GET['controller']) OR isset($_GET['tab'])){
                if (Configuration::get('update_'.$this->name) < (date("U")>86400)){
                    $actual_version = htmlboxproUpdate::verify($this->name,$this->mkey,$this->version);
                }
                if (htmlboxproUpdate::version($this->version)<htmlboxproUpdate::version(Configuration::get('updatev_'.$this->name))){
                    $this->warning=$this->l('New version available, check <a href="http://MyPresta.eu">MyPresta.eu</a> for more informations');
                }
            }
        }
        
    function trusted(){
            if (_PS_VERSION_ <= "1.6.0.8"){
                if (isset($_GET['controller'])){
                    if ($_GET['controller']=="AdminModules"){
                        if (defined('self::CACHE_FILE_TRUSTED_MODULES_LIST')==true){
                            $context = Context::getContext();
                    		$theme = new Theme($context->shop->id_theme);
                            $xml= simplexml_load_string(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST));
                            if ($xml){
                                $css=$xml->modules->addChild('module');
                                $css->addAttribute('name',$this->name);
                                $xmlcode=$xml->asXML();
                                if (!strpos(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST),$this->name))
                                    file_put_contents(_PS_ROOT_DIR_.self::CACHE_FILE_TRUSTED_MODULES_LIST,$xmlcode);
                            }
                            if (defined('self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST')==true){
                                $xml= simplexml_load_string(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST));
                                //$xml=new SimpleXMLElement('<modules/>');
                                //$cs=$xml->addChild('modules');
                                if ($xml){
                                $css=$xml->addChild('module');
                                $css->addChild('id',0);
                                $css->addChild('name',"<![CDATA[".$this->name."]]>");
                                $xmlcode=$xml->asXML();
                                $xmlcode=str_replace('&lt;',"<",$xmlcode);
                                $xmlcode=str_replace('&gt;',">",$xmlcode);
                                if (!strpos(file_get_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST),$this->name))
                                    file_put_contents(_PS_ROOT_DIR_.self::CACHE_FILE_DEFAULT_COUNTRY_MODULES_LIST,$xmlcode);                                    
                                }
                            }
                        }
                    }
                }
            }
        } 
        
            
	private function installdb() {
		$prefix = _DB_PREFIX_;
		$engine = _MYSQL_ENGINE_;
		$statements = array(); 
		
        $statements[]   = "
        CREATE TABLE IF NOT EXISTS `${prefix}hbp_block` (
    	`id` INT(10) NOT NULL AUTO_INCREMENT,
    	`position` INT(10) NOT NULL DEFAULT '1',
    	`hook` VARCHAR(50) NULL DEFAULT NULL,
    	`active` INT(11) NOT NULL DEFAULT '0',
    	`logged` INT(11) NOT NULL DEFAULT '0',
    	`name` VARCHAR(150) NULL DEFAULT NULL,
    	INDEX `indek1` (`id`)
        ) COLLATE='utf8_general_ci'";
        
        $statements[]   = "
        CREATE TABLE IF NOT EXISTS `${prefix}hbp_customhook` (
    	`id` INT(10) NOT NULL AUTO_INCREMENT,
    	`hook` VARCHAR(70) NULL DEFAULT NULL,
    	INDEX `indekch1` (`id`)
        ) COLLATE='utf8_general_ci'";
        
        
        $statements[]   = "
        CREATE TABLE IF NOT EXISTS `${prefix}hbp_block_lang` (
        `id` INT(10) NULL DEFAULT NULL,
        `id_lang` INT(10) NULL DEFAULT NULL,
        `body` TEXT NULL
        ) COLLATE='utf8_general_ci'";
        
        
        

		foreach ($statements as $statement) {
			if (@!Db :: getInstance()->Execute($statement)) {
				return false;
			}
		}
		return true;					
	}
    
    static function remove_doublewhitespace($s = null){
           return  $ret = preg_replace('/([\s])\1+/', ' ', $s);
    }

    static function remove_whitespace($s = null){
           $ret = preg_replace('/[\s]+/', '', $s );
           $ret = mysql_escape_string($ret);
           return $ret;
    }

    static function remove_whitespace_feed( $s = null){
            return $s;
           //return $ret = preg_replace('/[\t\n\r\0\x0B]/', ' ', $s);
    }

    static function smart_clean($s = null){
           return $ret = trim( self::remove_doublewhitespace( self::remove_whitespace_feed($s) ) );
    }
    
    public static function currentPageURL() {
         $pageURL = 'http';
         if (isset($_SERVER["HTTPS"])){
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
         }
         $pageURL .= "://";
         $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
         return $pageURL;
    }        
    
    
	function install(){
        if (parent::install() == false 
        OR $this->installdb() == false
        OR !Configuration::updateValue('update_'.$this->name,'0')
	    OR $this->registerHook('rightColumn') == false
        OR $this->registerHook('productActions') == false
        OR Configuration::updateValue('hbp_lasttab', '2') == false
        OR Configuration::updateValue('hbp_header', '0') == false
        OR Configuration::updateValue('hbp_top', '0') == false
        OR Configuration::updateValue('hbp_leftColumn', '0') == false
        OR Configuration::updateValue('hbp_rightColumn', '1') == false
        OR Configuration::updateValue('hbp_footer', '0') == false
        OR Configuration::updateValue('hbp_home', '0') == false
        OR Configuration::updateValue('hbp_extraLeft', '0') == false
        OR Configuration::updateValue('hbp_extraRight', '0') == false
        OR Configuration::updateValue('hbp_productOutOfStock', '0') == false
        OR Configuration::updateValue('hbp_productfooter', '0') == false
        OR Configuration::updateValue('hbp_productTab', '0') == false
        OR Configuration::updateValue('hbp_productTabContent', '0') == false
        OR Configuration::updateValue('hbp_shoppingCart', '0') == false
        OR Configuration::updateValue('hbp_shoppingCartExtra', '0') == false
        OR Configuration::updateValue('hbp_createAccountTop', '0') == false
        OR Configuration::updateValue('hbp_createAccountForm', '0') == false
        OR Configuration::updateValue('hbp_customerAccount', '0') == false
        OR Configuration::updateValue('hbp_myAccountBlock', '0') == false
        OR Configuration::updateValue('hbp_payment', '0') == false
        OR Configuration::updateValue('hbp_extraCarrier', '0') == false
        OR Configuration::updateValue('hbp_paymentReturn', '0') == false
        OR Configuration::updateValue('hbp_paymentTop', '0') == false
        ){
            return false;
        }
        return true;
	}
    
    
    
    public function get_blocks($hook,$active=null,$lang=null){
        $innerjoin='';
        $whereactive='';
        $wherelang='';
        $whereshop='';
        if ($this->psversion()==5 || $this->psversion()==6){
            if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE')==1){
                $whereshop = 'AND shop="'.$this->context->shop->id.'"';
            }
        }
        
        
        if ($active==1){
            $whereactive="AND active=1";
        }
        
        if (!is_null($lang)){
            $innerjoin='INNER JOIN `'._DB_PREFIX_.'hbp_block_lang` AS b ON a.id=b.id';
            $wherelang="AND b.id_lang=".$lang;
        }
        $query=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'hbp_block` AS a '.$innerjoin.' WHERE a.hook="'.$hook.'" '.$whereactive.' '.$wherelang.' '.$whereshop.' ORDER BY a.position');
        if (count($query)>0){
            foreach ($query AS $blck => $key){
                if (isset($query[$blck]['selectedproducts']))
                $query[$blck]['selectedproducts']=explode(',',$query[$blck]['selectedproducts']);
                if (isset($query[$blck]['selectedcms']))
                $query[$blck]['selectedcms']=explode(',',$query[$blck]['selectedcms']);
                if (isset($query[$blck]['selected_pcats']))
                $query[$blck]['selected_pcats']=explode(',',$query[$blck]['selected_pcats']);
                if (isset($query[$blck]['selected_cats']))
                $query[$blck]['selected_cats']=explode(',',$query[$blck]['selected_cats']);
                $query[$blck]['selected_manufs']=explode(',',$query[$blck]['selected_manufs']);
                
            }
        }
        return $query;
    }
    
    public static function get_block($id){
        
        $query=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'hbp_block` AS a WHERE a.id="'.$id.'"');
        $lang=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM `'._DB_PREFIX_.'hbp_block_lang` AS a WHERE a.id="'.$id.'"');
        foreach ($lang as $k=>$v){
            $query[0]['body'][$v['id_lang']]=$v['body'];
        }
        return $query;
    }
    
    public function lastslide(){
        $query=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT id FROM `'._DB_PREFIX_.'hbp_block` ORDER BY id DESC LIMIT 1');
        return $query[0]['id'];
    } 
    
	public function psversion($part=1) {
		$version=_PS_VERSION_;
		$exp=$explode=explode(".",$version);
        if ($part==1)
		  return $exp[1];
        if ($part==2)
		  return $exp[2];
        if ($part==3)
		  return $exp[3];
	}
    
    public function generateGroups($selected=null){
        $return='';
        foreach (Group::getGroups(Configuration::get('PS_LANG_DEFAULT')) AS $key=>$value){
            if ($selected){
                if ($value['id_group']==$selected){
                    $selectedd="selected='yes'";
                } else {
                    $selectedd="";
                }
            }
            $return.='<option value="'.$value['id_group'].'" '.$selectedd.'>('.$value['id_group'].') '.$value['name'].'</option>';
        }
        return $return;
    }
        
	    
	public function getContent(){
	   	$output="";
        
        if (Tools::isSubmit('selecttab')){
            Configuration::updateValue('hbp_lasttab',"{$_POST['selecttab']}");
        } 
        
        if (Tools::isSubmit('hbp_tmce_disabled')){
            Configuration::updateValue('hbp_tmce_disabled',"{$_POST['hbp_tmce_disabled']}");
        }
        
        if (Tools::isSubmit('hbp_forceurls')){
            Configuration::updateValue('hbp_forceurls',"{$_POST['hbp_forceurls']}");
        }
        
        if (Tools::isSubmit('hbp_rh_submit')){
            $this->rebuildModuleFile();
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
        }        
        
        if (Tools::isSubmit('create_new_block')){
            if ($this->psversion()==5 || $this->psversion()==6){
                if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE')==1){
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'hbp_block` (hcgroup, cgroup, date,datefrom,dateto,position,bssl,hook,name,active,logged,shop,homeonly,productsonly,selectedproducts,cmsonly,selectedcms,productscat,selected_pcats,selected_cats,catsonly, manufsonly, selected_manufs, urlonly, url) VALUES (\''.$_POST['hcgroup'].'\',\''.$_POST['cgroup'].'\',\''.$_POST['date'].'\',\''.$_POST['datefrom'].'\',\''.$_POST['dateto'].'\',\'0\',\''.$_POST['bssl'].'\',\''.$_POST['hook'].'\',\''.$_POST['name'].'\',\''.$_POST['bactive'].'\',\''.$_POST['logged'].'\',\''.$this->context->shop->id.'\',\''.$_POST['homeonly'].'\',\''.$_POST['productsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedproducts'])).'\',\''.$_POST['cmsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedcms'])).'\',\''.$_POST['productscat'].'\',\''.str_replace(' ','',trim($_POST['selected_pcats'])).'\',\''.str_replace(' ','',trim($_POST['selected_cats'])).'\',\''.$_POST['catsonly'].'\',\''.$_POST['manufsonly'].'\',\''.str_replace(' ','',trim($_POST['selected_manufs'])).'\',\''.$_POST['urlonly'].'\',\''.$_POST['selected_url'].'\')');
                } else {
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'hbp_block` (hcgroup, cgroup, date,datefrom,dateto,position,bssl,hook,name,active,logged,homeonly,productsonly,selectedproducts,cmsonly,selectedcms,productscat,selected_pcats,selected_cats,catsonly, manufsonly, selected_manufs, urlonly, url) VALUES (\''.$_POST['hcgroup'].'\',\''.$_POST['cgroup'].'\',\''.$_POST['date'].'\',\''.$_POST['datefrom'].'\',\''.$_POST['dateto'].'\',\'0\',\''.$_POST['bssl'].'\',\''.$_POST['hook'].'\',\''.$_POST['name'].'\',\''.$_POST['bactive'].'\',\''.$_POST['logged'].'\',\''.$_POST['homeonly'].'\',\''.$_POST['productsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedproducts'])).'\',\''.$_POST['cmsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedcms'])).'\',\''.$_POST['productscat'].'\',\''.str_replace(' ','',trim($_POST['selected_pcats'])).'\',\''.str_replace(' ','',trim($_POST['selected_cats'])).'\',\''.$_POST['catsonly'].'\',\''.$_POST['manufsonly'].'\',\''.str_replace(' ','',trim($_POST['selected_manufs'])).'\',\''.$_POST['urlonly'].'\',\''.$_POST['selected_url'].'\')');
                }
            } else {
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'hbp_block` (hcgroup, cgroup, date,datefrom,dateto,position,bssl,hook,name,active,logged,homeonly,productsonly,selectedproducts,cmsonly,selectedcms,productscat,selected_pcats,selected_cats,catsonly, manufsonly, selected_manufs, urlonly, url) VALUES (\''.$_POST['hcgroup'].'\',\''.$_POST['cgroup'].'\',\''.$_POST['date'].'\',\''.$_POST['datefrom'].'\',\''.$_POST['dateto'].'\',\'0\',\''.$_POST['bssl'].'\',\''.$_POST['hook'].'\',\''.$_POST['name'].'\',\''.$_POST['bactive'].'\',\''.$_POST['logged'].'\',\''.$_POST['homeonly'].'\',\''.$_POST['productsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedproducts'])).'\',\''.$_POST['cmsonly'].'\',\''.str_replace(' ','',trim($_POST['selectedcms'])).'\',\''.$_POST['productscat'].'\',\''.str_replace(' ','',trim($_POST['selected_pcats'])).'\',\''.str_replace(' ','',trim($_POST['selected_cats'])).'\',\''.$_POST['catsonly'].'\',\''.$_POST['manufsonly'].'\',\''.str_replace(' ','',trim($_POST['selected_manufs'])).'\',\''.$_POST['urlonly'].'\',\''.$_POST['selected_url'].'\')');
            }
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
            $lastslide=$this->lastslide();
        
            $languages = Language::getLanguages(true);
            foreach ($languages as $language){
                $body=$_POST['title'][$language['id_lang']];
                    $v=trim($body);
                    $v=self::smart_clean($v);
                    
                    //$v=preg_replace("/\s+/", ' ', $v);
                    //$v=preg_replace('/(\v|\s)+/', ' ', $v);
                    //$v=preg_replace("/[\r\n]+/", "", $v);
                    //$v=str_replace(CHR(13).CHR(10),"",$v);
                    //$v=str_replace("  "," ",$v);
                    //$v=str_replace(array("\rn", "\r", "\n","\t"), array(' ',' ',' '),$v);
                    //$v=str_replace(array("\\'","\\\\","\\\""), array('\'',"\""),$v); 
                    $v=@mysql_escape_string($v);
                    
                Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'hbp_block_lang` (id,id_lang,body) VALUES (\''.$lastslide.'\',\''.$language['id_lang'].'\',\''.$v.'\')');
            }
            
        }
        
        if (Tools::isSubmit('togglehook')){
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
            if (Tools::isSubmit('status')){$status=Tools::getValue('status');}else{$status=0;}
            Configuration::updateValue('hbp_'.Tools::getValue('togglehook'),$status);					
			$this->registerHook(Tools::getValue('togglehook'));
        }
        
        
        if (Tools::isSubmit('activate_block')){
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
            if (Tools::isSubmit('status')){$status=Tools::getValue('status');}else{$status=0;}
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'hbp_block` SET active="'.$status.'" WHERE id='.Tools::getValue('activate_block').' ');
        }
        
        if (Tools::isSubmit('removeblock')){
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'hbp_block` WHERE id='.Tools::getValue('removeblock').' ');
        }
        
        $customhook_conf="";
        if (Tools::isSubmit('hbp_nh_hook')){
            if ($this->psversion()==5 || $this->psversion()==6){
                if (Tools::getValue('hbp_nh_totally_new')==1){
                    $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
                    if (Hook::getIdByName(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))))==false){
                        $newhook = new Hook();
                        $newhook->name=preg_replace("/[^\da-z]/i",'',trim(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook')))));
                        $newhook->live_edit=1;
                        $newhook->position=1;
                        $newhook->add();
                        $newhook_verify= Hook::getIdByName(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $customhook_conf="
       					    <div class=\"bootstrap\" style=\"margin-top:20px;\">
                                <div class=\"alert alert-success\">
             			            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                     			    ".$this->l('new hook added')."
                          		</div>
                            </div>";
                        $output=$customhook_conf;
                        $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->rebuildModuleFile();
                    } else {
                        $customhook_conf="
       					    <div class=\"bootstrap\" style=\"margin-top:20px;\">
                                <div class=\"alert alert-warning\">
             			            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                     			    ".$this->l('hook already in database')."
                          		</div>
                            </div>";
                        $output=$customhook_conf;
                        $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->rebuildModuleFile();
                    }
                } else {
                    $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                    $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                    $this->rebuildModuleFile();
                }
            } elseif ($this->psversion()==4){
                if (Tools::getValue('hbp_nh_totally_new')==1){
                    $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
                    if ($this->getIdByName(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))))==false){
                        $newhook = new Hook();
                        $name=preg_replace("/[^\da-z]/i",'',trim(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook')))));
                        $newhook->name=$name;
                        $newhook->title=$name;
                        $newhook->live_edit=1;
                        $newhook->position=1;
                        $newhook->add();
                        $newhook_verify= $this->getIdByName(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $customhook_conf="
       					    <div class=\"bootstrap\" style=\"margin-top:20px;\">
                                <div class=\"alert alert-success\">
             			            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                     			    ".$this->l('new hook added')."
                          		</div>
                            </div>";
                        $output=$customhook_conf;
                        $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->rebuildModuleFile();
                    } else {
                        $customhook_conf="
       					    <div class=\"bootstrap\" style=\"margin-top:20px;\">
                                <div class=\"alert alert-warning\">
             			            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                     			    ".$this->l('hook already in database')."
                          		</div>
                            </div>";
                        $output=$customhook_conf;
                        $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                        $this->rebuildModuleFile();
                    }
                } else {
                    $this->registerHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                    $this->verifyNewHook(preg_replace("/[^\da-z]/i",'',trim(Tools::getValue('hbp_nh_hook'))));
                    $this->rebuildModuleFile();
                }
                
            }
        }
        
        if (Tools::isSubmit('save_block')){
            $output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
            //echo ('UPDATE `'._DB_PREFIX_.'hbp_block` SET  name="'.Tools::getValue('name').'", bssl="'.Tools::getValue('bssl').'", logged="'.Tools::getValue('logged').'", active="'.Tools::getValue('bactive').'" WHERE id='.Tools::getValue('idblock').' ');
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'hbp_block` SET hcgroup="'.Tools::getValue('hcgroup').'", cgroup="'.Tools::getValue('cgroup').'", date="'.Tools::getValue('date').'", datefrom="'.Tools::getValue('datefrom').'", dateto="'.Tools::getValue('dateto').'", selected_cats="'.Tools::getValue('selected_cats').'", catsonly="'.Tools::getValue('catsonly').'",  productscat="'.Tools::getValue('productscat').'", selected_pcats="'.Tools::getValue('selected_pcats').'",  name="'.Tools::getValue('name').'", bssl="'.Tools::getValue('bssl').'", logged="'.Tools::getValue('logged').'", active="'.Tools::getValue('bactive').'", homeonly="'.Tools::getValue('homeonly').'", productsonly="'.Tools::getValue('productsonly').'", selectedproducts="'.str_replace(' ','',trim(Tools::getValue('selectedproducts'))).'", cmsonly="'.Tools::getValue('cmsonly').'", selectedcms="'.str_replace(' ','',trim(Tools::getValue('selectedcms'))).'", selected_manufs="'.str_replace(' ','',trim(Tools::getValue('selected_manufs'))).'", manufsonly="'.Tools::getValue('manufsonly').'", urlonly="'.Tools::getValue('urlonly').'", url="'.Tools::getValue('selected_url').'" WHERE id='.Tools::getValue('idblock').' ');
            $languages = Language::getLanguages(true);            
            foreach ($languages as $language){
                    $body=$_POST['title'][$language['id_lang']];
                    $v=trim($body);
                    $v=self::smart_clean($v);
                    
                    //$v=preg_replace("/\s+/", ' ', $v);
                    //$v=preg_replace('/(\v|\s)+/', ' ', $v);
                    //$v=preg_replace("/[\r\n]+/", "", $v);
                    //$v=str_replace(CHR(13).CHR(10),"",$v);
                    //$v=str_replace("  "," ",$v);
                    //$v=str_replace(array("\rn", "\r", "\n","\t"), array(' ',' ',' '),$v);
                    //$v=str_replace(array("\\'","\\\\","\\\""), array('\'',"\""),$v); 
                    $v=@mysql_escape_string($v);
                    
                Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'hbp_block_lang` SET body=\''.$v.'\' WHERE id=\''.Tools::getValue('idblock').'\' AND id_lang=\''.$language['id_lang'].'\' ');
            }
        }
        
        
		if (Tools::isSubmit('submit_settings')){
			$output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="" /></div>';
			foreach ($_POST['hidden'] AS $hook=>$value){
			 
				if (isset($_POST['hook'][$hook]) && $_POST['hook'][$hook]==1){
					$install_hook=str_replace("hbp_","",$hook);					
					$this->registerHook($install_hook);
					Configuration::updateValue($hook, '1');
				} else {
					Configuration::updateValue($hook, '0');
				}
			}

			foreach ($_POST['body'] AS $key => $value){
                foreach ($value AS $k=>$v){
                    echo "";
                    $v=trim($v);
                    $v=self::smart_clean($v);
                    $value[$k]=preg_replace("/\s+/", ' ', $text);
                    $value[$k]=preg_replace('/(\v|\s)+/', ' ', $str);
                    $value[$k]=preg_replace("/[\r\n]+/", "", $v);
                    $value[$k]=str_replace(CHR(13).CHR(10),"",$v);
                    $value[$k]=str_replace("  "," ",$v);
                    $value[$k]=str_replace(array("\rn", "\r", "\n","\t"), array(' ',' ',' '),$v);
                    $value[$k]=str_replace(array("\\'","\\\\","\\\""), array('\'',"\""),$v); 
                }
                Configuration::updateValue($key."_body", $value, true);
            }    
		}                                    
       	   
        $output.="";
        return $output.$this->displayForm();
	}

    public function runStatement($statement){
       if (@!Db :: getInstance()->Execute($statement)) {
        return false;
	   }
        return true;
    }

public function inconsistency(){
    $prefix = _DB_PREFIX_;
    $engine = _MYSQL_ENGINE_;    
    /**
    foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."hbp_block'") AS $key => $column){ 
        $length=str_replace("(","",$column['COLUMN_TYPE']);
        $length=str_replace(")","",$length);
        $length=str_replace($column['DATA_TYPE'],"",$length);
        
        if ($column['DATA_TYPE']!="text"){
            $default=$column['COLUMN_DEFAULT'];
        } else {
            $default="X";
            $length="X";
        }
        
        echo'
        $table[\'hbp_block\'][\''.$column['COLUMN_NAME'].'\'][\'type\']='.$column['DATA_TYPE'].';
        $table[\'hbp_block\'][\''.$column['COLUMN_NAME'].'\'][\'length\']='.$length.';
        $table[\'hbp_block\'][\''.$column['COLUMN_NAME'].'\'][\'default\']='.$default.';';  
    }
    **/

        $table['hbp_block']['position']['type']='int';
        $table['hbp_block']['position']['length']=10;
        $table['hbp_block']['position']['default']=1;
        $table['hbp_block']['hook']['type']='varchar';
        $table['hbp_block']['hook']['length']=50;
        $table['hbp_block']['hook']['default']=NULL;
        $table['hbp_block']['active']['type']='int';
        $table['hbp_block']['active']['length']=11;
        $table['hbp_block']['active']['default']=0;
        $table['hbp_block']['logged']['type']='int';
        $table['hbp_block']['logged']['length']=11;
        $table['hbp_block']['logged']['default']=0;
        $table['hbp_block']['name']['type']='varchar';
        $table['hbp_block']['name']['length']=150;
        $table['hbp_block']['name']['default']=NULL;
        $table['hbp_block']['bssl']['type']='int';
        $table['hbp_block']['bssl']['length']=1;
        $table['hbp_block']['bssl']['default']=0;
        $table['hbp_block']['shop']['type']='int';
        $table['hbp_block']['shop']['length']=4;
        $table['hbp_block']['shop']['default']=1;
        $table['hbp_block']['homeonly']['type']='int';
        $table['hbp_block']['homeonly']['length']=1;
        $table['hbp_block']['homeonly']['default']=0;
        $table['hbp_block']['productsonly']['type']='int';
        $table['hbp_block']['productsonly']['length']=1;
        $table['hbp_block']['productsonly']['default']=0;
        $table['hbp_block']['selectedproducts']['type']='text';
        $table['hbp_block']['selectedproducts']['length']='X';
        $table['hbp_block']['selectedproducts']['default']='X';
        $table['hbp_block']['cmsonly']['type']='int';
        $table['hbp_block']['cmsonly']['length']=1;
        $table['hbp_block']['cmsonly']['default']=0;
        $table['hbp_block']['selectedcms']['type']='text';
        $table['hbp_block']['selectedcms']['length']='X';
        $table['hbp_block']['selectedcms']['default']='X';
        $table['hbp_block']['productscat']['type']='int';
        $table['hbp_block']['productscat']['length']=1;
        $table['hbp_block']['productscat']['default']=0;
        $table['hbp_block']['selected_pcats']['type']='text';
        $table['hbp_block']['selected_pcats']['length']='X';
        $table['hbp_block']['selected_pcats']['default']='X';
        $table['hbp_block']['catsonly']['type']='int';
        $table['hbp_block']['catsonly']['length']=1;
        $table['hbp_block']['catsonly']['default']=0;
        $table['hbp_block']['selected_cats']['type']='text';
        $table['hbp_block']['selected_cats']['length']='X';
        $table['hbp_block']['selected_cats']['default']='X';
        $table['hbp_block']['manufsonly']['type']='int';
        $table['hbp_block']['manufsonly']['length']=1;
        $table['hbp_block']['manufsonly']['default']=0;
        $table['hbp_block']['selected_manufs']['type']='text';
        $table['hbp_block']['selected_manufs']['length']='X';
        $table['hbp_block']['selected_manufs']['default']='X';
        $table['hbp_block']['date']['type']='int';
        $table['hbp_block']['date']['length']=1;
        $table['hbp_block']['date']['default']=0;
        $table['hbp_block']['datefrom']['type']='varchar';
        $table['hbp_block']['datefrom']['length']=60;
        $table['hbp_block']['datefrom']['default']=NULL;
        $table['hbp_block']['dateto']['type']='varchar';
        $table['hbp_block']['dateto']['length']=60;
        $table['hbp_block']['dateto']['default']=NULL;
        $table['hbp_block']['urlonly']['type']='int';
        $table['hbp_block']['urlonly']['length']=1;
        $table['hbp_block']['urlonly']['default']=0;
        $table['hbp_block']['url']['type']='text';
        $table['hbp_block']['url']['length']='X';
        $table['hbp_block']['url']['default']='X';
        $table['hbp_block']['cgroup']['type']='text';
        $table['hbp_block']['cgroup']['length']='X';
        $table['hbp_block']['cgroup']['default']='X';
        $table['hbp_block']['hcgroup']['type']='int';
        $table['hbp_block']['hcgroup']['length']=11;
        $table['hbp_block']['hcgroup']['default']=0;
    
        $return='';
        
        
        //hbp_block
        foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."hbp_block'") AS $key => $column){
            $return[$column['COLUMN_NAME']]="1";
        }
        foreach ($table['hbp_block'] as $key => $field){
            if (!isset($return[$key])){
                $error[$key]['type']="0";
                $error[$key]['message']=$this->l('Database inconsistency, column does not exist');
                if ($field['default']!="X"){
                    if ($this->runStatement("ALTER TABLE `${prefix}hbp_block` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT '".$field['default']."'")){
                        $error[$key]['fixed']=$this->l('... FIXED!');
                    } else {
                        $error[$key]['fixed']=$this->l('... ERROR!');
                    }
                } else {
                     if ($this->runStatement("ALTER TABLE `${prefix}hbp_block` ADD COLUMN `".$key."` ".$field['type'])){
                         $error[$key]['fixed']=$this->l('... FIXED!');
                     } else {
                         $error[$key]['fixed']=$this->l('... ERROR!');
                     }
                }
                if (isset($field['config'])){
                    Configuration::updateValue($field['config'],'1');
                }
            } else {
                $error[$key]['type']="1";
                $error[$key]['message']=$this->l('OK!');
                $error[$key]['fixed']=$this->l('');
                if (isset($field['config'])){
                    Configuration::updateValue($field['config'],'1');
                }
            }
        }
        
        
        $form.='<table class="inconsistency"><tr><td colspan="4" style="text-align:center">'.$this->l('POPUP TABLE').'</td></tr>';
        foreach ($error as $column => $info){
            $form.="<tr><td class='inconsistency".$info['type']."'></td><td>".$column."</td><td>".$info['message']."</td><td>".$info['fixed']."</td></tr>";
        }
        $form.="</table>";
        
        return $form;
        
    }


	public function displayForm(){
	        if (Configuration::get('hbp_lasttab')==1){$selected1="active";}else{$selected1="";}
            if (Configuration::get('hbp_lasttab')==2){$selected2="active";}else{$selected2="";}
            if (Configuration::get('hbp_lasttab')==3){$selected3="active";}else{$selected3="";}
            
            if (Configuration::get('hbp_lasttab')==3){
               $returntotal='<fieldset>
               <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Upgrade Check').'</legend>
               '.$this->inconsistency().'
               </fieldset>'; 
               
            }
            
            if (Configuration::get('hbp_lasttab')==1){
                $returntotal='
                <fieldset>
                    <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Global Settings').'</legend>
                    <form name="globalsettings" id="globalsettings" method="post">
                        <label>'.$this->l('TinyMCE editor disabled by default').'</label>
            			<div class="margin-form">
                            <select type="text" name="hbp_tmce_disabled" style="max-width:200px;">
                                <option value="1" '.(Configuration::get('hbp_tmce_disabled')==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                                <option value="0" '.(Configuration::get('hbp_tmce_disabled')==1 ? "" : "selected=\"selected\" ").'>'.$this->l('No').'</option>
                            </select>
           				</div>
                        <label>'.$this->l('Prevent removing URLs from images').'</label>
            			<div class="margin-form">
                            <select type="text" name="hbp_forceurls" style="max-width:200px;">
                                <option value="1" '.(Configuration::get('hbp_forceurls')==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                                <option value="0" '.(Configuration::get('hbp_forceurls')==1 ? "" : "selected=\"selected\" ").'>'.$this->l('No').'</option>
                            </select>
           				</div>
                        <input type="submit" class="extra button" name="global_settings" value="'.$this->l('save settings').' "/>
                     </form>
                 </fieldset>';
            }
            
            if (Configuration::get('hbp_lasttab')==2){
    	        $form2='';
    	   		$languages = Language::getLanguages(true);
                $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');	
    			global $cookie;
                $iso = Language::getIsoById((int)($cookie->id_lang));
                $isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
                $ad = dirname($_SERVER["PHP_SELF"]);
            
                $html4='';
                if ($this->psversion()==5 || $this->psversion()==6){
                    $this->context->controller->addJqueryUI('ui.sortable');
                } elseif ($this->psversion()==4 ){
                    $html4.='<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>';                
                    $html4.='<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
                    $html4.='<script>$(document).ready(function(){
                    	tinyMCE.init({
                		mode : "specific_textareas",
                		theme : "advanced",
                		skin:"cirkuit",
                		editor_selector : "rte",
                		editor_deselector : "noEditor",
                		plugins : "safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,fullscreen,xhtmlxtras,preview",
                		// Theme options
                		theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleprops,|,cite,abbr,acronym,del,ins,attribs,pagebreak",
                		theme_advanced_buttons2 : "styleselect,formatselect,fontselect,fontsizeselect,forecolor,backcolor",
                        theme_advanced_buttons3 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
                		theme_advanced_buttons4 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,fullscreen",
                		theme_advanced_toolbar_location : "top",
                		theme_advanced_toolbar_align : "left",
                		theme_advanced_statusbar_location : "bottom",
                		theme_advanced_resizing : false,
                        content_css : pathCSS+"global.css",
                		document_base_url : ad,
                		width: "600",
                		height: "auto",
                		font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
                		elements : "nourlconvert,ajaxfilemanager",
                		file_browser_callback : "ajaxfilemanager",
                		entity_encoding: "raw",
                		convert_urls : false,
                        language : iso,
                        theme_advanced_toolbar_location : "top",
                    		theme_advanced_toolbar_align : "left",
                    		theme_advanced_statusbar_location : "bottom",
                    		theme_advanced_resizing : false,
                            extended_valid_elements: \'pre[*],script[*],style[*],meta[*]\',
                            valid_children: "+body[style|script|meta],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                            valid_elements : \'*[*]\',
                            force_p_newlines : false,
                            cleanup: false,
                            forced_root_block : false,
                            force_br_newlines : true
                		
                	});
                    });
                    </script>';
                    $html4.='<script type="text/javascript" src="../modules/'.$this->name.'/jquery-ui-1.10.2.custom.min.js"></script>';
                } elseif ($this->psversion()==3 && $this->psversion(2)>4){
                    $html4.='<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>';
                    $html4.='<script type="text/javascript">
    					tinyMCE.init({
    						mode : "textareas",
    						theme : "advanced",
    						plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
    						// Theme options
    						theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
    						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,",
    						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
    						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
                            theme_advanced_buttons5 : "forecolor,backcolor, styleselect,formatselect,fontselect,fontsizeselect,",
    						theme_advanced_toolbar_location : "top",
    						theme_advanced_toolbar_align : "left",
    						theme_advanced_statusbar_location : "bottom",
    						theme_advanced_resizing : false,
    						content_css : "'.__PS_BASE_URI__.'themes/prestashop/css/global.css",
    						document_base_url : "'.__PS_BASE_URI__.'",
    						width: "500",
    						height: "auto",
    						font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
    						// Drop lists for link/image/media/template dialogs
    						template_external_list_url : "lists/template_list.js",
    						external_link_list_url : "lists/link_list.js",
    						external_image_list_url : "lists/image_list.js",
    						media_external_list_url : "lists/media_list.js",
    						elements : "nourlconvert",
    						entity_encoding: "raw",
    						convert_urls : false,
    						language : "en",
                            theme_advanced_toolbar_location : "top",
                    		theme_advanced_toolbar_align : "left",
                    		theme_advanced_statusbar_location : "bottom",
                    		theme_advanced_resizing : false,
                            extended_valid_elements: \'pre[*],script[*],style[*],emta[*]\',
                            valid_children: "+body[style|script|meta],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                            valid_elements : \'*[*]\',
                            force_p_newlines : false,
                            cleanup: false,
                            forced_root_block : false,
                            force_br_newlines : true
    						
    					});
    		</script>';
                } elseif ($this->psversion()==2 || $this->psversion()==3){
                    $html4.='<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>';
                    $html4.='<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>';
                    $html4.='<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>';
                    $html4.='<script type="text/javascript" src="../modules/'.$this->name.'/jquery-ui-1.10.2.custom.min.js"></script>';
                    $html4.='<script type="text/javascript">
    		function tinyMCEInit(element)
    		{
    			$().ready(function() {
    				$(element).tinymce({
    					// Location of TinyMCE script
    					script_url : \''.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js\',
    					// General options
    					theme : "advanced",
    					plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
    					// Theme options
    					theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
    					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
    					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
    					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
    					theme_advanced_toolbar_location : "top",
    					theme_advanced_toolbar_align : "left",
    					theme_advanced_statusbar_location : "bottom",
    					theme_advanced_resizing : true,
    					content_css : "'.__PS_BASE_URI__.'themes/prestashop/css/global.css",
    					// Drop lists for link/image/media/template dialogs
    					template_external_list_url : "lists/template_list.js",
    					external_link_list_url : "lists/link_list.js",
    					external_image_list_url : "lists/image_list.js",
    					media_external_list_url : "lists/media_list.js",
    					elements : "nourlconvert",
    					convert_urls : false,
    					language : "'.$isoTinyMCE.'",
                        theme_advanced_toolbar_location : "top",
                    		theme_advanced_toolbar_align : "left",
                    		theme_advanced_statusbar_location : "bottom",
                    		theme_advanced_resizing : false,
                            extended_valid_elements: \'pre[*],script[*],style[*],meta[*]\',
                            valid_children: "+body[style|script|meta],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                            valid_elements : \'*[*]\',
                            force_p_newlines : false,
                            cleanup: false,
                            forced_root_block : false,
                            force_br_newlines : true
    				});
    			});
    		}
    		tinyMCEInit(\'textarea.rte\');
    		</script>';
                }
    			$form=$html4.'
                
                <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>            
                
    			<script type="text/javascript">
    			var iso = \''.$isoTinyMCE.'\' ;
    			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
    			var ad = \''.$ad.'\' ;';
                
                
      
                
    			if ($this->psversion()==5 ){
    			$form.='
                    $(document).ready(function(){
        			    tinySetup({
            				editor_selector :"rte",
                    		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
                    		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                    		theme_advanced_toolbar_location : "top",
                    		theme_advanced_toolbar_align : "left",
                    		theme_advanced_statusbar_location : "bottom",
                    		theme_advanced_resizing : false,
                            extended_valid_elements: \'pre[*],script[*],style[*],meta[*]\',
                            valid_children: "+body[style|script|meta],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                            valid_elements : \'*[*]\',
                            force_p_newlines : false,
                            cleanup: false,
                            forced_root_block : false,
                            force_br_newlines : true
        				});
    			    });';
    			}
                
                if ($this->psversion()==6){
    			    $form.='
        	        function addClass(id){
        				tinyMCE.execCommand("mceToggleEditor", false, id);
        			}
        			
        			function removeClass(id){
        				tinyMCE.execCommand("mceToggleEditor", false, id);
        			}				
        			</script>	
        			';
                }
                
                if ($this->psversion()==5){
    			    $form.='
        	        function addClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        				tinyMCE.execCommand("mceAddControl", true, id);
        			}
        			
        			function removeClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        			}				
        			</script>	
        			';
                }
                
                if ($this->psversion()==4){
    			    $form.='
        	        function addClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        				tinyMCE.execCommand("mceAddControl", true, id);
        			}
        			
        			function removeClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        			}				
        			</script>	
        			';
                }
                
                if ($this->psversion()==3){
    			    $form.='
        	        function addClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        				tinyMCE.execCommand("mceAddControl", true, id);
        			}
        			
        			function removeClass(id){
        				tinyMCE.execCommand("mceRemoveControl", false, id);
        			}				
        			</script>	
        			';
                }                
                
               if ($this->psversion()==6){
                    if (Configuration::get('hbp_forceurls')!=1){
                    $form.='
                    <script type="text/javascript" src="../modules/htmlboxpro/tinymce16.inc.js"></script>
                    ';
                    } else {
                    $form.='
                    <script type="text/javascript" src="../modules/htmlboxpro/tinymce16-force-urls.inc.js"></script>
                    ';    
                    }
                }
                
                if ($this->psversion(1)==5){
                    $form.='<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
                }
    		
            
    		$radio="";
    		$body="";
    		foreach ($this->allhooks AS $key=>$value){
    		    $blocks='';
                
    		    $hook=$value;
    		    foreach ($this->get_blocks($value) as $k=>$v){
    		      $blocks.="
                  <li id=\"elements{$value}_{$v['id']}\">".$v['name']."
                  
                  <span class=\"activate\" onclick=\"activate{$v['id']}.submit();\">
                    <form id=\"activate{$v['id']}\" name=\"activate{$v['id']}\" method=\"post\"/>
                      <input type=\"hidden\" name=\"activate_block\" value=\"{$v['id']}\" />
                      <input type=\"checkbox\" name=\"status\" value=\"1\" ".($v['active']==1 ? 'checked="checked"':'')." />
                    </form>
                  </span>
                  
                  <span class=\"remove\" onclick=\"removeblock{$v['id']}.submit();\">
                    <form id=\"removeblock{$v['id']}\" name=\"removeblock{$v['id']}\" method=\"post\"/>
                      <input type=\"hidden\" name=\"removeblock\" value=\"{$v['id']}\" />
                    </form>
                  </span>
                  
                  <span class=\"edit\" onclick=\"editblock{$v['id']}.submit();\">
                      <form id=\"editblock{$v['id']}\" name=\"editblock{$v['id']}\" method=\"post\"/>
                          <input type=\"hidden\" name=\"editblock\" value=\"{$v['id']}\" />
                      </form>
                  </span>
                  
                  </li>
                  ";
    		    }
                if ($blocks==''){
                    $blocks.='<div class="info">'.$this->l('no blocks defined').'</div>';
                } else {
                $blocks="<ul class=\"slides\" id=\"elements$value\">$blocks</ul>".'<script type="text/javascript">
    			$(function() {
    				var $mySlides'.$value.' = $("#elements'.$value.'");
    				$mySlides'.$value.'.sortable({
    					opacity: 0.6,
    					cursor: "move",
    					update: function() {
    						var order = $(this).sortable("serialize") + "&hook='.$value.'&action=updateSlidesPosition";
    						$.post("'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/ajax_'.$this->name.'.php", order);
    						}
    					});
    				$mySlides'.$value.'.hover(function() {
    					$(this).css("cursor","move");
    					},
    					function() {
    					$(this).css("cursor","auto");
    				});
    			});
    		    </script>';
                }
                
    		    $radio.="
    				<tr class='hookslist ".(Configuration::get('hbp_'.$value)==1 ? 'active':'inactive')."'>
                        <td class=\"checkbx\">
                            <form id=\"togglehook$value\" name=\"togglehook$value\" method=\"post\"><input type=\"hidden\" name=\"togglehook\" value=\"".$value."\"><input type=\"checkbox\" name=\"status\" onchange=\"togglehook$value.submit();\" value=\"1\" ".(Configuration::get('hbp_'.$value)==1 ? 'checked="checked"':'')."/></form>
                        </td>
    					<td class=\"hname\"><a style=\"display:inline-block; float:left; \" target=\"_blank\" href=\"http://mypresta.eu/prestashop/hook/".strtolower($hook)."/\">$hook</a><span style=\"width:20px; height:20px; cursor:pointer; text-align:right; display:inline-block; float:right;\" onclick=\"newblock$value.submit();\"><form id=\"newblock$value\" name=\"newblock$value\" method=\"post\"><input type=\"hidden\" name=\"newblock\" value=\"$value\"/></form><img style=\"opacity:0.3; filter:alpha(opacity=30);\" class=\"editbutton\" src=\"../$this->dir/img/add-icon.png\" alt=\"{$hook}_{$id_lang_default}\" /></span></td>
    					<td class=\"hoptions\">
                            <span style=\"cursor:pointer; text-align:right; display:inline-block; float:right;\"><img style=\"opacity:0.3; filter:alpha(opacity=30); padding:0px;\" class=\"accordion\" src=\"../$this->dir/img/br_down.png\" alt=\"{$hook}\" /></span>
                        </td>
    				</tr>
                    <tr class='hookslist hook_blocks hook_$value'>
                        <td colspan=\"3\" style=\"border-left:1px solid #c0c0c0; background:#FFF;\" class=\"checkbox hoptions hname\">
                        ".$blocks."
                        </td>
                    </tr>		
    			";
    		}
            
            $customHooks=$this->getListNewHook();
            if (count($customHooks)>0){
                foreach ($customHooks AS $key=>$value){
                    $this->customhooks[$key]=$value['hook'];
                }
            }
            
            $radio2="";
    		$body2="";
            if (count($this->customhooks)>0){
        		foreach ($this->customhooks AS $key=>$value){
        		    $blocks2='';
        		    $hook=$value;
        		    foreach ($this->get_blocks($value) as $k=>$v){
        		      $blocks2.="
                      <li id=\"elements{$value}_{$v['id']}\">".$v['name']."
                      
                      <span class=\"activate\" onclick=\"activate{$v['id']}.submit();\">
                        <form id=\"activate{$v['id']}\" name=\"activate{$v['id']}\" method=\"post\"/>
                          <input type=\"hidden\" name=\"activate_block\" value=\"{$v['id']}\" />
                          <input type=\"checkbox\" name=\"status\" value=\"1\" ".($v['active']==1 ? 'checked="checked"':'')." />
                        </form>
                      </span>
                      
                      <span class=\"remove\" onclick=\"removeblock{$v['id']}.submit();\">
                        <form id=\"removeblock{$v['id']}\" name=\"removeblock{$v['id']}\" method=\"post\"/>
                          <input type=\"hidden\" name=\"removeblock\" value=\"{$v['id']}\" />
                        </form>
                      </span>
                      
                      <span class=\"edit\" onclick=\"editblock{$v['id']}.submit();\">
                          <form id=\"editblock{$v['id']}\" name=\"editblock{$v['id']}\" method=\"post\"/>
                              <input type=\"hidden\" name=\"editblock\" value=\"{$v['id']}\" />
                          </form>
                      </span>
                      
                      </li>
                      ";
        		    }
                    if ($blocks2==''){
                        $blocks2.='<div class="info">'.$this->l('no blocks defined').'</div>';
                    } else {
                    $blocks2="<ul class=\"slides\" id=\"elements$value\">$blocks2</ul>".'<script type="text/javascript">
        			$(function() {
        				var $mySlides'.$value.' = $("#elements'.$value.'");
        				$mySlides'.$value.'.sortable({
        					opacity: 0.6,
        					cursor: "move",
        					update: function() {
        						var order = $(this).sortable("serialize") + "&hook='.$value.'&action=updateSlidesPosition";
        						$.post("'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/ajax_'.$this->name.'.php", order);
        						}
        					});
        				$mySlides'.$value.'.hover(function() {
        					$(this).css("cursor","move");
        					},
        					function() {
        					$(this).css("cursor","auto");
        				});
        			});
        		    </script>';
                    }
                    
        		    $radio2.="
        				<tr class='hookslist ".(Configuration::get('hbp_'.$value)==1 ? 'active':'inactive')."'>
                            <td class=\"checkbx\">
                                <form id=\"togglehook$value\" name=\"togglehook$value\" method=\"post\"><input type=\"hidden\" name=\"togglehook\" value=\"".$value."\"><input type=\"checkbox\" name=\"status\" onchange=\"togglehook$value.submit();\" value=\"1\" ".(Configuration::get('hbp_'.$value)==1 ? 'checked="checked"':'')."/></form>
                            </td>
        					<td class=\"hname\">$hook<span style=\"width:20px; height:20px; cursor:pointer; text-align:right; display:inline-block; float:right;\" onclick=\"newblock$value.submit();\"><form id=\"newblock$value\" name=\"newblock$value\" method=\"post\"><input type=\"hidden\" name=\"newblock\" value=\"$value\"/></form><img style=\"opacity:0.3; filter:alpha(opacity=30);\" class=\"editbutton\" src=\"../$this->dir/img/add-icon.png\" alt=\"{$hook}_{$id_lang_default}\" /></span></td>
        					<td class=\"hoptions\">
                                <span style=\"cursor:pointer; text-align:right; display:inline-block; float:right;\"><img style=\"opacity:0.3; filter:alpha(opacity=30); padding:0px;\" class=\"accordion\" src=\"../$this->dir/img/br_down.png\" alt=\"{$hook}\" /></span>
                            </td>
        				</tr>
                        <tr class='hookslist hook_blocks hook_$value'>
                            <td colspan=\"3\" style=\"border-left:1px solid #c0c0c0; background:#FFF;\" class=\"checkbox hoptions hname\">
                            ".$blocks2."
                            </td>
                        </tr>		
        			";
        		}
            }    
                	
    		   
            $dfrom='';           
               
            if (isset($_POST['newblock'])){
                $title='';
                 foreach ($languages as $language){
                     $title.='
                     <div id="hbpbody_'.$language['id_lang'].'" style="width:100%; clear:both; display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').'; float: left;">
                      <div style="display:block; margin-bottom:10px;">
                      <span class="exclusive button" style="cursor:pointer;" onclick="addClass(\'title_'.$language['id_lang'].'\');">'.$this->l('Editor').'</span>
                      <span class="exclusive button" style="cursor:pointer;" onclick="removeClass(\'title_'.$language['id_lang'].'\');">'.$this->l('Code').'</span><br />
                      </div>
                     <textarea class="'.(Configuration::get('hbp_tmce_disabled')==1 ? '':'rte rtepro').'" style="margin-bottom:0px; width:619px; height:300px;" id="title_'.$language['id_lang'].'" name="title['.$language['id_lang'].']"></textarea>
                     </div>';               
                 }
                 $title.=$this->displayFlags($languages, $id_lang_default, 'hbpbody', 'hbpbody', true);
                 
                $form2='
                <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('New').' '.$_POST['newblock'].' '.$this->l('block').'</legend>
                <form name="newblock" id="newblock" method="post">
                    <input type="hidden" name="hook" value="'.$_POST['newblock'].'"/>
                    <input type="hidden" name="newblock" value="'.$_POST['newblock'].'"/>
                    
                    <label>'.$this->l('Name').'</label>
                    <div class="margin-form">    
                        <input type="text" name="name" value="" />
                        <p class="clear">'.$this->l('Name will be visible only for you').'</p>
                    </div>
                    
                    <label>'.$this->l('Active').'</label>
        			<div class="margin-form">
                        <select type="text" name="bactive" style="max-width:200px;">
                            <option value="1">'.$this->l('Yes').'</option>
                            <option value="0">'.$this->l('No').'</option>
                        </select>
       				</div>
                    
                    <label>'.$this->l('Only on SSL pages').'</label>
        			<div class="margin-form">
                        <select type="text" name="bssl" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
       				</div>
                    
                    <label>'.$this->l('Only on homepage').'</label>
        			<div class="margin-form">
                        <select type="text" name="homeonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
       				</div>
                    
                    <label>'.$this->l('Only on selected product page').'</label>
        			<div class="margin-form">
                        <select type="text" name="productsonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selectedproducts" />&nbsp;<br/>'.$this->l('Products ID, separate by commas').'
       				</div>
                    <label>'.$this->l('Only if viewed product is associated with category:').'</label>
        			<div class="margin-form">
                        <select type="text" name="productscat" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_pcats" />&nbsp;<br/>'.$this->l('Categories ID, separate by commas').'
       				</div>                
                    
                    <label>'.$this->l('Only on selected Category pages').'</label>
        			<div class="margin-form">
                        <select type="text" name="catsonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_cats" />&nbsp;<br/>'.$this->l('Categories ID, separate by commas').'
       				</div>        
                    
                    <label>'.$this->l('Only on selected CMS page').'</label>
        			<div class="margin-form">
                        <select type="text" name="cmsonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selectedcms" />&nbsp;<br/>'.$this->l('CMS ID, separate by commas').'
       				</div>
                    
                    
                    <label>'.$this->l('Only on selected Manufacturers page').'</label>
        			<div class="margin-form">
                        <select type="text" name="manufsonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_manufs" />&nbsp;<br/>'.$this->l('Manufacturer ID, separate by commas').'
       				</div>
                    <label>'.$this->l('Only on selected url').'</label>
        			<div class="margin-form">
                        <select type="text" name="urlonly" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_url" value=""/>&nbsp;<br/>'.$this->l('Enter here full url of page where you want to display block').'
       				</div>                    
                    
                    
                    <label>'.$this->l('Visibility for').'</label>
        			<div class="margin-form">
                        <select type="text" name="logged" style="max-width:200px;">
                            <option value="0" >'.$this->l('All users').'</option>
                            <option value="1" >'.$this->l('Logged only').'</option>
                            <option value="2" >'.$this->l('Unlogged only').'</option>
                        </select>
                        <select type="text" name="cgroup" style="max-width:200px;">
                            <option value="0">'.$this->l('All groups').'</option>
                            '.$this->generateGroups().'
                        </select>
       				</div>
                    <label>'.$this->l('Hide for').'</label>
        			<div class="margin-form">
                        <select type="text" name="hcgroup" style="max-width:200px;">
                            <option value="0">'.$this->l('-- select group--').'</option>
                            '.$this->generateGroups().'
                        </select><br/>'.$this->l('Select group from this option only if you want to hide block for selected customer group').'
       				</div>
                    <br/>
                    <label>'.$this->l('Display date').'</label>
                    <div class="margin-form">
                        <select type="text" name="date" style="max-width:200px;">
                            <option value="0">'.$this->l('No').'</option>
                            <option value="1">'.$this->l('Yes').'</option>
                        </select><br/>'.$this->l('Select this option only if you want to specify visibility of the block depending on date').'
                        <br/><br/><span style="display:inline-block; width:75px;">'.$this->l('Date from: ').'</span><input type="text" name="datefrom" value="'.date("Y/m/d").'"/><br/>
                        <br/><span style="display:inline-block; width:75px;">'.$this->l('Date to: ').'</span><input type="text" name="dateto" value="'.date("Y/m/d").'"/><br/>
       				</div> 
                   
                    '.$title.'
                    
                    <div class="margin-form" style="margin-top:30px; display:block; padding-top:20px;"> 
                        <input type="submit" class="extra button" name="create_new_block" value="'.$this->l('save block').' " onclick="'.$onclick.'"/>
                    </div>
                    
                </form>
                ';            
            }
            
           
            if (isset($_POST['editblock'])){
                $block = $this->get_block($_POST['editblock']);
                $languages = Language::getLanguages(true);
                
                $title='';
                foreach ($languages as $language){
                     $title.='
                     <div id="hbpbody_'.$language['id_lang'].'" style="clear:both; width:100%; display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">
                     <div style="display:block; margin-bottom:10px;">
                     <span class="exclusive button" style="cursor:pointer;" onclick="addClass(\'title_'.$language['id_lang'].'\');">'.$this->l('Editor').'</span>
                     <span class="exclusive button" style="cursor:pointer;" onclick="removeClass(\'title_'.$language['id_lang'].'\');">'.$this->l('Code').'</span><br />
                     </div>
                     <textarea class="'.(Configuration::get('hbp_tmce_disabled')==1 ? '':'rte rtepro').'" style="margin-bottom:4px; width:619px; max-width:580px; height:300px;" id="title_'.$language['id_lang'].'" name="title['.$language['id_lang'].']">'.(isset($block[0]['body'][$language['id_lang']]) ? stripslashes($block[0]['body'][$language['id_lang']]):'').'</textarea>
                     </div>';               
                }
                $title.=$this->displayFlags($languages, $id_lang_default, 'hbpbody', 'hbpbody', true);
                
                if (!in_array($block[0]['hook'],$this->allhooks)){
                    $custom_hook_info='<div class="bootstrap" style="margin-top:20px;">
                                <div class="alert alert-info">
             			            <button type="button" class="close" data-dismiss="alert">×</button>'.$this->l('This block is attached to custom hook. To display it in .tpl file use: ')."{".($this->psversion()==5 || $this->psversion()==6 ? 'Hook::exec':'Module::hookExec')."('".$block[0]['hook']."')}".'</div>
                            </div>';
                } else {
                    $custom_hook_info="";
                }
                
                
                $onclick='';
                foreach ($languages as $language){
                    if ($this->psversion()==5 || $this->psversion()==6){
                        $onclick.='if(tinyMCE.get(\'title_'.$language['id_lang'].'\').isHidden()==true){tinyMCE.get(\'title_'.$language['id_lang'].'\').setContent($(\'#title_'.$language['id_lang'].'\').val());}';
                    }
                }
                
                
                $form2='
                <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Edit block').'</legend>
                <form name="newblock" id="newblock" method="post">
                    <input type="hidden" name="editblock" value="'.$block[0]['id'].'"/>
                    <input type="hidden" name="idblock" value="'.$block[0]['id'].'"/>
                    
                    
                            
                     			    '.$custom_hook_info.'
                  		
     
                    
                    <label>'.$this->l('Name').'</label>
                    <div class="margin-form">    
                        <input type="text" name="name" value="'.$block[0]['name'].'" />
                        <p class="clear">'.$this->l('Name will be visible only for you').'</p>
                    </div>
                    
                    <label>'.$this->l('Active').'</label>
        			<div class="margin-form">
                        <select type="text" name="bactive" style="max-width:200px;">
                            <option value="1" '.($block[0]['active']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                            <option value="0" '.($block[0]['active']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                        </select>
       				</div>
                    
                    <label>'.$this->l('Only on SSL pages').'</label>
        			<div class="margin-form">
                        <select type="text" name="bssl" style="max-width:200px;">
                            <option value="0" '.($block[0]['bssl']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['bssl']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
       				</div>
                    <br/>
                    <label>'.$this->l('Only on homepage').'</label>
        			<div class="margin-form">
                        <select type="text" name="homeonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['homeonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['homeonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
       				</div>
                    
                    <label>'.$this->l('Only on selected product page').'</label>
        			<div class="margin-form">
                        <select type="text" name="productsonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['productsonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['productsonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selectedproducts" value="'.($block[0]['selectedproducts']).'"/>&nbsp;'.$this->l('Products ID, separate by commas').'
       				</div>
                    <br/>
                    <label>'.$this->l('Only if viewed product is associated with category:').'</label>
        			<div class="margin-form">
                        <select type="text" name="productscat" style="max-width:200px;">
                            <option value="0" '.($block[0]['productscat']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['productscat']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_pcats" value="'.($block[0]['selected_pcats']).'"/>&nbsp;'.$this->l('Categories ID, separate by commas').'
       				</div>
                       <br/>
                    <label>'.$this->l('Only on selected Category pages:').'</label>
        			<div class="margin-form">
                        <select type="text" name="catsonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['catsonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['catsonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_cats" value="'.($block[0]['selected_cats']).'"/>&nbsp;'.$this->l('Categories ID, separate by commas').'
       				</div>                           
                    <br/>
                    <label>'.$this->l('Only on selected CMS page').'</label>
        			<div class="margin-form">
                        <select type="text" name="cmsonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['cmsonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['cmsonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selectedcms" value="'.($block[0]['selectedcms']).'"/>&nbsp;'.$this->l('CMS ID, separate by commas').'
       				</div>
                   <br/>
                   
                    <label>'.$this->l('Only on selected Manufacturers page').'</label>
        			<div class="margin-form">
                        <select type="text" name="manufsonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['manufsonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['manufsonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_manufs" value="'.($block[0]['selected_manufs']).'"/>&nbsp;<br/>'.$this->l('Manufacturer ID, separate by commas').'
       				</div>
                    
                    <label>'.$this->l('Only on selected url').'</label>
        			<div class="margin-form">
                        <select type="text" name="urlonly" style="max-width:200px;">
                            <option value="0" '.($block[0]['urlonly']==0 ? "selected=\"selected\" " : "").'>'.$this->l('No').'</option>
                            <option value="1" '.($block[0]['urlonly']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select>
                        <input type="text" name="selected_url" value="'.($block[0]['url']).'"/>&nbsp;<br/>'.$this->l('Enter here full url of page where you want to display block').'
       				</div>

                    
                    <label>'.$this->l('Visibility for').'</label>
        			<div class="margin-form">
                        <select type="text" name="logged" style="max-width:200px;">
                            <option value="0" '.($block[0]['logged']==0 ? "selected=\"selected\" " : "").'>'.$this->l('All users').'</option>
                            <option value="1" '.($block[0]['logged']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Logged only').'</option>
                            <option value="2" '.($block[0]['logged']==2 ? "selected=\"selected\" " : "").'>'.$this->l('Unlogged only').'</option>
                        </select>
                        <select type="text" name="cgroup" style="max-width:200px;">
                            <option value="0" '.($block[0]['cgroup']==0 ? 'selected="yes"':'').'>'.$this->l('All groups').'</option>
                            '.$this->generateGroups($block[0]['cgroup']).'
                        </select>
       				</div>
                    
                    <label>'.$this->l('Hide for').'</label>
        			<div class="margin-form">
                        <select type="text" name="hcgroup" style="max-width:200px;">
                            <option value="0">'.$this->l('-- select group--').'</option>
                            '.$this->generateGroups($block[0]['hcgroup']).'
                        </select><br/>'.$this->l('Select group from this option only if you want to hide block for selected customer group').'
       				</div>
                    
                    <label>'.$this->l('Display date').'</label>
                    <div class="margin-form">
                        <select type="text" name="date" style="max-width:200px;">
                            <option value="0"  '.($block[0]['date']==1 ? "" : "selected=\"selected\"").'>'.$this->l('No').'</option>
                            <option value="1"  '.($block[0]['date']==1 ? "selected=\"selected\" " : "").'>'.$this->l('Yes').'</option>
                        </select><br/>'.$this->l('Select this option only if you want to specify visibility of the block depending on date').'
                        <br/><br/><span style="display:inline-block; width:75px;">'.$this->l('Date from: ').'</span><input type="text" name="datefrom" value="'.$block[0]['datefrom'].'"/><br/>
                        <br/><span style="display:inline-block; width:75px;">'.$this->l('Date to: ').'</span><input type="text" name="dateto" value="'.$block[0]['dateto'].'"/><br/>
       				</div>
                    
                    '.$title.'
                    <div class="margin-form" style="margin-top:30px; display:block; padding-top:20px;"> 
                        <input type="submit" class="extra button" name="save_block" value="'.$this->l('save block').' " onclick="'.$onclick.'"/>
                    </div>
                    
                </form>
                ';    
            }
            
            
            // NEW HOOK FORM
            
     
            $howto="
                    <tr class='hookslist active'>
    					<td class=\"hname\" style=\"width:100%; text-align:center;\">
                            <a href=\"http://mypresta.eu/en/art/news/html-content-box-custom-hooks.html\" target=\"_blank\">".$this->l('How to use custom hooks?')."</a>
                        </td>
    				</tr>";
            
            $customhook_addnew="
            <div style=\"display:block; background:#FFF; clear:both; overflow:hidden;\">
                <form method=\"post\" name=\"hbp_nh\" id=\"hbp_nh\">
                 <table style=\"width:100%; margin-bottom:10px; margin-top:10px;\">
                    <tr>
                        <td style=\"text-align:center;\">
                            ".$this->l('Hook name').":
                        </td>
                        <td style=\"text-align:center;\">
                            <input type=\"text\" name=\"hbp_nh_hook\" style=\"max-width:100px;\"/><br/>
                        </td>
                    </tr>
                </table>
                
                <table style=\"width:100%; margin-bottom:30px;\">
                    <tr>
                        <td style=\"text-align:center;\">
                            ".$this->l('Totally new hook')." <input type=\"checkbox\" value=\"1\" name=\"hbp_nh_totally_new\" />
                        </td>
                        <td style=\"text-align:center;\">
                            <input type=\"submit\" class=\"button\" value=\"".$this->l("add hook")."\" />
                        </td>
                    </tr>
                </table>
                </form>
            </div>";
                    if ($this->psversion()==4 || $this->psversion()==5 || $this->psversion()==6){
                        $customization=''.$customhook_conf."
                        <table style=\"width:220px; margin-top:20px;\">
                        <tr class='hookslist active'>
        					<td class=\"hname\">
                                ".$this->l('Add new hook')."
                            </td>
        				</tr>
                        </table>
                        ".$customhook_addnew."
                        <table style=\"width:220px; margin-top:20px;\">
                        <tr class='hookslist active'>
        					<td class=\"hname\">
                                ".$this->l('Regenerate hooks')."
                            </td>
        				</tr>
                        </table>
                        <div style=\"margin-bottom:20px; text-align:center; display:block; background:#FFF; clear:both; overflow:hidden;\">
                            <div style=\"margin-top:10px; margin-bottom:10px; display:block;\">
                                <form method=\"post\" name=\"hbp_rh\" id=\"hbp_rh\">
                                    <input class=\"button\" type=\"submit\" name=\"hbp_rh_submit\" value=\"".$this->l('regenerate now!')."\"/>
                                </form>
                            </div>
                        </div>
                        ".'
    							<table style="border-bottom:1px solid #c0c0c0;  clear:both; width:218px; vertical-align:top;" cellspacing="0" cellpadding="0">
    							'.$radio2.'
    							</table>
                                <table style="border-bottom:1px solid #c0c0c0;  clear:both; width:218px; vertical-align:top;" cellspacing="0" cellpadding="0">
    							'.$howto.'
    							</table>
                                ';
                    }
            
            
            $returntotal=$form.'
    		<script type="text/javascript" src="../modules/htmlboxpro/script.js"></script>
                <div style="display:block; margin:auto; overflow:hidden; width:100%; vertical-align:top;">
                        <div style="clear:both; display:block; vertical-align:top;">
    						<fieldset style="float:left; display:inline-block;  margin-right:10px; vertical-align:top;">
    							<legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Places (hooks)').'</legend>   
    							<table style="border-bottom:1px solid #c0c0c0;  clear:both; width:218px; vertical-align:top;" cellspacing="0" cellpadding="0">
    							'.$radio.'
    							</table>
                                '.$customization.'
    						</fieldset>
                            <fieldset style="float:left; display:inline-block; width:600px;  vertical-align:top;">
                            '.$form2.'              
                            </fieldset>
                        </div>
                </div>
            ';
    	}
        
        return '
            <style>
                .inactive {opacity:0.5}
                .hookslist ul {padding:0px;}
                .slides li {font-size:10px!important; list-style: none; margin: 0 0 4px 0; padding: 10px 5px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
                .hname a {font-size:12px!important;}
                .slides li:hover {border:1px #000 dashed;}
                .checkbx {padding:5px; border-left:1px solid #c0c0c0; background:#f2f2f2; border-bottom:1px solid #FFF; border-top:1px solid #c0c0c0;}
               
                .activate {display:inline-block; float:left; padding-right:3px;}
                .remove {opacity:0.3; position:relative; top:-5px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/htmlboxpro/img/trash.png") top no-repeat; cursor:pointer;}
                .edit {margin-right:6px; opacity:0.3; position:relative; top:-4px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/htmlboxpro/img/edit.png") top no-repeat; cursor:pointer;}
                
                .hookslist .hoptions {padding:10px; background:#fff; border-right:1px solid #c0c0c0;  border-bottom:1px solid #FFF; border-top:1px solid #c0c0c0; margin-right:10px;}
                .hookslist .hname {min-width:144px; padding:10px; padding-left:5px; background:#f2f2f2; border-bottom:1px solid #FFF; border-top:1px solid #c0c0c0; margin-right:10px;}
    			.hookslist a {font-weight:bold;}
    			.hookslist a:hover {font-weight:bold; color:red;}
                .hook_blocks {display:none; }
                .hook_blocks .hname, hook_blocks .hoptions {border-top:none;}
                
                .language_flags {display:none;}
                .displayed_flag {float:right; margin-left:10px;}
    		</style>
            <link href="../modules/htmlboxpro/css.css" rel="stylesheet" type="text/css" />
            <form name="selectform1" id="selectform1" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="1"></form>
            <form name="selectform2" id="selectform2" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="2"></form>
            <form name="selectform3" id="selectform3" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="3"></form>
            '."
            
                <div id='cssmenu'>
                    <ul>
                       <li class='bgver'><a><span>v".$this->version."</span></a></li>
                       <li class='$selected1' onclick=\"selectform1.submit()\"><a href=\"#\"><span>".$this->l('Settings')."</span></a></li>
                       <li class='$selected2' onclick=\"selectform2.submit()\"><a href=\"#\"><span>".$this->l('Boxes')."</span></a></li>
                       <li class='$selected3' onclick=\"selectform3.submit()\"><a href=\"#\"><span>".$this->l('Upgrade Check')."</span></a></li>
                       <li style='position:relative; display:inline-block; float:right; '><a href='http://mypresta.eu' target='_blank' title='prestashop modules'><img src='../modules/htmlboxpro/logo-white.png' alt='prestashop modules' style=\"position:absolute; top:17px; right:16px;\"/></a></li>
                       <li style='position:relative; display:inline-block; float:right;' class=''><a href='http://mypresta.eu/contact.html' target='_blank'><span>".$this->l('Support')."</span></a></li>
                       <li style='position:relative; display:inline-block; float:right;' class=''><a href='http://mypresta.eu/modules/front-office-features/html-box-pro.html' target='_blank'><span>".$this->l('Updates')."</span></a></li>
                    </ul>
                </div>".$returntotal;
    }

	public static function getIdByName($hook_name){

			$hook_ids = array();
			$result = Db::getInstance()->ExecuteS('
			SELECT `id_hook`, `name`
			FROM `'._DB_PREFIX_.'hook`
			UNION
			SELECT `id_hook`, ha.`alias` as name
			FROM `'._DB_PREFIX_.'hook_alias` ha
			INNER JOIN `'._DB_PREFIX_.'hook` h ON ha.name = h.name');
			foreach ($result as $row)
				$hook_ids[strtolower($row['name'])] = $row['id_hook'];
		

		return (isset($hook_ids[$hook_name]) ? $hook_ids[$hook_name] : false);
	}       
   
    public function installNewHook($name){
        return Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'hbp_customhook` (hook) VALUES ("'.$name.'")');
    }
    public function verifyNewHook($name){
        $return=Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'hbp_customhook` WHERE hook="'.$name.'"');
        if ($return==false){
            $this->installNewHook($name);
        }
    }
    public function getListNewHook(){
        return Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'hbp_customhook`');
    }
    public function rebuildModuleFile(){
        $functions_code=$this->regenerateFunctions();
        $content = file_get_contents("../modules/htmlboxpro/htmlboxpro.php");
        $part1="//~~";
        $part2="explode";
        $part3="~~//";
        $m=explode("//~~explode~~//",$content);
        $full_content=$m[0].$part1.$part2.$part3.$m[1]."\n".$part1.$part2.$part3."\n".$functions_code."\n".$part1.$part2.$part3."\n".$m[3];
        file_put_contents("../modules/htmlboxpro/htmlboxpro.php",$full_content);
    }
    public function regenerateFunctions(){
       $functions=""; 
       foreach ($this->getListNewHook() AS $k=>$v){
           $functions.='
           	function hook'.$v['hook'].'($params){
    		    if (Configuration::get(\'hbp_'.$v['hook'].'\')==1){
                    if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
                    if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
                    if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
                    $blocks=$this->get_blocks(\''.$v['hook'].'\',1,$this->context->cookie->id_lang);
            	    global $smarty;
                    $smarty->assign(\'customer_popup\', $this->context->cookie);
            	    $smarty->assign(array(\'blocks\' => $blocks)); $smarty->assign(array(\'is_https\' => (array_key_exists(\'HTTPS\', $_SERVER) && $_SERVER[\'HTTPS\'] == "on"?1:0)));
            		return $this->display(__FILE__, \'html.tpl\');
                }
        	}
            '; 
       }
       return $functions;
    }
    
	function hookRightColumn($params){
		if (Configuration::get('hbp_rightColumn')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('rightColumn',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
            
    		return $this->display(__FILE__, 'html.tpl');
        }
	}	 
    
	function hookleftColumn($params){
		if (Configuration::get('hbp_leftColumn')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('leftColumn',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); 
            $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
    
	function hookhome($params){
		if (Configuration::get('hbp_home')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('home',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
    
	function hookfooter($params){
		if (Configuration::get('hbp_footer')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('footer',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	} 
	 
	function hookheader($params){
		if (Configuration::get('hbp_header')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('header',1,$this->context->cookie->id_lang);
    	    global $smarty;
            $smarty->assign('customer_popup', $this->context->cookie);
    	    $smarty->assign(array('blocks' => $blocks));
            $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}

	function hooktop($params){
		if (Configuration::get('hbp_top')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('top',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	} 
	
	function hookextraLeft($params){
		if (Configuration::get('hbp_extraLeft')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('extraLeft',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}

	function hookextraRight($params){
		if (Configuration::get('hbp_extraRight')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('extraRight',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
		
	function hookproductActions($params){
		if (Configuration::get('hbp_productActions')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('productActions',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}	

	function hookproductOutOfStock($params){
		if (Configuration::get('hbp_productOurOfStock')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('productOutOfStock',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}	

	function hookproductfooter($params){
		if (Configuration::get('hbp_productfooter')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('productfooter',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}

	function hookproductTab($params){
		if (Configuration::get('hbp_productTab')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('productTab',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}	

	function hookproductTabContent($params){
		if (Configuration::get('hbp_productTabContent')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('productTabContent',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}

	function hookextraCarrier($params){
		if (Configuration::get('hbp_extraCarrier')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('extraCarrier',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookpayment($params){
		if (Configuration::get('hbp_payment')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('payment',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookpaymentReturn($params){
		if (Configuration::get('hbp_paymentReturn')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('paymentReturn',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookmyAccountBlock($params){
		if (Configuration::get('hbp_myAccountBlock')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('myAccountBlock',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
		
	function hookcustomerAccount($params){
		if (Configuration::get('hbp_customerAccount')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('customerAccount',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookshoppingCart($params){
		if (Configuration::get('hbp_shoppingCart')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('shoppingCart',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookshoppingCartExtra($params){
		if (Configuration::get('hbp_shoppingCartExtra')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('shoppingCartExtra',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
            
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookcreateAccountTop($params){
		if (Configuration::get('hbp_createAccountTop')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('createAccountTop',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}		
	
	function hookcreateAccountForm($params){
		if (Configuration::get('hbp_createAccountForm')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('createAccountForm',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}

	function hookbeforeCarrier($params){
		if (Configuration::get('hbp_beforeCarrier')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('beforeCarrier',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
	
	function hookpaymentTop($params){
		if (Configuration::get('hbp_paymentTop')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}  
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('paymentTop',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
    
    function hookOrderConfirmation($params){
		if (Configuration::get('hbp_orderConfirmation')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('orderConfirmation',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}
    function hookdisplayTopColumn($params){
		if (Configuration::get('hbp_displayTopColumn')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('displayTopColumn',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}    
    function hookdisplayNav($params){
		if (Configuration::get('hbp_displayNav')==1){
            if ($this->psversion()==4){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==3){global $cookie; $this->context->cookie=$cookie;}
            if ($this->psversion()==2){global $cookie; $this->context->cookie=$cookie;}
            $blocks=$this->get_blocks('displayNav',1,$this->context->cookie->id_lang);
    	    global $smarty;
    	    $smarty->assign(array('blocks' => $blocks)); $smarty->assign(array( 'is_https' => (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on"?1:0)));
    		return $this->display(__FILE__, 'html.tpl');
        }
	}        
    
    
    
    
    
    
/** CUSTOM HOOKS **/

//~~explode~~//
//~~explode~~//

}

class htmlboxproUpdate extends htmlboxpro {  
    public static function version($version){
        $version=(int)str_replace(".","",$version);
        if (strlen($version)==3){$version=(int)$version."0";}
        if (strlen($version)==2){$version=(int)$version."00";}
        if (strlen($version)==1){$version=(int)$version."000";}
        if (strlen($version)==0){$version=(int)$version."0000";}
        return (int)$version;
    }
    
    public static function encrypt($string){
        return base64_encode($string);
    }
    
    public static function verify($module,$key,$version){
        if (ini_get("allow_url_fopen")) {
             if (function_exists("file_get_contents")){
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module='.$module."&version=".self::encrypt($version)."&lic=$key&u=".self::encrypt(_PS_BASE_URL_.__PS_BASE_URI__));
             }
        }
        Configuration::updateValue("update_".$module,date("U"));
        Configuration::updateValue("updatev_".$module,$actual_version); 
        return $actual_version;
    }
}

?>