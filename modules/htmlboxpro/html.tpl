{foreach from=$blocks item=block}
    {assign var='hideblock' value=0}
    {if isset($block.homeonly)}{if $block.homeonly==1}{if $page_name!='index'}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.date)}
        {if $block.date==1}
            {if $smarty.now|date_format:"%Y/%m/%d" < $block.datefrom}
                {assign var='hideblock' value=1}
            {/if}
            {if $smarty.now|date_format:"%Y/%m/%d" > $block.dateto}
                {assign var='hideblock' value=1}
            {/if}
        {/if}
    {/if}
    {if isset($block.bssl)}{if $block.bssl==1}{if $is_https!=1}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.logged)}{if $block.logged==1}{if !$logged}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.logged)}{if $block.logged==2}{if $logged}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {* {if $block.productsonly==1}{if isset($smarty.get['id_product']) && $page_name=='product'}{if !preg_match("/{$smarty.get['id_product']}/",$block.selectedproducts)}{assign var='hideblock' value=1}{/if}{/if}{/if} *}
    {if isset($block.productsonly)}{if $block.productsonly==1}{if isset($smarty.get.id_product) && $page_name=='product'}{if !in_array($smarty.get.id_product, $block.selectedproducts)}{assign var='hideblock' value=1}{/if}{else}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.catsonly)}{if $block.catsonly==1}{if isset($smarty.get.id_category) && $page_name=='category'}{if !in_array($smarty.get.id_category, $block.selected_cats)}{assign var='hideblock' value=1}{/if}{else}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.cmsonly)}{if $block.cmsonly==1}{if isset($smarty.get.id_cms) && $page_name=='cms'}{if !in_array($smarty.get.id_cms, $block.selectedcms)}{assign var='hideblock' value=1}{/if}{else}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.manufsonly)}{if $block.manufsonly==1}{if isset($smarty.get.id_manufacturer) && $page_name=='manufacturer'}{if !in_array($smarty.get.id_manufacturer, $block.selected_manufs)}{assign var='hideblock' value=1}{/if}{else}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.urlonly)}{if $block.urlonly==1}{if htmlboxpro::currentPageURL()!=$block.url}{assign var='hideblock' value=1}{/if}{/if}{/if}
    {if isset($block.productscat)}
        {if $block.productscat==1}
            {if isset($smarty.get.id_product)}
                {assign var='associated' value=0}                
                {foreach Product::getProductCategories($smarty.get.id_product) as $category}
                    {if in_array($category, $block.selected_pcats)}
                        {assign var='associated' value=1}
                    {/if}
                {/foreach}
                {if $associated==0}
                    {assign var='hideblock' value=1}
                {/if}
            {else}
                {assign var='hideblock' value=1}
            {/if}
        {/if}
    {/if}
    {if isset($block.cgroup)}
        {if $block.cgroup!=0}
            {if !$logged}{assign var='hideblock' value=1}{/if}
            {if $logged}
                {assign var='associated_category' value=0}
                {foreach Customer::getGroupsStatic($customer_popup->id_customer) AS $customer_group}
                    {if $customer_group == $block.cgroup}
                        {assign var='associated_category' value=1}
                    {/if}
                {/foreach}
                {if $associated_category==0}
                    {assign var='hideblock' value=1}
                {/if}
            {/if}
        {/if}
    {/if}  
    {if isset($block.hcgroup)}
        {if $block.hcgroup!=0}
            {if !$logged}{assign var='hideblock' value=1}{/if}
            {if $logged}
                {assign var='associated_group' value=0}
                {foreach Customer::getGroupsStatic($customer_popup->id_customer) AS $customer_group}
                    {if $customer_group == $block.hcgroup}
                        {assign var='associated_group' value=1}
                    {/if}
                {/foreach}
                {if $associated_group==1}
                    {assign var='hideblock' value=1}
                {/if}
            {/if}
        {/if}
    {/if}    
    
    {if $hideblock!=1}
        {$block.body|stripslashes}
    {/if}
    {*
        {if $block.bssl==1 && $is_https==1}
            {if $block.logged==0 && $is_https==1}
                {$block.body|stripslashes}
            {/if}
            {if $block.logged==1 && $is_https==1}
                {if $logged}
                    {$block.body|stripslashes}
                {/if}
            {/if}
            {if $block.logged==2 && $is_https==1}
                {if !$logged}
                    {$block.body|stripslashes}
                {/if}
            {/if}
        {/if}
    
        {if $block.bssl==0}
                {if $block.logged==0}
                    {$block.body|stripslashes}
                {/if}
                {if $block.logged==1}
                    {if $logged}
                        {$block.body|stripslashes}
                    {/if}
                {/if}
                {if $block.logged==2}
                    {if !$logged}
                        {$block.body|stripslashes}
                    {/if}
                {/if}
        {/if}
    *}
    {assign var='hideblock' value=0}
{/foreach}