<?php
global $_MODULE;
$_MODULE = array();
$_MODULE['<{pscookiebanner}prestashop>pscookiebanner_7cf649fdc395a3013ea2b3bce432eba1'] = 'Cookie-Banner';
$_MODULE['<{pscookiebanner}prestashop>pscookiebanner_ec42bfb3e1220b5a131121d47dd421a1'] = 'Mit diesem Modul können Sie auf Ihrer Webseite ein Banner anzeigen';
$_MODULE['<{pscookiebanner}prestashop>pscookiebanner_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Sind Sie sicher, dass Sie dieses Modul deinstallieren wollen?';
$_MODULE['<{pscookiebanner}prestashop>pscookiebanner_d31973a74cd470ab2a778ff8bebbabe7'] = 'Bei der Installation ist ein Fehler aufgetreten. Bitte setzen Sie sich mit uns über die Addons-Website in Verbindung';
$_MODULE['<{pscookiebanner}prestashop>pscookiebanner_bfda8a090ebad983aa1084ef787a51c2'] = 'Aktualisierung erfolgreich!';
$_MODULE['<{pscookiebanner}prestashop>configuration_255b1f5de0fec56536debe558fb64173'] = 'Ihren Buy-Later-Block individuell gestalten';
$_MODULE['<{pscookiebanner}prestashop>configuration_f0459059b5aace5ffb2ad36dbff95163'] = 'Eine neue Seite zu Cookie-Richtlinien erstellen';
$_MODULE['<{pscookiebanner}prestashop>configuration_b8ac1082decab2250d0d1a856f08272d'] = 'Noch ehe Sie Ihr Cookie-Banner konfigurieren, sollten Sie sicherstellen, dass Sie Ihre Seite zu Cookie-Richtlinien erstellt haben';
$_MODULE['<{pscookiebanner}prestashop>configuration_ed2f0ac904f2f49802855ad45f25b69d'] = 'Klicken Sie hier, um eine neue Seite zu Cookie-Richtlinien auf Ihrer Website zu erstellen und den Inhalt individuell zu gestalten,';
$_MODULE['<{pscookiebanner}prestashop>configuration_a4ea91cd0d1ea33d8113457644dd6157'] = 'Klicken Sie bitte hier';
$_MODULE['<{pscookiebanner}prestashop>configuration_e39581453fb7b3cb116f1e5d930be247'] = 'Hinsichtlich des Inhalts dieser Seite müssen Sie den geltenden Bestimmungen zu E-Commerce entsprechen, einschließlich der Gesetzgebung zu persönlichen Angaben und Cookies. Klicken Sie hier, um einige hilfreiche Links zur Verwaltung von Cookies von verschiedenen Plattformen aus anzuzeigen.';
$_MODULE['<{pscookiebanner}prestashop>configuration_936ccdb97115e9f35a11d35e3d5b5cad'] = 'Klicken Sie bitte hier';
$_MODULE['<{pscookiebanner}prestashop>configuration_c88efb2ef057cbcbe5c36434e3591a3f'] = 'Ein neues Cookie-Banner erstellen';
$_MODULE['<{pscookiebanner}prestashop>configuration_531fb79b0cbcecd45859f576ed2d79d2'] = 'Banner-Position';
$_MODULE['<{pscookiebanner}prestashop>configuration_a4ffdcf0dc1f31b9acaf295d75b51d00'] = 'Oben ';
$_MODULE['<{pscookiebanner}prestashop>configuration_2ad9d63b69c4a10a5cc9cad923133bc4'] = 'Unten ';
$_MODULE['<{pscookiebanner}prestashop>configuration_5968e3c37145f57587afcba727d2b120'] = 'Unten rechts';
$_MODULE['<{pscookiebanner}prestashop>configuration_b67554192c50f4f6a4c1bd4797f6c3f8'] = 'Unten links';
$_MODULE['<{pscookiebanner}prestashop>configuration_2a633cd4057ceb8d4fd6d3215e0d826e'] = 'Wählen Sie die Hintergrundfarbe Ihres Cookie-Banners aus';
$_MODULE['<{pscookiebanner}prestashop>configuration_368d9ac76af05f714092bc808a426bfc'] = 'Hintergrundfarbe';
$_MODULE['<{pscookiebanner}prestashop>configuration_bad6a5dd8c28e6b14f8e986615e3dc98'] = 'Opazität';
$_MODULE['<{pscookiebanner}prestashop>configuration_a1c983fc4959f3a8efe6a5d7c0d7ef24'] = 'Stellen Sie die Deckkraft Ihres Banners ein';
$_MODULE['<{pscookiebanner}prestashop>configuration_231efdc72b5e3bea133d54f71007fd3c'] = 'Text hinzufügen';
$_MODULE['<{pscookiebanner}prestashop>configuration_734fd458ea564fee65504f31d72f66c8'] = 'Empfohlene maximale Zeichenanzahl insgesamt: 300 Für beste Anzeigequalität auf dem Desktop und Smartphone';
$_MODULE['<{pscookiebanner}prestashop>configuration_fdc87ea4599daad972040a480dfa73fb'] = 'Schriftart';
$_MODULE['<{pscookiebanner}prestashop>configuration_5f111ae4c490902059da2004cbc8b424'] = 'Textfarbe';
$_MODULE['<{pscookiebanner}prestashop>configuration_c4943062b634c56348c67fdebba808eb'] = 'Textgröße';
$_MODULE['<{pscookiebanner}prestashop>configuration_10abd52ce0bf953429c3c92b691bc7c4'] = 'Einzelheiten zum Linktext';
$_MODULE['<{pscookiebanner}prestashop>configuration_1f79616810c21a23a470ea53d0fcda90'] = 'Weitere Informationen hier';
$_MODULE['<{pscookiebanner}prestashop>configuration_4c823adc3a5ba07558f5270922992dd2'] = 'Verknüpfungsfarbe';
$_MODULE['<{pscookiebanner}prestashop>configuration_ec0dcff6590b15feae023a8a77a6de49'] = 'Einzelheiten zur CMS-Seite';
$_MODULE['<{pscookiebanner}prestashop>configuration_5c0aec85564ac7f41ab0a3c46a7a429f'] = 'Text der Schaltfläche „Akzeptieren“';
$_MODULE['<{pscookiebanner}prestashop>configuration_320620f678cb15ebf7a577abd04427f3'] = 'Zustimmen';
$_MODULE['<{pscookiebanner}prestashop>configuration_0c251cba6906ea207e512bf5c546edca'] = 'Schaltfläche Hintergrundfarbe';
$_MODULE['<{pscookiebanner}prestashop>configuration_33251f15c45eafebef656b1639283aee'] = 'Diese Farbe wird sichtbar, wenn man mit der Maus darüber fährt';
$_MODULE['<{pscookiebanner}prestashop>configuration_990ee0c36ae303e39619d12562e31a29'] = 'Wenn man mit der Maus darüber fährt';
$_MODULE['<{pscookiebanner}prestashop>configuration_7ff6c740f697bf3053b9b4bc61090ed0'] = 'Farbe wird sichtbar, wenn man mit der Maus darüber fährt';
$_MODULE['<{pscookiebanner}prestashop>configuration_90c599b1c29354c8d3cac24beb2c9c26'] = 'Schaltfläche Textfarbe';
$_MODULE['<{pscookiebanner}prestashop>configuration_5e54238b4ccaedbf46f5688774e59bd2'] = 'Redisplay Cookie Banner alle';
$_MODULE['<{pscookiebanner}prestashop>configuration_da36cfaf48b9e19896e23e1207040d1e'] = 'Monate';
$_MODULE['<{pscookiebanner}prestashop>configuration_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{pscookiebanner}prestashop>help_1fe917b01f9a3f87fa2d7d3b7643fac1'] = 'FAQ';
$_MODULE['<{pscookiebanner}prestashop>help_6079bf09f9efb4e1bf3ccf0b5dad88da'] = 'Sie benötigen Hilfe?';
$_MODULE['<{pscookiebanner}prestashop>help_92e72eef5706b51bcc4b656ad11c69bd'] = 'Sie finden Sie Dokumentation des Moduls hier ';
$_MODULE['<{pscookiebanner}prestashop>help_5b6cf869265c13af8566f192b4ab3d2a'] = 'Dokumentation';
$_MODULE['<{pscookiebanner}prestashop>help_929028b121a4e0d4758509691bf72eda'] = 'Sie können uns auch kontaktieren ';
$_MODULE['<{pscookiebanner}prestashop>help_164b7380deb0d897551d0c89c88de883'] = 'PrestaShop Addons';
$_MODULE['<{pscookiebanner}prestashop>configure_254f642527b45bc260048e30704edb39'] = 'Konfiguration';
$_MODULE['<{pscookiebanner}prestashop>configure_da22c93ccb398c72070f4000cc7b59a1'] = 'Individuelle Einstellungen';
$_MODULE['<{pscookiebanner}prestashop>configure_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Hilfe';
$_MODULE['<{pscookiebanner}prestashop>configure_f0816a194dffacd8b32b4f6e17c0b345'] = 'Vorschaumodus aktivieren';
$_MODULE['<{pscookiebanner}prestashop>configure_34b6cd75171affba6957e308dcbd92be'] = 'Version';
