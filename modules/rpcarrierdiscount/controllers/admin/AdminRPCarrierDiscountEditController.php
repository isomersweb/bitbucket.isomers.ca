<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

require_once(dirname(__FILE__).'/../../classes/RPCarrierDiscounts.php');

class AdminRPCarrierDiscountEditController extends AdminController
{
    public function __construct()
    {
        $context = Context::getContext();
        $this->table = 'rpcarrierdiscount';
        $this->className = 'RPCarrierDiscounts';
        $this->lang = true;

        $this->carrier_array_results =
            Carrier::getCarriers((int)$context->language->id, null, null, null, null, Carrier::ALL_CARRIERS);
        foreach ($this->carrier_array_results as $value) {
            $this->carrier_array[$value['id_carrier']] = $value['name'];
        }

        $this->shop_array_results = Shop::getShops();
        foreach ($this->shop_array_results as $value) {
            $this->shop_array[$value['id_shop']] = $value['name'];
        }

        $this->fields_value = array('size' => 10);
        $this->fields_list = array(
            'id_rpcarrierdiscount' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 25
            ),
            'shop' => array(
                'title' => $this->l('Shop'),
                'width' => 'auto',
                'type' => 'select',
                'filter_key' => 'a!id_shop',
                'list' => $this->shop_array
            ),
            'min_order' => array(
                'title' => $this->l('Min order'),
                'width' => 'auto',
                'filter_key' => 'min_order',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 'auto',
                'filter_key' => 'b!name',
            ),
            'carrier' => array(
                'title' => $this->l('Carrier'),
                'width' => 'auto',
                'type' => 'select',
                'filter_key' => 'cl!name',
                'list' => $this->carrier_array
            ),
            'discount' => array(
                'title' => $this->l('Discount'),
                'width' => 'auto'
            ),
            'active' => array(
                'title' => $this->l('Active'),
                'active' => 'status',
                'align' => 'center',
                'type' => 'bool',
                'width' => 70,
                'orderby' => false
            )
        );

        $this->bootstrap = (version_compare(_PS_VERSION_, 1.6) >= 0) ? true : false;

        if (count(Shop::getShops()) <= 1) {
            unset($this->fields_list['shop']);
        }

        parent::__construct();
    }

    public function renderList()
    {
        // Adds an Edit button for each result
        $this->addRowAction('edit');
        // Adds a Delete button for each result
        $this->addRowAction('delete');

        $this->_select = '
cl.name as carrier,
if (
    a.`apply_discount` = 1,
    CONCAT(a.`reduction_percent`, \' %\'),
    if (
        a.`apply_discount` = 2,
        CONCAT(
            a.`reduction_amount`,
            \' \',
            cur.`sign`
        ),
        if (
            a.`apply_discount` = 3,
            CONCAT(a.`reduction_percent`, \' % from order\'),
            \'--\'
        )
    )
) as discount,
IFNULL(s.name, \''.$this->l('All').'\') AS shop ';
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'carrier` cl ON (a.`id_carrier` = cl.`id_carrier`)
                        LEFT JOIN `'._DB_PREFIX_.'currency` cur ON (a.`reduction_currency` = cur.`id_currency`)
                        LEFT JOIN `'._DB_PREFIX_.'shop` s ON (s.id_shop = a.id_shop)
                        ';
        return parent::renderList();
    }

    public function renderForm()
    {
        /*$helper = new Helper();*/
        $banner_positions = array();
        for ($i = 1; $i <= Configuration::get('RPBANNERSLIDER_BANNER'); $i++) {
            $banner_positions[$i] = array('id_option' => $i, 'name' => $i);
        }

        $shops = Shop::getShops();
        $currency_def = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

        $options_currency = array();
        foreach (Currency::getCurrencies() as $currency) {
            /*{*/
            $options_currency[] = array(
                'value' => $currency['id_currency'],
                'text' => $currency['iso_code'],
            );
        }
            /*$isSelected = ($this->object->reduction_currency == $currency['id_currency']);*/
        /*}*/

        $options_discount_types = array(
            array('value' => '0', 'text' => 'None'),
            array('value' => '1', 'text' => 'Percentage (%)'),
            array('value' => '2', 'text' => 'Amount'),
            array('value' => '3', 'text' => 'Percentage from order (%)'),
        );

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Carrier discount')
            ),
            'input' => array(
                array(
                    'type'  => 'select',
                    'label' => $this->l('Carrier'),
                    'name'  => 'id_carrier',
                    'options' => array(
                        'query' => Carrier::getCarriers(
                            (int)$this->context->language->id,
                            null,
                            null,
                            null,
                            null,
                            Carrier::ALL_CARRIERS
                        ),
                        'id'    => 'id_carrier',
                        'name'  => 'name',
                    ),
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Name:'),
                    'name' => 'name',
                    'lang' => true,
                    'required' => true,
                    'hint' => $this->l('Illegal symbols:').' <>;=#{}',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Min order:'),
                    'name' => 'min_order',
                    'required' => true,
                    'desc' => $this->l('Currency: ').$currency_def->sign,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Discount type'),
                    'name' => 'apply_discount',
                    'options' => array(
                        'query' => $options_discount_types,
                        'id'    => 'value',
                        'name'  => 'text',
                    ),
                ),
                array(
                    'type'     => 'text',
                    'label'    => $this->l('Reduction percentage'),
                    'name'     => 'reduction_percent',
                    'suffix'   => '%',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Reduction amount'),
                    'name' => 'reduction_amount',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Reduction currency'),
                    'name' => 'reduction_currency',
                    'options' => array(
                        'query' => $options_currency,
                        'id'    => 'value',
                        'name'  => 'text',
                    ),
                ),
                array(
                    'type' => $this->bootstrap ? 'switch' : 'radio',
                    'label' => $this->l('Active:'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array('id' => 'active_on', 'value' => 1, 'label' => $this->l('Yes')),
                        array('id' => 'active_off','value' => 0, 'label' => $this->l('No') ),
                    ),
                ),
                array(
                    'type' => 'free',
                    'name' => 'discount_js'
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => (_PS_VERSION_ < 1.6 ? 'button' : null)
            )
        );

        $this->object->discount_js = Context::getContext()->smarty->fetch(
            _PS_MODULE_DIR_. 'rpcarrierdiscount/views/templates/admin/discount_js.tpl'
        );

        /*$shops = Shop::getShops();*/

        if (count($shops) > 1) {
            $this->fields_form['input'][] = array(
                'type' => 'select',
                'label' => $this->l('Shop:'),
                'name' => 'id_shop',
                'options' => array(
                    'query' => array_merge(array(array('id_shop' => '0', 'name' => $this->l('All'))), $shops),
                    'id' => 'id_shop',
                    'name' => 'name'
                )
            );
            if ($this->object->id == null) {
                if ($this->context->shop->getContextShopID() == null) {
                    $this->object->id_shop = 0;
                } else {
                    $this->object->id_shop = $this->context->shop->getContextShopID();
                }
            }
        }

        return parent::renderForm();
    }
}
