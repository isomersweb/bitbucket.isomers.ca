# RPCarrierDiscount #

Additional carrier (shipping) discount based on order amount.

## Things to do before publishing module .zip archive ##
* Remove .git folder if you zipped up local repository
* Remove .gitattributes and .gitignore files
