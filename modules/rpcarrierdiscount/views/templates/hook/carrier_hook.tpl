{*
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 *}

<script src="{$module_uri|escape:'htmlall':'UTF-8'}/views/js/rpcarrierdiscount.js"></script>
<script>
	$(document).ready(function(){
		if (typeof rpcarrierdiscount.discounts == "undefined")
		{
			rpcarrierdiscount.discounts = new Array();
			{foreach $carrier_discounts as $carrier_discount}
			
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}] = new Object();
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].id_carrier = {$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'};
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].name = '{$carrier_discount['name']|escape:'htmlall':'UTF-8'}';
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].apply_discount = {$carrier_discount['apply_discount']|escape:'htmlall':'UTF-8'};
			/*rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].reduction_tax = {* $carrier_discount['reduction_tax'] *}; */
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].reduction_currency = {$carrier_discount['reduction_currency']|escape:'htmlall':'UTF-8'};
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].reduction_amount = {$carrier_discount['reduction_amount']|escape:'htmlall':'UTF-8'};
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].reduction_amount_converted = {$carrier_discount['reduction_amount_converted']|escape:'htmlall':'UTF-8'};
			rpcarrierdiscount.discounts[{$carrier_discount['id_carrier']|escape:'htmlall':'UTF-8'}].reduction_percent = {$carrier_discount['reduction_percent']|escape:'htmlall':'UTF-8'};
			{/foreach}
	
			rpcarrierdiscount.aditional_text = " {if $use_taxes == 1}{if $priceDisplay == 1}{l s='(tax excl.)' mod='rpcarrierdiscount'}{else}{l s='(tax incl.)' mod='rpcarrierdiscount'}{/if}{else}{l s='(tax excl.)' mod='rpcarrierdiscount'}{/if}";
			rpcarrierdiscount.sign = "{$currency_sign|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.format = "{$currency_format|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.decimals = "{$currency_decimals|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.blank = "{$currency_blank|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.is_rtl = "{$currency_is_rtl|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.round_mode = "{$currency_round_mode|escape:'htmlall':'UTF-8'}";
			rpcarrierdiscount.totalProducts = {$totalProducts|escape:'htmlall':'UTF-8'};
			
			rpcarrierdiscount.reWriteDiscounts();
			
			rpcarrierdiscount.ajax_link = "{$update_link|escape:'htmlall':'UTF-8'}";

			$('input:radio').change(
				function(){
					rpcarrierdiscount.updateCartList();
				}
			);
		}
	});
</script>