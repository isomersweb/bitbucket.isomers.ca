<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

class RPCarrierDiscountsCart extends ObjectModel
{

    public $id_cart;
    public $id_cart_rule;
    public $id_carrier;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rpcarrierdiscount_cart',
        'primary' => 'id_rpcarrierdiscount_cart',
        'fields' => array(
            'id_cart' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'copy_post' => false),
            'id_cart_rule' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'copy_post' => false),
            'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'copy_post' => false),
        )
    );

    public static function getIdByCartId($id_cart)
    {
        return (int)Db::getInstance()->getValue(
            'SELECT id_rpcarrierdiscount_cart
            FROM `'._DB_PREFIX_.'rpcarrierdiscount_cart`
            WHERE `id_cart` = '.(int)$id_cart
        );
    }

    public static function getIdCartRule($id_cart)
    {
        return (int)Db::getInstance()->getValue(
            'SELECT id_cart_rule
            FROM `'._DB_PREFIX_.'rpcarrierdiscount_cart`
            WHERE `id_cart` = '.(int)$id_cart
        );
    }

    public static function getIdByCartRule($id_cart_rule)
    {
        return (int)Db::getInstance()->getValue(
            'SELECT id_rpcarrierdiscount_cart
            FROM `'._DB_PREFIX_.'rpcarrierdiscount_cart`
            WHERE `id_cart_rule` = '.(int)$id_cart_rule
        );
    }
}
