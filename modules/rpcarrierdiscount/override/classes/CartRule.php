<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

class CartRule extends CartRuleCore
{
    public $rpcarrierdiscount_cart = 0;

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        self::$definition['fields']['rpcarrierdiscount_cart'] =
            array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1);
        parent::__construct($id, $id_lang, $id_shop);

    }
}
