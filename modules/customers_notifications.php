<?php
/*
 Check if SALES TEAM Leader can earn cash back commission on own and referral purchases on orders(combined) greater than or equal to $500.00 within a 6 month period          
*/

define('PRESTASHOP_INTEGRATION_VERSION', true);
define ('PS_DIR', __DIR__ . '/../');
//define('_PS_MODE_DEV_', true);
require_once PS_DIR .'/config/config.inc.php';
require_once _PS_MODULE_DIR_ .'/allinone_rewards/allinone_rewards.php';
if (!defined('_PS_VERSION_'))
	exit;
        
$context = Context::getContext();
$context->language = new Language((int)$context->cookie->id_lang);        
$lang = $context->cookie->id_lang;
$rewards = Module::getInstanceByName('allinone_rewards');

$id_template = 3;
$st_cancel_time = (int)MyConf::get('RLOYALTY_SALESACTIVEDURATION', null, $id_template);
$st_email_time = (int)MyConf::get('RLOYALTY_SALESEMAILDURATION', null, $id_template);
$st_amount = (int)MyConf::get('RLOYALTY_SALESACTIVESALESAMOUNT', null, $id_template);
$st_members = (int)MyConf::get('RLOYALTY_SALESACTIVEMINTREEMEMBERS', null, $id_template);
$st_renew_duration = (int)MyConf::get('RLOYALTY_SALESRENEWDURATION', null, $id_template);
$id_template = 4;
$rf_cancel_time = (int)MyConf::get('RLOYALTY_REFERAFRIENDACTIVEDURATION', null, $id_template);
$rf_email_time = (int)MyConf::get('RLOYALTY_REFERAFRIENDEMAILDURATION', null, $id_template);
$rf_amount = (int)MyConf::get('RLOYALTY_REFERAFRIENDACTIVESALESAMOUNT', null, $id_template);
$rf_members = (int)MyConf::get('RLOYALTY_REFERAFRIENDACTIVEMINTREEMEMBERS', null, $id_template);
$rf_renew_duration = (int)MyConf::get('RLOYALTY_REFERAFRIENDRENEWDURATION', null, $id_template);

/*
$subject = $rewards->l('Sales Team Notification', (int)$lang);
$template = 'sales-team-attention';
$data = array(
	'{customer_firstname}' => $customer->firstname,
	'{customer_lastname}' => $customer->lastname,
);
$rewards->sendMail($lang, $template, $subject, $data, $customer->email, $customer->firstname.' '.$customer->lastname);
*/
$customers = Customer::getCustomers(true);
foreach ($customers as $val) {    
    $renew_duration = 0;
    $customer = new Customer($val['id_customer']);
    if($customer->id_default_group == 5 || $customer->id_default_group == 7){
        if(!$sponsor_ids = Db::getInstance()->getValue('SELECT GROUP_CONCAT(CONCAT(`id_sponsor`, \',\' ,`id_customer`)) AS sponsors FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE id_sponsor = '. $customer->id .' AND id_customer != 0 GROUP BY \'ALL\'')) {
            $sponsor_ids = $customer->id;
        }
        $sql = 'SELECT (SUM(o.total_paid_tax_excl) - SUM(o.total_discounts_tax_excl) - SUM(o.total_shipping_tax_excl)) AS sum, 
                (SELECT date_add FROM `'._DB_PREFIX_.'orders` WHERE id_customer = '. $customer->id .' ORDER BY id_order DESC LIMIT 1) as date_add, 
                (SELECT COUNT(*) FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE id_sponsor ='. $customer->id .'  AND id_customer !=0) as tree_members
            FROM `'._DB_PREFIX_.'orders` o
            WHERE o.id_customer IN ('. $sponsor_ids .') 
            AND o.date_add > NOW() - INTERVAL '.$st_cancel_time.' DAY
            AND o.current_state NOT IN (6, 7, 19)
            ';
        $result = Db::getInstance()->getRow($sql);
        if (empty($result['date_add'])) continue;
    
        if($customer->id_default_group == 5){
            if (((int)$result['sum'] < $st_amount || (int)$result['sum'] == 0) || 
                ((int)$result['tree_members'] < $st_members || (int)$result['tree_members'] == 0)) {
echo "ST: $customer->id <br>";
//p($result);
//continue;
                $subject = $rewards->l('Sales Team Notification', (int)$lang);
                $data = array(
        			'{customer_firstname}' => $customer->firstname,
        			'{customer_lastname}' => $customer->lastname,
                );
        
                if (strtotime($result['date_add']) < (time()-(60*60*24*$st_email_time)) && $customer->st_send_email == 0) {
                    $template = 'sales-team-attention';
                    
                    $rewards->sendMail($lang, $template, $subject, $data, $customer->email, $customer->firstname.' '.$customer->lastname);
                    $customer->st_send_email = 1;
                    $customer->update();
                }
                elseif (strtotime($result['date_add']) < (time()-(60*60*24*$st_cancel_time))) {
                    $template = 'sales-team-cancel';
                    
                    $rewards->sendMail($lang, $template, $subject, $data, $customer->email, $customer->firstname.' '.$customer->lastname);
                    $customer->date_st_expire = date("Y-m-d H:i:s");
                    $customer->id_default_group = 3;
                    $customer->st_send_email = 0;
                    $customer->update();
                    cancelRewards($customer);
                    Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE `id_sponsor` = '. (int)$customer->id);
                }
            }
            $renew_duration = $st_renew_duration;
        }
        else if ($customer->id_default_group == 7) {
            /*if(!$sponsor_ids = Db::getInstance()->getValue('SELECT GROUP_CONCAT(CONCAT(`id_sponsor`, \',\' ,`id_customer`)) AS sponsors FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE id_sponsor = '. $customer->id .' AND id_customer != 0 GROUP BY \'ALL\'')) {
                $sponsor_ids = $customer->id;
            }
            $sql = ' SELECT COUNT(*) as count, 
                    (SELECT date_add FROM `'._DB_PREFIX_.'orders` WHERE id_customer = '. $customer->id .' ORDER BY id_order DESC LIMIT 1) as date_add
                FROM `'._DB_PREFIX_.'orders`
                WHERE id_customer = '. $customer->id .'
                AND date_add > NOW() - INTERVAL '.$rf_cancel_time.' DAY
                AND current_state NOT IN (6, 7, 19)
                ORDER BY id_order DESC
                ';
            $result = Db::getInstance()->getRow($sql);
            if (empty($result['date_add'])) continue;
    */
            if (((int)$result['sum'] < $rf_amount || (int)$result['sum'] == 0) || 
                ((int)$result['tree_members'] < $rf_members || (int)$result['tree_members'] == 0)) {
echo "RF: $customer->id <br>";
//p($result);
//continue;            
                $subject = $rewards->l('Refer a Friend Notification', (int)$lang);;
                $data = array(
        			'{customer_firstname}' => $customer->firstname,
        			'{customer_lastname}' => $customer->lastname,
                );
        
                if (strtotime($result['date_add']) < (time()-(60*60*24*$rf_email_time)) && $customer->st_send_email == 0) {
                    $template = 'refer-friend-attention';
                    
                    $rewards->sendMail($lang, $template, $subject, $data, $customer->email, $customer->firstname.' '.$customer->lastname);
                    $customer->st_send_email = 1;
                    $customer->update();
                }
                elseif (strtotime($result['date_add']) < (time()-(60*60*24*$rf_cancel_time))) {
                    $template = 'refer-friend-cancel';
                    
                    $rewards->sendMail($lang, $template, $subject, $data, $customer->email, $customer->firstname.' '.$customer->lastname);
                    $customer->date_st_expire = date("Y-m-d H:i:s");
                    $customer->id_default_group = 3;
                    $customer->st_send_email = 0;
                    $customer->update();
                    cancelRewards($customer);
                    Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE `id_sponsor` = '. (int)$customer->id);
                }
            }
            $renew_duration = $rf_renew_duration;
        }
    }
    
    if ($customer->id_default_group == 3 && ((int)$customer->date_st_expire != 0) && (strtotime($customer->date_st_expire) < (time()-(60*60*24*$renew_duration)))) {
                $customer->date_st_expire = NULL;
                $customer->st_send_email = 0;
                $customer->update();         
    }
}

d('OK');

function cancelRewards($customer = null) {
    if ($customer == null) return;
    
    $items = RewardsModel::getAllByIdCustomer((int)$customer->id, false, true);
	foreach($items AS $item) {
        $r = new RewardsModel((int)$item['id_reward']);
		$r->id_reward_state = (int)RewardsStateModel::getCancelId();
		$r->save();
	}
    return;
}    