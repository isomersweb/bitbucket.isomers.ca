<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class AdminOrdersReportController extends ModuleAdminController
{

    private $print_report = false;
    private $export_csv = false;
    private $multistore = false;
    private $order_str = '';

    private $action_bulk_inv = 'createInvoices';
    private $bulk_inv = false;

    private $addr_name = false;
    private $addr_company = false;
    private $addr_country = false;
    private $carrier = false;
    private $payment = false;
    private $show_curr = false;
    private $delivery_date = false;
    private $invoice_num = false;

    private $g_bIfPost_OrderStateFilter = false;
    private $g_bSave_OrderStateFilter = false;
    private $g_aOrderStateCount = array();
    private $g_aOrderStateFilter = array();
    private $g_sCfg_OrderStateFilter = '';
    private $psver17 = false;
    //private $list_no_link = true;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->explicitSelect = true;
        $this->table = 'order';
        $this->allow_export = true;
        $this->_orderBy = 'id_order';
        $this->_orderWay = 'DESC';
        $this->className = 'Order';

        $this->currencies = array();
        $this->order_states = array();

        require_once(_PS_MODULE_DIR_.'ordersreport/classes/IRFunctions.php');
        $this->functions = new IRFunctions();

        $this->shopLinkType = 'shop';
        $this->shopShareDatas = Shop::SHARE_ORDER;
        $this->multistore = $this->multishop_context && Shop::isFeatureActive();

        $this->addr_name = Configuration::get('DNG_ORDER_DELIVERY_NAME');
        $this->addr_company = Configuration::get('DNG_ORDER_DELIVERY_COMPANY');
        $this->addr_country = Configuration::get('DNG_ORDER_DELIVERY_COUNTRY');
        $this->payment = Configuration::get('DNG_ORDER_PAYMENT');
        $this->carrier = Configuration::get('DNG_ORDER_CARRIER');
        $this->show_curr = Configuration::get('DNG_ORDER_CURRENCY');
        $this->delivery_date = Configuration::get('DNG_ORDER_DELIVERY_DATE');
        $this->invoice_num = Configuration::get('DNG_ORDER_INV_NUM');

        /* get filters cfg */
        $this->g_sCfg_OrderStateFilter = Configuration::get('DNG_ORDREP_ORDER_STATE_FILTER');
        /* early check for submit post or get */
        if (Tools::getIsset('submit_save_orderstatefilter')) {
            $this->g_bIfPost_OrderStateFilter = true;
        }
        if (version_compare(_PS_VERSION_, '1.7', '>=') === true) {
            $this->psver17 = true;
        }
        /* check script exec timeout */
        if (!$this->bCheckScriptTimeout()) {
            $this->displayWarning($this->l('Generate report script timeout is not valid integer or not in interval from 1 to 600 !'));
        }

        $this->_select = ' a.`id_currency`, a.`id_order` AS `id_pdf`, `reference`,
            CONCAT(c.`firstname`," ",c.`lastname`) AS `customer`, a.`date_add`, os.`id_order_state`, os.`color`,
            osl.`name` AS order_state_name, a.`total_paid_tax_excl` AS `total_excl`,
            a.`total_paid_tax_incl` AS `total_incl`, CONCAT_WS(", ", ad.`address1`, ad.`city`, st.`iso_code`, con.`iso_code`, ad.`postcode`) as full_address ';

        $join_address = false;
        $join_address_sql = ' INNER JOIN '._DB_PREFIX_.'address ad ON a.`id_address_delivery` = ad.`id_address` ';
        $join_address_sql .= ' LEFT JOIN '._DB_PREFIX_.'state st ON ad.`id_state` = st.`id_state`';
        
        $this->_select .= ', (SELECT SUM(od.`product_weight` * od.`product_quantity`) FROM '._DB_PREFIX_.'order_detail od WHERE od.`id_order` = a.`id_order`) AS total_product_weight';
        if ($this->addr_name) {
            $this->_select .= ',CONCAT(ad.`firstname`," ",ad.`lastname`) AS `address_customer`';
            $join_address = true;
        }
        if ($this->addr_company) {
            $this->_select .= ',ad.`company` AS address_company';
            $join_address = true;
        }

        if ($this->payment) {
            $this->_select .= ',`payment`';
        }

        if ($this->delivery_date) {
            $this->_select .= ',a.`delivery_date`';
        }

        if ($this->invoice_num) {
            $this->_select .= ',a.`invoice_number`';
        }

        if ($this->carrier) {
            $this->_select .= ',car.`name` AS carrier';
            $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'carrier car ON a.`id_carrier` = car.`id_carrier`';
        }
        if ($join_address) {
            $this->_join .= $join_address_sql;
        }
        if ($this->addr_country) {
            if (!$join_address) {
                $this->_join .= $join_address_sql;
            }
            $this->_select .= ',con.`iso_code` AS country_code';
            $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'country con ON ad.`id_country` = con.`id_country`';
        }

        $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'order_state os ON a.`current_state` = os.`id_order_state`
                        LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON os.`id_order_state` = osl.`id_order_state`
                        LEFT JOIN '._DB_PREFIX_.'customer c ON a.`id_customer` = c.`id_customer`';

        if ($this->show_curr) {
            $this->_select .= ',cur.`id_currency`, cur.`iso_code` AS currency_code';
            $this->_join .= ' LEFT JOIN '._DB_PREFIX_.'currency cur ON a.`id_currency` = cur.`id_currency`';
            $cur_table = Db::getInstance()->executeS('SELECT `id_currency`, `iso_code` FROM '._DB_PREFIX_.'currency');
            foreach ($cur_table as $row) {
                $this->currencies[$row['id_currency']] = $row['iso_code'];
            }
        }

        // ORDER STATUS FILTER
        /* order state count */
        $sql_states_count = 'SELECT
            COUNT(os.`id_order_state`) AS `count`,
            os.`id_order_state`,
            osl.`name`
            FROM '._DB_PREFIX_.'orders a
            LEFT JOIN '._DB_PREFIX_.'carrier car ON a.`id_carrier` = car.`id_carrier`
            LEFT JOIN '._DB_PREFIX_.'address ad ON a.`id_address_delivery` = ad.`id_address`
            LEFT JOIN '._DB_PREFIX_.'country con ON ad.`id_country` = con.`id_country`
            LEFT JOIN '._DB_PREFIX_.'order_state os ON a.`current_state` = os.`id_order_state`
            LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON os.`id_order_state` = osl.`id_order_state`
            LEFT JOIN '._DB_PREFIX_.'customer c ON a.`id_customer` = c.`id_customer`
            LEFT JOIN '._DB_PREFIX_.'currency cur ON a.`id_currency` = cur.`id_currency`
            WHERE
            1 AND osl.`id_lang` = '.(int)$this->context->language->id.'
            GROUP BY os.`id_order_state`
            ORDER BY os.`id_order_state` ASC';
        $orderstatescount_table = Db::getInstance()->executeS($sql_states_count);
        foreach ($orderstatescount_table as $row) {
            array_push(
                $this->g_aOrderStateCount,
                array(
                    'id' => $row['id_order_state'],
                    'name' => $row['name'],
                    'count' => $row['count']
                )
            );
        }
        /* order state sql */
        $sql_states = 'SELECT os.`id_order_state`, `name` FROM '._DB_PREFIX_.
            'order_state os LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON
            os.`id_order_state` = osl.`id_order_state`
            WHERE osl.`id_lang` = '.(int)$this->context->language->id;
        /* order state table .. all rows */
        $orderstatesfilter_table = Db::getInstance()->executeS(
            $sql_states . ' ORDER BY os.`id_order_state` ASC'
        );
        foreach ($orderstatesfilter_table as $row) {
            array_push(
                $this->g_aOrderStateFilter,
                array(
                    'id' => $row['id_order_state'],
                    'name' => $row['name'],
                    'enabled' => $this->bCheckOrderStateFilter((int)$row['id_order_state']),
                    'count' => $this->iGetOrderStateCount((int)$row['id_order_state'])
                )
            );
        }
        if ($this->g_bIfPost_OrderStateFilter) {
            $this->saveOrderStateFilter();
        }
        /* order state table filtered */
        $sql_orderstatefilter = '';
        if (Tools::strlen($this->g_sCfg_OrderStateFilter) > 0) {
            $sql_orderstatefilter = ' AND os.`id_order_state` IN ('.$this->g_sCfg_OrderStateFilter.')';
        }
        $states_table = Db::getInstance()->executeS(
            $sql_states . $sql_orderstatefilter
        );
        foreach ($states_table as $row) {
            $this->order_states[$row['id_order_state']] = $row['name'];
        }
        
        $cur_table = Db::getInstance()->executeS(
            'SELECT `id_currency`, `iso_code` FROM '._DB_PREFIX_.'currency
            WHERE EXISTS (SELECT * FROM '._DB_PREFIX_.'orders
            WHERE '._DB_PREFIX_.'orders.`id_currency` = '._DB_PREFIX_.'currency.`id_currency`)'
        );

        foreach ($cur_table as $row) {
            $this->currencies[$row['id_currency']] = $row['iso_code'];
        }

        /* setup list columns */
        $this->setupListFields();
        /* setup bulk actions */
        $this->bulk_actions = array(
            $this->action_bulk_inv => array(
                'text' => $this->l('Generate invoice'),
                'icon' => 'icon-repeat',
                'confirm' => $this->l('Generate invoice for all selected orders ?')
            )
        );
        /* setup options config */
        $this->setupOptions();
        parent::__construct();
    }

    private function bCheckScriptTimeout()
    {
        $timeout = Configuration::get('DNG_ORDREP_SCRIPT_TIMEOUT');
        if (!Validate::isInt($timeout)) {
            Configuration::updateValue('DNG_ORDREP_SCRIPT_TIMEOUT', '60');
            return false;
        }
        if ((int)$timeout < 1 || (int)$timeout > 600) {
            Configuration::updateValue('DNG_ORDREP_SCRIPT_TIMEOUT', '60');
            return false;
        }

        return true;
    }

    private function iGetOrderStateCount($_sOrderState)
    {
        if (count($this->g_aOrderStateCount) > 0) {
            foreach ($this->g_aOrderStateCount as $row) {
                if ((int)$row['id'] == (int)$_sOrderState) {
                    return $row['count'];
                }
            }
            return 0;
        }
        return 0;
    }

    private function bCheckOrderStateFilter($_sOrderState)
    {
        $this->g_sCfg_OrderStateFilter = Configuration::get('DNG_ORDREP_ORDER_STATE_FILTER');
        if (Tools::strlen($this->g_sCfg_OrderStateFilter) > 0) {
            $array_orderstatefilter = array();
            $array_orderstatefilter = explode(',', $this->g_sCfg_OrderStateFilter);
            foreach ($array_orderstatefilter as $item_orderstate) {
                if ($item_orderstate == $_sOrderState) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    private function setupListFields()
    {
        $arr1 = array(
            'id_order' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'reference' => array(
                'title' => $this->l('Reference'),
                'hint' => $this->l('Order reference'),
                'align' => 'center'
            ),
            'customer' => array(
                'title' => $this->l('Customer'),
                'hint' => $this->l('Customer name'),
                'havingFilter' => true,
                'align' => 'center'
            )
        );
        $name_arr = array();
        if ($this->addr_name) {
            $name_arr = array(
                'address_customer' => array(
                    'title' => $this->l('Address name'),
                    'hint' => $this->l('Delivery address name'),
                    'havingFilter' => true,
                    'align' => 'center'
                )
            );
        }
        $company_arr = array();
        if ($this->addr_company) {
            $company_arr = array(
                'address_company' => array(
                'title' => $this->l('Company'),
                'align' => 'center',
                'havingFilter' => true,
                'hint' =>  $this->l('Delivery address company')
                )
            );
        }
        $country_arr = array();
        if ($this->addr_country) {
            $country_arr = array(
                'contry_code' => array(
                'title' => $this->l('Country'),
                'align' => 'center',
                'filter_key' => 'con!iso_code',
                'filter_type' => 'select',
                'hint' =>  $this->l('Delivery address country')
                )
            );
        }
        $total_type = 'price';
        $curr_arr = array();
        if ($this->show_curr) {
            $total_type = 'decimal';
            $curr_arr = array(
                'currency_code' => array(
                    'title' => $this->l('Currency'),
                    'type' => 'select',
                    'list' => $this->currencies,
                    'filter_key' => 'cur!id_currency',
                    'filter_type' => 'int',
                    'align' => 'center',
                    'class' => 'fixed-width-xs'
                )
            );
        }
        $arr2 = array(
            'total_excl' => array(
            'title' => $this->l('Paid tax excl.'),
            'type' => $total_type,
            'align' => 'text-right',
            'search' => false
            ),
            'total_incl' => array(
                'title' => $this->l('Paid tax incl.'),
                'type' => $total_type,
                'align' => 'text-right',
                'search' => false
            )
        );
        $inv_num_arr = array();
        if ($this->invoice_num) {
            $inv_num_arr = array(
                'invoice_number' => array(
                    'title' => $this->l('Invoice'),
                    'hint' => $this->l('Invoice number'),
                    'havingFilter' => true,
                    'align' => 'center',
                    'callback' => 'setInvoiceNum',
                )
            );
        }
        $payment_arr = array();
        if ($this->payment) {
            $payment_arr = array(
                'payment' => array(
                    'title' => $this->l('Payment'),
                    'hint' => $this->l('Payment method'),
                    'havingFilter' => true,
                    'align' => 'center'
                )
            );
        }
        $carrier_arr = array();
        if ($this->carrier) {
            $carrier_arr = array(
                'carrier' => array(
                    'title' => $this->l('Carrier'),
                    'hint' => $this->l('Shipping carrier'),
                    'havingFilter' => true,
                    'align' => 'center'
                )
            );
        }
        $arr3 = array(
            'date_add' => array(
                    'title' => $this->l('Order date'),
                    'type' => 'date',
                    'align' => 'center',
                    'filter_key' => 'a!date_add',
                    'class' => 'fixed-width-xs'
            )
        );
        $delivery_arr = array();
        if ($this->delivery_date) {
            $delivery_arr = array(
                'delivery_date' => array(
                    'title' => $this->l('Delivery'),
                    'hint' => $this->l('Delivery date'),
                    'type' => 'date',
                    'filter_key' => 'a!delivery_date',
                    'class' => 'fixed-width-xs',
                    'align' => 'center',
                    'callback' => 'setDeliveryDate'
                )
            );
        }
        $arr4 = array(
            'order_state_name' => array(
                'title' => $this->l('Order Status'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->order_states,
                'filter_key' => 'os!id_order_state',
                'order_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'id_pdf' => array(
                'title' => $this->l('PDF'),
                'align' => 'text-center',
                'callback' => 'printPDFIcons',
                'orderby' => false,
                'search' => false,
                'remove_onclick' => true
            )
        );
        $this->fields_list = array_merge(
            $arr1,
            $name_arr,
            $company_arr,
            $country_arr,
            $arr2,
            $curr_arr,
            $inv_num_arr,
            $payment_arr,
            $carrier_arr,
            $arr3,
            $delivery_arr,
            $arr4
        );
    }

    private function setupOptions()
    {
        $this->fields_options = array(
            'export' =>	array(
                'title' =>	$this->l('Settings :: Export'),
                'fields' => array(
                    'DNG_ORDREP_SCRIPT_TIMEOUT' => array(
                        'title' => $this->l('Generate report script timeout'),
                        'desc' => $this->l('Use this setting to increase generate report script execution timeout! Value must be between 1 (s) and 600 (s).'),
                        'type' => 'text',
                        'cast' => 'intval',
                        'required' => true,
                        'class' => 'fixed-width-xs',
                        'suffix' => 'seconds'
                    ),
                    'DNG_ORDER_EXPORT_ADDR' => array(
                        'title' => $this->l('Export delivery address details'),
                        'desc' => $this->l(
                            'If enabled, additional delivery address details are exported in CSV file: address 1,
                            address 2, ZIP, City, State.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PHONE' => array(
                        'title' => $this->l('Export phone contacts'),
                        'desc' => $this->l(
                            'If enabled, phone contacts of the delivery addresses are exported in CSV file.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_EMAIL' => array(
                        'title' => $this->l('Export customer email'),
                        'desc' => $this->l(
                            'If enabled, customer email addresses are exported in CSV file.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PREF' => array(
                        'title' => $this->l('Export product references'),
                        'desc' => $this->l(
                            'If enabled, product references of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PNAME' => array(
                        'title' => $this->l('Export product names'),
                        'desc' => $this->l(
                            'If enabled, product names of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PSUP' => array(
                        'title' => $this->l('Export product suppliers'),
                        'desc' => $this->l(
                            'If enabled, product supplier names of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PEAN' => array(
                        'title' => $this->l('Export product EAN codes'),
                        'desc' => $this->l(
                            'If enabled, product EAN codes of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PUPC' => array(
                        'title' => $this->l('Export product UPC codes'),
                        'desc' => $this->l(
                            'If enabled, product UPC codes of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PQUA' => array(
                        'title' => $this->l('Export product quantities'),
                        'desc' => $this->l(
                            'If enabled, product quantities of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_EXPORT_PPRI' => array(
                        'title' => $this->l('Export product prices'),
                        'desc' => $this->l(
                            'If enabled, total product prices of each order are exported in CSV file
                            as additional table columns.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    )
                 ),
                'submit' => array('title' => $this->l('Save'))
             ),
            'report' =>	array(
                'title' =>	$this->l('Settings :: Report'),
                'fields' => array(
                    'DNG_REPORT_ORDER_LIST' => array(
                        'title' => $this->l('Display list of orders on PDF report'),
                        'desc' => $this->l(
                            'If enabled, list of orders is displayed in PDF above the summary of total values.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_WRAPPING' => array(
                        'title' => $this->l('Display wrapping costs'),
                        'desc' => $this->l(
                            'If enabled, wrapping costs are displayed on report and export.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
             'columns' => array(
                'title' =>	$this->l('Settings :: Columns'),
                'fields' => array(
                    'DNG_ORDER_PAYMENT' => array(
                        'title' => $this->l('Display payment method'),
                        'desc' => $this->l(
                            'If enabled, payment method is displayed on the list of orders.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_CARRIER' => array(
                        'title' => $this->l('Display shipping'),
                        'desc' => $this->l(
                            'If enabled, shipping carrier and number is displayed on the list of orders.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_DELIVERY_NAME' => array(
                        'title' => $this->l('Display delivery address name'),
                        'desc' => $this->l('If enabled, delivery address name is displayed on the list of orders.'),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_DELIVERY_COMPANY' => array(
                        'title' => $this->l('Display delivery address company'),
                        'desc' => $this->l('If enabled, delivery address company is displayed on the list of orders.'),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_DELIVERY_COUNTRY' => array(
                        'title' => $this->l('Display delivery address country'),
                        'desc' => $this->l(
                            'If enabled, delivery address country code is displayed on the list of orders.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_DELIVERY_DATE' => array(
                        'title' => $this->l('Display delivery date'),
                        'desc' => $this->l(
                            'If enabled, delivery date is displayed on the list of orders.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_INV_NUM' => array(
                        'title' => $this->l('Display invoice number'),
                        'desc' => $this->l(
                            'If enabled, invoice number is displayed on the list of orders.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    ),
                    'DNG_ORDER_CURRENCY' => array(
                        'title' => $this->l('Enable search by currency'),
                        'desc' => $this->l(
                            'If enabled, currency is displayed on the list of orders as a separate column and
                            orders can be filtered by different currencies.'
                        ),
                        'cast' => 'intval',
                        'type' => 'bool'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            )
        );
    }

    public function initToolbar()
    {
        $this->toolbar_btn['export'] = array(
            'href' => self::$currentIndex.'&exportordersreport&token='.$this->token,
            'desc' => $this->l('Export to CSV')
        );
    }

    public function initContent()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $this->initPageHeaderToolbar();
        }
//        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/controller/AdminController.php');
        $this->initToolbar();
        $this->content .= $this->renderOrderStateFilter();
        $this->content .= $this->renderList();
        $this->content .= $this->renderPPFormReport();
        $this->content .= $this->renderFormReport();
        $this->content .= $this->renderOptions();

        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            $this->context->smarty->assign(
                array(
                    'content' => $this->content,
                    'show_page_header_toolbar' => $this->show_page_header_toolbar,
                    'page_header_toolbar_title' => $this->page_header_toolbar_title,
                    'page_header_toolbar_btn' => $this->page_header_toolbar_btn
                )
            );
        } else {
            $this->context->smarty->assign(array('content' => $this->content));
        }
    }    

    /*
     * ORDER STATUS FILTER
     */

    private function renderOrderStateFilter()
    {
        if ($this->g_bSave_OrderStateFilter) {
            $this->g_bSave_OrderStateFilter = false;
            $this->saveOrderStateFilter();
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('FILTER :: ORDER STATUS'),
                'icon' => 'icon-search',
                'name' => 'form_orderstatefilter'
            ),
            'input' => array(
                array(
                    'type' => 'type_name',
                    'name' => 'field_name',
                    'values' => ''
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'id' => 'submit_save_orderstatefilter',
                'name' => 'submit_save_orderstatefilter',
                'icon' => 'process-icon-save'
              )
        );

        $this->tpl_form_vars = array(
            'currencyfilter' => '',
            'amounts' => '',
            'orderstatefilter' => $this->getOrderStateFilterTemplate(),
            'style' => ''
        );

        return parent::renderForm();
    }

    private function getOrderStateFilterTemplate()
    {
        $this->context->smarty->assign(array(
            'section_orderstatefilter' => $this->g_aOrderStateFilter,
            'psver' => $this->getPrestaShopVersion()
        ));

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_.'/ordersreport/views/templates/admin/orderstatefilter.tpl'
        );
    }

    private function saveOrderStateFilter()
    {
        $orderstatefilter = '';
        foreach ($this->g_aOrderStateFilter as $key => $value) {
            if (Tools::getValue('_orderstate_'.$value['id'], false) == true) {
                $this->g_aOrderStateFilter[$key]['enabled'] = true;
                $orderstatefilter .= (Tools::strlen($orderstatefilter) > 0 ? ',' : $orderstatefilter);
                $orderstatefilter .= $value['id'];
            } else {
                $this->g_aOrderStateFilter[$key]['enabled'] = false;
            }
        }
        $this->g_sCfg_OrderStateFilter = $orderstatefilter;
        Configuration::updateValue('DNG_ORDREP_ORDER_STATE_FILTER', $this->g_sCfg_OrderStateFilter);
        $this->confirmations[] = $this->l('Order status filter saved!');
    }

    /*
     * REPORT FORM
     */

    private function renderFormReport()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('REPORT - summed amounts for listed orders'),
                'icon' => 'icon-info-sign'
            ),
            'input' => array(
                array(
                    'type' => 'total-vals',
                    'name' => 'field_name',
                    'values' => '',
                    'hint' => $this->l('Summed amounts for listed orders')
                )
            ),
            'submit' => array(
                'title' => $this->l('Generate Report PDF'),
                'id' => 'submitPrint',
                'name' => 'generateReport',
                'icon' => 'process-icon-download-alt'
                )
        );

        $this->totals = $this->getTotals();

        $this->tpl_form_vars = array(
            'orderstatefilter' => '',
            'currencyfilter' => '',
            'amounts' => $this->getAmountsContent(),
            'style' => ''
        );
        if ($this->print_report) {
            $this->generateReport();
        }
        return parent::renderForm();
    }
    
        private function renderPPFormReport()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Pick & Pack report', 'ordersreport'),
                'icon' => 'icon-info-sign'
            ),
            'submit' => array(
                'title' => $this->l('Pick & Pack Report PDF', 'ordersreport'),
                'id' => 'submitPPPrint',
                'name' => 'generatePPReport',
                'icon' => 'process-icon-download-alt'
                )
        );

        //$this->totals = $this->getTotals();

        /*$this->tpl_form_vars = array(


            'amounts' => $this->getAmountsContent(),
            'style' => ''
        );*/
        if ($this->print_ppreport) {
            $this->generatePPReport();
        }
        return parent::renderForm();
    }


    /**
    * returns temlate content which is the same in PDF doc and in form
    *
    * @param boolean $pdf indicates if the content is for PDF
    */
    private function getAmountsContent($pdf = false)
    {
		$this->totals = $this->getTotals();
        $show_wrapping = Configuration::get('DNG_ORDER_WRAPPING');
        $this->context->smarty->assign(array(
                'totals' => $this->totals,
                'functions' => $this->functions,
                'show_wrapping' => $show_wrapping,
                'pdf' => $pdf
            ));
        return $this->context->smarty->fetch(_PS_MODULE_DIR_.'/ordersreport/views/templates/admin/amounts.tpl');
    }

    private function getTotals()
    {
        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/TotalCalculator.php');
        require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');
        $shop_filter = $this->getShopFilter();
        $having = '';
        if (isset($this->_filterHaving)) {
            $having = $this->_filterHaving;
        }
        return TotalCalculator::getTotalValues($this->_filter.$having, $shop_filter, $this->context->language->id, $this->g_sCfg_OrderStateFilter);
    }

    public function setInvoiceNum($inv_num, $tr)
    {
        if ($inv_num) {
            return $inv_num;
        } else {
            return '';
        }
    }
    
    public function setDeliveryDate($delivery_date, $tr)
    {
        if ((strtotime($delivery_date) == '') || ($delivery_date == '0000-00-00 00:00:00')) {
            return '';
        } else {
            return $delivery_date;
        }
    }

    public function printPDFIcons($id_order, $tr)
    {
        static $valid_order_state = array();
        $order = new Order($id_order);
        if (!Validate::isLoadedObject($order)) {
            return '';
        }
        $order_state = $order->getCurrentOrderState();
        if (!isset($valid_order_state[$order->current_state])) {
            $valid_order_state[$order->current_state] = Validate::isLoadedObject($order_state);
        }
        if (!$valid_order_state[$order->current_state]) {
            return '';
        }
        $this->context->smarty->assign(array(
            'order' => $order,
            'order_state' =>  $order_state,
            'tr' => $tr
        ));
        $tpl_name = '_print_pdf_icon.tpl';

        if ($this->override_folder) {
            if (!Configuration::get('PS_DISABLE_OVERRIDES')
                   && file_exists(
                       $this->context->smarty->getTemplateDir(1)
                       .DIRECTORY_SEPARATOR.$this->override_folder.$tpl_name
                   )
                ) {
                    return $this->context->smarty->createTemplate(
                        $this->override_folder.$tpl_name,
                        $this->context->smarty
                    );
            } elseif (file_exists(
                $this->context->smarty->getTemplateDir(0)
                .'controllers'.DIRECTORY_SEPARATOR
                .$this->override_folder
                .$tpl_name
            )) {
                return $this->context->smarty->createTemplate(
                    'controllers'.DIRECTORY_SEPARATOR.$this->override_folder.$tpl_name,
                    $this->context->smarty
                );
            }
        }
        return $this->context->smarty->createTemplate(
            $this->context->smarty->getTemplateDir(0).
            'controllers/orders/'.$tpl_name,
            $this->context->smarty
        )->fetch();
    }
    private function getShopFilter()
    {
        $shops = '';
        if ($this->multistore) {
            $shops = Shop::addSqlRestriction($this->shopShareDatas, 'a', $this->shopLinkType);
        }
        return $shops;
    }

    /*
    This function parses out the instruction to limit the number of rows returned from DB
    */
    public static function parseLimit($sql)
    {
        $str = 'SQL_CALC_FOUND_ROWS';
        $len = Tools::strlen($str);
        $pos = strpos($sql, $str);
        $tmp = substr_replace($sql, '', $pos, $len);
        $len = Tools::strlen($tmp);
        $pos = strpos($tmp, 'LIMIT');
        $tmp = substr_replace($tmp, '', $pos, $len - $pos);
        return $tmp;
    }

    /*
    geretate PDF document
    */
    public function generateReport()
    {
        if ($this->bCheckScriptTimeout()) {
            $timeout = Configuration::get('DNG_ORDREP_SCRIPT_TIMEOUT');
            set_time_limit($timeout);
        }

        $this->print_report = false;

        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/OrderList.php');
        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/HTMLTemplateOrdersReport.php');
        require_once(_PS_MODULE_DIR_.'/ordersreport/controllers/admin/OrdersReportPDFController.php');
        require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');

        $list = array();
        if (Configuration::get('DNG_REPORT_ORDER_LIST')) {
            /*
            * $this->_listsql is SQL query from parent class AdminController, security already coded there !!
            */
            $listsql = (self::parseLimit($this->_listsql)); // parse out SQL_CALC_FOUND_ROWS and LIMIT
            if (!($list = Db::getInstance()->executeS($listsql, true, false))) {
                $this->_list_error = Db::getInstance()->getMsgError();
            }
        }
        p($listsql);
        $shop_filter = $this->getShopFilter();
        $shops = FilterParser::getShops($shop_filter);

        $having = '';
        if (isset($this->_filterHaving)) {
            $having = $this->_filterHaving;
        }
        $lang_id = (int)$this->context->language->id;

        $search = FilterParser::getSearchFields($this->_filter.$having);

        $fields = array();
        if ($this->addr_name) {
            $fields['addr_name'] = 1;
        }
        if ($this->addr_company) {
            $fields['addr_company'] = 1;
        }
        if ($this->addr_country) {
            $fields['addr_country'] = 1;
        }
        if ($this->carrier) {
            $fields['carrier'] = 1;
        }
        if ($this->payment) {
            $fields['payment'] = 1;
        }
        if ($this->show_curr) {
            $fields['show_curr'] = 1;
        }
        if ($this->invoice_num) {
            $fields['invoice_num'] = 1;
        }
        if ($this->delivery_date) {
            $fields['delivery_date'] = 1;
        }
        FilterParser::setFriendlyFields($search, $lang_id);
        $pdf_list = new OrderList(
            $list,
            $this->multistore,
            $search,
            $fields,
            $shops,
            $this->functions,
            $this->getAmountsContent(true),
            $this->g_aOrderStateFilter
        );

        $pdf_controller = new OrdersReportPDFController();
        $pdf_controller->generateReport($pdf_list);
    }

    public function generatePPReport()
    {
        $this->print_ppreport = false;

        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/OrderList.php');
        require_once(_PS_MODULE_DIR_.'/ordersreport/classes/HTMLTemplatePPOrdersReport.php');
        require_once(_PS_MODULE_DIR_.'/ordersreport/controllers/admin/OrdersReportPDFController.php');
        require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');

        $list = array();
        if (Configuration::get('IRR_REPORT_ORDER_LIST')) {
            /*
            * $this->_listsql is SQL query from parent class AdminController, security already coded there !!
            */
            $listsql = (self::parseLimit($this->_listsql)); // parse out SQL_CALC_FOUND_ROWS and LIMIT
            if (!($list = Db::getInstance()->executeS($listsql, true, false))) {
                $this->_list_error = Db::getInstance()->getMsgError();
            }
        }
        $shop_filter = $this->getShopFilter();
        $shops = FilterParser::getShops($shop_filter);

        $having = '';
        if (isset($this->_filterHaving)) {
            $having = $this->_filterHaving;
        }
        $lang_id = (int)$this->context->language->id;
         
        $search = FilterParser::getSearchFields($this->_filter.$having);

        $fields = array();
        if ($this->addr_name) {
            $fields['addr_name'] = 1;
        }
        if ($this->addr_company) {
            $fields['addr_company'] = 1;
        }
        if ($this->addr_country) {
            $fields['addr_country'] = 1;
        }
        if ($this->carrier) {
            $fields['carrier'] = 1;
        }
        if ($this->payment) {
            $fields['payment'] = 1;
        }
        if ($this->show_curr) {
            $fields['show_curr'] = 1;
        }
//echo "<pre>###";print_r($this->_filterHaving);die;   

        FilterParser::setFriendlyFields($search, $lang_id);
        $pdf_list = new OrderList(
            $list,
            $this->multistore,
            $search,
            $fields,
            $shops,
            $this->functions,
            $this->getAmountsContent(true)
        );
            $pdf_controller = new OrdersReportPDFController();
            $pdf_controller->generateReport($pdf_list);
    }

    /*
    export to CSV
    */
    public function processExport($text_delimiter = ',')
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }
        $this->text_delimiter = $text_delimiter;
        $this->export_csv = true;
    }

    private function csvExport($order_ids = null)
    {
        require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');
        $this->order_str = FilterParser::getOrder($this->_listsql);
        $this->export_csv = false;
        $text_delimiter = ',';
        $shop_filter = '';
        $shop_criteria = '';
        $show_wrapping = Configuration::get('DNG_ORDER_WRAPPING');
        $show_addr_name = Configuration::get('DNG_ORDER_DELIVERY_NAME');
        $show_addr_company = Configuration::get('DNG_ORDER_DELIVERY_COMPANY');
        $show_addr_country = Configuration::get('DNG_ORDER_DELIVERY_COUNTRY');
        $show_delivery_date = Configuration::get('DNG_ORDER_DELIVERY_DATE');
        $show_payment = Configuration::get('DNG_ORDER_PAYMENT');
        $show_curr = Configuration::get('DNG_ORDER_CURRENCY');
        $show_inv_num = Configuration::get('DNG_ORDER_INV_NUM');
        $show_carrier = Configuration::get('DNG_ORDER_CARRIER');
        $addr_details = Configuration::get('DNG_ORDER_EXPORT_ADDR');
        $show_phone =  Configuration::get('DNG_ORDER_EXPORT_PHONE');
        $show_email =  Configuration::get('DNG_ORDER_EXPORT_EMAIL');
        $prod_ref =  Configuration::get('DNG_ORDER_EXPORT_PREF');
        $prod_name =  Configuration::get('DNG_ORDER_EXPORT_PNAME');
        $prod_ean =  Configuration::get('DNG_ORDER_EXPORT_PEAN');
        $prod_upc =  Configuration::get('DNG_ORDER_EXPORT_PUPC');
        $prod_qua =  Configuration::get('DNG_ORDER_EXPORT_PQUA');
        $prod_pri =  Configuration::get('DNG_ORDER_EXPORT_PPRI');
        $prod_sup =  Configuration::get('DNG_ORDER_EXPORT_PSUP');
        $product_details = $prod_ref || $prod_name || $prod_ean || $prod_upc || $prod_qua || $prod_pri || $prod_sup;
        $addr_fields = '';
        $email_field = '';
        $phone_fields = '';
        $join_state = '';
        $join_supplier = '';
        if ($addr_details) {
            $join_state = ' LEFT JOIN '._DB_PREFIX_.'state sta ON ad.`id_state` = sta.`id_state`';
            $addr_fields = ', `address1`, `address2`, `postcode`, `city`, sta.`name` AS `state`';
        }
        if ($show_phone) {
            $phone_fields = ', `phone`, `phone_mobile`';
        }
        if ($show_email) {
            $email_field = ', `email`';
        }
        $headers = array();
        $field_list_export = array();
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Order reference'));
        $field_list_export[] = array('name' => 'reference', 'type' => 'string');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Order date'));
        $field_list_export[] = array('name' => 'date_add', 'type' => 'date');
        if ($show_delivery_date) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Delivery date'));
            $field_list_export[] = array('name' => 'delivery_date', 'type' => 'date_null');
        }
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Customer name'));
        $field_list_export[] = array('name' => 'customer', 'type' => 'string');
        if ($show_email) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Email'));
            $field_list_export[] = array('name' => 'email', 'type' => 'string');
        }
        if ($show_addr_name) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Delivery address name'));
            $field_list_export[] = array('name' => 'address_customer', 'type' => 'string');
        }
        if ($show_addr_company) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Company'));
            $field_list_export[] = array('name' => 'address_company', 'type' => 'string');
        }
        if ($addr_details) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Address 1'));
            $field_list_export[] = array('name' => 'address1', 'type' => 'string');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Address 2'));
            $field_list_export[] = array('name' => 'address2', 'type' => 'string');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('ZIP'));
            $field_list_export[] = array('name' => 'postcode', 'type' => 'string');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('City'));
            $field_list_export[] = array('name' => 'city', 'type' => 'string');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('State'));
            $field_list_export[] = array('name' => 'state', 'type' => 'string');
        }
        if ($show_addr_country) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Country'));
            $field_list_export[] = array('name' => 'country_code', 'type' => 'string');
        }
        if ($show_phone) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Phone'));
            $field_list_export[] = array('name' => 'phone', 'type' => 'string');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Mobile'));
            $field_list_export[] = array('name' => 'phone_mobile', 'type' => 'string');
        }
        if ($show_carrier) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Shipping carrier'));
            $field_list_export[] = array('name' => 'carrier', 'type' => 'num_null');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Shipping number'));
            $field_list_export[] = array('name' => 'shipping_number', 'type' => 'string');
        }
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Order Status'));
        $field_list_export[] = array('name' => 'order_state_name', 'type' => 'price');
        if ($this->multistore) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Shop'));
            $field_list_export[] = array('name' => 'shop_name', 'type' => 'string');
            $shop_filter = $this->getShopFilter();
            $shops = FilterParser::getShops($shop_filter);
            $shop_criteria = $this->getCriteriaShops($shops);
        }
        if ($show_curr) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Currency'));
            $field_list_export[] = array('name' => 'currency_code', 'type' => 'string');
        }
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Paid tax excl.'));
        $field_list_export[] = array('name' => 'total_excl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Paid tax incl.'));
        $field_list_export[] = array('name' => 'total_incl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Products tax excl.'));
        $field_list_export[] = array('name' => 'products_excl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Products tax incl.'));
        $field_list_export[] = array('name' => 'products_incl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Shipping tax excl.'));
        $field_list_export[] = array('name' => 'shipping_excl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Shipping tax incl.'));
        $field_list_export[] = array('name' => 'shipping_incl', 'type' => 'price');
        if ($show_wrapping) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Gift wrapping'));
            $field_list_export[] = array('name' => 'gift', 'type' => 'bool');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Wrapping tax excl.'));
            $field_list_export[] = array('name' => 'wrapping_excl', 'type' => 'price');
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Wrapping tax incl.'));
            $field_list_export[] = array('name' => 'wrapping_incl', 'type' => 'price');
        }
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Discount tax excl.'));
        $field_list_export[] = array('name' => 'discount_excl', 'type' => 'price');
        $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Discount tax incl.'));
        $field_list_export[] = array('name' => 'discount_incl', 'type' => 'price');
        if ($show_payment) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Payment method'));
            $field_list_export[] = array('name' => 'payment', 'type' => 'string');
        }
        if ($show_inv_num) {
            $headers[] = Tools::htmlentitiesDecodeUTF8($this->l('Invoice number'));
            $field_list_export[] = array('name' => 'invoice_number', 'type' => 'num_null');
        }
        if ($product_details) {
            $add_prod_heads = array();
            if ($prod_name) {
                $add_prod_heads['product_name'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product name'));
            }
            if ($prod_ean) {
                $add_prod_heads['product_ean13'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product EAN13'));
            }
            if ($prod_upc) {
                $add_prod_heads['product_upc'] = Tools::htmlentitiesDecodeUTF8($this->l('Product UPC'));
            }
            if ($prod_ref) {
                $add_prod_heads['product_reference'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product reference'));
            }
            if ($prod_sup) {
                $add_prod_heads['product_supplier'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product supplier'));
            }
            if ($prod_qua) {
                $add_prod_heads['product_quantity'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product quantity'));
            }
            if ($prod_pri) {
                $add_prod_heads['total_price_tax_excl'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product price tax excl.'));
                $add_prod_heads['total_price_tax_incl'] =
                    Tools::htmlentitiesDecodeUTF8($this->l('Product price tax incl.'));
            }
        }
        $having = '';
        if (isset($this->_filterHaving)) {
            $having = $this->_filterHaving;
        }

        $search_fields = FilterParser::getSearchFields($this->_filter.$having);
        $search_criteria = $this->getCriteriaSearch($search_fields);
        if (empty($search_criteria)) {
            $search_criteria = $this->l('All orders');
        }
        $where_clause = '';
        if (isset($order_ids)) {
            $where_clause = 'WHERE a.`id_order` IN ('.implode($order_ids, ',').')';
        } else {
            $where_clause = 'WHERE 1 '. FilterParser::getWhereClause($search_fields);
        }
        $join_shop = '';
        $shop_field = '';
        if (!empty($shop_filter)) {
            $join_shop = 'LEFT JOIN '._DB_PREFIX_.'shop shop ON a.`id_shop` = shop.`id_shop` ';
            $where_clause .= pSQL($shop_filter);
            $shop_field = ', shop.`name` as shop_name';
        }
        $lang_id = $this->context->language->id;
        $where_clause .= ' AND osl.`id_lang` = '.(int)$lang_id;
        // CUSTOM FILTERS
        if (Tools::strlen($this->g_sCfg_OrderStateFilter) > 0) {
            $where_clause .= ' AND os.`id_order_state` IN ('.$this->g_sCfg_OrderStateFilter.')';
        }
        $orderstr = $this->order_str;
        if ($product_details) {
            $orderstr = 'a.`id_order` DESC';
        }
        $join_str = ' LEFT JOIN '._DB_PREFIX_.'order_state os ON a.`current_state` = os.`id_order_state`
            LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON os.`id_order_state` = osl.`id_order_state`
            LEFT JOIN '._DB_PREFIX_.'customer c ON a.`id_customer`=c.`id_customer`
            LEFT JOIN '._DB_PREFIX_.'address ad ON a.`id_address_delivery` = ad.`id_address`
            LEFT JOIN '._DB_PREFIX_.'carrier car ON a.`id_carrier` = car.`id_carrier`
            LEFT JOIN '._DB_PREFIX_.'country con ON ad.`id_country` = con.`id_country`
            LEFT JOIN '._DB_PREFIX_.'currency cur ON a.`id_currency`=cur.`id_currency` '.
            $join_state.$join_supplier.$join_shop;

        $sql_orders = 'SELECT '.$this->_select.$addr_fields.$phone_fields.$email_field.', a.`id_order`,
            a.`total_products` AS products_excl,a.`total_products_wt` AS products_incl,
            a.`total_shipping_tax_excl` AS shipping_excl, a.`gift`, a.`total_shipping_tax_incl` AS shipping_incl,
            a.`total_wrapping_tax_excl` AS wrapping_excl, a.`total_wrapping_tax_incl` AS wrapping_incl,
            a.`total_discounts_tax_excl` AS discount_excl, a.`total_discounts_tax_incl` AS discount_incl'.$shop_field.
            ' FROM '._DB_PREFIX_.'orders a'.$join_str.$where_clause.' ORDER BY '.pSQL($orderstr);

        if ($product_details) {
            $sql_products = 'SELECT a.`id_order`, od.`product_reference`,
            od.`product_name`, od.`product_quantity`, od.`product_ean13`, od.`product_upc`,
            od.`total_price_tax_excl`, od.`total_price_tax_incl`, s.`name` AS product_supplier
			FROM '._DB_PREFIX_.'orders a
            LEFT JOIN '._DB_PREFIX_.'order_detail od ON a.`id_order` = od.`id_order`
            LEFT JOIN '._DB_PREFIX_.'product p ON od.`product_id` =  p.`id_product`
            LEFT JOIN '._DB_PREFIX_.'supplier s ON p.`id_supplier` = s.`id_supplier`'.
            $join_str.$where_clause.' ORDER BY '.pSQL($orderstr);
            $products = Db::getInstance()->executeS($sql_products);
        }

        $orders = Db::getInstance()->executeS($sql_orders);

        header('Content-type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-disposition: attachment; filename="ordersreport_'.date('Y-m-d_His').'.csv"');

        $content = array();
        if ($product_details) {
            $prod_num =  count($products);
        }
        $added_prod_cols = 0;
        $product_idx = 0;

        foreach ($orders as $i => $row) {
            $content[$i] = array();
            foreach ($field_list_export as $field) {
                $value = '';
                if (isset($row[$field['name']])) {
                    $value = Tools::htmlentitiesDecodeUTF8(Tools::nl2br($row[$field['name']]));
                    if ($field['type'] == 'date') {
                        $params = array();
                        $params['date'] = $value;
                        $params['full'] = false;
                        $value = Tools::dateFormat($params, $this->context->smarty);
                    } elseif ($field['type'] == 'price') {
                        $value = IRFunctions::displayPriceIr($value, true);
                    } elseif ($field['type'] == 'bool') {
                        if ((int)$value) {
                            $value = Tools::htmlentitiesDecodeUTF8($this->l('Yes'));
                        } else {
                            $value = Tools::htmlentitiesDecodeUTF8($this->l('No'));
                        }
                    } elseif ($field['type'] == 'num_null') {
                        if ($value == '0') {
                            $value = '';
                        }
                    } elseif ($field['type'] == 'date_null') {
                        if ((strtotime($value) == '') || ($value == '0000-00-00 00:00:00')) {
                             $value = '';
                        } else {
                             $params = array();
                             $params['date'] = $value;
                             $params['full'] = false;
                             $value = Tools::dateFormat($params, $this->context->smarty);
                        }
                    }
                }
                $content[$i][] = $value;
            }
            if ($product_details && $prod_num) {
                $products_count = 0;
                while (($product_idx < $prod_num) && ($products[$product_idx]['id_order'] == $row['id_order'])) {
                    $products_count++;
                    if ($products_count > $added_prod_cols) {
                        foreach ($add_prod_heads as $key => $head) {
                            $headers[] = $head;
                        }
                        $added_prod_cols++;
                    }
                    foreach (array_keys($add_prod_heads) as $key) {
                        $val = $products[$product_idx][$key];
                        if (($key == 'total_price_tax_excl') || ($key == 'total_price_tax_incl')) {
                            $val = IRFunctions::displayPriceIr($products[$product_idx][$key], true);
                        }
                        $content[$i][] = $val;
                    }
                    $product_idx++;
                }
            }
        }
        require_once(_PS_MODULE_DIR_.'ordersreport/classes/IRFunctions.php');
        $curr_date = IRFunctions::getCurrentDateTime($this->context->smarty);
        $export_date = $this->l('Created on').': '.$curr_date;

        $this->context->smarty->assign(
            array(
            'export_headers' => $headers,
            'export_content' => $content,
            'export_date' => $export_date,
            'search' => $search_criteria,
            'shops' => $shop_criteria,
            'text_delimiter' => $text_delimiter
            )
        );
        $this->layout = _PS_MODULE_DIR_.'/ordersreport/views/templates/admin/export.tpl';

    }

    /*
    This function is used to to generate shop criteria string to be put to CSV file
    */
    private function getCriteriaShops($shops)
    {
        $result = '';
        if ($this->multistore) {
            $result .= $this->l('Shops').': ';
            $n = count($shops);
            if (isset($shops)) {
                for ($i = 0; $i < $n; ++$i) {
                    $result .= $shops[$i];
                    if ($i < $n - 1) {
                        $result .= ', ';
                    }
                }
            } else {
                $result .= $this->l('All shops');
            }
        }
        return $result;
    }

    /*
    get string with search fields to fit nicely to a column of a CSV file
    */
    private function getCriteriaSearch($search)
    {
        FilterParser::setFriendlyFields($search, (int)$this->context->language->id);

        $filtered = false;
        $result = $this->l('Filter by').' ';
        if (isset($search['id_order'])) {
            $result .= $this->l('ID', 'ordersreport').':'.$search['id_order'].' ';
            $filtered = true;
        }
        if (isset($search['date_from'])) {
            $result .= $this->l('Date from').':'.$search['date_from'].' ';
            $filtered = true;
        }
        if (isset($search['date_to'])) {
            $result .= $this->l('Date to').':'.$search['date_to'].' ';
            $filtered = true;
        }
        if (isset($search['delivery_from'])) {
            $result .= $this->l('Delivery from').':'.$search['delivery_from'].' ';
            $filtered = true;
        }
        if (isset($search['delivery_to'])) {
            $result .= $this->l('Delivery to').':'.$search['delivery_to'].' ';
            $filtered = true;
        }
        if (isset($search['order_state'])) {
            $result .= $this->l('Order status').':'.$search['order_state'].' ';
            $filtered = true;
        }
        if (isset($search['number'])) {
            $result .= $this->l('Order number').':`'.$search['number'].'` ';
            $filtered = true;
        }
        if (isset($search['reference'])) {
            $result .= $this->l('Order reference').':`'.$search['reference'].'` ';
            $filtered = true;
        }
        if (isset($search['customer'])) {
            $result .= $this->l('Customer').':`'.$search['customer'].'` ';
            $filtered = true;
        }
        if (isset($search['address_customer'])) {
            $result .= $this->l('Address name').':`'.$search['address_customer'].'` ';
            $filtered = true;
        }
        if (isset($search['address_company'])) {
            $result .= $this->l('Company').':`'.$search['address_company'].'` ';
            $filtered = true;
        }
        if (isset($search['address_country'])) {
            $result .= $this->l('Country').':`'.$search['address_country'].'` ';
            $filtered = true;
        }
        if (isset($search['carrier'])) {
            $result .= $this->l('Carrier').':`'.$search['carrier'].'` ';
            $filtered = true;
        }
        if (isset($search['invoice_number'])) {
            $result .= $this->l('Invoice number').':`'.$search['invoice_number'].'` ';
            $filtered = true;
        }
        if (isset($search['payment'])) {
            $result .= $this->l('Payment').':`'.$search['payment'].'` ';
            $filtered = true;
        }
        if (isset($search['currency'])) {
            $result .= $this->l('Currency').':'.$search['currency'].' ';
            $filtered = true;
        }
        if (!$filtered) {
            $result .= $this->l('All orders');
        }
        return $result;
    }

    public function initProcess()
    {
        parent::initProcess();
        if (Tools::getIsset('exportordersreport')) {
            $this->action = 'export';
        }
    }

    public function postProcess()
    {
        // CUSTOM FILTERS
        if (Tools::getIsset('submit_save_orderstatefilter')) {
            $this->g_bSave_OrderStateFilter = true;
            return true;
        }
        // GENERATE REPORT
        if (Tools::getIsset('generateReport')) {
            $this->print_report = true;
        }
        if (Tools::getIsset('generatePPReport')) {
            $this->print_ppreport = true;
        }
        // BULK
        if (Tools::getIsset('submitBulk'.$this->action_bulk_inv.$this->table)) {
            $this->bulk_inv = true;
        }
        parent::postProcess();
        $this->addJS(_PS_MODULE_DIR_.'/ordersreport/views/js/prodsearch.js');
    }

    public function getList(
        $id_lang,
        $order_by = null,
        $order_way = null,
        $start = 0,
        $limit = null,
        $id_lang_shop = false
    ) {
        $this->_where .= ' AND osl.`id_lang` = '.(int)$this->context->language->id;
        // CUSTOM FILTERS
        if (Tools::strlen($this->g_sCfg_OrderStateFilter) > 0) {
            $this->_where .= ' AND os.`id_order_state` IN ('.$this->g_sCfg_OrderStateFilter.')';
        }
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);

        if ($this->export_csv) {
            require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');
            $this->csvExport();
        }
        if ($this->bulk_inv) {
            $this->bulkCreateInvoices();
            
        }
    }

    private function bulkCreateInvoices()
    {
        $this->bulk_inv = false;
        if (Tools::getIsset('submitFilter')) {
            return;
        }
        if (is_array($this->_list)
            && !empty($this->_list)
            && is_array($this->boxes)
            && !empty($this->boxes)) {
            $orders = array();
            foreach ($this->_list as $row) {
                if (in_array($row['id_order'], $this->boxes)) {
                    $order = new Order($row['id_order']);
                    if (!Validate::isLoadedObject($order)) {
                        throw new PrestaShopException(
                            'Can\'t load Order object for id: '.$row['id_order']
                        );
                    }
                    $orders[] = $order;
                }
            }
            foreach ($orders as $order) {
                $order->setInvoice();
            }
        }
    }

    /*
     * PRESTA SHOP VERSION 1500, 1531, 1619, 1700, 1702, ..
     */
    private function getPrestaShopVersion()
    {
        $psver = str_replace('.', '', _PS_VERSION_);
        return $psver;
    }

    /*
    * this function must be overriden for compatibility with PS 1.5
    */
    public function processAdd()
    {
        if (!isset($this->className) || empty($this->className)) {
            return false;
        }
        parent::processAdd();
    }

    /*
    * some versions of PS show error 'object cannot be loaded' on click on a list row
    * set display action so that controller does not try to load object for edit or view
    */
    public function initPageHeaderToolbar()
    {
        $this->display = 'list';
        parent::initPageHeaderToolbar();
    }

    /*
    * avoid to display caption Edit when click on row
    */
    public function initToolbarTitle()
    {
        $this->display = 'list';
        parent::initToolbarTitle();
    }

    protected function l($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if ($this->psver17) {
            $class = Tools::substr(get_class($this), 0, -10);
            return Translate::getAdminTranslation($string, $class, false, true);
        } else {
            return parent::l($string);
        }
    }
}
