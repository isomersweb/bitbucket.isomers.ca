{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

<span class="btn-group-action">
	<span class="btn-group">
           <a class="btn btn-default" target="_blank" href="{$link->getAdminLink('AdminPdf')|escape:'htmlall':'UTF-8'}&submitAction=generateInvoicePDF&id_order_invoice={$order_invoice->id|escape:'htmlall':'UTF-8'}">
			<img src="../img/admin/tab-invoice.gif" alt="invoice" />
		</a>
	</span>
</span>