{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

{foreach from=$export_headers item=header}"{$header|escape:'html':'UTF-8'}"{$text_delimiter|escape:'html':'UTF-8'}{/foreach}

{foreach from=$export_content item=line}
{foreach from=$line item=content}"{$content|escape:'html':'UTF-8'}"{$text_delimiter|escape:'html':'UTF-8'}{/foreach}

{/foreach}

"{$export_date|escape:'html':'UTF-8'}"
"{$search|escape:'html':'UTF-8'}"
"{$shops|escape:'html':'UTF-8'}"