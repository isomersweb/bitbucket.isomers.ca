{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

<style type="text/css">
	{literal}
		table {width: 100%, background-color:#FFF}
		tr {line-height:5px; font-size: 6pt}
		td {font-weight: bold; border-bottom: 1px solid #dddddd}
		div {font-size: 6pt; color: #333}
		h4 {font-size: 9pt; font-weight: bold;}
		h5 {font-size: 7pt; font-weight: bold;}
	{/literal}
</style>

<div>
<h4>{l s='ORDERS - REPORT' mod='ordersreport'}</h4>
<br>
{l s='Created on' mod='ordersreport'}:&nbsp;{$report_date|escape:'html':'UTF-8'}
<br><br>
&nbsp;&nbsp;{l s='Filter by' mod='ordersreport'}

{if $filtered eq false}
	:&nbsp;{l s='All orders' mod='ordersreport'}
{else}

{assign var= "filters" value = count($search)}

{if isset($search['date_from'])}
	&nbsp;{l s='Date from' mod='ordersreport'}:&nbsp;{$search['date_from']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
	
{if isset($search['date_to'])}
	&nbsp;{l s='Date to' mod='ordersreport'}:&nbsp;{$search['date_to']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{if isset($search['order_state'])}
	&nbsp;{l s='Order state' mod='ordersreport'}:&nbsp;{$search['order_state']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['reference'])}
	&nbsp;{l s='Order reference' mod='ordersreport'}:&nbsp;"{$search['reference']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
	
{if isset($search['customer'])}
	&nbsp;{l s='Customer name' mod='ordersreport'}:&nbsp;"{$search['customer']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{if isset($search['address_customer'])}
	&nbsp;{l s='Address name' mod='ordersreport'}:&nbsp;"{$search['address_customer']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['address_company'])}
	&nbsp;{l s='Company' mod='ordersreport'}:&nbsp;"{$search['address_company']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['address_country'])}
	&nbsp;{l s='Country' mod='ordersreport'}:&nbsp;"{$search['address_country']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['carrier'])}
	&nbsp;{l s='Carrier' mod='ordersreport'}:&nbsp;"{$search['carrier']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
    
{if isset($search['payment'])}
	&nbsp;{l s='Payment method' mod='ordersreport'}:&nbsp;"{$search['payment']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['currency'])}
	&nbsp;{l s='Currency' mod='ordersreport'}:&nbsp;{$search['currency']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{/if}
    
<br>

{if $multistore}
    {l s='Shops' mod='ordersreport'}:
	{if (isset($shops)) }
		{foreach from=$shops item=shop name=i}
			{$shop|escape:'html':'UTF-8'}
			{if !$smarty.foreach.i.last}
                ,
			{/if}
		{/foreach}
	{else}
		{l s='All shops' mod='ordersreport'}
	{/if}
{/if}

<br><br>

{if $orders}
<table class="table">
	<tr style="line-height:4px; background-color: #ddd;">
		<td style="text-align: left">
			{l s='Order ref.' mod='ordersreport'}
		</td>
		<td style="text-align: left">
			{l s='Customer' mod='ordersreport'}
		</td>
        {if isset($fields['addr_name'])}
		  <td style="text-align: left">
			 {l s='Address name' mod='ordersreport'}
		  </td>
        {/if}
        {if isset($fields['addr_company'])}
		  <td style="text-align: left">
			 {l s='Company' mod='ordersreport'}
		  </td>
        {/if}
        {if isset($fields['addr_country'])}
		  <td style="text-align: center">
			 {l s='Country' mod='ordersreport'}
		  </td>
        {/if}
        {if isset($fields['carrier'])}
		  <td style="text-align: center">
			 {l s='Carrier' mod='ordersreport'}
		  </td>
        {/if}
		<td style="text-align: center">
			{l s='Date' mod='ordersreport'}
		</td>
		<td style="text-align: right;">
			{l s='Paid tax excl.' mod='ordersreport'}
		</td>
		<td style="text-align: right">
			 {l s='Paid tax incl.' mod='ordersreport'} 
		</td>
        {if isset($fields['show_curr'])}
		  <td style="text-align: center">
			  {l s='Currency' mod='ordersreport'}
		  </td>
        {/if}
        {if isset($fields['payment'])}
            <td style="text-align: center">
			   {l s='Payment method' mod='ordersreport'}
		    </td>
        {/if}
		{if $multistore}
			<td style="text-align: right">
				{l s='Shop' mod='ordersreport'} 
			</td>
		{/if}
	</tr>

	{section name=i loop=$orders}
		<tr>
                    <td style="text-align: left">{$orders[i]['reference']|escape:'html':'UTF-8'}</td>
			<td style="text-align: left">{$orders[i]['customer']|escape:'html':'UTF-8'}</td>
            {if isset($fields['addr_name'])}
                <td style="text-align: left">{$orders[i]['address_customer']|escape:'html':'UTF-8'}</td>
            {/if}
            {if isset($fields['addr_company'])}
			    <td style="text-align: left">{$orders[i]['address_company']|escape:'html':'UTF-8'}</td>
	    {/if}
            {if isset($fields['addr_country'])}
			    <td style="text-align: center">{$orders[i]['country_code']|escape:'html':'UTF-8'}</td>
	    {/if}
            {if isset($fields['carrier'])}
			    <td style="text-align: center">{$orders[i]['carrier']|escape:'html':'UTF-8'}</td>
	    {/if}
            <td style="text-align: center">{dateFormat date=$orders[i]['date_add'] full=0}</td>
			<td style="text-align: right">{$functions->displayPriceIr($orders[i]['total_excl'])|escape:'html':'UTF-8'}</td>
			<td style="text-align: right">{$functions->displayPriceIr($orders[i]['total_incl'])|escape:'html':'UTF-8'}</td>
            {if isset($fields['show_curr'])}
			     <td style="text-align: center">{$orders[i]['currency_code']|escape:'html':'UTF-8'}</td>
            {/if}
            {if isset($fields['payment'])}
                <td style="text-align: center">{$orders[i]['payment']|escape:'html':'UTF-8'}</td>
            {/if}
			{if $multistore}
				<td style="text-align: right">{$orders[i]['shop_name']|escape:'html':'UTF-8'}</td>
			{/if}
		</tr>
	{/section}
	<tr><td style="border-bottom: none"></td></tr>
</table>
{/if}

{$amounts|escape:'quotes':'UTF-8'}
	
</div>