{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

<style type="text/css">
	{literal}
		table {width: 100%, background-color:#FFF}
        .main-row {border-top: 1px solid #dddddd; border-bottom: 1px solid #dddddd;background-color: #f5f5f5;}
		tr {line-height:5px; font-size: 6pt}
		td {font-weight: bold;line-height: 5px; font-size: 8pt}
        div {font-size: 6pt; color: #333}
		h4 {font-size: 9pt; font-weight: bold;}
		h5 {font-size: 7pt; font-weight: bold;}
	{/literal}
</style>

<div>
<h4>{l s='PICK & PACK - REPORT' mod='ordersreport'}</h4>
<br>
{l s='Created on' mod='ordersreport'}:&nbsp;{$report_date|escape:'html':'UTF-8'}
<br><br>
&nbsp;&nbsp;{l s='Filter by' mod='ordersreport'}

{if $filtered eq false}
	:&nbsp;{l s='All orders' mod='ordersreport'}
{else}

{assign var= "filters" value = count($search)}

{if isset($search['date_from'])}
	&nbsp;{l s='Date from' mod='ordersreport'}:&nbsp;{$search['date_from']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
	
{if isset($search['date_to'])}
	&nbsp;{l s='Date to' mod='ordersreport'}:&nbsp;{$search['date_to']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{if isset($search['order_state'])}
	&nbsp;{l s='Order state' mod='ordersreport'}:&nbsp;{$search['order_state']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['reference'])}
	&nbsp;{l s='Order reference' mod='ordersreport'}:&nbsp;"{$search['reference']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
	
{if isset($search['customer'])}
	&nbsp;{l s='Customer name' mod='ordersreport'}:&nbsp;"{$search['customer']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{if isset($search['address_customer'])}
	&nbsp;{l s='Address name' mod='ordersreport'}:&nbsp;"{$search['address_customer']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['address_company'])}
	&nbsp;{l s='Company' mod='ordersreport'}:&nbsp;"{$search['address_company']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['address_country'])}
	&nbsp;{l s='Country' mod='ordersreport'}:&nbsp;"{$search['address_country']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['carrier'])}
	&nbsp;{l s='Carrier' mod='ordersreport'}:&nbsp;"{$search['carrier']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
    
{if isset($search['payment'])}
	&nbsp;{l s='Payment method' mod='ordersreport'}:&nbsp;"{$search['payment']|escape:'html':'UTF-8'}"
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}

{if isset($search['currency'])}
	&nbsp;{l s='Currency' mod='ordersreport'}:&nbsp;{$search['currency']|escape:'html':'UTF-8'}
    {$filters = $filters - 1}
    {if $filters gt 0}
        ,
    {/if}
{/if}
{/if}
    
<br>

{if $multistore}
    {l s='Shops' mod='ordersreport'}:
	{if (isset($shops)) }
		{foreach from=$shops item=shop name=i}
			{$shop|escape:'html':'UTF-8'}
			{if !$smarty.foreach.i.last}
                ,
			{/if}
		{/foreach}
	{else}
		{l s='All shops' mod='ordersreport'}
	{/if}
{/if}

<br><br>

{if $orders}
<table class="table main-row">
	<tr style="line-height:4px; background-color: #555;">
		<td style="text-align: center;color:#FFF;" width="100%">
			{l s='MASTER PICK' mod='ordersreport'}
		</td>        
	</tr>
	<tr style="line-height:4px; background-color: #ddd;">
		<td style="text-align: left" width="5%">
			{l s='Qty.' mod='ordersreport'}
		</td>
		<td style="text-align: left" width="40%">
			{l s='Product' mod='ordersreport'}
		</td>
		<td style="text-align: left" width="35%">
			 {l s='Volume' mod='ordersreport'}
   	    </td>
		<td style="text-align: left" width="20%">
			 {l s='Unit Weight' mod='ordersreport'}
   	    </td>
	</tr>
    {section name=i loop=$products_order_list}
        <tr {if $smarty.section.i.index is even}style="background-color: #FFF;"{/if}>
            <td style="text-align: left; font-size: 7pt; border-right: 1px solid #dddddd;" width="5%">X {$products_order_list[i]['product_quantity']|escape:'html':'UTF-8'}</td>
            <td style="text-align: left; font-size: 7pt;" width="40%">   {$products_order_list[i]['product_name']|escape:'html':'UTF-8'}</td>
            <td style="text-align: left; font-size: 7pt;" width="35%">{$products_order_list[i]['description_short']|escape:'html':'UTF-8'}</td>
            <td style="text-align: left; font-size: 7pt;" width="20%">{$products_order_list[i]['product_weight']|string_format:"%.3f"|escape:'html':'UTF-8'}</td>
        </tr>
    {/section}    
 </table>
 <br><br>
<table class="table">
	<tr style="line-height:4px; background-color: #555;">
		<td style="text-align: center;color:#FFF;" width="100%">
			{l s='ORDER PACK' mod='ordersreport'}
		</td>        
	</tr>
	<tr style="line-height:4px; background-color: #ddd;">
		<td style="text-align: left" width="13%">
			{l s='Order ref.' mod='ordersreport'}
		</td>
		<td style="text-align: left" width="20%">
			{l s='Customer' mod='ordersreport'}
		</td>
		<td style="text-align: left" width="27%">
			 {l s='Address' mod='ordersreport'}
   	    </td>
		<td style="text-align: left" width="20%">
			 {l s='Carrier' mod='ordersreport'}
   	    </td>
		<td style="text-align: center" width="10%">
			 {l s='Total Order Weight' mod='ordersreport'}
   	    </td>
		<td style="text-align: center" width="10%">
			 {l s='Date Due' mod='ordersreport'}
   	    </td>
        
	</tr>

	{section name=i loop=$orders}
		<tr>
            
            <table width="100%" class="main-row">
            <tr>
            <td style="text-align: left;" width="13%">{$orders[i]['reference']|escape:'html':'UTF-8'}</td>
			<td style="text-align: left" width="20%">{$orders[i]['customer']|escape:'html':'UTF-8'}</td>
            <td style="text-align: left" width="27%">{$orders[i]['full_address']|escape:'html':'UTF-8'}</td>
		    <td style="text-align: left" width="20%">{$orders[i]['carrier']|escape:'html':'UTF-8'}</td>
		    <td style="text-align: center" width="10%">{$orders[i]['total_product_weight']|string_format:"%.3f"|escape:'html':'UTF-8'}</td>
		    <td style="text-align: center" width="10%">{$orders[i]['date_due']|escape:'html':'UTF-8'}</td>            
            </tr>
            </table>
            <table width="100%">
            {*<tr>
            <td><b> {$products_list[$orders[i]['id_order']]|@print_r}### {$orders[i]} ### {$orders[i]['id_order']}</b></td>
            <tr>*}
            {assign var="id_order" value = $orders[i]['id_order']}
            {section name=j loop=$products_list[$id_order]}
                <tr>
                    <td style="text-align: left; font-size: 7pt; border-right: 1px solid #dddddd;" width="5%">X {$products_list[$id_order][j]['product_quantity']|escape:'html':'UTF-8'}</td>
                    <td style="text-align: left; font-size: 7pt;" width="40%">   {$products_list[$id_order][j]['product_name']|escape:'html':'UTF-8'}</td>
                    <td style="text-align: left; font-size: 7pt;" width="35%">{$products_list[$id_order][j]['description_short']|escape:'html':'UTF-8'}</td>
                    <td style="text-align: left; font-size: 7pt;" width="20%">Unit weight: {$products_list[$id_order][j]['product_weight']|string_format:"%.3f"|escape:'html':'UTF-8'}</td>
                </tr>
            {/section}
            </table>
            
		</tr>
	{/section}
	<tr><td style="border-bottom: none"></td></tr>
</table>
{/if}
{* kang-removing this, its not required 10/102/2018
*{$amounts|escape:'quotes':'UTF-8'}
*}	
</div>