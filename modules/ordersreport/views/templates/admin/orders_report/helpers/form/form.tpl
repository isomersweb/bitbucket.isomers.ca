{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

{extends file="helpers/form/form.tpl"}

{block name="input"}

{$orderstatefilter|escape:'quotes':'UTF-8'}
{$currencyfilter|escape:'quotes':'UTF-8'}
{$amounts|escape:'quotes':'UTF-8'}

{/block}
