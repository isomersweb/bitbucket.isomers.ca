<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2017 Denago d.o.o.
*  @license   Proprietary license
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class OrdersReport extends Module
{
    private $g_aHooks = array(
        'leftColumn',
        'displayBackOfficeHeader'
    );

    public function __construct()
    {
        $this->name = 'ordersreport';
        $this->tab = 'administration';
        $this->version = '1.4.2';
        $this->author = 'DENAGO';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = '18f3f55391b155702e1479b91f8e941e';

        $this->push_filename = _PS_CACHE_DIR_.'push/activity';
        $this->allow_push = true;

        $max_version = _PS_VERSION_;
        $min_version = '1.5.1.0';
        /**
        * This is required beceause in Module::checkCompliancy() of the 1.5.1.0
        * operator >= is used to check for max version!
        **/
        if (version_compare(_PS_VERSION_, '1.6.0', '<') === true) {
            $max_version = '1.6.0';
        }
        $this->ps_versions_compliancy = array('min' => $min_version, 'max' => $max_version);

        parent::__construct();

        $this->displayName = $this->l('Orders report');
        $this->description = $this->l(
            'Displays the list of orders found by search parameters.
            Summed order amounts are displayed for the listed orders. Exportable to PDF or CSV file.'
        );
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall `'.$this->name.'` module ?');
    }

    public function install()
    {
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            if (Shop::isFeatureActive()) {
                Shop::setContext(Shop::CONTEXT_ALL);
            }
        }
        // install tab for 1.5, 1.6
        if (version_compare(_PS_VERSION_, '1.7', '<') === true) {
            if (!$this->InstallTab()) {
                return false;
            }
        // install tab for 1.7
        } else if (version_compare(_PS_VERSION_, '1.7', '>=') === true) {
            if (!$this->InstallTab17()) {
                return false;
            }
        }
        if (!parent::install()
            || !Configuration::updateValue('DNG_REPORT_ORDER_LIST', '1')
            || !Configuration::updateValue('DNG_ORDER_WRAPPING', '1')
            || !Configuration::updateValue('DNG_ORDER_PAYMENT', '1')
            || !Configuration::updateValue('DNG_ORDER_CARRIER', '1')
            || !Configuration::updateValue('DNG_ORDER_DELIVERY_NAME', '1')
            || !Configuration::updateValue('DNG_ORDER_DELIVERY_COMPANY', '0')
            || !Configuration::updateValue('DNG_ORDER_DELIVERY_COUNTRY', '0')
            || !Configuration::updateValue('DNG_ORDER_DELIVERY_DATE', '0')
            || !Configuration::updateValue('DNG_ORDER_INV_NUM', '0')
            || !Configuration::updateValue('DNG_ORDER_CURRENCY', '1')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PREF', '1')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PNAME', '1')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PEAN', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PUPC', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PQUA', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PPRI', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PSUP', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_ADDR', '1')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_PHONE', '0')
            || !Configuration::updateValue('DNG_ORDER_EXPORT_EMAIL', '0')
            || !Configuration::updateValue('DNG_ORDREP_ORDER_STATE_FILTER', '')
            || !Configuration::updateValue('DNG_ORDREP_SCRIPT_TIMEOUT', '60')
            ) {
            return false;
        }
        foreach ($this->g_aHooks as $hook) {
            if (!$this->registerHook($hook)) {
                return false;
            }
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallTab()
            || !Configuration::deleteByName('DNG_REPORT_ORDER_LIST')
            || !Configuration::deleteByName('DNG_ORDER_WRAPPING')
            || !Configuration::deleteByName('DNG_ORDER_PAYMENT')
            || !Configuration::deleteByName('DNG_ORDER_CARRIER')
            || !Configuration::deleteByName('DNG_ORDER_DELIVERY_NAME')
            || !Configuration::deleteByName('DNG_ORDER_DELIVERY_COMPANY')
            || !Configuration::deleteByName('DNG_ORDER_DELIVERY_COUNTRY')
            || !Configuration::deleteByName('DNG_ORDER_CURRENCY')
            || !Configuration::deleteByName('DNG_ORDER_DELIVERY_DATE')
            || !Configuration::deleteByName('DNG_ORDER_INV_NUM')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PREF')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PNAME')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PEAN')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PUPC')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PQUA')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PPRI')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PSUP')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_ADDR')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_PHONE')
            || !Configuration::deleteByName('DNG_ORDER_EXPORT_EMAIL')
            || !Configuration::deleteByName('DNG_ORDREP_ORDER_STATE_FILTER')
            || !Configuration::deleteByName('DNG_ORDREP_SCRIPT_TIMEOUT')
            ) {
            return false;
        }
        return true;
    }

    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminOrdersReport';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Orders Report';
        }
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminOrders');
        $tab->module = $this->name;
        return $tab->add();
    }

    public function installTab17()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminOrdersReport';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Orders Report';
        }
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentOrders');
        $tab->module = $this->name;
        return $tab->add();
    }

    public function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminOrdersReport');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();

            return true;
        }
        return false;
    }
}
