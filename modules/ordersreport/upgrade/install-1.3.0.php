<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

function upgrade_module_1_3_0($object)
{
    $object = $object;
    if (!Configuration::updateValue('DNG_REPORT_ORDER_LIST', '1')
        || !Configuration::updateValue('DNG_ORDER_WRAPPING', '1')
        || !Configuration::updateValue('DNG_ORDER_PAYMENT', '1')
        || !Configuration::updateValue('DNG_ORDER_CARRIER', '1')
        || !Configuration::updateValue('DNG_ORDER_DELIVERY_NAME', '1')
        || !Configuration::updateValue('DNG_ORDER_DELIVERY_COMPANY', '1')
        || !Configuration::updateValue('DNG_ORDER_DELIVERY_COUNTRY', '1')
        || !Configuration::updateValue('DNG_ORDER_CURRENCY', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PREF', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PNAME', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PEAN', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PUPC', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PQUA', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PPRI', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PSUP', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_ADDR', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PHONE', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_EMAIL', '1')
        || !Configuration::updateValue('DNG_ORDER_STATE_FILTER', '')) {
        return false;
    }
    return true;
}
