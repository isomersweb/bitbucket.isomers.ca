<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class OrderList
{
    private $orders = array();
    private $totals = array();
    private $search_fields = array();
    private $shops = array();
    private $functions;
    private $amounts;
    private $multistore;
    private $g_aOrderStateFilter = array();
	// $_aOrderStateFilter 8 th argument of contructor
    public function __construct(array $orders, $multistore, $search, $fields, $shops, $functions, $amounts)
    {
        $this->orders = $orders;
        $this->amounts = $amounts;
        $this->multistore = $multistore;
        $this->search = $search;
        $this->fields = $fields;
        $this->shops = $shops;
        $this->functions = $functions;
        //$this->g_aOrderStateFilter = $_aOrderStateFilter;

        require_once(_PS_MODULE_DIR_.'ordersreport/classes/FilterParser.php');
    }

    public function getList()
    {
        return $this->orders;
    }

    public function getAmounts()
    {
        return $this->amounts;
    }

    public function getMultistore()
    {
        return $this->multistore;
    }

    public function getShops()
    {
        return $this->shops;
    }

    public function getSearch()
    {
        return $this->search;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getFunctions()
    {
        return $this->functions;
    }

    public function aGetOrderStateFilter()
    {
        return $this->g_aOrderStateFilter;
    }
}
