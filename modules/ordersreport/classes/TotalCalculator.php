<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class TotalCalculator
{
    public static function getTotalValues($filter, $shop_filter, $language_id, $_sOrderStateFilter)
    {
        $where_clause = ' WHERE 1';
        $fields = FilterParser::getSearchFields($filter);
        $where_clause .= FilterParser::getWhereClause($fields);
        $join_customer = '';
        if (isset($fields['customer'])) {
             $join_customer .= ' LEFT JOIN '._DB_PREFIX_.'customer c ON a.`id_customer` = c.`id_customer`';
        }
        $join_addres = '';
        if (isset($fields['address_customer'])
            || isset($fields['address_company'])
            || isset($fields['address_country'])) {
            $join_addres .= ' LEFT JOIN '._DB_PREFIX_.'address ad ON a.`id_address_delivery` = ad.`id_address`';
        }
        $join_carrier = '';
        if (isset($fields['carrier'])) {
            $join_carrier .= ' LEFT JOIN '._DB_PREFIX_.'carrier car ON a.`id_carrier` = car.`id_carrier`';
        }
        $join_country = '';
        if (isset($fields['address_country'])) {
            $join_country .= ' LEFT JOIN '._DB_PREFIX_.'country con ON ad.`id_country` = con.`id_country`';
        }
        $join_state = '';
        if (isset($fields['order_state'])) {
            $join_state .= ' LEFT JOIN '._DB_PREFIX_.'order_state os ON a.`current_state` = os.`id_order_state`
                LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON os.`id_order_state` = osl.`id_order_state`';
            $where_clause .= ' AND osl.`id_lang` = '.(int)$language_id;
        }
        $join_shop = '';
        if (!empty($shop_filter)) {
            $join_shop = ' LEFT JOIN '._DB_PREFIX_.'shop shop ON a.`id_shop` = shop.`id_shop` ';
            $where_clause .= pSQL($shop_filter);
        }
        $join_curr =  ' LEFT JOIN '._DB_PREFIX_.'currency cur ON a.`id_currency` = cur.`id_currency`';

        $precision = Configuration::get('PS_PRICE_DISPLAY_PRECISION');
        if (!$precision) {
            $precision = 2;
        }
        // CUSTOM FILTERS
        if (Tools::strlen($_sOrderStateFilter) > 0) {
            $where_clause .= ' AND a.`current_state` IN ('.$_sOrderStateFilter.')';
        }
        $sql_orders = 'SELECT
            SUM(a.`total_products`) as products,
            SUM(a.`total_products_wt`) as products_wt,
            SUM(a.`total_discounts_tax_excl`) as discount,
            SUM(a.`total_discounts_tax_incl`) as discount_wt,
            SUM(a.`total_paid_tax_excl`) as total,
            SUM(a.`total_paid_tax_incl`) as total_wt,
            SUM(a.`total_shipping_tax_excl`) as shipping,
            SUM(a.`total_shipping_tax_incl`) as shipping_wt,
            SUM(a.`total_wrapping_tax_excl`) as wrapping,
            SUM(a.`total_wrapping_tax_incl`) as wrapping_wt,
            cur.`iso_code`, cur.`id_currency` AS id_currency
            FROM '._DB_PREFIX_.'orders a '
            .$join_state.$join_customer.$join_addres.$join_carrier.$join_country.$join_curr.$join_shop.$where_clause.
            ' GROUP BY cur.`iso_code` ';
            /*
            * variable $where_clause is set securely in FilterParser::getWhereClause() !
            */
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql_orders);
    }
}
