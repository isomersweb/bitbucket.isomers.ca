<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class FilterParser
{
    public static function getWhereClause($fields)
    {
        $where_clause = '';
        foreach ($fields as $key => $value) {
            switch ($key) {
                 case 'id_order':
                    $where_clause .= ' AND a.`id_order` IN '.pSQL($value);
                    break;
                case 'date_from':
                    $where_clause .= ' AND a.`date_add` >= \''.pSQL(Tools::dateFrom($value)).'\'';
                    break;
                case 'date_to':
                    $where_clause .= ' AND a.`date_add` <= \''.pSQL(Tools::dateTo($value)).'\'';
                    break;
                case 'delivery_from':
                    $where_clause .= ' AND a.`delivery_date` >= \''.pSQL(Tools::dateFrom($value)).'\'';
                    break;
                case 'delivery_to':
                    $where_clause .= ' AND a.`delivery_date` > 0 AND a.`delivery_date` <= \''.pSQL(Tools::dateTo($value)).'\'';
                    break;
                case 'currency':
                    $where_clause .= ' AND cur.`id_currency` = '.(int)$value;
                    break;
                case 'customer':
                    $where_clause .= ' AND CONCAT(c.`firstname`, " ",c.`lastname`) LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'address_customer':
                    $where_clause .= ' AND CONCAT(ad.`firstname`, " ",ad.`lastname`) LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'carrier':
                    $where_clause .= ' AND car.`name` LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'address_company':
                    $where_clause .= ' AND ad.`company` LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'address_country':
                    $where_clause .= ' AND con.`iso_code` = \''.pSQL($value).'\'';
                    break;
                case 'payment':
                    $where_clause .= ' AND `payment` LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'order_state':
                    $where_clause .= ' AND os.`id_order_state` = '.(int)$value;
                    break;
                case 'reference':
                    $where_clause .= ' AND a.`reference` LIKE \'%'.pSQL($value).'%\'';
                    break;
                case 'invoice_number':
                    $where_clause .= ' AND `invoice_number` LIKE \'%'.pSQL($value).'%\'';
                    break;
                default:
                    $where_clause .= ' AND '.pSQL($key).' LIKE \'%'.pSQL($value).'%\'';
            }
        }
        return $where_clause;
    }

    public static function getOrder($sql)
    {
        $ret = '';
        $pos = strpos($sql, 'ORDER BY') + Tools::strlen('ORDER BY');
        if ($pos === false) {
            return $ret;
        }

        $par = '`';
        $pos1 = strpos($sql, $par, $pos);
        if ($pos1 === false) {
            /*
            * in some versionos of PS there is no '`' arround the order field name
            */
            $par = ' ';
            $pos1 = strpos($sql, ' ', $pos);
            if ($pos1 === false) {
                return $ret;
            }
        }

        $pos2 = strpos($sql, $par, $pos1 + 1);
        if ($pos2 === false) {
            return $ret;
        }

        $ret = Tools::substr($sql, $pos1, $pos2 - $pos1 + 1);

        $pos = strpos($sql, 'asc', $pos2);
        if ($pos) {
            $ret .= ' ASC';
        } else {
            $ret .= ' DESC';
        }

        return $ret;
    }

    /*
    from SQL string extract the array of fields values used to filter the invoice list
    this is used also to generate search criteria text to be displayed in PDF and in CSV
    */
    public static function getSearchFields($filter)
    {
        $results = array();
        $filter_keys = array(
                        'id_order' => 'a.`id_order` IN (',
                        'date_from' => 'a.`date_add` >= ',
                        'date_to' => 'a.`date_add` <= ',
                        'reference' => '`reference` LIKE ',
                        'currency' => 'cur.`id_currency` = ',
                        'customer' => '`customer` LIKE ',
                        'payment' => '`payment` LIKE ',
                        'order_state' => 'os.`id_order_state` = ',
                        'address_customer' => '`address_customer` LIKE ',
                        'address_company' => '`address_company` LIKE ',
                        'address_country' => 'con.`iso_code` = ',
                        'carrier' => '`carrier` LIKE ',
                        'delivery_from' => 'a.`delivery_date` >= ',
                        'delivery_to' => 'a.`delivery_date` <= ',
                        'invoice_number' =>'`invoice_number` LIKE '
        );

        $filter_array = explode('AND', $filter);

        foreach ($filter_array as $filter) {
            foreach ($filter_keys as $key => $val) {
                $pos = strpos($filter, $val);
                if ($pos) {
                    $raw_result = Tools::substr($filter, Tools::strlen($val));
                    switch ($key)
                    {
                        case 'id_order':
                            $results[$key] = $raw_result;
                        break;
                        case 'date_from':
                        case 'date_to':
                        case 'delivery_from':
                        case 'delivery_to':
                            $pos1 = strpos($raw_result, "'");
                            if ($pos1) {
                                $pos2 = strpos($raw_result, ' ', $pos1 + 1);
                                if ($pos2) {
                                    $result = Tools::substr($raw_result, $pos1 + 1, $pos2 - $pos1 - 1);
                                }
                            }
                            $results[$key] = $result;
                            break;
                        case 'order_state':
                        case 'currency':
                            $results[$key] = $raw_result;
                            break;
                        case 'address_country':
                            $results[$key] = str_replace('\'', '', trim($raw_result));
                            break;
                        default:
                            $pos1 = strpos($raw_result, '%');
                            if ($pos1) {
                                $pos2 = strpos($raw_result, '%', $pos1 + 1);
                                if ($pos2) {
                                    $result = Tools::substr($raw_result, $pos1 + 1, $pos2 - $pos1 - 1);
                                }
                            }
                            $results[$key] = $result;
                    }
                }
            }
        }
        return $results;
    }

    /*
    from SQL string extract the array of shops used to filter the invoice list
    */
    public static function getShops($where_shop)
    {
        $results = array();
        $filtered = false;
        $pos1 = strpos($where_shop, '(');
        $pos2 = strpos($where_shop, ')');
        $shops = Tools::substr($where_shop, $pos1 + 1, $pos2 - $pos1 - 1);

        $shops_array = array();
        if (strpos($shops, ',')) {
            $shops_array = explode(',', $shops);
        } else {
            array_push($shops_array, $shops);
        }

        $sql = 'SELECT id_shop, name FROM '._DB_PREFIX_.'shop WHERE active = 1 AND deleted = 0';

        $active_shops = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(pSQL($sql));

        foreach ($active_shops as $row) {
            if (array_search($row['id_shop'], $shops_array) === false) {
                $filtered = true;
            } else {
                array_push($results, $row['name']);
            }
        }

        if (!$filtered) {
            $results = null;
        }

        return $results;
    }

    public static function setFriendlyFields(&$search, $id_lang)
    {
        if (isset($search['order_state'])) {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
                'SELECT name FROM `'._DB_PREFIX_.'order_state_lang` WHERE id_order_state = '.$search['order_state'].
                ' AND `id_lang` = '.(int)$id_lang
            );
            $search['order_state'] = $result[0]['name'];
        }
        if (isset($search['currency'])) {
            $currency = new Currency($search['currency']);
            $search['currency'] = $currency->iso_code;
        }
    }
}
