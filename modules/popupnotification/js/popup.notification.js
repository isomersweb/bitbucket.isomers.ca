function PF_resizePopup(resize_start_point) {
	$('.white-popup, .mfp-content').each(function() {
		var ratio = $(window).width()/parseInt(resize_start_point);
		ratio = ratio > 1 ? 1:ratio; 
		var width = Math.ceil(500 * ratio);
		$(this).attr('style', 'width: '+width+'px !important; height:auto;');
	});
}

function PF_addRels(){
	var $elem = $('.mfp-content').find('*');
	$elem.each(function() {
		var font_size = $(this).css('font-size');
		$(this).attr('rel', parseInt(font_size));
	});
}

function PF_resizeFontSize(resize_start_point) {
	$('.mfp-content *').each(function() {

		var defaultFS = $(this).attr('rel');

		var ratio = $(window).width()/parseInt(resize_start_point);
		ratio = ratio > 1 ? 1:ratio; 
		var font_size = Math.ceil(defaultFS * ratio);

		$(this).css('font-size', font_size+'px');
		$(this).css('line-height', font_size+'px');
	});
}

function PF_resetStyles(){
	$('.white-popup, .mfp-content').each(function() {
		$(this).removeAttr('style');
	}); 

	var $elem = $('.mfp-content').find('*');
	$elem.each(function() {
		var font_size = $(this).attr('rel');
		$(this).css('font-size', font_size+'px');
		$(this).css('line-height', font_size+'px');
	});
}

function PF_addResponsivnes(resize_start_point, enable_responsive){
	if(enable_responsive == 'true'){
		PF_addRels();
		if ($( window ).width() <= parseInt(resize_start_point)) {
			PF_resizePopup(resize_start_point);
			PF_resizeFontSize(resize_start_point);
		}

		$( window ).resize(function() {
			if ($( window ).width() <= parseInt(resize_start_point)) {
				PF_resizePopup(resize_start_point);
				PF_resizeFontSize(resize_start_point);
			}else{
				PF_resetStyles();
			}
		});
	}

}

$.fn.shake = function(intShakes, intDistance, intDuration) {
    this.each(function() {
        $(this).css("position","relative"); 
        for (var x=1; x<=intShakes; x++) {
        	$(this).animate({left:(intDistance*-1)}, (((intDuration/intShakes)/4)))
        		.animate({left:intDistance}, ((intDuration/intShakes)/2))
        		.animate({left:0}, (((intDuration/intShakes)/4)));
    		}
  		});
		return this;
};

if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $(window).load(function(){
        $('#pf_popup_login_box input:-webkit-autofill').each(function(){
            var text = $(this).val();
            var name = $(this).attr('name');
            $(this).after(this.outerHTML).remove();
            $('input[name=' + name + ']').val(text);
        });
    });
}