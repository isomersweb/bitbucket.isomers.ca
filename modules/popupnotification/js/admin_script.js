$(document).ready(function(){
	var home_val = $("#home_content_type").val();
	var product_val = $("#product_content_type").val();
	var custom_val = $("#custom_content_type").val();

	if(home_val == 'login')
		$('.login_note').show();
	else if(home_val == 'register')
		$('.register_note').show();
	else if(home_val == 'login_and_register')
		$('.login_and_register_note').show();

	$("#home_"+home_val).css('display','block');
	$("#product_"+product_val).css('display','block');
	$("#custom_"+custom_val).css('display','block');
	console.log('done');
	console.log(home_val);
	$('#home_content_type').change(function(){
		$("#home_"+home_val).slideUp();
		home_val = $("#home_content_type").val();
		$("#home_"+home_val).slideDown();
		$('.home_size_note').hide();
		
		if(home_val == 'login')
			$('.login_note').show();
		else if(home_val == 'register')
			$('.register_note').show();
		else if(home_val == 'login_and_register')
			$('.login_and_register_note').show();
	});
	$('#product_content_type').change(function(){
		$("#product_"+product_val).slideUp();
		product_val = $("#product_content_type").val();
		$("#product_"+product_val).slideDown();
	});
	$('#custom_content_type').change(function(){
		$("#custom_"+custom_val).slideUp();
		custom_val = $("#custom_content_type").val();
		$("#custom_"+custom_val).slideDown();
	});

	$( "#home_box_start_date, #home_box_end_date, #product_box_start_date, #product_box_end_date, #category_box_start_date, #category_box_end_date, #custom_box_start_date, #custom_box_end_date" ).datepicker({dateFormat: 'yy-mm-dd'});


	/* Tabs */

	// $('.popupTabs a').on('click', function(){

	// 	$('.popupTabs a').removeClass('active');
	// 	$(this).addClass('active');

	// 	$('.popup-tab-content').hide();
	// 	var did = $(this).attr('data-id');

	// 	$('#popup-tab-content-'+did).show();

	// 	return false;
	// });


});