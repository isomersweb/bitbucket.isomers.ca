<?php
	include(dirname(__FILE__).'/../../../config/config.inc.php');
	include(dirname(__FILE__).'/../../../init.php');
	include(dirname(__FILE__).'/../popupnotification.php');

	$pn = new PopupNotification();
	
	$email = Tools::getValue('email');
	if(!Validate::isEmail($email)){
		$error = $pn->l('The email address is not valid.');
		die(json_encode(array('hasError' => true, 'error' => $error)));
	}

	$res = Db::getInstance()->ExecuteS("SELECT * from "._DB_PREFIX_."customer where email='$email'");

	if(empty($res)){
		$context = Context::getContext();	
		$current_email = $context->customer->email;
		$password = Tools::passwdGen();

		$customer = new Customer();
		$customer->getByEmail($current_email);

		$customer->email = $email;
		$customer->passwd = md5(pSQL(_COOKIE_KEY_.$password));
		$customer->update();
		$pn->sendConfirmationMail($customer, $password);

		Db::getInstance()->Execute("UPDATE "._DB_PREFIX_."pf_popup_twitter_users SET email='$email', active=1 WHERE email ='$current_email'");

		die(json_encode(array('hasError' => false)));
	}else{
		$error = $pn->l('An account using this email address has already been registered.');
		die(json_encode(array('hasError' => true, 'error' => $error)));
	}