<?php

	include(dirname(__FILE__).'/../../../config/config.inc.php');
	include(dirname(__FILE__).'/../../../init.php');
	include(dirname(__FILE__).'/../popupnotification.php');
	include(dirname(__FILE__).'/../classes/googleUsers.php');

	$access_token  = Tools::getValue('access_token');
	$url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$access_token;
	$user_json_data = Tools::file_get_contents($url);
	$user_data = json_decode($user_json_data);
	
	$gender = $user_data->gender;
	$id = $user_data->id;
	$email = $user_data->emails[0]->value;
	$first_name = $user_data->name->givenName;
	$last_name = $user_data->name->familyName;

	$popupnotification = new PopupNotification();

	$customer = new Customer();
	$customer->getByEmail($email, null, true);
	if($customer->id){
		$context = Context::getContext();	
		Hook::exec('actionBeforeAuthentication');
		$context->cookie->id_compare = isset($context->cookie->id_compare) ? $context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
		$context->cookie->id_customer = (int)($customer->id);
		$context->cookie->customer_lastname = $customer->lastname;
		$context->cookie->customer_firstname = $customer->firstname;
		$context->cookie->logged = 1;
		$customer->logged = 1;
		$context->cookie->is_guest = $customer->isGuest();
		$context->cookie->passwd = $customer->passwd;
		$context->cookie->email = $customer->email;
				
		// Add customer to the context
		$context->customer = $customer;
				
		if (Configuration::get('PS_CART_FOLLOWING') && (empty($context->cookie->id_cart) || Cart::getNbProducts($context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($context->customer->id))
			$context->cart = new Cart($id_cart);
		else{
			$id_carrier = (int)$context->cart->id_carrier;
			$context->cart->id_carrier = 0;
			$context->cart->setDeliveryOption(null);
			$context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($customer->id));
			$context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($customer->id));
		}
		$context->cart->id_customer = (int)$customer->id;
		$context->cart->secure_key = $customer->secure_key;
		$context->cart->save();
		$context->cookie->id_cart = (int)$context->cart->id;
		$context->cookie->write();
		$context->cart->autosetProductAddress();
		Hook::exec('actionAuthentication');
		// Login information have changed, so we check if the cart rules still apply
		CartRule::autoRemoveFromCart($context);
		CartRule::autoAddToCart($context);
		//print_r($customer);

	}else{
		$result = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pf_popup_google_users WHERE id_user ='.$id);				
		if(empty($result)){
			$google_users = new googleUsers();
			$google_users->id_user = $id;
			$google_users->first_name = $first_name;
			$google_users->last_name = $last_name;
			$google_users->email = $email;
			$google_users->gender = $gender;
			$google_users->add();

			Hook::exec('actionBeforeSubmitAccount');
		
			$customer->firstname = $first_name;
			$customer->lastname  = $last_name;
			$customer->email     = $email;

			$password = Tools::passwdGen();

			$customer->passwd    = md5(pSQL(_COOKIE_KEY_.$password));
			if($gender == 'male'){
				$id_gender = 1;
			}elseif($gender == 'female'){
				$id_gender = 2;
			}else{
				$id_gender = null;
			}
			$customer->id_gender = $id_gender;
			$customer->is_guest = 0;
			$customer->active = 1;
	
			
			$customer->add();
			$popupnotification->sendConfirmationMail($customer, $password);
			$context = Context::getContext();
			$context->customer = $customer;
			$context->cookie->id_customer = (int)$customer->id;
			$context->cookie->customer_lastname = $customer->lastname;
			$context->cookie->customer_firstname = $customer->firstname;
			$context->cookie->passwd = $customer->passwd;
			$context->cookie->logged = 1;
			$customer->logged = 1;
			$context->cookie->email = $customer->email;
			$context->cookie->is_guest = $customer->is_guest;
			// Update cart address
			$context->cart->secure_key = $customer->secure_key;
			$context->cookie->update();
			$context->cart->update();
	
		}		
	}