<div id="product-popup" class="panel product-tab">
	<h3 class="tab"> {l s='Popup Notification'}</h3>

	<div class="form-group">
		<label class="control-label col-lg-3">
			<span>{l s='Description'}</span>
		</label>
		<div class="col-lg-9">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
				<div class="translatable-field lang-{$language.id_lang}">
					<div class="col-lg-9">
				{/if}
						<textarea
							id="{$input_name}_{$language.id_lang}"
							name="{$input_name}_{$language.id_lang}"
							class="{if isset($class)}{$class}{else}textarea-autosize{/if} autoload_rte1">{if isset($input_value[$language.id_lang])}{$input_value[$language.id_lang]|htmlentitiesUTF8}{/if}</textarea>
				    <span class="counter" max="{if isset($max)}{$max}{else}none{/if}"></span>
				{if $languages|count > 1}
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=language}
							<li><a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a></li>
							{/foreach}
						</ul>
					</div>
				</div>
				{/if}
				{/foreach}

				<script type="text/javascript">
					$(".textarea-autosize").autosize();
					$(document).ready(function(){

						//small hack for PS 1.5
						if ( window.tinySetup ) {
							tinySetup({
								editor_selector :"autoload_rte1"
							});
						}
					});
				</script>
		</div>
	</div>
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay'}</button>
	</div>
</div>