<script type="text/javascript">
	$(document).ready(function(){
		$('#popup_subscribe_form').on('submit', function(){
			$.ajax({
				type: 'POST',
      			dataType : 'html',
    			data:{
    				action:'nlc_subscribtion',
    				first_name: $('#popup_first_name').val(),
    				last_name: $('#popup_last_name').val(),
    				email: $('#popup_email').val(),
    				popup_terms: $('input[type="checkbox"]:checked').val()
    			},
				url: baseDir + 'modules/popupnotification/ajax-coupon.php',
				beforeSend: function(){
					$('#popup_subscribe_form').find('.popup-ajax-loader').show();
				},
				success: function(data){
					$('#popup_subscribe_form').find('.popup-ajax-loader').hide();
					var obj = $.parseJSON(data);
					if(obj.hasError){
						var errors = obj.errors;
						if(errors.first_name){
							$('#popup_first_name').addClass('popup-field-error');
							$('#popup_first_name').attr('title', errors.first_name);
						}
						if(errors.last_name){
							$('#popup_last_name').addClass('popup-field-error');
							$('#popup_last_name').attr('title', errors.last_name);
						}
						if(errors.email){
							$('#popup_email').addClass('popup-field-error');
							$('#popup_email').attr('title', errors.email);
						}
						if(errors.terms){
							$('label[for="popup_terms"]').addClass('popup-checkbox-error');
						}else
							$('label[for="popup_terms"]').removeClass('popup-checkbox-error');
					}
					else{
						if(obj.html){
							$('#popup_subscribe_form').replaceWith(obj.html);
	            		}
	            		if(!obj.isVoucher)
	            			setTimeout(function(){
	            				$.magnificPopup.close();
	            			}, 2000);
					}
					$('#popup_first_name, #popup_last_name, #popup_email').on('focus', function(){
						$(this).removeClass('popup-field-error');
						$(this).removeAttr('title');
					});
				}
			});
			return false;
		});
	});
</script>