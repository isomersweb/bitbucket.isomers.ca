<h4>{l s='Popup Notification'}</h4>
<div class="separation"></div>
<table>
    <tr>
        <td class="col-left">
            <label>{l s='Description:'}</label>
        </td>
        <td>
            <div class="translatable">
				{foreach from=$languages item=language}
				<div class="lang_{$language.id_lang}" style="{if !$language.is_default}display:none;{/if}float: left;">
					<textarea cols="100" rows="10" id="{$input_name}_{$language.id_lang}" 
						name="{$input_name}_{$language.id_lang}" 
						class="autoload_rte1" >{if isset($input_value[$language.id_lang])}{$input_value[$language.id_lang]}{/if}</textarea>
					<span class="counter" max="{if isset($max)}{$max}{else}none{/if}"></span>
					<span class="hint">{$hint|default:''}<span class="hint-pointer">&nbsp;</span></span>
				</div>
				{/foreach}
			</div>
			<script type="text/javascript">
				$(document).ready(function(){

					//small hack for PS 1.5
					if ( window.tinySetup ) {
						tinySetup({
							editor_selector :"autoload_rte1"
						});
					}
				});
			</script>
        </td>
    </tr>
</table>