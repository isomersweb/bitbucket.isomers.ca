{if isset($product_box_valid) && $product_box_valid == 1}
	<div  id="product-popup-notification" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
	<a href="#product-popup-notification" class="open-popup-notification"></a>
	<script type="text/javascript">

		$('.open-popup-notification').magnificPopup({
		  	type:'inline',
		  	midClick: true,
		  	removalDelay: 500,
			callbacks: {
			    beforeOpen: function() {
			       this.st.mainClass = '{$product_box_effect}';
			    },
			    open: function(){
			    	PF_addResponsivnes(resize_start_point, enable_responsive);
			    }
			}
		});

		var product_box_delay = '{$product_box_delay|intval}';
		var product_box_autoclose_time = '{$product_box_autoclose_time|intval}';

		$(window).load(function(){
			if($.trim($("#product-popup-notification").html()).length != 0){
				setTimeout(function(){
					$('.open-popup-notification').click();	
				}, product_box_delay*1000);
				if(product_box_autoclose_time != 0){
					var hide_time = (parseInt(product_box_delay)+parseInt(product_box_autoclose_time))*1000;
					setTimeout(function(){
						$.magnificPopup.close();
					}, hide_time);	
				}			
			}

		});

	</script>
{/if}