{if !$email_active}
	<a class="pf_tw_active" href="#pf_tw_active" style="display:none;"></a>
	<div id="pf_tw_active" class="clearfix" style="display:none;text-align:center;padding-bottom:5px;">
		{l s='Please enter your email address' mod='popupnotification'}
		<br />
		{l s='so we can send you email with the password' mod='popupnotification'}
		<br />
		<input type="email" placeholder="{l s='Email' mod='popupnotification'}" name="pf_tw_email" id="pf_tw_email">
		<p class="tw_activation_error"></p>
		<button type="submit" class="submit_pf_tw_activation_button">
			<img src="{$base_dir}modules/popupnotification/images/ajax-loader.gif" class="popup-ajax-loader">
			{l s='Save' mod='popupnotification'}
		</button>		
	</div>
	<style type="text/css">
		{literal}
			#pf_tw_active p.tw_activation_error{
				display: none;
				margin-bottom: 0;
				padding-top: 3px;
				text-align: left;
				color: red;
			}
		{/literal}
	</style>
	<script>
		{literal}
			$(document).ready(function() {
				$(".pf_tw_active").fancybox({
					maxWidth	: 800,
					maxHeight	: 600,
					fitToView	: true,
					autoSize	: true,
					closeClick	: false
				}).trigger('click');
				$(".submit_pf_tw_activation_button").on('click', function(){
					$('#pf_tw_active p.tw_activation_error').slideUp();
					$.ajax({
						type: 'POST',
						url: baseDir+"/modules/popupnotification/ajax/tw_activate.php",
						data:{
							email: $("#pf_tw_email").val()
						},
						beforeSend: function(){
							$('#pf_tw_active').find('.popup-ajax-loader').show();
						},
						success: function(response){
							$('#pf_tw_active').find('.popup-ajax-loader').hide();
							var obj = $.parseJSON(response);
							if(obj.hasError){
								$('#pf_tw_active p.tw_activation_error').text(obj.error);
								$('#pf_tw_active p.tw_activation_error').slideDown();
							}else{
								window.location.reload();
							}
						}

					});
				});
			});
		{/literal}
	</script>
{/if}