{if isset($category_box_valid) && $category_box_valid == 1}
	<div  id="category-popup-notification" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
	<a href="#category-popup-notification" class="open-popup-notification"></a>
	<script type="text/javascript">

		$('.open-popup-notification').magnificPopup({
		  	type:'inline',
		  	midClick: true,
		 	removalDelay: 500,
			callbacks: {
			    beforeOpen: function() {
			       this.st.mainClass = '{$category_box_effect}';
			    },
			    open: function(){
			    	PF_addResponsivnes(resize_start_point, enable_responsive);
			    }
			}
		});

		var category_box_delay = '{$category_box_delay|intval}';
		var category_box_autoclose_time = '{$category_box_autoclose_time|intval}';

		$(window).load(function(){
			if($.trim($("#category-popup-notification").html()).length != 0){
				setTimeout(function(){
					$('.open-popup-notification').click();	
				}, category_box_delay*1000);
				if(category_box_autoclose_time != 0){
					var hide_time = (parseInt(category_box_delay)+parseInt(category_box_autoclose_time))*1000;
					setTimeout(function(){
						$.magnificPopup.close();
					}, hide_time);	
				}
			}

		});

	</script>
{/if}