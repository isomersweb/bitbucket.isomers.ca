<?php

$output .= '
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" '.($psv == 1.5?'style="width:900px; margin:0 auto;"':'').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').'>'.
		($psv >= 1.6?'<div class="panel">':'<fieldset>').
			($psv < 1.6 ? '<legend>': '<h3>')
				.($psv >=1.6?'<i class="icon-cogs"></i>':'').$this->l(' Newsletter').
			($psv < 1.6 ? '</legend>': '</h3>').'

			<!--
			<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Would you like to send a verification email after subscription?').'</label>
				<div class="col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="nl_verification_email" id="nl_verification_email_on" value="1" '.(Configuration::get('nl_verification_email') == '1'?'checked = "checked"':'').'>
						<label for="nl_verification_email_on">'.$this->l('Yes').'</label>
						<input type="radio" name="nl_verification_email" id="nl_verification_email_off" value="0" '.(Configuration::get('nl_verification_email') == '0'?'checked = "checked"':'').'>
						<label for="nl_verification_email_off">'.$this->l('No').'</label>
						<a class="slide-button btn"></a>
					</span>								
				</div>
			</div>				
			<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Would you like to send a confirmation email after subscription?').'</label>
				<div class="col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="nl_confirmation_email" id="nl_confirmation_email_on" value="1" '.(Configuration::get('nl_confirmation_email') == '1'?'checked = "checked"':'').'>
						<label for="nl_confirmation_email_on">'.$this->l('Yes').'</label>
						<input type="radio" name="nl_confirmation_email" id="nl_confirmation_email_off" value="0" '.(Configuration::get('nl_confirmation_email') == '0'?'checked = "checked"':'').'>
						<label for="nl_confirmation_email_off">'.$this->l('No').'</label>
						<a class="slide-button btn"></a>
					</span>								
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-3">'.$this->l('Enable Coupon?').'</label>
				<div class="col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="nl_coupon" id="nl_coupon_on" value="1" '.(Configuration::get('nl_coupon') == '1'?'checked = "checked"':'').'>
						<label for="nl_coupon_on">'.$this->l('Yes').'</label>
						<input type="radio" name="nl_coupon" id="nl_coupon_off" value="0" '.(Configuration::get('nl_coupon') == '0'?'checked = "checked"':'').'>
						<label for="nl_coupon_off">No</label>
						<a class="slide-button btn"></a>
					</span>								
				</div>
			</div>
			-->
			<p class="help-block">'.$this->l('The newsletter subscribtion required the module "Newsletter block"').'</p>


			'.($psv < 1.6?'<label for="pn_send_voucher_email">'.$this->l("Send Voucher Code by Email?").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="pn_send_voucher_email" class="control-label col-lg-3">'.$this->l("Send Voucher Code by Email?").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="pn_send_voucher_email" id="pn_send_voucher_email_on" value="1" '.(($this->_sendVoucherEmail == "1") ? 'checked="checked" ' : '').'>
							<label for="pn_send_voucher_email_on">'.$this->l('Yes').'</label>
							<input type="radio" name="pn_send_voucher_email" id="pn_send_voucher_email_off" value="0" '.(($this->_sendVoucherEmail == "0") ? 'checked="checked" ' : '').'>
							<label for="pn_send_voucher_email_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>
						<p class="help-block">'.$this->l('If you are using not only English, copy all files from /popupnotification/mails/en/ to /popupnotification/mails/xx/ (xx is the language code).').'</p>							
					</div>
					':'
					<input type="hidden" name="pn_send_voucher_email" value="0" />
					<input type="checkbox" id="pn_send_voucher_email" name="pn_send_voucher_email" value="1" '.(($this->_sendVoucherEmail == "1") ? 'checked="checked" ' : '').' >
					<p class="help-block">'.$this->l('If you are using not only English, copy all files from /popupnotification/mails/en/ to /popupnotification/mails/xx/ (xx is the language code).').'</p>
					').'	
				
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="nl_terms_url">'.$this->l("Terms URL").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="nl_terms_url" class="control-label col-lg-3">'.$this->l("Terms URL").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="nl_terms_url" id="nl_terms_url" class="fixed-width-xl" value="'.Configuration::get("nl_terms_url").'" />
					<p class="help-block">'.$this->l('Leave blank to disable by default.').'</p>'.
				($psv >= 1.6?'</div>':'').'
				
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="nl_welcome_coupon">'.$this->l("Voucher code").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="nl_welcome_coupon" class="control-label col-lg-3">'.$this->l("voucher code").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="nl_welcome_coupon" id="nl_welcome_coupon" class="fixed-width-xl" value="'.Configuration::get("nl_welcome_coupon").'" />
					<p class="help-block">'.$this->l('Leave blank to disable by default.').'</p>'.
				($psv >= 1.6?'</div>':'').'
				
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Popup Description:").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Popup Description:").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'');
					foreach($languages as $language){
						$output .= '
						<div id="nlc_popup_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<textarea style="width:276px; height:70px;" class="fixed-width-xxl" name="nlc_popup_description_'.$language['id_lang'].'">'.Configuration::get('nlc_popup_description_'.$language['id_lang']).'</textarea>
						</div>';
					}
					$output .= $this->displayFlags($languages, $defaultLanguage, 'nlc_popup_description', 'nlc_popup_description', true);
				$output .=
				($psv >= 1.6?'</div>':'').'
			</div>
			
			';



			$languages = Language::getLanguages(false);
			$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
			$defaultCurrency = intval(Configuration::get('PS_CURRENCY_DEFAULT'));
			$currencies = Currency::getCurrencies();
			$output .= '
			<!--
			<div id="coupon_options_block" style="display:none;">	
				'.($psv < 1.6?'<label for="nlc_name">'.$this->l("Name").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label for="nlc_name" class="control-label col-lg-3">'.$this->l("Name").'</label>':'');
					
					foreach($languages as $language){
							$output .= '
								<div id="nlc_name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
									'.($psv >= 1.6?'<div class="col-lg-9">':'').'
										<input type="text" name="nlc_name_'.$language['id_lang'].'" value="'.Configuration::get('nlc_name_'.$language['id_lang']).'" class="fixed-width-xl" />'.
									($psv >= 1.6?'</div>':'').'
								</div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'nlc_name', 'nlc_name', true);
					$output .= '
					<p class="help-block">'.$this->l('This will be displayed in the cart summary, as well as on the invoice.').'</p>
				</div>
				<div class="clear"></div>

				<div class="form-group">
					<label class="control-label col-lg-3">'.$this->l('Highlight').'</label>
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="nlc_highlight" id="nlc_highlight_on" value="1" '.(Configuration::get('nlc_highlight') == '1'?'checked = "checked"':'').'>
							<label for="nlc_highlight_on">'.$this->l('Yes').'</label>
							<input type="radio" name="nlc_highlight" id="nlc_highlight_off" value="0" '.(Configuration::get('nlc_highlight') == '0'?'checked = "checked"':'').'>
							<label for="nlc_highlight_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>								
						<p class="help-block">'.$this->l('If the voucher is not yet in the cart, it will be displayed in the cart summary.').'</p>
					</div>
				</div>
				<div class="clear"></div>

				<div class="form-group">
					<label class="control-label col-lg-3">'.$this->l('Partial use').'</label>
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="nlc_partial_use" id="nlc_partial_use_on" value="1" '.(Configuration::get('nlc_partial_use') == '1'?'checked = "checked"':'').'>
							<label for="nlc_partial_use_on">'.$this->l('Yes').'</label>
							<input type="radio" name="nlc_partial_use" id="nlc_partial_use_off" value="0" '.(Configuration::get('nlc_partial_use') == '0'?'checked = "checked"':'').'>
							<label for="nlc_partial_use_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>	
						<p class="help-block">'.$this->l('Only applicable if the voucher value is greater than the cart total.').'<br />'.$this->l("If you do not allow partial use, the voucher value will be lowered to the total order amount. If you allow partial use, however, a new voucher will be created with the remainder.").'</p>							
					</div>		
				</div>
				<div class="clear"></div>					
				
				'.($psv < 1.6?'<label for="nlc_valid">'.$this->l("Voucher valid days").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label for="nlc_valid" class="control-label col-lg-3">'.$this->l("Voucher valid days").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'').'
						<input type="text" name="nlc_valid" id="nlc_valid" class="fixed-width-xl" value="'.Configuration::get("nlc_valid").'" />'.
					($psv >= 1.6?'</div>':'').'
				</div>
				<div class="clear"></div>
				
				<div class="form-group">
					<label class="control-label col-lg-3">'.$this->l('Minimum amount').'</label>
					<div class="col-lg-9 ">
						<div class="row">
							<div class="col-lg-3">
								<input type="text" name="nlc_minimum_amount" value="'.Configuration::get('nlc_minimum_amount').'" />
							</div>
							<div class="col-lg-2">
								<select name="nlc_minimum_amount_currency">';
									foreach($currencies as $currency){
										$output .= '<option value="'.$currency['id_currency'].'" '.(Configuration::get('nlc_minimum_amount_currency') == $currency['id_currency']?'selected="selected"':'').'>'.$currency['iso_code'].'</option>';
									}
									$output .= '
								</select>
							</div>
							<div class="col-lg-3">
								<select name="nlc_minimum_amount_tax">
									<option value="0" '.(Configuration::get('nlc_minimum_amount_tax') == '0'?'selected="selected"':'').'>'.$this->l("Tax excluded").'</option>
									<option value="1" '.(Configuration::get('nlc_minimum_amount_tax') == '1'?'selected="selected"':'').'>'.$this->l("Tax included").'</option>
								</select>
							</div>
							<div class="col-lg-4">
								<select name="nlc_minimum_amount_shipping">
									<option value="0" '.(Configuration::get('nlc_minimum_amount_shipping') == '0'?'selected="selected"':'').'>'.$this->l("Shipping excluded").'</option>
									<option value="1" '.(Configuration::get('nlc_minimum_amount_shipping') == '1'?'selected="selected"':'').'>'.$this->l("Shipping included").'</option>
								</select>
							</div>
							<p class="help-block">'.$this->l("You can choose a minimum amount for the cart either with or without the taxes and shipping.").'</p>
						</div>					
					</div>		
				</div>
				<div class="clear"></div>

				'.($psv < 1.6?'<label for="nlc_quantity">'.$this->l("Total available").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label for="nlc_quantity" class="control-label col-lg-3">'.$this->l("Total available").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'').'
						<input type="text" name="nlc_quantity" id="nlc_quantity" class="fixed-width-xl" value="'.Configuration::get("nlc_quantity").'" />
						<p class="help-block">'.$this->l("A customer will only be able to use the voucher \"X\" time(s).").'</p>'.
					($psv >= 1.6?'</div>':'').'
				</div>
				<div class="clear"></div>
					
				<div class="form-group">
					<label class="control-label col-lg-3">'.$this->l('Free shipping').'</label>
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="nlc_free_shipping" id="nlc_free_shipping_on" value="1" '.(Configuration::get('nlc_free_shipping') == '1'?'checked = "checked"':'').'>
							<label for="nlc_free_shipping_on">'.$this->l('Yes').'</label>
							<input type="radio" name="nlc_free_shipping" id="nlc_free_shipping_off" value="0" '.(Configuration::get('nlc_free_shipping') == '0'?'checked = "checked"':'').'>
							<label for="nlc_free_shipping_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>							
					</div>		
				</div>
				<div class="clear"></div>

				<div class="form-group">
					<label class="control-label col-lg-3">'.$this->l("Apply a discount").'</label>
					<div class="col-lg-9">
						<div class="radio">
							<label for="apply_discount_percent">
								<input type="radio" name="nlc_apply_discount" id="apply_discount_percent" value="percent" '.(Configuration::get('nlc_apply_discount') == 'percent'?'checked="checked"':'').' />
								'.$this->l('Percent (%)').'
							</label>
						</div>
						<div class="radio">
							<label for="apply_discount_amount">
								<input type="radio" name="nlc_apply_discount" id="apply_discount_amount" value="amount" '.(Configuration::get('nlc_apply_discount') == 'amount'?'checked="checked"':'').' />
								'.$this->l('Amount').'
							</label>
						</div>
						<div class="radio">
							<label for="apply_discount_off">
								<input type="radio" name="nlc_apply_discount" id="apply_discount_off" value="off" '.(Configuration::get('nlc_apply_discount') == 'off'?'checked="checked"':'').' />
								'.$this->l('None').'
							</label>
						</div>
					</div>
				</div>
				<div class="clear"></div>

				<div id="apply_discount_percent_div" style="display:none;">
					'.($psv < 1.6?'<label for="reduction_percent">'.$this->l("Value").'</label>':'').'
					<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
						'.($psv >= 1.6?'<label for="reduction_percent" class="control-label col-lg-3">'.$this->l("Value").'</label>':'').
						($psv >= 1.6?'<div class="col-lg-9">':'').'
							<input type="text" id="reduction_percent" name="nlc_reduction_percent" value="'.(Configuration::get('nlc_reduction_percent')).'" class="fixed-width-xl" style="display:inline-block;" /> %
							<p class="help-block">'.$this->l("Does not apply to the shipping costs").'</p>'.
						($psv >= 1.6?'</div>':'').'
					</div>
				</div>
				<div class="clear"></div>

				<div id="apply_discount_amount_div" class="form-group" style="display:none;">
					<label class="control-label col-lg-3">'.$this->l('Amount').'</label>
					<div class="col-lg-7">
						<div class="row">
							<div class="col-lg-4">
								<input type="text" id="reduction_amount" name="nlc_reduction_amount"  onchange="this.value = this.value.replace(/,/g, \'.\');" value="'.(Configuration::get('nlc_reduction_amount')).'">
							</div>
							<div class="col-lg-4">
								<select name="nlc_reduction_currency">';
									foreach($currencies as $currency){
										$output .= '<option value="'.$currency['id_currency'].'" '.(Configuration::get('nlc_reduction_currency') == $currency['iso_code']?'selected="selected"':'').'>'.$currency['iso_code'].'</option>';
									}
									$output .='
								</select>
							</div>
							<div class="col-lg-4">
								<select name="nlc_reduction_tax">
									<option value="0" '.(Configuration::get('nlc_reduction_tax') == '0'?'selected="selected"':'').'>'.$this->l("Tax excluded").'</option>
									<option value="1" '.(Configuration::get('nlc_reduction_tax') == '1'?'selected="selected"':'').'>'.$this->l("Tax included").'</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>

				<div id="apply_discount_to_div" class="form-group" style="display: none;">
					<label class="control-label col-lg-3">'.$this->l("Apply a discount to").'</label>
					<div class="col-lg-7">
						<p class="radio">
							<label for="apply_discount_to_order">
								<input type="radio" name="nlc_apply_discount_to" id="apply_discount_to_order" value="order" '.(Configuration::get('nlc_apply_discount_to') == 'order'?'checked="checked"':'').' />
								 '.$this->l('Order (without shipping)').'
							</label>
						</p>
						<p class="radio">
							<label for="apply_discount_to_product">
								<input type="radio" name="nlc_apply_discount_to" id="apply_discount_to_product" value="specific" '.(Configuration::get('nlc_apply_discount_to') == 'specific'?'checked="checked"':'').' />
								'.$this->l('Specific product').'
							</label>
						</p>
						<p class="radio">
							<label for="apply_discount_to_cheapest" style="display:none;">
								<input type="radio" name="nlc_apply_discount_to" id="apply_discount_to_cheapest" value="cheapest" '.(Configuration::get('nlc_apply_discount_to') == 'cheapest'?'checked="checked"':'').' />
								'.$this->l('Cheapest product').'
							</label>
						</p>
					</div>
				</div>	
				<div class="clear"></div>	

				<div id="apply_discount_to_product_div" style="display:none;">								
					'.($psv < 1.6?'<label for="reduction_percent">'.$this->l("Product ID").'</label>':'').'
					<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
						'.($psv >= 1.6?'<label for="reduction_percent" class="control-label col-lg-3">'.$this->l("Product ID").'</label>':'').
						($psv >= 1.6?'<div class="col-lg-9"><div class="input-group col-lg-5">':'').'
							<input type="text" id="reductionProductFilter" name="nlc_reduction_product" value="'.Configuration::get('nlc_reduction_product').'" />'.
						($psv >= 1.6?'<span class="input-group-addon"><i class="icon-search"></i></span></div></div>':'').'
					</div>
				</div>	
			
			</div>		
				

			<script type="text/javascript">
				$(document).ready(function(){
					var discount_type = $(\'input[name="nlc_apply_discount"]:checked\').val();
					var discount_to = $(\'input[name="nlc_apply_discount_to"]:checked\').val();
					if(discount_type == \'percent\'){
						$(\'#apply_discount_percent_div, #apply_discount_to_div, #apply_discount_to_div input, #apply_discount_to_div label\').show();
					}else if(discount_type == \'amount\'){
						$(\'#apply_discount_amount_div, #apply_discount_to_div\').show();
					}

					if(discount_to == \'specific\'){
						$(\'#apply_discount_to_product_div\').show();
					}

					$(\'input[name="nlc_apply_discount"]\').change(function(){
						var discount_type = $(\'input[name="nlc_apply_discount"]:checked\').val();
						if(discount_type == \'percent\'){
							$(\'#apply_discount_amount_div\').slideUp();
							$(\'label[for="apply_discount_to_cheapest"], #apply_discount_percent_div, #apply_discount_to_div\').slideDown();
						}else if(discount_type == \'amount\'){
							$(\'#apply_discount_percent_div, label[for="apply_discount_to_cheapest"]\').slideUp();
							$(\'#apply_discount_amount_div, #apply_discount_to_div\').slideDown();
							$(\'label[for="apply_discount_to_cheapest"]\').hide();
							console.log(\'ff\');
						}else if(discount_type == \'off\'){
							$(\'#apply_discount_percent_div, #apply_discount_amount_div, #apply_discount_to_div, label[for="apply_discount_to_cheapest"]\').slideUp();
						}
					});

					$(\'input[name="nlc_apply_discount_to"]\').change(function(){
						var discount_to = $(\'input[name="nlc_apply_discount_to"]:checked\').val();
						if(discount_to == \'specific\'){
							$(\'#apply_discount_to_product_div\').slideDown();		
						}else{
							$(\'#apply_discount_to_product_div\').slideUp();
						}
					});
					$(\'#reductionProductFilter\').autocomplete(\''._MODULE_DIR_.'/'.$this->name.'/search-ajax.php\', {
							minChars: 2,
							max:50,
							width:500,
							formatItem:function (data) {
								return data[0]+ \' (\'+data[2] + \' > \' + data[1]+\')\';
							},
							scroll: false,
							multiple: false,
							extraParams:{productFilter:1,id_lang:' . $this->context->cookie->id_lang . '}
					})

					var coupon_enabled = $(\'input[name="nl_coupon"]:checked\').val();

					if(coupon_enabled == 1){
						$(\'#coupon_options_block\').show();
					}else{
						$(\'#coupon_options_block\').hide();
					}

					$(\'input[name="nl_coupon"]\').change(function(){
						var coupon_enabled = $(\'input[name="nl_coupon"]:checked\').val();

						if(coupon_enabled == 1){
							$(\'#coupon_options_block\').slideDown();
						}else{
							$(\'#coupon_options_block\').slideUp();
						}
					})
					
				});
			</script>

			-->

										
			<div class="clear"></div>

			'.($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitnl" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitnl" value="'.$this->l('Update Settings').'" class="button" />').
		($psv < 1.6 ? '</fieldset>': '</div>').'

	</form>';