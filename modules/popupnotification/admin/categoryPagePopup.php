<?php

$output .= '				
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" '.($psv == 1.5?'style="width:900px; margin:0 auto;"':'').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').' enctype="multipart/form-data">'.
		($psv >= 1.6?'<div class="panel">':'<fieldset>').
			($psv < 1.6 ? '<legend>': '<h3>')
				.($psv >=1.6?'<i class="icon-cogs"></i>':'').$this->l(' Category Page Popup').
			($psv < 1.6 ? '</legend>': '</h3>').

			($psv >= 1.6?'<!--<div class="panel">':'<fieldset>').
				($psv < 1.6 ? '<legend>': '<h3>')
					.($psv >=1.6?'<i class="icon-cogs"></i>':'<img src="../img/admin/information.png">').$this->l(' Information').''.
				($psv < 1.6 ? '</legend>': '</h3>').'

			
				<b style="color:#FF25CB">If you want to enable different popups for each category, please make sure that you did all below steps.<br /> If you want to use the same popup for all categories, you can leave all steps and use custom popup.<br /> If you have any difficults doing this, please, feel free to <a href="http://codecanyon.net/user/shoppresta" target="_blank">contact</a> me</b>
				<ul style="margin-top:15px;">
					<li style="margin-top:5px;">1. If you are using custom popup in category page, find your added {hook h=\'customPopup\'} and delete it.</li>
					<li style="margin-top:5px;">2. If you don\'t have following files in your /override/ folder, so you can just copy them from /modules/'.$this->name.'/override_files'.($psv == 1.5?'1.5':'').($psv == 1.6?'1.6':'').'/ to your /override/ folder, else you have to merge them</li>
					<ul style="list-style:circle; margin-left:40px; margin-top:5px;">
						<li style="color: #2E42D6; margin-top:5px;"><b>/modules/'.$this->name.'/override_files'.($psv == 1.5?'1.5':'').($psv == 1.6?'1.6':'').'/controllers/admin/AdminCategoriesController.php to /override/controllers/admin/AdminCategoriesController.php</b></li>
						<li style="color: #2E42D6; margin-top:5px;"><b>/modules/'.$this->name.'/override_files'.($psv == 1.5?'1.5':'').($psv == 1.6?'1.6':'').'/classes/Category.php to /override/classes/Category.php</b></li>
					</ul>
					<li style="margin-top:5px;">3. '.$this->l('Make sure to delete /cache/class_index.php file, it will be regenerated automatically').'</li>
					<li style="margin-top:5px;">4. Add {hook h=\'categoryPopup\'} to your category.tpl</li>
					<li style="margin-top:5px;">5. For adding popup in category, go to Catalog->Categories->edit</li>
				</ul>'.
			($psv < 1.6 ? '</fieldset>': '</div>').'-->

			'.($psv < 1.6?'<label for="category_page_popup_enable">'.$this->l("Enable category page popup").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_page_popup_enable" class="control-label col-lg-3">'.$this->l("Enable category page popup").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="category_page_popup_enable" id="category_page_popup_enable_on" value="true" '.((Configuration::get('category_page_popup_enable') == "true") ? 'checked="checked" ' : '').'>
							<label for="category_page_popup_enable_on">'.$this->l('Yes').'</label>
							<input type="radio" name="category_page_popup_enable" id="category_page_popup_enable_off" value="false" '.((Configuration::get('category_page_popup_enable') == "false") ? 'checked="checked" ' : '').'>
							<label for="category_page_popup_enable_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>								
					</div>
					':'
					<input type="hidden" name="category_page_popup_enable" value="false" />
					<input type="checkbox" id="category_page_popup_enable" name="category_page_popup_enable" value="true" '.((Configuration::get('category_page_popup_enable') == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_width">'.$this->l("Category Box Width").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_width" class="control-label col-lg-3">'.$this->l("Category Box Width").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" id="category_box_width" name="category_box_width" class="fixed-width-xl" value="'.Configuration::get('category_box_width').'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_height">'.$this->l("Category Box Height").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_height" class="control-label col-lg-3">'.$this->l("Category Box Height").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" id="category_box_height" name="category_box_height" class="fixed-width-xl" value="'.Configuration::get('category_box_height').'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>
            
            '.($psv < 1.6?'<label>'.$this->l("Content Type").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Content Type").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<select name="category_content_type" id="category_content_type" class="fixed-width-xl">
						<option value="html" '.(Configuration::get('category_content_type') == 'html'?'selected':'').'>'.$this->l('HTML').'</option>
						<option value="youtube" '.(Configuration::get('category_content_type') == 'youtube'?'selected':'').'>'.$this->l('Youtube').'</option>
						<option value="vimeo" '.(Configuration::get('category_content_type') == 'vimeo'?'selected':'').'>'.$this->l('Vimeo').'</option>
						<option value="gmaps" '.(Configuration::get('category_content_type') == 'gmaps'?'selected':'').'>'.$this->l('Google Maps').'</option>
						<option value="facebook" '.(Configuration::get('category_content_type') == 'facebook'?'selected':'').'>'.$this->l('Facebook Like Box').'</option>
						<option value="newsletter" '.(Configuration::get('category_content_type') == 'newsletter'?'selected':'').'>'.$this->l('Newsletter Subscribtion').'</option>
						<option value="image" '.(Configuration::get('category_content_type') == 'image'?'selected':'').'>'.$this->l('Image').'</option>
					</select>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			<div id="category_html" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Description").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Description").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');
						foreach($languages as $language){
							$output .= '
								<div id="category_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
									<textarea cols="100" rows="'.($psv<1.6?'30':'10').'" class="rte autoload_rte"  name="category_description_'.$language['id_lang'].'">'.Configuration::get('category_description_'.$language['id_lang']).'</textarea></div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'category_description', 'category_description', true);
					$output .=
					($psv >= 1.6?'</div>':'').'
				</div>
			</div>
			<div class="clear"></div>

			<div id="category_box_bg_container">
				'.($psv < 1.6?'<br /><label>'.$this->l("Background Image").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Background Image").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');

						if($this->category_box_bg != ''){
							$output .= '<img style="max-width:50%; clear:both; display:block; float:left;" src="'._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->category_box_bg.'" />';
							$output .= '<button type="submit" value="1" name="remove_category_box_bg" class="btn btn-default" style="float:left;"><i class="icon-remove"></i> '.$this->l('Delete').'</button>';
						}

						if($psv < 1.6)
							$output .= '
								<div class="clear"></div>
								<input type="file" name="category_box_bg" id="category_box_bg" />';

						if($psv >= 1.6)
							$output .= '
							<div class="col-sm-5" style="clear:left;">
								<input id="category_box_bg" type="file" name="category_box_bg" class="hide">
								<div class="dummyfile input-group">
									<span class="input-group-addon"><i class="icon-file"></i></span>
									<input id="category_box_bg-name" type="text" name="filename_category_box_bg" readonly="">
									<span class="input-group-btn">
										<button id="category_box_bg-selectbutton" type="button" name="submitAddAttachments_category_box_bg" class="btn btn-default">
											<i class="icon-folder-open"></i> '.$this->l('Add file').'
										</button>
									</span>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function(){
									$(\'#category_box_bg-selectbutton\').click(function(e) {
										$(\'#category_box_bg\').trigger(\'click\');
									});

									$(\'#category_box_bg-name\').click(function(e) {
										$(\'#category_box_bg\').trigger(\'click\');
									});

									$(\'#category_box_bg-name\').on(\'dragenter\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#category_box_bg-name\').on(\'dragover\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#category_box_bg-name\').on(\'drop\', function(e) {
										e.preventDefault();
										var files = e.originalEvent.dataTransfer.files;
										$(\'#category_box_bg\')[0].files = files;
										$(this).val(files[0].name);
									});

									$(\'#category_box_bg\').change(function(e) {
										if ($(this)[0].files !== undefined)
										{
											var files = $(this)[0].files;
											var name  = \'\';

											$.each(files, function(index, value) {
												name += value.name+\', \';
											});

											$(\'#category_box_bg-name\').val(name.slice(0, -2));
										}
										else // Internet Explorer 9 Compatibility
										{
											var name = $(this).val().split(/[\\/]/);
											$(\'#category_box_bg-name\').val(name[name.length-1]);
										}
									});
								});
							</script>';
					$output .=
					($psv >= 1.6?'</div>':'');
					if ($psv >= 1.6)
						$output .= '
						<div class="form-group">
							<label class="control-label col-lg-3">'.$this->l('Repeat').'</label>
							<div class="col-lg-9">
								<div class="radio">
									<label for="repeat_x">
										<input type="radio" name="category_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->category_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_y">
										<input type="radio" name="category_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->category_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
										'.$this->l('Repeat-y').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_x_y">
										<input type="radio" name="category_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->category_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x-y').'
									</label>
								</div>
								<div class="radio">
									<label for="no_repeat">
										<input type="radio" name="category_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->category_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
										'.$this->l('No Repeat').'
									</label>
								</div>
							</div>
						</div>';
				$output .= '
				</div>
				'.($psv < 1.6?'
				<div class="clear"></div>
				<label>'.$this->l('Repeat').'</label>
				<div class="margin-form">
					&nbsp;&nbsp;
					<input type="radio" name="category_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->category_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x">'.$this->l('Repeat-x').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="category_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->category_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
					<label class="t" for="repeat_y">'.$this->l('Repeat-y').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="category_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->category_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x_y">'.$this->l('Repeat-x-y').'</label>
					<input type="radio" name="category_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->category_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
					<label class="t" for="no_repeat">'.$this->l('No Repeat').'</label>
				</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_start_date">'.$this->l("Start Date").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_start_date" class="control-label col-lg-3">'.$this->l("Start Date").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="category_box_start_date" id="category_box_start_date" class="fixed-width-xl" value="'.Configuration::get("category_box_start_date").'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_end_date">'.$this->l("End Date").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_end_date" class="control-label col-lg-3">'.$this->l("End Date").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="category_box_end_date" id="category_box_end_date" class="fixed-width-xl" value="'.Configuration::get("category_box_end_date").'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_autoclose_time">'.$this->l("Close popup automatically").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_autoclose_time" class="control-label col-lg-3">'.$this->l("Close popup automatically").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="category_box_autoclose_time" id="category_box_autoclose_time" class="fixed-width-xl" value="'.Configuration::get("category_box_autoclose_time").'" />
					<p class="help-block">'.$this->l('"0" for no autoclose (seconds)').'</p>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="category_box_delay">'.$this->l("Display popup with delay").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="category_box_delay" class="control-label col-lg-3">'.$this->l("Display popup with delay").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="category_box_delay" id="category_box_delay" class="fixed-width-xl" value="'.Configuration::get("category_box_delay").'" />
					<p class="help-block">'.$this->l('"0" for no delay (seconds)').'</p>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Animation Effect").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Animation Effect").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<select name="category_box_effect" id="category_box_effect" class="fixed-width-xl">
						<option value="" '.(Configuration::get('category_box_effect') == ''?'selected':'').'>'.$this->l('None').'</option>
						<option value="mfp-zoom-in" '.(Configuration::get('category_box_effect') == 'mfp-zoom-in'?'selected':'').'>'.$this->l('Zoom').'</option>
						<option value="mfp-newspaper" '.(Configuration::get('category_box_effect') == 'mfp-newspaper'?'selected':'').'>'.$this->l('Newspaper').'</option>
						<option value="mfp-move-horizontal" '.(Configuration::get('category_box_effect') == 'mfp-move-horizontal'?'selected':'').'>'.$this->l('Horizontal move').'</option>
						<option value="mfp-move-from-top" '.(Configuration::get('category_box_effect') == 'mfp-move-from-top'?'selected':'').'>'.$this->l('Move from top').'</option>
						<option value="mfp-3d-unfold" '.(Configuration::get('category_box_effect') == 'mfp-3d-unfold'?'selected':'').'>'.$this->l('3d unfold').'</option>
						<option value="mfp-zoom-out" '.(Configuration::get('category_box_effect') == 'mfp-zoom-out'?'selected':'').'>'.$this->l('Zoom-out').'</option>
					</select>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>'.

			($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitcategorypagepopup" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitcategorypagepopup" value="'.$this->l('Update Settings').'" class="button" />').

		($psv < 1.6 ? '</fieldset>': '</div>').'
	</form>';