<?php

$output .= '
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data" '.($psv == 1.5?'style="width:900px; margin:0 auto;"':'').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').'>'.
		($psv >= 1.6?'<div class="panel">':'<fieldset>').
			($psv < 1.6 ? '<legend>': '<h3>')
				.($psv >=1.6?'<i class="icon-cogs"></i> ':' ').$this->l('Login And Register Settings').
			($psv < 1.6 ? '</legend>': '</h3>').'

			'.($psv < 1.6?'<label for="popup_login_enable_popup_login">'.$this->l("Enable popup for header 'Sign in'").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_login_enable_popup_login" class="control-label col-lg-3">'.$this->l("Enable popup for header 'Sign in'").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="popup_login_enable_popup_login" id="popup_login_enable_popup_login_on" value="true" '.(($this->_popup_login_enable_popup_login == "true") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_popup_login_on">'.$this->l('Yes').'</label>
							<input type="radio" name="popup_login_enable_popup_login" id="popup_login_enable_popup_login_off" value="false" '.(($this->_popup_login_enable_popup_login == "false") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_popup_login_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
					':'
					<input type="hidden" name="popup_login_enable_popup_login" value="false" />
					<input type="checkbox" id="popup_login_enable_popup_login" name="popup_login_enable_popup_login" value="true" '.(($this->_popup_login_enable_popup_login == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="popup_login_enable_login">'.$this->l("Enable Login Form").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_login_enable_login" class="control-label col-lg-3">'.$this->l("Enable Login Form").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="popup_login_enable_login" id="popup_login_enable_login_on" value="true" '.(($this->_popup_login_enable_login == "true") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_login_on">'.$this->l('Yes').'</label>
							<input type="radio" name="popup_login_enable_login" id="popup_login_enable_login_off" value="false" '.(($this->_popup_login_enable_login == "false") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_login_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
					':'
					<input type="hidden" name="popup_login_enable_login" value="false" />
					<input type="checkbox" id="popup_login_enable_login" name="popup_login_enable_login" value="true" '.(($this->_popup_login_enable_login == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="popup_login_enable_register">'.$this->l("Enable Register Form").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_login_enable_register" class="control-label col-lg-3">'.$this->l("Enable Register Form").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="popup_login_enable_register" id="popup_login_enable_register_on" value="true" '.(($this->_popup_login_enable_register == "true") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_register_on">'.$this->l('Yes').'</label>
							<input type="radio" name="popup_login_enable_register" id="popup_login_enable_register_off" value="false" '.(($this->_popup_login_enable_register == "false") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_register_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
					':'
					<input type="hidden" name="popup_login_enable_register" value="false" />
					<input type="checkbox" id="popup_login_enable_register" name="popup_login_enable_register" value="true" '.(($this->_popup_login_enable_register == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>'.

			($psv < 1.6?'<br /><label>'.$this->l("Background Image").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Background Image").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');

						if($this->login_box_bg != ''){
							$output .= '<img style="max-width:50%; clear:both; display:block; float:left;" src="'._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->login_box_bg.'" />';
							$output .= '<button type="submit" value="1" name="remove_login_box_bg" class="btn btn-default" style="float:left;"><i class="icon-remove"></i> '.$this->l('Delete').'</button>';
						}

						if($psv < 1.6)
							$output .= '
								<div class="clear"></div>
								<input type="file" name="login_box_bg" id="login_box_bg" />';

						if($psv >= 1.6)
							$output .= '
							<div class="col-sm-5" style="clear:left;">
								<input id="login_box_bg" type="file" name="login_box_bg" class="hide">
								<div class="dummyfile input-group">
									<span class="input-group-addon"><i class="icon-file"></i></span>
									<input id="login_box_bg-name" type="text" name="filename_login_box_bg" readonly="">
									<span class="input-group-btn">
										<button id="login_box_bg-selectbutton" type="button" name="submitAddAttachments_login_box_bg" class="btn btn-default">
											<i class="icon-folder-open"></i> '.$this->l('Add file').'
										</button>
									</span>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function(){
									$(\'#login_box_bg-selectbutton\').click(function(e) {
										$(\'#login_box_bg\').trigger(\'click\');
									});

									$(\'#login_box_bg-name\').click(function(e) {
										$(\'#login_box_bg\').trigger(\'click\');
									});

									$(\'#login_box_bg-name\').on(\'dragenter\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#login_box_bg-name\').on(\'dragover\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#login_box_bg-name\').on(\'drop\', function(e) {
										e.preventDefault();
										var files = e.originalEvent.dataTransfer.files;
										$(\'#login_box_bg\')[0].files = files;
										$(this).val(files[0].name);
									});

									$(\'#login_box_bg\').change(function(e) {
										if ($(this)[0].files !== undefined)
										{
											var files = $(this)[0].files;
											var name  = \'\';

											$.each(files, function(index, value) {
												name += value.name+\', \';
											});

											$(\'#login_box_bg-name\').val(name.slice(0, -2));
										}
										else // Internet Explorer 9 Compatibility
										{
											var name = $(this).val().split(/[\\/]/);
											$(\'#login_box_bg-name\').val(name[name.length-1]);
										}
									});
								});
							</script>';
					$output .=
					($psv >= 1.6?'</div>':'');
					if ($psv >= 1.6)
						$output .= '
						<div class="form-group">
							<label class="control-label col-lg-3">'.$this->l('Repeat').'</label>
							<div class="col-lg-9">
								<div class="radio">
									<label for="repeat_x">
										<input type="radio" name="login_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->login_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_y">
										<input type="radio" name="login_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->login_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
										'.$this->l('Repeat-y').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_x_y">
										<input type="radio" name="login_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->login_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x-y').'
									</label>
								</div>
								<div class="radio">
									<label for="no_repeat">
										<input type="radio" name="login_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->login_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
										'.$this->l('No Repeat').'
									</label>
								</div>
							</div>
						</div>';
				$output .= '
				</div>
				'.($psv < 1.6?'
				<div class="clear"></div>
				<label>'.$this->l('Repeat').'</label>
				<div class="margin-form">
					&nbsp;&nbsp;
					<input type="radio" name="login_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->login_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x">'.$this->l('Repeat-x').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="login_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->login_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
					<label class="t" for="repeat_y">'.$this->l('Repeat-y').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="login_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->login_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x_y">'.$this->l('Repeat-x-y').'</label>
					<input type="radio" name="login_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->login_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
					<label class="t" for="no_repeat">'.$this->l('No Repeat').'</label>
				</div>':'').'
			</div>
			<div class="clear"></div>'.

			($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitloginsettings" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitloginsettings" value="'.$this->l('Update Settings').'" class="button" />').

		($psv < 1.6 ? '</fieldset>': '</div>').'
	</form>';