{if isset($home_box_valid) && $home_box_valid == 1}	
	{if $home_content_type == 'html' || $home_content_type == 'facebook' || $home_content_type == 'newsletter' || $home_content_type == 'login' || $home_content_type == 'register' || $home_content_type == 'login_and_register'}
	<div  id="home-popup-notification" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
	{if $home_content_type == 'newsletter'}
		{include file="./newsletter.tpl"}
	{/if}
	
	<a href="#home-popup-notification" class="open-popup-notification"></a>
	<script type="text/javascript">
		var home_box_effect = '{$home_box_effect}';
		var home_remove_padding = "{if $home_content_type == 'login' || $home_content_type == 'register' || $home_content_type == 'login_and_register'}popup_remove_padding{/if}";
		
		$('.open-popup-notification').magnificPopup({
			  type:'inline',
			  midClick: true,
			  removalDelay: 500,
			  callbacks: {
			    beforeOpen: function() {
			       this.st.mainClass = home_box_effect+' '+home_remove_padding;
			    },
			    open: function(){
			    	PF_addResponsivnes(resize_start_point, enable_responsive);
			    }
			  }
			});

		var home_box_cookie = '{$home_box_cookie|intval}';
		var home_box_delay = '{$home_box_delay|intval}';
		var home_box_autoclose_time = '{$home_box_autoclose_time|intval}';

			if(home_box_cookie == "0"){
				$.cookie("home_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($("#home-popup-notification").html()).length != 0){
					if($.cookie("home_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(home_box_cookie != 0){
								$.cookie("home_box_cookie", "1", { expires: parseInt(home_box_cookie) });
							}
						}, home_box_delay*1000);
						if(home_box_autoclose_time != 0){
							var hide_time = (parseInt(home_box_delay)+parseInt(home_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
						
					}
				}

			});
	</script>
	{else if $home_content_type == 'youtube' || $home_content_type == 'vimeo' || $home_content_type == 'gmaps'}
		<a href="{$description}" class="open-popup-notification"></a>
		<script type="text/javascript">
			$('.open-popup-notification').magnificPopup({
			  	type:'iframe',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$home_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	iframe: {
				  	markup: '<div class="mfp-with-anim" style="height:100%;">'+
				  			'<div class="mfp-iframe-scaler">'+
				            '<div class="mfp-close"></div>'+
				            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
				          '</div></div>'
		      	}
			});

			var home_box_cookie = '{$home_box_cookie|intval}';
			var home_box_delay = '{$home_box_delay|intval}';
			var home_box_autoclose_time = '{$home_box_autoclose_time|intval}';

			if(home_box_cookie == "0"){
				$.cookie("home_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("home_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(home_box_cookie != 0){
								$.cookie("home_box_cookie", "1", { expires: parseInt(home_box_cookie) });
							}	
						}, home_box_delay*1000);
						if(home_box_autoclose_time != 0){
							var hide_time = (parseInt(home_box_delay)+parseInt(home_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{else if $home_content_type == 'image'}
		<a href="{if $description != ''}{$image_dir}/{$description}{/if}" class="open-popup-notification"></a>
		<script type="text/javascript">
			var home_image_link = '{$home_image_link}';

			if(home_image_link != '')
				var html = '<a href="'+home_image_link+'"><div class="mfp-img"></div></a>';
			else
				var html = '<div class="mfp-img"></div>';

			$('.open-popup-notification').magnificPopup({
			  	type:'image',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$home_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	image: {
				  	markup: '<div class="mfp-figure">'+
				            	'<div class="mfp-close"></div>'+
				            	html+
					            '<div class="mfp-bottom-bar">'+
					              	'<div class="mfp-title"></div>'+
					              	'<div class="mfp-counter"></div>'+
					            '</div>'+
				          	'</div>'
				}
			});

			var home_box_cookie = '{$home_box_cookie|intval}';
			var home_box_delay = '{$home_box_delay|intval}';
			var home_box_autoclose_time = '{$home_box_autoclose_time|intval}';

			if(home_box_cookie == "0"){
				$.cookie("home_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("home_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(home_box_cookie != 0){
								$.cookie("home_box_cookie", "1", { expires: parseInt(home_box_cookie) });
							}	
						}, home_box_delay*1000);
						if(home_box_autoclose_time != 0){
							var hide_time = (parseInt(home_box_delay)+parseInt(home_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{/if}

{/if}