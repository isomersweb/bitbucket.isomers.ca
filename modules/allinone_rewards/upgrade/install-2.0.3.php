<?php
/**
 * All-in-one Rewards Module
 *
 * @category  Prestashop
 * @category  Module
 * @author    Yann BONNAILLIE - ByWEB
 * @copyright 2012-2015 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
 * @license   Commercial license see license.txt
 * Support by mail  : contact@prestaplugins.com
 * Support on forum : Patanock
 * Support on Skype : Patanock13
 */

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_0_3($object)
{
	$result = true;

	/* Change field type */
    Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'rewards_product` CHANGE `value` `value` DECIMAL(20, 2) UNSIGNED NOT NULL DEFAULT \'0\'');

	/* new version */
	Configuration::updateValue('REWARDS_VERSION', $object->version);

	return $result;
}