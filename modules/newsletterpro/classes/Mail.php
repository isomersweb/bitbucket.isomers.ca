<?php
/**
* 2013-2015 Ovidiu Cimpean
*
* Ovidiu Cimpean - Newsletter Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at addons4prestashop@gmail.com.
*
* @author    Ovidiu Cimpean <addons4prestashop@gmail.com>
* @copyright 2013-2015 Ovidiu Cimpean
* @version   Release: 3
* @license   Do not edit, modify or copy this file
*/

include_once(_PS_SWIFT_DIR_.'Swift.php');
include_once(_PS_SWIFT_DIR_.'Swift/Connection/SMTP.php');
include_once(_PS_SWIFT_DIR_.'Swift/Connection/NativeMail.php');
include_once(_PS_SWIFT_DIR_.'Swift/Plugin/Decorator.php');

require_once dirname(__FILE__).'/Forward.php';
require_once dirname(__FILE__).'/ForwardRecipients.php';

class NewsletterProMail extends ObjectModel
{
	/* database variables */
	public $method;

	public $name;

	public $from_name;

	public $from_email;

	public $reply_to;

	public $domain;

	public $server;

	public $user;

	public $passwd;

	public $encryption;

	public $port;

	/* defined variables */

	public $context;

	public $errors = array();

	public $fwd_success_emails = array();

	const METHOD_MAIL = 1;

	const METHOD_SMTP = 2;

	public static $definition = array(
		'table'     => 'newsletter_pro_smtp',
		'primary'   => 'id_newsletter_pro_smtp',
		'fields' => array(
			'name'       => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'from_email' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
			'from_name'  => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'reply_to'   => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'domain'     => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'server'     => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'user'       => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'passwd'     => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'encryption' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'port'       => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'method'     => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
		)
	);

	public function __construct($id = null)
	{
		parent::__construct($id);

		$this->context = Context::getContext();
		$this->module  = NewsletterPro::getInstance();
	}

	public function setFromName($name)
	{
		$this->from_name = $name;
	}

	public function getSwiftConnection()
	{
		return new Swift_Connection_SMTP(
			$this->server,
			$this->port,
			($this->encryption == 'off' ? Swift_Connection_SMTP::ENC_OFF : ($this->encryption == 'tls' ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_SSL))
		);
	}

	public function addError($error)
	{
		$this->errors[] = $error;
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function hasErrors()
	{
		return !empty($this->errors);
	}

	private function getSwiftAddress($to)
	{
		if (is_array($to) && count($to) == 1)
		{
			reset($to);
			$to_name = key($to);
			$to_email = $to[$to_name];

			if (Validate::isMailName($to_name) && $to_name != '0')
				return new Swift_Address($to_email, $to_name);
			else
				return new Swift_Address($to_email);
		}
		else if (is_array($to))
		{
			$to_list = new Swift_RecipientList();
			foreach ($to as $to_name => $to_email)
			{
				$to_email = trim($to_email);

				if (!Validate::isEmail($to_email))
					$this->addError(sprintf($this->module->l('Invalid email address %s'), $to_email));
				elseif (!Validate::isMailName($to_name))
					$to_list->addTo($to_email);
				else
					$to_list->addTo($to_email, $to_name);
			}
			return $to_list;
		}
		else
			return new Swift_Address($to);
	}

	public function defineFromNameAndReplyTo()
	{
		// if this values are empty, fill them with the default shop values
		if (trim($this->from_name) == '')
			$this->from_name  = (string)$this->context->shop->name;

		if (trim($this->reply_to) == '')
			$this->reply_to  = (string)$this->from_email;
	}

	/**
	 * Send the email
	 * @param  string $subject  Email subject
	 * @param  string $template Email html template
	 * @param  array/string $to Email to
	 * @return boolean          Retrun true if the email was successfuly sent
	 */
	public function send($subject, $template, $to)
	{
		$this->defineFromNameAndReplyTo();

		try
		{
			$to_address = $this->getSwiftAddress($to);

			if (!Validate::isMailSubject($subject))
			{
				$this->addError($this->module->l('Invalid email subject.'));
				return false;
			}

			if ($this->method == self::METHOD_MAIL)
				$swift = new Swift(new Swift_Connection_NativeMail(), $this->domain);
			else if ($this->method == self::METHOD_SMTP)
			{
				$connection = $this->getSwiftConnection();
				$connection->setUsername($this->user);
				$connection->setpassword($this->passwd);
				$connection->setTimeout(5);

				$swift = new Swift($connection, $this->domain);
			}
			else
			{
				$this->addError($this->module->l('Invalid mail method.'));
				return false;
			}

			$message = new Swift_Message($subject, $template, 'text/html', '8bit', 'utf-8');
			$message->headers->set('From', $this->from_name.'<'.(string)$this->from_email.'>');
			$message->headers->set('Reply-To', $this->from_name.'<'.(string)$this->reply_to.'>');

			if (!@$swift->send($message, $to_address, $this->from_email))
				$this->addError($this->module->l('Failed to send the email.'));

			$swift->disconnect();
		}
		catch(Exception $e)
		{
			$this->addError($e->getMessage());
		}

		return !$this->hasErrors();
	}

	/**
	 * Send the newsletter to the forwarders
	 * If a forwarder exists in the database, will be deleted from the forwarder list
	 * @param  array/string $from The forwarder email
	 * @return boolean         Retrun true if the email was successfuly sent
	 */
	public function sendForward($data, $type, $from, $sleep = 1)
	{
		$errors_status = array();
		try
		{
			if ($forwards = NewsletterProForward::getForwarders($from))
			{
				$email_info       = self::getEmailInfo($from);

				$fwd_recipients = new NewsletterProForwardRecipients();
				$fwd_recipients->add(array($email_info['name'] => $email_info['email']), $forwards);

				$fwd_recipients->buildForwardersRecursive($email_info['email']);

				$recipients = $fwd_recipients->getRecipients();

				foreach ($recipients as $parent_email => $child_emails)
				{
					$this->from_name  = $fwd_recipients->getRecipientsName($parent_email);

					foreach ($child_emails as $email)
					{
						$this->from_email = $email;

						if ($template = $this->getTemplate($email, $data, $type))
						{
							$template->setForwarder(true);
							$template->forwarder_data = array('sadfad');
							$template->setForwarderData(array(
								'forwarder_email' => $email_info['email'],
							));

							$template_content = $template->getContent();
							$subject          = $template_content['render']['title'];
							$render           = $template_content['render']['full'];

							if (@$this->send($subject, $render, $email))
								$this->addSuccessFwd($email);
							else
								$errors_status[] = true;

							sleep($sleep);
						}
					}
				}

				return (empty($errors_status));
			}
		}
		catch(Exception $e)
		{
			$this->addError($e->getMessage());
		}

		return !$this->hasErrors();
	}

	public function addSuccessFwd($email)
	{
		$this->fwd_success_emails[] = $email;
	}

	public function getSuccessFwdCount()
	{
		return count($this->fwd_success_emails);
	}

	public function getTemplate($email, $data, $type)
	{
		$template = false;

		switch ($type)
		{
			case 'history':
				$template = new NewsletterProTemplate(array(
					'user' => $email,
					'data' => $data,
					'type' => 'history',
				));
				break;
		}

		return $template;
	}

	/**
	 * Get email info
	 * @param  array/string $email Name and Email address
	 * @return array          
	 */
	public static function getEmailInfo($email)
	{
		if (is_array($email))
		{
			reset($email);
			$from_name = key($email);
			$from_email = $email[$from_name];

			if (!Validate::isMailName($from_name) && $from_name != '0')
				$from_name = '';
		}
		else
		{
			$from_name = '';
			$from_email = $email;
		}

		return array(
			'name'  => $from_name,
			'email' => $from_email,
		);
	}

	/**
	 * Get an instance of the class
	 * @param  array $smtp Define de SMTP connection
	 * @return array       SMTP connection
	 */
	public static function getInstance($connection = array())
	{
		$shop_email = Configuration::get('PS_SHOP_EMAIL');

		$mail = new NewsletterProMail();
		$mail->name       = isset($connection['name']) ? $connection['name'] : uniqid();
		$mail->from_name  = isset($connection['from_name']) ? $connection['from_name'] : (string)$mail->context->shop->name;
		$mail->from_email = isset($connection['from_email']) ? $connection['from_email'] : $shop_email;
		$mail->reply_to   = isset($connection['from_email']) ? $connection['from_email'] : $shop_email;
		$mail->domain     = isset($connection['domain']) ? $connection['domain'] : '';
		$mail->server     = isset($connection['server']) ? $connection['server'] : '';
		$mail->user       = isset($connection['user']) ? $connection['user'] : '';
		$mail->passwd     = isset($connection['passwd']) ? $connection['passwd'] : '';
		$mail->encryption = isset($connection['encryption']) ? $connection['encryption'] : 'off';
		$mail->port       = isset($connection['port']) ? $connection['port'] : 'default';
		$mail->method     = isset($connection['method']) ? $connection['method'] : self::METHOD_MAIL;

		return $mail;
	}

	/**
	 * Get the prestashp default SMTP connection
	 * @param  array  $smtp Override the default SMTP values
	 * @return array/boolean  SMTP connection or false
	 */
	public static function getDefaultSMTP()
	{
		$context    = Context::getContext();
		$connection = Configuration::getMultiple(array(
			'PS_SHOP_EMAIL',
			'PS_MAIL_SERVER',
			'PS_MAIL_USER',
			'PS_MAIL_PASSWD',
			'PS_MAIL_SMTP_ENCRYPTION',
			'PS_MAIL_SMTP_PORT',
			'PS_MAIL_DOMAIN',
		));

		if ($connection)
		{
			return array(
				'from_name'  => (string)$context->shop->name,
				'from_email' => $connection['PS_SHOP_EMAIL'],
				'reply_to'   => $connection['PS_SHOP_EMAIL'],
				'domain'     => $connection['PS_MAIL_DOMAIN'],
				'server'     => $connection['PS_MAIL_SERVER'],
				'user'       => $connection['PS_MAIL_USER'],
				'passwd'     => $connection['PS_MAIL_PASSWD'],
				'encryption' => $connection['PS_MAIL_SMTP_ENCRYPTION'],
				'port'       => $connection['PS_MAIL_SMTP_PORT'],
				'method'     => self::METHOD_SMTP,
			);
		}

		return false;
	}

	/**
	 * Get the default mail() connection
	 * @return array/boolean Mail connection or false
	 */
	public static function getDefaultMail()
	{
		$context    = Context::getContext();
		$connection = Configuration::getMultiple(array(
			'PS_SHOP_EMAIL',
		));

		if ($connection)
		{
			return array(
				'from_name'  => (string)$context->shop->name,
				'from_email' => $connection['PS_SHOP_EMAIL'],
				'reply_to'   => $connection['PS_SHOP_EMAIL'],
				'method'     => self::METHOD_MAIL,
			);
		}

		return false;
	}

	/**
	 * Get default connection
	 * @return array/boolean Return the default connection of false
	 */
	public static function getDefaultConnection()
	{
		$method = (int)Configuration::get('PS_MAIL_METHOD');

		if ($method == self::METHOD_MAIL)
			return self::getDefaultMail();
		else if ($method == self::METHOD_SMTP)
			return self::getDefaultSMTP();

		return false;
	}

	/**
	 * Get the active instance [SMTP, function mail(), or the default prestashop method]
	 * @return object/false return an instance or false
	 */
	public static function getInstanceByContext()
	{
		$module  = NewsletterPro::getInstance();

		if ((int)$module->getConfiguration('SMTP_ACTIVE'))
		{
			$mail = new NewsletterProMail((int)$module->getConfiguration('SMTP'));
			if (Validate::isLoadedObject($mail))
				return $mail;
		}
		else
			return NewsletterProMail::getInstance(self::getDefaultConnection());
		return false;
	}

	public static function getAllMails()
	{
		return Db::getInstance()->executeS(
			'SELECT `id_newsletter_pro_smtp`, 
					`method`, 
					`name`, 
					`domain`, 
					`server`, 
					`user`, 
					`from_name`, 
					`from_email`, 
					`reply_to`, 
					`encryption`, 
					`port`,
			CASE WHEN `passwd` = 0 THEN "" ELSE "" END AS `passwd` 
			FROM `'._DB_PREFIX_.'newsletter_pro_smtp` 
			WHERE 1;'
		);
	}

	/**
	 * Extract the full name and email from the user object
	 * @param  object $user The user object
	 * @return array/string User full name and email address
	 */
	public static function getToFromUser($user)
	{
		$to = $user->email;
		if (Tools::strlen($user->firstname) > 0)
			$to = array($user->firstname.' '.$user->lastname => $user->email);
		return $to;
	}
}