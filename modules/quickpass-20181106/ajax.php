<?php
/**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

require_once dirname(__FILE__).'/../../config/config.inc.php';
require_once dirname(__FILE__).'/../../init.php';
require_once dirname(__FILE__).'/quickpass.php';
if (Tools::getIsset('action') && Tools::getIsset('token') && Tools::getValue('token') == Tools::getToken(false)) {
    $qp = new QuickPass;
    switch (Tools::getValue('action')) {
        case 'passreco':
            echo Tools::jsonEncode($qp->recoverPassword(Tools::getValue('email')));
            break;
        case 'confirmpass':
            echo Tools::jsonEncode($qp->resetPassword(
                Tools::getValue('pass'),
                Tools::getValue('cpass'),
                Tools::getValue('qptoken')
            ));
            break;
        default:
            die('Invalid option');
    }
} else {
    die('You are not allowed to access this page.');
}
