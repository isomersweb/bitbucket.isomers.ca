<?php
/**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class QuickPass extends Module
{
    public function __construct()
    {
        $this->name = 'quickpass';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Vipul Hadiya';
        $this->controllers = array('newpass');
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = 'fd338809d6a3a81f763af9cac1bb40f5';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Simple Password Reset');
        $this->description = $this->l('This module allows customer to retrieve the password in the more 
        convenient way.');
        $this->confirmUninstall = $this->l('Are you sure about removing these details?');
    }

    public function install()
    {
        include_once($this->local_path.'sql/install.php');
        if (!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayQuickpass') ||
            !$this->registerHook('displayTop')) {
            return false;
        }
        return true;
    }
    public function uninstall()
    {
        include_once($this->local_path.'sql/uninstall.php');
        if (!$this->unregisterHook('header') ||
            !$this->unregisterHook('displayHeader') ||
            !$this->unregisterHook('displayTop') ||
            !parent::uninstall()) {
            return false;
        }
        return true;
    }
    public function hookHeader()
    {
        $this->context->controller->addCSS(array(
            //$this->_path.'/views/css/quickpass.fo.css'
        ));
        $this->context->controller->addJquery();
        $this->context->controller->addJS(array(
            //$this->_path.'/views/js/quickpass.fo.js'
        ));
        $this->context->smarty->assign(array(
            'js' => $this->_path.'/views/js/quickpass.fo.js',
            'css' => $this->_path.'/views/css/quickpass.fo.css',
            'qp_ajax' => $this->_path.'ajax.php',
            'newpasslink' => $this->context->link->getModuleLink($this->name, 'newpass')
        ));
        return $this->display(__FILE__, 'views/templates/hook/header.tpl');
    }
    public function hookBackOfficeHeader()
    {
        if (!Tools::getIsset('configure') || Tools::getValue('configure') != $this->name) {
            return false;
        }
        $this->context->controller->addCSS(array(
            $this->_path.'/views/css/quickpass.bo.css'
        ));
        $this->context->controller->addJquery();
        $this->context->controller->addJS(array(
            $this->_path.'/views/js/quickpass.bo.js'
        ));
    }
    public function hookDisplayHeader()
    {
        return $this->hookHeader();
    }
    public function hookDisplayTop()
    {
        $this->smarty->assign(array(
            'qp_ajax' => $this->_path.'ajax.php',
            'loader_img' => $this->_path.'views/img/indicator_bar.gif',
        ));
        return $this->display(__FILE__, 'views/templates/hook/top.tpl');
    }
    public function getContent()
    {
        $id_shop = $this->context->shop->id;
        $this->smarty->assign(array(
            'attempts' => Db::getInstance()->executeS('SELECT qp.*, c.`email` FROM `'._DB_PREFIX_.'quickpass` qp, `'
            ._DB_PREFIX_.'customer` c WHERE c.`id_customer` = qp.`id_customer` AND qp.`id_shop` = '.(int)$id_shop)
        ));
        return $this->display(__FILE__, 'views/templates/admin/admin.tpl');
    }
    public function hookDisplayQuickpass()
    {
        return $this->display(__FILE__, 'views/templates/hook/quickpass.tpl');
    }
    public function recoverPassword($email)
    {
        $flag = 1;
        $msg = '';
        if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return array(
                'flag' => 0,
                'msg' => $this->l('Please enter valid email.')
            );
        }
        $id_shop = $this->context->shop->id;
        $customer = Db::getInstance()->getRow('SELECT *  FROM `'._DB_PREFIX_.'customer` WHERE `email` = \''
        .pSQL($email).'\' AND `id_shop` = '.(int)$id_shop.' AND `is_guest` != 1 AND `active` = 1');
        
        if (isset($customer['email']) && isset($customer['id_customer'])) {
            $link = new Link;
            $passkey = md5(date('YmdHis').$customer['email']);
            $variables = array(
                '{firstname}' => $customer['firstname'],
                '{reset}' => $link->getModuleLink('quickpass', 'newpass',
                array('token' => $passkey), null, $this->context->language->id, $id_shop
                ),
            );
            $flag = Mail::Send(
                $this->context->language->id,
                'recover',
                Mail::l('Recover your password.', $this->context->language->id),
                $variables,
                $email,
                null,
                null,
                null,
                null,
                null,
                dirname(__file__).'/mails/',
                false,
                $this->context->shop->id
            );
             
            $sql = 'INSERT INTO `'._DB_PREFIX_.'quickpass`(
				`id_customer`,
                `id_shop`,
				`passkey`,
				`passkey_generated`,
				`is_verified`,
				`ip`,
				`passkey_verified`) VALUES(
				'.(int)$customer['id_customer'].',
                '.(int)$id_shop.',
				\''.pSQL($passkey).'\',
				\''.date('Y-m-d H:i:s').'\',
				0,
				\''.pSQL($_SERVER['REMOTE_ADDR']).'\',
				NULL
				)';
            Db::getInstance()->execute($sql);
            $msg = $this->l('Check your email for password recovery link.');
        } else {
            $flag = 0;
            $msg = $this->l(sprintf('No matching email found for %s', $email));
        }
        
        return array(
            'flag' => $flag,
            'msg' => $msg
        );
    }
    public function resetPassword($pass, $cpass, $token)
    {
        $id_shop = $this->context->shop->id;
        if (empty($token)) {
            return array(
                'flag' => 0,
                'msg' => $this->l('Missing token.')
            );
        }
        $id_customer = Db::getInstance()->getValue('SELECT `id_customer` FROM `'._DB_PREFIX_.'quickpass`
        WHERE `passkey` = \''.pSQL($token).'\' AND `id_shop` ='.(int)$this->context->shop->id.' AND `is_verified` = 0');
        if (!$id_customer) {
            return array(
                'flag' => 0,
                'msg' => $this->l('We can not find matching user or the token has been expired.')
            );
        }
        if (empty($pass) || empty($cpass)) {
            return array(
                'flag' => 0,
                'msg' => $this->l('Please enter password and confirm it.')
            );
        }
        if ($cpass != $pass) {
            return array(
                'flag' => 0,
                'msg' => $this->l('Password does not matc with confirm password.')
            );
        }
        if ($pass == $cpass) {
            $password = Tools::encrypt($pass);
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'customer` SET `passwd` = \''
            .pSQL($password).'\' WHERE `id_customer` = '.(int)$id_customer.' AND `id_shop` = '.$id_shop);
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'quickpass` SET `is_verified` = 1,
            `passkey_verified` = \''.date('Y-m-d H:i:s').'\' WHERE `passkey` = \''.pSQL($token).'\'');
            $link = new Link;
            return array(
                'flag' => 1,
                'msg' => str_replace(array('[1]', '[2]', '[3]'),
                array('<a href="','">','</a>'),
                sprintf($this->l('Your password has been changed successfully. Please click [1]%s[2]here[3] to login'),
                    $link->getPageLink('authentication')
                )
                )
            );
        } else {
            return array(
                'flag' => 0,
                'msg' => $this->l('Error ocuured while.')
            );
        }
    }
}
