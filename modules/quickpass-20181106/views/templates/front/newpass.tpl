{**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 *}
 
 <div class="panel panel-default panel-ugly">
	<div class="panel-heading">{l s='Set new password' mod='quickpass'}</div>
	<div class="pane-body">
		<form class="passbox">
			{if isset($is_logged) && $is_logged}
                <div class="alert alert-success">{l s='You are already logged in' mod='quickpass'}</div>
            {else}
                {if isset($record['id_customer']) && $record['id_customer']}
    				<div class="form-group">
    					<label>{l s='New password' mod='quickpass'}</label>
    					<input type="password" class="form-control qptext" id="qp_newpass" name="qp_newpass" />
    				</div>
    				<div class="form-group">
    					<label>{l s='Confirm new password' mod='quickpass'}</label>
    					<input type="password" class="form-control qptext" id="qp_cnewpass" name="qp_cnewpass" />
    				</div>
    				<div class="form-group">
    				<button type="button" id="resetqp" name="resetqp" class="btn btn-default pull-right">{l s='Reset' mod='quickpass'}</button>
    					<div class="clearfix"></div>
    				</div>
    				<input type="hidden" value="{$token|escape:'htmlall':'UTF-8'}" name="qptoken" id="qptoken" />
    			{else}
                    {if isset($token) && $token}
                        <div class="alert alert-danger">
        					{l s ='Either invalid token or link is expired.' mod='quickpass'}
        				</div>
                    {else}
        				<div class="form-group">
        					<label>{l s='Enter your registered email address' mod='quickpass'}</label>
                            <div class="input-group">
                				<input type="email" class="form-control qp_email" name="qp_email" id="qp_email" value="" placeholder="" />
                				<span class="input-group-btn">
                					<button name="qp_passreco" id="qp_passreco" type="button" class="btn btn-secondary">{l s='Recover' mod='quickpass'}</button>
                				</span>
                				<div class="clearfix"></div>
                			</div>
                            <div class="input-group">
                                <label class="qp-alert" id="resltmsg"></label>
                            </div>
        				</div>
                    {/if}
    			{/if}
			{/if}
			<div class="qp-alert" id="resetmsg"></div>
			<div class="overlay forcehide"><img src="{$loader|escape:'htmlall':'UTF-8'}" alt="Title" alt="Title" /></div>
		</form>
	</div>
</div>