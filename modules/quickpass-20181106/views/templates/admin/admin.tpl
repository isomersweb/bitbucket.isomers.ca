{**
 * Simple Password Reset allows customer to retrieve the password in the more convenient way..
 *
 * Simple Password Reset by Vipul Hadiya(http://www.vipulhadiya.com) is licensed under a
 * Creative Creative Commons Attribution-NoDerivatives 4.0 International License.
 * Based on a work at http://www.clavolike.com.
 * Permissions beyond the scope of this license may be available at http://www.vipulhadiya.com.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/4.0/.
 *
 * @author    Vipul Hadiya <vip@vipulhadiya.com>
 * @copyright 2018 Vipul Hadiya
 * @license   http://creativecommons.org/licenses/by-nd/4.0/
 *}
 <div class="panel panel-defaul">
    <div class="panel-heading"><i class="icon-list-ul"></i>&nbsp;{l s='Password recover requests' mod='quickpass'}</div>
    <div class="panel-body">
        <table class="table table-hover table-stripped">
            <thead>
                <tr>
                    <th>{l s='ID' mod='quickpass'}</th>
                    <th>{l s='Email' mod='quickpass'}</th>
                    <th>{l s='IP' mod='quickpass'}</th>
                    <th>{l s='Datetime' mod='quickpass'}</th>
                </tr>
            </thead>
            {if isset($attempts) && $attempts}
                <tbody>
                    {foreach from=$attempts item=at}
                        <tr>
                            <td>{$at['id_attempt']|escape:'htmlall':'UTF-8'}</td>
                            <td>{$at['email']|escape:'htmlall':'UTF-8'}</td>
                            <td>{$at['ip']|escape:'htmlall':'UTF-8'}</td>
                            <td>{$at['passkey_generated']|escape:'htmlall':'UTF-8'}</td>
                        </tr>
                    {/foreach}
                </tbody>
            {/if}
        </table>
    </div>
    <div class="panel-footer">
        <a href="{$newpass}" target="_blank">{$newpass}</a>
    </div>
 </div>