<?php
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 12.0.0

global $request;
global $iso_country_reciever;
global $iso_country_shipper;

$dirpath = _PS_ROOT_DIR_ . '/modules/fedexcarrier/';
$addressDelivery = new Address($order->id_address_delivery, $this->context->language->id);

$fd_carrier = Db::getInstance()->ExecuteS('SELECT sc.`code` as code 
            FROM `'._DB_PREFIX_.'fedex_rate_service_code` sc
            WHERE sc.id_carrier = '. Tools::getValue('fd_method'));

$iso_state = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '. $addressDelivery->id_state);
$iso_country_reciever = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '. $addressDelivery->id_country);
$iso_country_shipper = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '. Configuration::get('FEDEX_CARRIER_COUNTRY'));
$iso_country_reciever = $iso_country_reciever[0]['iso_code'];
$iso_country_shipper = $iso_country_shipper[0]['iso_code'];


require_once($dirpath . 'fedex-common.php5');

//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = $dirpath . "ShipService_v19.wsdl";

ini_set("soap.wsdl_cache_enabled", "0");

$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

$request['WebAuthenticationDetail'] = array(
	'UserCredential' => array(
		'Key' => Configuration::get("FEDEX_CARRIER_API_KEY"), 
		'Password' => Configuration::get("FEDEX_CARRIER_PASSWORD")
	)
);

$request['ClientDetail'] = array(
	'AccountNumber' => Configuration::get("FEDEX_CARRIER_ACCOUNT"), 
	'MeterNumber' => Configuration::get("FEDEX_CARRIER_METER")
);
$request['TransactionDetail'] = array('CustomerTransactionId' => '*** ProcessShip ***');
$request['Version'] = array(
	'ServiceId' => 'ship', 
	'Major' => '19', 
	'Intermediate' => '0', 
	'Minor' => '0'
);
$request['RequestedShipment'] = array(
	'ShipTimestamp' => date('c', strtotime(Tools::getValue('fd_date'))),
	'DropoffType' => Configuration::get("FEDEX_CARRIER_PICKUP_TYPE"), // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	'ServiceType' => $fd_carrier[0]['code'], // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	'PackagingType' => 'YOUR_PACKAGING', //Tools::getValue('fd_package'), // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ... 
    'Shipper' => addShipper(),
    'Recipient' => addRecipient($addressDelivery, $fd_carrier[0]['code']),   
    'Origin' => addShipper(),
	'ShippingChargesPayment' => addShippingChargesPayment(),
	'CustomsClearanceDetail' => addCustomClearanceDetail($order, $addressDelivery),                                                                                                       
	'LabelSpecification' => addLabelSpecification($order, $addressDelivery),
	'TotalWeight' => array(
		'Value' =>  Tools::getValue('fd_box_weight') + Tools::getValue('fd_weight'), 
		'Units' => 'KG' // valid values LB and KG
	), 
    
/*	'CustomerSpecifiedDetail' => array(
		'MaskedData'=> 'SHIPPER_ACCOUNT_NUMBER'
	),*/ 
	'PackageCount' => 1,
		'RequestedPackageLineItems' => array(
		'0' => addPackageLineItem1($order)
	),
	/*'CustomerReferences' => array(
		'0' => array(
			'CustomerReferenceType' => 'CUSTOMER_REFERENCE', 
			'Value' => 'TC007_07_PT1_ST01_PK01_SNDUS_RCPCA_POS'
		)
	)*/
);

$fd_etd = (int)Tools::getValue('fd_etd');

if (($iso_country_reciever != $iso_country_shipper) && ($fd_etd != 0)) {
    $request['RequestedShipment']['SpecialServicesRequested'] = array(
        'SpecialServiceTypes' => 'ELECTRONIC_TRADE_DOCUMENTS',
        'EtdDetail' => array(
            'RequestedDocumentCopies' => 'COMMERCIAL_INVOICE',
        )
    );
    $request['RequestedShipment']['ShippingDocumentSpecification'] = array(
    'ShippingDocumentTypes' => 'COMMERCIAL_INVOICE',
    'CommercialInvoiceDetail' => array(
        'Format' => array(
            'ImageType' => 'PDF',
            'StockType' => 'PAPER_LETTER',
        )
    )
    );
    $request['RequestedShipment']['EdtRequestType'] = 'ALL';
}

try{
	if(setEndpoint('changeEndpoint')){
		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
	}
//d($request);
	$response = $client->processShipment($request); // FedEx web service invocation
    
//        printSuccess($client, $response);d();
    if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
        $fp = fopen(SHIP_LABEL, 'wb');   
        fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
        fclose($fp);
        
        if (($iso_country_reciever != $iso_country_shipper) && ($fd_etd != 0)) {        
            $fp = fopen(_PS_ROOT_DIR_. '/orders/labels/' . 'FD-'.$order->id.'-invoice.pdf', 'wb');  
            fwrite($fp, ($response->CompletedShipmentDetail->ShipmentDocuments->Parts->Image));
            fclose($fp);
        }
        
//d($response);
        //echo 'Label <a href="./'.SHIP_LABEL.'">'.SHIP_LABEL.'</a> was generated.';   
        return $response;         
    }else{
        printError($client, $response);d();
        return $response;
    }

	writeToLog($client);    // Write to log file
} catch (SoapFault $exception) {
    printFault($exception, $client);d();
    return $exception;
}


function addShipper(){
    $shipper_state = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '. Configuration::get('FEDEX_CARRIER_STATE'));
	$shipper = array(
		'Contact' => array(
			'PersonName' => '',
			'CompanyName' => Configuration::get('PS_SHOP_NAME'),	
			'PhoneNumber' => Configuration::get('PS_SHOP_PHONE'),	
		),
		'Address' => array(
			'StreetLines' => array(Configuration::get('FEDEX_CARRIER_ADDRESS1')),
			'City' => Configuration::get('FEDEX_CARRIER_CITY'),	
			'StateOrProvinceCode' => $shipper_state[0]['iso_code'],	
			'PostalCode' => Configuration::get('FEDEX_CARRIER_POSTAL_CODE'),	
			'CountryCode' => 'CA',		
		)
	);
	return $shipper;
}
function addRecipient($addressDelivery, $ServiceType = ''){
    $iso_state = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '. $addressDelivery->id_state);
    $iso_country = Db::getInstance()->ExecuteS('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '. $addressDelivery->id_country);
    $StreetLines = $addressDelivery->address1 . (!empty($addressDelivery->address2) ? ", " . $addressDelivery->address2 : "");

	$recipient = array(
		'Contact' => array(
			'PersonName' => $addressDelivery->firstname.' '.$addressDelivery->lastname,	
			'CompanyName' => $addressDelivery->company,	
			'PhoneNumber' => $addressDelivery->phone
		),
		'Address' => array(
			'StreetLines' => $StreetLines,
			'City' => $addressDelivery->city,
			'StateOrProvinceCode' => $iso_state[0]['iso_code'],	
			'PostalCode' => $addressDelivery->postcode,
			'CountryCode' => $iso_country[0]['iso_code'],	
			'Residential' => false
		)
	);
    if ($ServiceType == 'GROUND_HOME_DELIVERY') {
        $recipient['Address']['Residential'] = true;
    }
	return $recipient;	                                    
}
function addShippingChargesPayment(){
    global $request;
	$shippingChargesPayment = array('PaymentType' => 'SENDER',
        'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => Configuration::get("FEDEX_CARRIER_ACCOUNT"),
				'Contact' => $request['RequestedShipment']['Shipper']['Contact'],
				'Address' => $request['RequestedShipment']['Shipper']['Address'],
			)
		)
	);
	return $shippingChargesPayment;
}
function addLabelSpecification($order, $addressDelivery){
    unlink(_PS_ROOT_DIR_. '/orders/labels/' . 'FD-'.$order->id.'.zpl');
    unlink(_PS_ROOT_DIR_. '/orders/labels/' . 'FD-'.$order->id.'.pdf');
    
    if (strpos(Tools::getValue('fd_label'), '4X6') != 0) {
        $imageType = 'ZPLII';
        define('SHIP_LABEL', _PS_ROOT_DIR_. '/orders/labels/' . 'FD-'.$order->id.'.zpl');
    }
    else {
        define('SHIP_LABEL', _PS_ROOT_DIR_. '/orders/labels/' . 'FD-'.$order->id.'.pdf');   
    }
    $to = addRecipient($addressDelivery, $ServiceType);
    $StreetLines = $addressDelivery->address1 . (!empty($addressDelivery->address2) ? "," . $addressDelivery->address2 : "");
    
	$labelSpecification = array(
		'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => $imageType,  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => Tools::getValue('fd_label'),
        'LabelPrintingOrientation'=> 'TOP_EDGE_OF_TEXT_FIRST',
        'CustomerSpecifiedDetail'=> array(
            'DocTabContent'=>array(
                'DocTabContentType' => 'ZONE001',
                'Zone001' => array(
                    'DocTabZoneSpecifications' => array(
                        array(
                            'ZoneNumber' => 1,
                            'Header' => 'TO',
                            'LiteralValue' => $StreetLines.",".$addressDelivery->city.",".$addressDelivery->postcode,//'REQUEST/SHIPMENT/Recipient/Address/StreetLines[1]'
                        ),
                        array(
                            'ZoneNumber' => 2,
                            'Header' => 'REF',
                            'DataField' => 'REQUEST/PACKAGE/CustomerReferences[1]/Value'
                        ),
                        array(
                            'ZoneNumber' => 4,
                            'Header' => 'Pkg',
                            'LiteralValue' => '1 of 1'
                        ),
                        array(
                            'ZoneNumber' => 5,
                            'Header' => 'Date',
                            'DataField' => 'REQUEST/SHIPMENT/ShipTimestamp',
                            'Justification' => 'RIGHT'
                        ),
                        array(
                            'ZoneNumber' => 6,
                            'Header' => 'ACTWGT',
                            'DataField' => 'REQUEST/SHIPMENT/TotalWeight/Value',
                            'Justification' => 'RIGHT'
                        ),
                        array(
                            'ZoneNumber' => 7,
                            'Header' => 'Ship Cost',
                            'DataField' => 'REPLY/SHIPMENT/RATES/ACTUAL/TotalNetFreight/Amount',
                            'Justification' => 'RIGHT'
                        ),
                        array(
                            'ZoneNumber' => 8,
                            'Header' => 'DIMMED',
                            'LiteralValue' => Tools::getValue('fd_box_length').'x'.Tools::getValue('fd_box_width').'x'.Tools::getValue('fd_box_height').' '.Configuration::get('PS_DIMENSION_UNIT'),
                            'Justification' => 'RIGHT'
                        ),
                    )
                )
            )
        )
	);
//    p($labelSpecification);d();
	return $labelSpecification;
}
/*
function addSpecialServices(){
	$specialServices = array(
		'SpecialServiceTypes' => array('COD'),
		'CodDetail' => array(
			'CodCollectionAmount' => array(
				'Currency' => 'USD', 
				'Amount' => 150
			),
			'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
		)
	);
	return $specialServices; 
}
*/

function addCustomClearanceDetail($order, $addressDelivery){
    global $request;
    global $iso_country_reciever;
    global $iso_country_shipper;

    if($iso_country_shipper == $iso_country_reciever) return; 
    
    $order_details = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'order_detail` WHERE `id_order` = '.$order->id);
    $currency = Currency::getCurrency($order->id_currency);
    $currency = $currency['iso_code'];
    $prod_qty = 0;

	$customerClearanceDetail = array(
		'DutiesPayment' => array(
			'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
			'Payor' => array(
				'ResponsibleParty' => array(
					'AccountNumber' => Configuration::get("FEDEX_CARRIER_ACCOUNT"),
					'Contact' => $request['RequestedShipment']['Shipper']['Contact'],
					'Address' => $request['RequestedShipment']['Shipper']['Address'],
				)
			)
		),
		'DocumentContent' => 'NON_DOCUMENTS', 
        'CommercialInvoice' => array(
            //'Comments' => 'Cosmetic skin care products. NOT FOR RESALE. Samples only.',
             'CustomerReferences' => array (
                'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
                'Value' => $order->id
            ),
          'PaymentTerms' => '',
            'TermsOfSale' => 'FOB',            
        ),                                                                                           
		'CustomsValue' => array(
			'Currency' => $currency, 
			'Amount' => $prod_qty
		),
		'ExportDetail' => array(
			'B13AFilingOption' => 'NOT_REQUIRED'
		)
	);

    //$i = 0;
    $weight_unit = 'KG';
    foreach ($order_details  as $val) {
        $customerClearanceDetail['Commodities'][] = array(
    		'NumberOfPieces' => 1,
            'Name' => $val['product_name'],
    		'Description' => $val['product_name']."\nCosmetic skin care products. NOT FOR RESALE. Samples only.",
    		'CountryOfManufacture' => $iso_country_shipper,
            'HarmonizedCode' => '3304.99.50',
    		'Weight' => array(
    			'Units' => $weight_unit, 
    			'Value' => $val['product_weight'],
    		),
    		'Quantity' => $val['product_quantity'],
    		'QuantityUnits' => 'EA',
    		'UnitPrice' => array(
    			'Currency' => $currency, 
    			'Amount' => 2.5
    		),
    		'CustomsValue' => array(
    			'Currency' => $currency, 
    			'Amount' => $val['product_quantity']*2.5
    		)
    	);
        $prod_qty +=  $val['product_quantity']*2.5;
        //if ($i++ == 3) break;
    }
    $customerClearanceDetail['CustomsValue']['Amount'] = $prod_qty;
   
//p($customerClearanceDetail);d();
    return $customerClearanceDetail;
}
function addPackageLineItem1($order){
    $packageLineItem = array(
		'SequenceNumber'=>1,
		'GroupPackageCount'=>1,
        	'CustomerReferences' => array(
    		'0' => array(
    			'CustomerReferenceType' => 'CUSTOMER_REFERENCE', 
    			'Value' => $order->id
    		)
    	),
		'Weight' => array(
			'Value' => Tools::getValue('fd_box_weight') + Tools::getValue('fd_weight'),
			'Units' => 'KG'
		),
  		'Dimensions' => array(
			'Length' => Tools::getValue('fd_box_length'),
			'Width' => Tools::getValue('fd_box_width'),
			'Height' => Tools::getValue('fd_box_height'),
			'Units' => Configuration::get('PS_DIMENSION_UNIT')
		)
	);
	return $packageLineItem;
}
?>