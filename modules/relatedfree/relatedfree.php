<?php

/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2015 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */
class Relatedfree extends Module
{
    public function __construct()
    {
        $this->name = 'relatedfree';
        $this->tab = 'advertising_marketing';
        $this->author = 'MyPresta.eu';
        $this->version = '1.5.1';
        $this->module_key = 'de6f0cf17c8cb0d314ec544203a9f5f5';
        parent::__construct();
        $this->displayName = $this->l('Related products free');
        $this->description = $this->l('Module allows to display custom related products block with products from selected category');
        $this->addproduct = $this->l('Add');
        $this->noproductsfound = $this->l('No products found');
    }

    public static function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = explode('.', $version);
        if ($part == 1)
        {
            return $exp[1];
        }
        if ($part == 2)
        {
            return $exp[2];
        }
        if ($part == 3)
        {
            return $exp[3];
        }
    }

    public function install()
    {
        if ($this->psversion() == 5 || $this->psversion() == 6)
        {
            if (parent::install() == false or !$this->registerHook('displayHeader') or !$this->registerHook('productFooter') or !$this->registerHook('displayAdminProductsExtra') or !$this->registerHook('actionProductUpdate'))
            {
                return false;
            }
        }
        return true;
    }

    public function hookActionProductUpdate($params)
    {
        if (Tools::isSubmit('relatedfree'))
        {
            Configuration::updateValue('related_category' . Tools::getValue('id_product'), Tools::getValue('related_category'));
            Configuration::updateValue('related_nb' . Tools::getValue('id_product'), Tools::getValue('related_nb'));
            Configuration::updateValue('related_link' . Tools::getValue('id_product'), Tools::getValue('related_link'));
        }
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        $this->context->smarty->assign('related_category', Configuration::get('related_category' . Tools::getValue('id_product')));
        $this->context->smarty->assign('related_nb', Configuration::get('related_nb' . Tools::getValue('id_product')));
        $this->context->smarty->assign('related_link', Configuration::get('related_link' . Tools::getValue('id_product')));
        return $this->display(__FILE__, 'views/templates/admin/tabs.tpl');
    }

    public function hookProductFooter($params)
    {
        $category = new Category(Configuration::get('related_category' . Tools::getValue('id_product')));
        $blocks_products = $category->getProducts($this->context->cookie->id_lang, 0, Configuration::get('related_nb' . Tools::getValue('id_product')));
        $this->context->smarty->assign('blocks_products', $blocks_products);
        $this->context->smarty->assign('id_lang', $this->context->language->id);
        $this->context->smarty->assign('related_category', Configuration::get('related_category' . Tools::getValue('id_product')));
        $this->context->smarty->assign('related_link', Configuration::get('related_link' . Tools::getValue('id_product')));
        return $this->display(__file__, 'products16.tpl');
    }

    public function hookHeader($params)
    {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'product')
        {
            $this->context->controller->addCSS(_THEME_CSS_DIR_ . 'product_list.css');
        }
    }

    public function msg_saved()
    {
        return '<div class="conf confirm">' . $this->l('Saved') . '</div>';
    }

    public function displayForm()
    {
        $output = '';
        $form = '';
        $form .= '
                <fieldset>
                    <legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Global Settings') . '</legend>
                    <form name="globalsettings" id="globalsettings" method="post">
                        <div class="bootstrap">
	                        <div class="alert alert-info">
								' . $this->l('Select the way of how tabs on product page will appear. If you are on prestashop 1.6 with default-bootstrap theme and want to use real product tabs like it was in 1.5 please follow this guide: ') . ' <a href="http://mypresta.eu/en/art/prestashop-16/product-tabs.html" target="_blank"/>' . $this->l('real product tabs in prestashop 1.6') . '</a>
							</div>
						</div>
                        <label>' . $this->l('Select tabs type') . '</label>
            			<div class="margin-form">
                            <select type="text" name="ppb_internal_tabs" style="max-width:200px;">
                                <option value="1" ' . (Configuration::get('ppb_internal_tabs') == 1 ? "selected=\"selected\" " : "") . '>' . $this->l('PrestaShop 1.6 type (wide bars)') . '</option>
                                <option value="0" ' . (Configuration::get('ppb_internal_tabs') == 1 ? "" : "selected=\"selected\" ") . '>' . $this->l('PrestaShop 1.5 type (real tabs)') . '</option>
                            </select>
           				</div>
                        <input type="submit" class="extra button" name="global_settings" value="' . $this->l('save settings') . ' "/>
                     </form>
                 </fieldset>';

        return $output . '
        <script type="text/javascript" src="../modules/ppb/js/script.js"/></script>
        <script type="text/javascript">
    			$(function() {
    				var $mySlides = $("#homepageblocks");
    				$mySlides.sortable({
    					opacity: 0.6,
    					cursor: "move",
    					update: function() {
    						var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
    						$.post("' . _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/' . $this->name . '/ajax_' . $this->name . '.php", order);
    						}
    					});
    				$mySlides.hover(function() {
    					$(this).css("cursor","move");
    					},
    					function() {
    					$(this).css("cursor","auto");
    				});
    			});
    		    </script>
        <style>
            .language_flags {text-align:left;}
            #topmenu-horizontal-module {overflow:hidden; background-color: #F8F8F8; border: 1px solid #CCCCCC; margin-bottom: 10px; padding: 10px 0; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;}
            #topmenu-horizontal-module .addnew, #topmenu-horizontal-module .btnpro {-webkit-border-radius:4px; -moz-border-radius:4px; -border-radius:4px; padding:5px; margin-right:10px; cursor:pointer; width:52px; height:52px; display:inline-block; float:right; text-align:center; border:1px dotted #c0c0c0; }
            #topmenu-horizontal-module .addnew:hover, #topmenu-horizontal-module .btnpro:hover {border:1px solid #bfbfbf; background:#f3f3f3;}
            #topmenu-horizontal-module span.img {margin:auto; width:32px; height:32px; display:block;}
            #topmenu-horizontal-module span.txt {margin-top:3px; width:52px; display:block; text-align:center;}
            #topmenu-horizontal-module .addnew span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/add.png\') no-repeat center;}
            #topmenu-horizontal-module .save span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/on.png\') no-repeat center;}
            #topmenu-horizontal-module .back span.img {background:url(\'' . _MODULE_DIR_ . $this->name . '/img/back.png\') no-repeat center;}

                .slides {margin:0px; padding:0px;}
                .slides li { font-size:15px!important; list-style: none; margin: 0 0 4px 0; padding: 15px 10px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
                .slides li:hover {border:1px #000 dashed; cursor:move;}
                .slides li .name {font-size:18px!important;}
                .slides li .nb {color:#FFF; background:#000; padding:5px 10px; font-size:18px; font-weight:bold; margin-right:10px; }

                .activate {display:inline-block; float:right; padding-right:5px;  cursor:pointer; position:relative; top:2px;}
                .activate img {max-width:50px; height:auto;}
                .remove {opacity:0.3; position:relative; top:-1px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/' . $this->name . '/img/trash.png") top no-repeat; cursor:pointer;}
                .edit {margin-right:6px; opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/' . $this->name . '/img/edit.png") top no-repeat; cursor:pointer;}

                .remove:hover, .edit:hover, .activate:hover { opacity:1.0; }
                .edit,.remove {margin-right:5px;}


        </style>
        <form name="selectform1" id="selectform1" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="1"></form>
        <form name="selectform2" id="selectform2" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="2"></form>
        <form name="selectform3" id="selectform3" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="3"></form>
        <form name="selectform4" id="selectform4" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="4"></form>
        <form name="selectform99" id="selectform99" action="' . $_SERVER['REQUEST_URI'] . '" method="post"><input type="hidden" name="selecttab" value="99"></form>
        ' . "<div id='cssmenu'>
            <ul>
               <li class='bgver'><a><span>v" . $this->version . "</span></a></li>
               <li style='position:relative; display:inline-block; float:right; '><a style=\"display:block; width:40px;\" href='http://mypresta.eu' target='_blank' title='prestashop modules'><span ><img src='../modules/ppb/img/logo-white.png' alt='prestashop modules' style=\"position:absolute; top:17px; right:16px;\"/></span></a></li>
            </ul>
        </div>" . '<link href="../modules/' . $this->name . '/css.css" rel="stylesheet" type="text/css" />' . $form;
    }

}

?>