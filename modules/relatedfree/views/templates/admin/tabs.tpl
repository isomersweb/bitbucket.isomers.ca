{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2016 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}
<div style="overflow:hidden;" class="panel">
    <input type="hidden" name="relatedfree" value="1"/>
    <h3 class="tab">{l s='Related products free' mod='relatedfree'}</h3>
    <div class="separation"></div>
    <div style="overflow:hidden;">
        <table>
            <td><img src="../modules/relatedfree/related-pro.png" class="img-responsive"/></td>
            <td style="width:20px;"></td>
            <td>
                1. {l s='Want much more appearance options?' mod='relatedfree'}<br/>
                2. {l s='Want to display products in random order?' mod='relatedfree'}<br/>
                3. {l s='Want to display custom selected products only?' mod='relatedfree'}<br/>
                4. {l s='Want to display list of products in "tabs" section?' mod='relatedfree'}<br/>
                5. {l s='Want to display unlimited number of blocks?' mod='relatedfree'}<br/>
                6. {l s='Want to create related products carousell?' mod='relatedfree'}<br/>
                7. {l s='Want the best related products tool?' mod='relatedfree'}<br/>
                <span style="font-size:18px;">{l s='check' mod='relatedfree'} <a href="http://mypresta.eu/modules/front-office-features/related-products-pro.html" target="_blank">related products pro</a> {l s='module' mod='relatedfree'}.</span>
            </td>
        </table>
        <div style="display:block; margin-right:10px; margin-top:40px;">
            <h4>{l s='Module settings' mod='relatedfree'}</h4>
            <div class="separation"></div>
            <fieldset style="border:none;">
                <label>{l s='Category ID' mod='relatedfree'} <a href="http://mypresta.eu/en/art/basic-tutorials/prestashop-how-to-get-category-id.html" target="_blank">{l s='How to get category ID ?' mod='relatedfree'}</a></label>
                <input type="text" name="related_category" value="{$related_category|escape:'int':'utf-8'}"/>
                <label>{l s='Number of products' mod='relatedfree'}</label>
                <input type="text" name="related_nb" value="{$related_nb|escape:'int':'utf-8'}"/>
                <label>{l s='Add category link to title of block' mod='relatedfree'}</label>
                <select name="related_link">
                    <option value="1" {if $related_link == 1}selected{/if}>{l s='Yes' mod='relatedfree'}</option>
                    <option value="0" {if $related_link == 0}selected{/if}>{l s='No' mod='relatedfree'}</option>
                </select>
                <div class="separation"></div>
                <div class="clear">&nbsp;</div>
                <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i>{l s='Save and stay' mod='relatedfree'}</button>
            </fieldset>
            <div class="separation"></div>
            <iframe src="//apps.facepages.eu/somestuff/whatsgoingon.html" width="100%" height="150" border="0" style="margin-top:40px; margin-bottom: 50px; border:none; opacity:0.7"></iframe>
        </div>
    </div>
</div>