<?php
global $_MODULE;
$_MODULE = array();
$_MODULE['<{psgdpr}prestashop>psgdpr_5966265f35dd87febf4d59029bc9ef66'] = 'Conformitate cu RGPD oficial';
$_MODULE['<{psgdpr}prestashop>psgdpr_dbf90f78fc135d723570a4a43041a3dc'] = 'Respectați principalele cerințe ale Regulamentului european general privind protecția datelor, datorită acestui modul dezvoltat de PrestaShop.';
$_MODULE['<{psgdpr}prestashop>psgdpr_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Sigur doriți să dezinstalați acest modul?';
$_MODULE['<{psgdpr}prestashop>psgdpr_e9415612c1d72517733c98e6877a6b46'] = 'A survenit o eroare în timpul dezinstalării. Vă rugăm să ne contactați prin site-ul web Addons.';
$_MODULE['<{psgdpr}prestashop>psgdpr_78d320af42aca685d1fcd1113f09939e'] = 'A survenit o eroare în timpul dezinstalării. Contactați-ne prin site-ul web Addons';
$_MODULE['<{psgdpr}prestashop>psgdpr_e7502bc086a11cfa8789053af27eab2d'] = 'S-a salvat cu succes!';
$_MODULE['<{psgdpr}prestashop>psgdpr_cb84a315de222f4aa4ab6d6d5219314e'] = 'Vă rugăm să completați toate câmpurile necesare.';
$_MODULE['<{psgdpr}prestashop>htmltemplatepsgdprmodule_9ad5a301cfed1c7f825506bf57205ab6'] = 'DATE PERSONALE';
$_MODULE['<{psgdpr}prestashop>customeraccount_5868129c4526891dddb05b8e59c33572'] = 'RGPD - Date personale';
$_MODULE['<{psgdpr}prestashop>customeraccount_5868129c4526891dddb05b8e59c33572'] = 'RGPD - Date personale';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_93bd48ecb9c4d5c4eec7fefffbb2070f'] = 'Ultimele conectări';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_33e29c1d042c0923008f78b46af94984'] = 'Originea solicitării';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_57f32d7d0e6672cc2b60bc7a49f91453'] = 'Pagină vizualizată';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_fae6aba9bc4e69842be0ac98e15c4c99'] = 'Timp petrecut pe pagină';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_75ba8d70e3692ba200f0e0df37b4d2ae'] = 'Adresă IP';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>personaldata.connections-tab_c51e6bdf66e5d601e85d055301014410'] = 'Nicio conectare';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_284b47b0bb63ae2df3b29f0e691d6fcf'] = 'Adrese';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_effdb9ce6c5d44df31b89d7069c8e0fb'] = 'Pseudonim';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_dd7bf230fde8d4836917806aff6a6b27'] = 'Adresă';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_fac322c3b42d04806299ae195f8a9238'] = 'Telefon (Telefoane)';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_59716c97497eb9694541f7c3d37b1a4d'] = 'Țară';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>personaldata.addresses-tab_587bb937485e3dbe02ea0d281600bb52'] = 'Nicio adresă';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Comenzi';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_63d5049791d9d79d86e9a108b0a999ca'] = 'Referință';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_c453a4b8e8d98e82f35b67f433e3b4da'] = 'Plată';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_d02bbc3cb147c272b0445ac5ca7d1a36'] = 'Situația comenzii';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_ea067eb37801c5aab1a1c685eb97d601'] = 'Total plătit';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_0e166fe6d96b79167a1cdc0dcecb43dd'] = 'Produs (Produse) din comandă';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_63d5049791d9d79d86e9a108b0a999ca'] = 'Referință';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantitate';
$_MODULE['<{psgdpr}prestashop>personaldata.orders-tab_08cfa7751d1812e961560f623e082aba'] = 'Nicio comandă';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_fc26e55e0993a75e892175deb02aae15'] = 'Coșuri';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_490aa6e856ccf208a054389e47ce0d06'] = 'ID';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_db205f01b4fd580fb5daa9072d96849d'] = 'Produse totale';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_83a234de54312687bf9ab33fe4168f6c'] = 'Produs (Produse) în coș';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_63d5049791d9d79d86e9a108b0a999ca'] = 'Referință';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantitate';
$_MODULE['<{psgdpr}prestashop>personaldata.carts-tab_bbd47e26c95290218b2fc449e54e8cdc'] = 'Niciun coș';
$_MODULE['<{psgdpr}prestashop>personaldata.messages-tab_41de6d6cfb8953c021bbe4ba0701c8a1'] = 'Mesaje';
$_MODULE['<{psgdpr}prestashop>personaldata.messages-tab_a12a3079e14ced46e69ba52b8a90b21a'] = 'IP';
$_MODULE['<{psgdpr}prestashop>personaldata.messages-tab_4c2a8fe7eaf24721cc7a9f0175115bd4'] = 'Mesaj';
$_MODULE['<{psgdpr}prestashop>personaldata.messages-tab_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>personaldata.messages-tab_58a747ef5d07d22101bdcd058e772ff9'] = 'Niciun mesaj';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_3bcfe5c02494f4ff326c5dbd1dcffa91'] = 'Informații generale';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_019ec3132cdf8ee0f2e2a75cf5d3e459'] = 'Sex';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_9c37b7b6ff829e977df287900543ea54'] = 'Data nașterii';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_9d8d2d5ab12b515182a505f54db7f538'] = 'Vârstă';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_4994a8ffeba4ac3140beb89e8d41f174'] = 'Limbă';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_c4aebdbba922c239df53567d2991e510'] = 'Data creării contului';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_5e5914912e8d2f2765525840acf98bea'] = 'Ultima vizită';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_60adc330494a66981dec101c81e27f03'] = 'Număr de înregistrare SIRET';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_6c5a79a30b5c5c4fb6ec45735d32edc2'] = 'Cod APE';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_15bbb9d0bbf25e8d2978de1168c749dc'] = 'Site web';
$_MODULE['<{psgdpr}prestashop>personaldata.generalinfo-tab_019ec3132cdf8ee0f2e2a75cf5d3e459'] = 'Sex';
$_MODULE['<{psgdpr}prestashop>personaldata.modules-tab_e55f75a29310d7b60f7ac1d390c8ae42'] = 'Modul';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Contul meu';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_5868129c4526891dddb05b8e59c33572'] = 'RGPD - Date personale';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_5868129c4526891dddb05b8e59c33572'] = 'RGPD - Date personale';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_a8308387f3ffc2d045d2b6125e74317d'] = 'Acces la datele mele';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_915f4446a47b51378e37374cfbb72d7f'] = 'În orice moment, aveți dreptul să obțineți datele pe care le-ați furnizat pe site-ul nostru. Faceți clic pe „Obține datele mele” pentru a descărca automat o copie a datelor dvs. personale, într-un fișier pdf sau csv.';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_49b5d04f5f20820830f080d4674c8669'] = 'OBȚINE DATELE MELE ÎN CSV';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_7b0b06bfe69473081553bcccb12ca068'] = 'OBȚINE DATELE MELE ÎN PDF';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_91a49c1d45a5d37e85658d01b6f37423'] = 'Solicitări de rectificare și ștergere';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_01cb324ae91e9bd16213cc2945334249'] = 'Aveți dreptul să modificați toate informațiile personale care se găsesc pe pagina „Contul meu”. Pentru orice altă solicitare pe care ați putea-o avea cu privire la rectificarea și/sau ștergerea datelor dvs. personale, vă rugăm să ne contactați prin';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_af5efea250326c1c34d69aa9364b482c'] = 'pagina noastră de contract';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_332087a4991d8bc866abd3fd78a2e514'] = 'Vom examina solicitarea dvs. și vom răspunde în cel mai scurt timp.';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_0b3db27bc15f682e92ff250ebb167d4b'] = 'Înapoi la contul dvs.';
$_MODULE['<{psgdpr}prestashop>customerpersonaldata_8cf04a9734132302f96da8e113e80ce5'] = 'Pagina principală';
$_MODULE['<{psgdpr}prestashop>consentcheckbox_fb0440f9ca32a8b49eded51b09e70821'] = '';
$_MODULE['<{psgdpr}prestashop>dataconsent_9aa5e987f351139f2b4e3d14b9353e56'] = 'Configurați casetele de bifare';
$_MODULE['<{psgdpr}prestashop>dataconsent_62442110c88c18b3645145cee9211474'] = 'Vă rugăm să personalizați mesajele de solicitare a consimțământului în câmpurile dedicate de mai jos:';
$_MODULE['<{psgdpr}prestashop>dataconsent_40cfa1617e59da6c05189eb33fd19b34'] = 'Vă recomandăm să includeți un link către pagina cu politica dvs. de confidențialitate în fiecare dintre mesajele personalizate. Rețineți că este obligatorie o pagină dedicată politicii de confidențialitate pe site-ul dvs. web. Dacă nu aveți una încă, faceți clic';
$_MODULE['<{psgdpr}prestashop>dataconsent_6c92285fa6d3e827b198d120ea3ac674'] = 'aici';
$_MODULE['<{psgdpr}prestashop>dataconsent_b1e293bbb668bf8c149fff2865d0273d'] = 'Formular de creare a contului';
$_MODULE['<{psgdpr}prestashop>dataconsent_7469a286259799e5b37e5db9296f00b3'] = 'DA';
$_MODULE['<{psgdpr}prestashop>dataconsent_c2f3f489a00553e7a01d369c103c7251'] = 'NU';
$_MODULE['<{psgdpr}prestashop>dataconsent_f782036f044fe2a6de1677dcac0868c2'] = 'Mesaj de solicitare a consimțământului';
$_MODULE['<{psgdpr}prestashop>dataconsent_c7ad735a42df6942cbad2402dd3fad2a'] = 'Acest mesaj va fi afișat pe formularul de creare a clientului';
$_MODULE['<{psgdpr}prestashop>dataconsent_7469a286259799e5b37e5db9296f00b3'] = 'DA';
$_MODULE['<{psgdpr}prestashop>dataconsent_c2f3f489a00553e7a01d369c103c7251'] = 'NU';
$_MODULE['<{psgdpr}prestashop>dataconsent_f782036f044fe2a6de1677dcac0868c2'] = 'Mesaj de solicitare a consimțământului';
$_MODULE['<{psgdpr}prestashop>dataconsent_5562e13c7ff921bf7907e1d5e0ffc294'] = 'Acest mesaj va fi însoțit de o casetă de bifare';
$_MODULE['<{psgdpr}prestashop>dataconsent_a8b79d64d6a4c3e13b3c805542292d78'] = 'Celelalte module instalate care necesită confirmarea consimțământului vor fi afișate în această filă numai dacă au făcut actualizarea RGPD. Câmpurile corespunzătoare vor fi afișate automat pentru ca să le personalizați în casetele de bifare pentru confirmarea consimțământului.';
$_MODULE['<{psgdpr}prestashop>dataconsent_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
$_MODULE['<{psgdpr}prestashop>help_c2ba71bfcb3f9d58df2ccacc5b949c2d'] = 'Ajutor și contact';
$_MODULE['<{psgdpr}prestashop>help_a19f5b117715d61a9fe7474260e1f6bf'] = 'Acest modul vă dă posibilitatea să:';
$_MODULE['<{psgdpr}prestashop>help_129d6406c710afbbd8e26fdfdec211e5'] = 'Ștergeți orice cont de client cu datele sale personale colectate de magazinul dvs., dacă clientul vă solicită';
$_MODULE['<{psgdpr}prestashop>help_cd66b7179c438249ca40ff6498bb28c4'] = 'Adăugați o casetă de bifare pentru confirmarea consimțământului în formularul unui modul care colectează date personale și personalizați-o';
$_MODULE['<{psgdpr}prestashop>help_bd47684406e8441b4e2e354ad3adafa7'] = 'Permiteți-i clientului să consulte și să exporte datele sale personale colectate de magazinul dvs. despre contul său de client';
$_MODULE['<{psgdpr}prestashop>help_38dfa44543ebf8ab68578ead08b1c19a'] = 'Vedeți toate acțiunile clienților dvs. legate de datele dvs. personale';
$_MODULE['<{psgdpr}prestashop>help_73878c19cdc6ef2ab0d0fac6943cc958'] = 'Aveți nevoie de ajutor?';
$_MODULE['<{psgdpr}prestashop>help_5eaa63820c162e960a23700319f7e3cc'] = 'Găsiți aici documentația aferentă acestui modul';
$_MODULE['<{psgdpr}prestashop>help_5b6cf869265c13af8566f192b4ab3d2a'] = 'Documentație';
$_MODULE['<{psgdpr}prestashop>help_1fe917b01f9a3f87fa2d7d3b7643fac1'] = 'Întrebări frecvente';
$_MODULE['<{psgdpr}prestashop>help_986c907a7f2adcdbadb4785a441f9b23'] = 'Nu ați găsit niciun răspuns la întrebarea dvs.?';
$_MODULE['<{psgdpr}prestashop>help_0b4f23e625077174e2bda0b340e02632'] = 'Contactați-ne pe PrestaShop Addons';
$_MODULE['<{psgdpr}prestashop>customeractivity_f5a289ea76ecee6abfd43c2e7eddf58f'] = 'Lista cu activitatea clientului';
$_MODULE['<{psgdpr}prestashop>customeractivity_f078af4e0b76b98d53f3820e0ce629b2'] = 'Țineți evidența activității clientului legată de accesibilitatea datelor, consimțământ și ștergere.';
$_MODULE['<{psgdpr}prestashop>customeractivity_9579cdef0bf63ffa6b1e3e3c616d9cac'] = 'Numele/ID-ul clientului';
$_MODULE['<{psgdpr}prestashop>customeractivity_b9263f280c2c2ef949e10b7aa7a61549'] = 'Tip de solicitare';
$_MODULE['<{psgdpr}prestashop>customeractivity_f0dc5a7e50af112eee854e8cee968e06'] = 'Data trimiterii';
$_MODULE['<{psgdpr}prestashop>customeractivity_b62799f8fdbb9329b450c490004a6213'] = 'Confirmarea consimțământului';
$_MODULE['<{psgdpr}prestashop>customeractivity_e0e4fc6213e8b3593495a7260c3a4c2e'] = 'Accesibilitate';
$_MODULE['<{psgdpr}prestashop>customeractivity_e0e4fc6213e8b3593495a7260c3a4c2e'] = 'Accesibilitate';
$_MODULE['<{psgdpr}prestashop>customeractivity_530479efffb195651bdbfbd50cfb8a4c'] = 'Ștergere';
$_MODULE['<{psgdpr}prestashop>getstarted_be11c74c1dd7f307bb80183a90dc2067'] = 'Începeți';
$_MODULE['<{psgdpr}prestashop>getstarted_20719d3a43ec43a93df59b1bbfdecb98'] = 'Bun venit la modulul dvs. RGPD';
$_MODULE['<{psgdpr}prestashop>getstarted_4d47947737e64fb8d082cd23e655c58c'] = 'Această interfață vă va ajuta să vă familiarizați cu RGPD și vă va îndruma pentru a vă ajuta să asigurați conformitatea cu acest regulament.';
$_MODULE['<{psgdpr}prestashop>getstarted_7126b43016dfc29aba0a051092789885'] = 'Acest modul îndeplinește principalele cerințe ale regulamentului privind datele personale ale clienților dvs., inclusiv:';
$_MODULE['<{psgdpr}prestashop>getstarted_d743912f3dd67b05698f902990065dc2'] = 'Dreptul acestora de a avea acces la datele lor personale și portabilitatea datelor';
$_MODULE['<{psgdpr}prestashop>getstarted_674eec70af9feb40728481ce959ca7ea'] = 'Dreptul de a obține rectificarea și/sau ștergerea datelor lor personale';
$_MODULE['<{psgdpr}prestashop>getstarted_56ebf6e5564c6b92e4d77e88949fd52c'] = 'Dreptul de a-și da și a retrage consimțământul';
$_MODULE['<{psgdpr}prestashop>getstarted_eff0cc2d13389da1aa5ae9405418354f'] = 'De asemenea, vă permite să țineți evidența activităților de prelucrare (în special pentru acces, consimțământ și ștergere).';
$_MODULE['<{psgdpr}prestashop>getstarted_cb5830c9314fa880d3607bbac3d78af8'] = 'Urmați cei 3 pași ai noștri pentru a configura modulul și a vă ajuta să asigurați conformitatea cu RGPD!';
$_MODULE['<{psgdpr}prestashop>getstarted_34e34c43ec6b943c10a3cc1a1a16fb11'] = 'Gestionare';
$_MODULE['<{psgdpr}prestashop>getstarted_604c6226ffa8d05ba2e9d637852673a6'] = 'Consultați fila Gestionarea datelor personale pentru a vizualiza datele colectate de PrestaShop și modulele pentru comunitate și gestionați datele personale ale clienților dvs.';
$_MODULE['<{psgdpr}prestashop>getstarted_63a78ed4647f7c63c2929e35ec1c95e3'] = 'Personalizare';
$_MODULE['<{psgdpr}prestashop>getstarted_232c8c9b71d0a7d5197d29735ef4b903'] = 'Personalizați conținutul casetelor de bifare pentru confirmarea consimțământului și mesaj de solicitare a consimțământului în diferitele formulare ale magazinului dvs., în special pentru crearea contului și abonarea la newsletter.';
$_MODULE['<{psgdpr}prestashop>getstarted_afc9168648f61aa6abd0bf76b84abc3b'] = 'Monitorizare';
$_MODULE['<{psgdpr}prestashop>getstarted_c9c1dc03aed08d4c60bde968b3417f60'] = 'Vizualizați acțiunile clienților dvs. legate de datele lor și gestionați solicitările de ștergere.';
$_MODULE['<{psgdpr}prestashop>getstarted_eb36499a1ed94957bf4988899d45aa1a'] = 'Notă: Vă rugăm să vă asigurați că aveți acces la cea mai recentă versiune a modulului (modulelor) instalat(e), pentru a beneficia pe deplin de funcționalitățile modulului RGPD. Dacă unul sau mai multe module nu furnizează lista de date, vă invităm să-i contactați direct pe dezvoltatorii acestora.';
$_MODULE['<{psgdpr}prestashop>getstarted_011400bd7b57639a3aa5a2dd70f275ee'] = 'Mai multe informații despre RGPD';
$_MODULE['<{psgdpr}prestashop>getstarted_75ab3e8ff5e5eb97d91eb14e5c82beb2'] = 'Site-urile web al autorităților pentru protecția datelor';
$_MODULE['<{psgdpr}prestashop>getstarted_47d4e5149c64dfa18b87cf6dc464e5ee'] = 'Cartea albă a PrestaShop despre RGPD';
$_MODULE['<{psgdpr}prestashop>getstarted_5040ffa592d5e7ed29a396a4c416ebda'] = 'Documentația modulului';
$_MODULE['<{psgdpr}prestashop>getstarted_34e2d1989a1dbf75cd631596133ee5ee'] = 'Videoclip';
$_MODULE['<{psgdpr}prestashop>getstarted_712b9d33de9cca47077a2d2b1831edb8'] = 'Construire articol';
$_MODULE['<{psgdpr}prestashop>getstarted_1972bd6acee82b7e9fc7af320ae4e673'] = 'Notă: Aceste funcționalități sunt menite să vă ajute să asigurați conformitatea cu RGPD. Utilizarea lor nu garantează însă că site-ul dvs. este pe deplin conform cu cerințele RGPD. Este ';
$_MODULE['<{psgdpr}prestashop>getstarted_85e3340d54570d5167c2daaff75308c6'] = 'Este responsabilitatea dvs.';
$_MODULE['<{psgdpr}prestashop>getstarted_651f509e8c074849330167fa1dd41c19'] = 'să configurați modulele și să întreprindeți acțiunile necesare pentru a asigura conformitatea. Pentru întrebări, vă recomandăm să contactați un avocat specializat în chestiuni de legislație în domeniul datelor personale.';
$_MODULE['<{psgdpr}prestashop>dataconfig_aa4de6aaed26c361322b279d34d8a7dd'] = 'Vizualizarea datelor și acțiuni automate';
$_MODULE['<{psgdpr}prestashop>dataconfig_c32a80e0c500e5a7537f5b70fe8129fd'] = 'Găsiți aici lista tuturor datelor personale colectate de PrestaShop și modulele instalate de dvs.';
$_MODULE['<{psgdpr}prestashop>dataconfig_3a7a2fb701b6c416ccfd2328cb1461d6'] = 'Aceste date vor fi utilizate la 2 niveluri diferite:';
$_MODULE['<{psgdpr}prestashop>dataconfig_34c7656d69407f8ff8bc0ae5ad35daca'] = 'Când un client solicită accesul la datele sale: primește o copie a datelor sale personale colectate în magazinul dvs.';
$_MODULE['<{psgdpr}prestashop>dataconfig_d61bbb8ed4a9c97a676591d853b71488'] = 'Când un client solicită ștergerea datelor: dacă acceptați cererea lui, datele sale vor fi șterse definitiv.';
$_MODULE['<{psgdpr}prestashop>dataconfig_5680774f084e3133256426b423793cdf'] = 'Lista modulelor conforme';
$_MODULE['<{psgdpr}prestashop>dataconfig_fbc7f08ab791ff80697db8706f83ac05'] = 'Găsiți aici lista tuturor elementelor care sunt conforme cu GDPR.';
$_MODULE['<{psgdpr}prestashop>dataconfig_e7040e6151b9ba04bebf1ac35a3b9773'] = 'Datele PrestaShop';
$_MODULE['<{psgdpr}prestashop>dataconfig_007798f2b7231fa9572e704aa3e2b864'] = 'Găsiți mai jos lista modulelor instalate care nu sunt afișate mai sus: Dacă considerați că unul sau mai multe module colectează datele dvs. personale (';
$_MODULE['<{psgdpr}prestashop>dataconfig_a4ea91cd0d1ea33d8113457644dd6157'] = 'faceți clic aici';
$_MODULE['<{psgdpr}prestashop>dataconfig_b1cb373e07897abad098c86e8f026b20'] = 'pentru a afla care sunt datele definite ca personale):';
$_MODULE['<{psgdpr}prestashop>dataconfig_d50e4299564d3d86275eaa685cc64a0b'] = 'Vă rugăm să vă asigurați că aveți acces la cea mai recentă versiune a acestor module, pentru a beneficia pe deplin de actualizarea RGPD.';
$_MODULE['<{psgdpr}prestashop>dataconfig_9f776b9ffe1e70cc7e59acbc33dff365'] = 'Dacă acestea tot nu sunt afișate în blocul de mai sus, vă invităm să-i contactați pe dezvoltatorii respectivi pentru a afla mai multe informații despre aceste module. ';
$_MODULE['<{psgdpr}prestashop>dataconfig_301d4d850b1fd6e10ae104b17235e3c7'] = 'Gestionați datele personale ale clientului';
$_MODULE['<{psgdpr}prestashop>dataconfig_2e0a7d4ff8913a1d22b0c74c72c7008c'] = 'Căutați un client existent tastând primele litere ale numelui sau e-mailului său.';
$_MODULE['<{psgdpr}prestashop>dataconfig_bac34a31ff6a35a479858c60dffe3705'] = 'Căutați numele SAU e-mailul clientului';
$_MODULE['<{psgdpr}prestashop>dataconfig_d69e451e32539a7b9b79381d327fc028'] = 'De. ex. ion popescu...';
$_MODULE['<{psgdpr}prestashop>dataconfig_13348442cc6a27032d2b4aa28b75a5d3'] = 'Căutare';
$_MODULE['<{psgdpr}prestashop>dataconfig_2f43ddb446e84df137be34b78029835f'] = 'Pentru a vizualiza toate datele pe care magazinul dvs. le-a colectat de la un anumit client, faceți clic pe blocul corespunzător al clientului';
$_MODULE['<{psgdpr}prestashop>dataconfig_261e15beaa4972092243d9a678afa8a3'] = 'Numărul comenzilor';
$_MODULE['<{psgdpr}prestashop>dataconfig_3ec365dd533ddb7ef3d1c111186ce872'] = 'Detalii';
$_MODULE['<{psgdpr}prestashop>dataconfig_596403b61367ca30d6de75812275f9cd'] = 'Eliminați datele';
$_MODULE['<{psgdpr}prestashop>dataconfig_5fde470e78d8f75e778801d4ec4bd91b'] = 'Descărcați facturile';
$_MODULE['<{psgdpr}prestashop>dataconfig_6016c2bb7bd8dd79293094d37ebfea3f'] = 'Datele clientului';
$_MODULE['<{psgdpr}prestashop>dataconfig_997c785cce775d7c1014ffbd6e446d50'] = 'Informații generale';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_019ec3132cdf8ee0f2e2a75cf5d3e459'] = 'Sex';
$_MODULE['<{psgdpr}prestashop>dataconfig_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>dataconfig_9c37b7b6ff829e977df287900543ea54'] = 'Data nașterii';
$_MODULE['<{psgdpr}prestashop>dataconfig_9d8d2d5ab12b515182a505f54db7f538'] = 'Vârstă';
$_MODULE['<{psgdpr}prestashop>dataconfig_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail';
$_MODULE['<{psgdpr}prestashop>dataconfig_4994a8ffeba4ac3140beb89e8d41f174'] = 'Limbă';
$_MODULE['<{psgdpr}prestashop>dataconfig_3112209b2dd9b55cf5dbc4865dd15afd'] = 'Data creării';
$_MODULE['<{psgdpr}prestashop>dataconfig_5e5914912e8d2f2765525840acf98bea'] = 'Ultima vizită';
$_MODULE['<{psgdpr}prestashop>dataconfig_60adc330494a66981dec101c81e27f03'] = 'Număr de înregistrare SIRET';
$_MODULE['<{psgdpr}prestashop>dataconfig_6c5a79a30b5c5c4fb6ec45735d32edc2'] = 'Cod APE';
$_MODULE['<{psgdpr}prestashop>dataconfig_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>dataconfig_15bbb9d0bbf25e8d2978de1168c749dc'] = 'Site web';
$_MODULE['<{psgdpr}prestashop>dataconfig_284b47b0bb63ae2df3b29f0e691d6fcf'] = 'Adrese';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_effdb9ce6c5d44df31b89d7069c8e0fb'] = 'Pseudonim';
$_MODULE['<{psgdpr}prestashop>dataconfig_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>dataconfig_49ee3087348e8d44e1feda1917443987'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>dataconfig_dd7bf230fde8d4836917806aff6a6b27'] = 'Adresă';
$_MODULE['<{psgdpr}prestashop>dataconfig_fac322c3b42d04806299ae195f8a9238'] = 'Telefon (Telefoane)';
$_MODULE['<{psgdpr}prestashop>dataconfig_59716c97497eb9694541f7c3d37b1a4d'] = 'Țară';
$_MODULE['<{psgdpr}prestashop>dataconfig_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_587bb937485e3dbe02ea0d281600bb52'] = 'Nicio adresă';
$_MODULE['<{psgdpr}prestashop>dataconfig_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Comenzi';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_63d5049791d9d79d86e9a108b0a999ca'] = 'Referință';
$_MODULE['<{psgdpr}prestashop>dataconfig_c453a4b8e8d98e82f35b67f433e3b4da'] = 'Plată';
$_MODULE['<{psgdpr}prestashop>dataconfig_d02bbc3cb147c272b0445ac5ca7d1a36'] = 'Situația comenzii';
$_MODULE['<{psgdpr}prestashop>dataconfig_ea067eb37801c5aab1a1c685eb97d601'] = 'Total plătit';
$_MODULE['<{psgdpr}prestashop>dataconfig_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_08cfa7751d1812e961560f623e082aba'] = 'Nicio comandă';
$_MODULE['<{psgdpr}prestashop>dataconfig_fc26e55e0993a75e892175deb02aae15'] = 'Coșuri';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_490aa6e856ccf208a054389e47ce0d06'] = 'ID';
$_MODULE['<{psgdpr}prestashop>dataconfig_1070734fc83ac44f690c17af28986fb7'] = 'Total produs(e)';
$_MODULE['<{psgdpr}prestashop>dataconfig_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_bbd47e26c95290218b2fc449e54e8cdc'] = 'Niciun coș';
$_MODULE['<{psgdpr}prestashop>dataconfig_41de6d6cfb8953c021bbe4ba0701c8a1'] = 'Mesaje';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_a12a3079e14ced46e69ba52b8a90b21a'] = 'IP';
$_MODULE['<{psgdpr}prestashop>dataconfig_4c2a8fe7eaf24721cc7a9f0175115bd4'] = 'Mesaj';
$_MODULE['<{psgdpr}prestashop>dataconfig_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_58a747ef5d07d22101bdcd058e772ff9'] = 'Niciun mesaj';
$_MODULE['<{psgdpr}prestashop>dataconfig_93bd48ecb9c4d5c4eec7fefffbb2070f'] = 'Ultimele conectări';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_33e29c1d042c0923008f78b46af94984'] = 'Originea solicitării';
$_MODULE['<{psgdpr}prestashop>dataconfig_57f32d7d0e6672cc2b60bc7a49f91453'] = 'Pagină vizualizată';
$_MODULE['<{psgdpr}prestashop>dataconfig_fae6aba9bc4e69842be0ac98e15c4c99'] = 'Timp petrecut pe pagină';
$_MODULE['<{psgdpr}prestashop>dataconfig_75ba8d70e3692ba200f0e0df37b4d2ae'] = 'Adresă IP';
$_MODULE['<{psgdpr}prestashop>dataconfig_44749712dbec183e983dcd78a7736c41'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_c51e6bdf66e5d601e85d055301014410'] = 'Nicio conectare';
$_MODULE['<{psgdpr}prestashop>dataconfig_e55f75a29310d7b60f7ac1d390c8ae42'] = 'Modul';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_61a649a33f2869e5e35fbb7aff3a80d9'] = 'E-MAIL';
$_MODULE['<{psgdpr}prestashop>dataconfig_596403b61367ca30d6de75812275f9cd'] = 'Eliminați datele';
$_MODULE['<{psgdpr}prestashop>dataconfig_e55f75a29310d7b60f7ac1d390c8ae42'] = 'Modul';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_f9dd946cc89c1f3b41a0edbe0f36931d'] = 'TELEFON';
$_MODULE['<{psgdpr}prestashop>dataconfig_596403b61367ca30d6de75812275f9cd'] = 'Eliminați datele';
$_MODULE['<{psgdpr}prestashop>dataconfig_e55f75a29310d7b60f7ac1d390c8ae42'] = 'Modul';
$_MODULE['<{psgdpr}prestashop>dataconfig_6370c5a9859b827de2be9d30ec4b997e'] = 'Date personale';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_42a7b2626eae970122e01f65af2f5092'] = 'Nicio dată';
$_MODULE['<{psgdpr}prestashop>dataconfig_f3f5e3b297c7980c5af3c17a4c0ebcf2'] = 'Nu sunt rezultate în baza de date pentru';
$_MODULE['<{psgdpr}prestashop>dataconfig_e9a837cf85ac119ec97ac242df3ce0d3'] = 'Dacă căutați pe cineva fără un cont de client, vă rugăm să căutați adresa completă de e-mail sau numărul de telefon pe care l-a lăsat.';
$_MODULE['<{psgdpr}prestashop>dataconfig_f049593e15945e4e954d5a8803ec1e9f'] = 'Totuși, puteți continua procesul de ștergere pentru această adresă (numai dacă ați făcut actualizarea RGPD).';
$_MODULE['<{psgdpr}prestashop>menu_be11c74c1dd7f307bb80183a90dc2067'] = 'Începeți';
$_MODULE['<{psgdpr}prestashop>menu_949de8585e0cc176f0e47317ce985023'] = 'Gestionarea datelor personale';
$_MODULE['<{psgdpr}prestashop>menu_04d691f0f2b299343d6431e23cfe6be4'] = 'Personalizarea casetei de bifare a consimțământului';
$_MODULE['<{psgdpr}prestashop>menu_e898759faaefeae772f161b7dbdc31ec'] = 'Urmărirea activității clientului';
$_MODULE['<{psgdpr}prestashop>menu_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Ajutor';
$_MODULE['<{psgdpr}prestashop>menu_34b6cd75171affba6957e308dcbd92be'] = 'Versiune';
$_MODULE['<{psgdpr}prestashop>menu_4fb72931ca985b1f33b9b2b5b604c6ef'] = 'URL-ul a fost copiat în clipboard!';
$_MODULE['<{psgdpr}prestashop>menu_cdec11f09fb339e01121a4ef5224594c'] = 'Descărcarea facturilor a reușit.';
$_MODULE['<{psgdpr}prestashop>menu_f0a90a69e73126cef322ef544ebb641f'] = 'Nu sunt facturi disponibile pentru acest client.';
$_MODULE['<{psgdpr}prestashop>menu_729a51874fe901b092899e9e8b31c97a'] = 'Sigur?';
$_MODULE['<{psgdpr}prestashop>menu_7db050b741b6de7a1f955472f33d8325'] = 'Atenție! Această acțiune este ireversibilă. Asigurați-vă că ați descărcat toate facturile clientului (dacă acesta are) înainte de a face clic pe Confirmare ștergere.';
$_MODULE['<{psgdpr}prestashop>menu_1bf87e44da33943a3baad59edd51f4a0'] = 'Anulare acțiune';
$_MODULE['<{psgdpr}prestashop>menu_0cac91b206582769ea4876fbbae5f2ab'] = 'Confirmare ștergere';
$_MODULE['<{psgdpr}prestashop>menu_3906c6e10eba20bf0f2b77d442cba324'] = 'Ștergerea datelor clientului a reușit!';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_d9fa187eaea160a2e658d8c699a54141'] = 'Informații generale';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_b488d087b6f8bc41d29b24d4b32357cf'] = 'Sex';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_78d60cacb77218861fcced054f5b5ecb'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_3a040a2948e8f40c4ab496686f08c7d4'] = 'Data nașterii';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_8e527e5855d1d2cf120bb010f793a121'] = 'Vârstă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_19468e772074db41cd940412da526cf7'] = 'E-mail';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_48a3de529a86a54e01542b49895e4f79'] = 'Limbă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_6dd060f16f012a57b8858abec6f07ece'] = 'Data creării';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_8fae5f5351510ae838b82b8cb664329e'] = 'Ultima vizită';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_32c0b03971a17ed3e9e930441c8c8981'] = 'Număr de înregistrare SIRET';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_3711b13fcc0fab67d450bf58e4ca8041'] = 'Cod APE';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_89cb9ebf2e9ae6cd2e01241713d58a60'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_ddfe92c4accf6219d3037cedff216f96'] = 'Site web';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_d1c6745e9875bebccff132963d238892'] = 'Adrese';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_c7dadd0fc75816d6f4461b0a4337ec77'] = 'Pseudonim';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_89cb9ebf2e9ae6cd2e01241713d58a60'] = 'Companie';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_78d60cacb77218861fcced054f5b5ecb'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_b3c2fa25ef2fd9094c8db6a6be0d86b7'] = 'Adresă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_c553b70595458adb6f9c482586a7c975'] = 'Țară';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_7f1dd25cb56a7743b5de6b25c83381ce'] = 'Nicio adresă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_223f09070fedef21b9d048bd9cb6e5bb'] = 'Comenzi';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_9f3eba662fe6a91554327253d121d3fe'] = 'Referință';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_4ff3965036cc17d1b1d235247e289e3a'] = 'Plată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_1e92522a9eae7bb47cb6f9ecda64772e'] = 'Situația comenzii';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_186831f877326ee6ffb28b100e264466'] = 'Total plătit';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_8d285d22f04c8aef4bb0f77c64d18a07'] = 'Nicio comandă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_881813627711526c5fb904dea2fbbce9'] = 'Produsele achiziționate';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_5c38c19739b663587c2675e9226fbafa'] = 'Comandă';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_313cee5fa1fb1bdcd755e401f010e17c'] = 'Referința produsului';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_78d60cacb77218861fcced054f5b5ecb'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_301dfe493cf43a97d0364a1ce74f7cd6'] = 'Cantitate';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_99c7411294897a9684b1febf645c97a6'] = 'Coșuri';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_29707e1af13d8d168aeea19503b81a66'] = 'ID';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_265fe0f121d8825800814407a7178f99'] = 'Total produs(e)';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_9f0725e38a9b4a39abc435af0c032359'] = 'Niciun coș';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_eeca13ee2bfe0f4206bb854f786a92e6'] = 'ID-ul coșului';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_21629b6404a39cd3a29e77f5874a2aa6'] = 'Referința produsului';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_78d60cacb77218861fcced054f5b5ecb'] = 'Nume';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_301dfe493cf43a97d0364a1ce74f7cd6'] = 'Cantitate';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_32a76f42056c7b229e8c18bbbc4e3f34'] = 'Nici un produs';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_9f0725e38a9b4a39abc435af0c032359'] = 'Niciun coș';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_cd4413f44677bebbe8affec0ac0ab677'] = 'Mesaje';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_1feb8ecdfe346cc08e78292d3cdb68b7'] = 'IP';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_d854a9322c0bda796c31e492ecf25cfa'] = 'Mesaj';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_d20b797edc3f122c244a97c76100a2ca'] = 'Niciun mesaj';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_5290bfc0524778b4595d06d4b39a9b5f'] = 'Ultimele conectări';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_8383d11f810ff57cbfed6cbe811d9755'] = 'Originea solicitării';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_aecc3e17bba4b6276779d11270b4260f'] = 'Pagină vizualizată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_72a16917743126e62fa5ad7ac12d3b64'] = 'Timp petrecut pe pagină';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_1cce42e207bdb4b19a3f6f0bdade02bf'] = 'Adresă IP';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_c553b70595458adb6f9c482586a7c975'] = 'Țară';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_294c41fee9e8c290202ebc85be5d88d3'] = 'Dată';
$_MODULE['<{psgdpr}prestashop>exportdatatocsv_f4dd87d5cf980f2f91a84b338fd5fe4f'] = 'Nicio conectare';
