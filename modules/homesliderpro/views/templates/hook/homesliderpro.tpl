<!-- Module SlidersEverywhere -->
{if isset($homeslider_slides) && $homeslider_slides|@count > 0}

    <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="{$configuration.pause}">
        <ol class="carousel-indicators clearfix">
            {assign "number" "0"}
            {foreach from=$homeslider_slides item=slide}
                <li data-target="#carousel" data-slide-to="{$number}" {if $number == 0}class="active"{/if}></li>
                {$number = $number+1}
            {/foreach}
        </ol>
         <div class="carousel-inner" role="listbox">
            {assign "number" "0"}
    		{foreach from=$homeslider_slides item=slide}
    			{$number = $number+1}
    			{if $slide.active && $slide.image != ''}
                    <div class="item {if $number == 1}active{/if}">
                        <div class="img-wrapper">
                            <img class="img-responsive" src="{$content_dir}modules/homesliderpro/images/{$slide.image|escape:'htmlall':'UTF-8'}" alt="{$slide.title|escape:'htmlall':'UTF-8'}" height="{$configuration.height|intval}" width="{$configuration.width|intval}"  />
                        </div>
                        <div class="carousel-caption" style="{$slide.legend|escape:'htmlall':'UTF-8'}">
                            {if $slide.url != ''}
        						<a href="{$slide.url|escape:'htmlall':'UTF-8'}" {if $slide.new_window == 1}target="_blank"{/if}>
        					{/if}
                                {if $configuration.show_title == 1 && $slide.title != ''}
            						<h1 class="slidetitle{if $configuration.title_pos == 1} right{else} left{/if}">{$slide.title|escape:'htmlall':'UTF-8'}</h1>
            					{/if}
                            {if $slide.url != ''}
    						</a>
    					{/if}
                            {if $slide.description != ''}
        						<span class="slide_description">{$slide.description}</span>
        					{/if}
                        </div>
                    </div>            
    			{/if}
    		{/foreach}
        </div>
    </div>
{/if}