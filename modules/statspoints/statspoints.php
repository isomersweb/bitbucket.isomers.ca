<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StatsPoints extends ModuleGrid
{
	private $html;
	private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statspoints';
		$this->tab = 'analytics_stats';
		$this->version = '1.0.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

		parent::__construct();

		$this->default_sort_column = 'lastname';
		$this->default_sort_direction = 'DESC';
		$this->empty_message = $this->l('Empty recordset returned');
		$this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');

		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$this->columns = array(
			array(
				'id' => 'lastname',
				'header' => $this->l('Last Name'),
				'dataIndex' => 'lastname',
				'align' => 'center'
			),
			array(
				'id' => 'firstname',
				'header' => $this->l('First Name'),
				'dataIndex' => 'firstname',
				'align' => 'center'
			),
			array(
				'id' => 'email',
				'header' => $this->l('Email'),
				'dataIndex' => 'email',
				'align' => 'center'
			),
			array(
				'id' => 'totalBPs',
				'header' => $this->l('Visits'),
				'dataIndex' => 'totalBPs',
				'align' => 'center'
			),
			array(
				'id' => 'totalCredits',
				'header' => $this->l('Credits'),
				'dataIndex' => 'totalCredits',
				'align' => 'center'
			),
			array(
				'id' => 'totalCommissions',
				'header' => $this->l('Commissions'),
				'dataIndex' => 'totalCommissions',
				'align' => 'center'
			),
		);

		$this->displayName = $this->l('Report based on Beauty Points');
//		$this->description = $this->l('Adds a Report based on Beauty Points to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		return (parent::install() && $this->registerHook('AdminStatsModules'));
	}

	public function hookAdminStatsModules($params)
	{
		$engine_params = array(
			'id' => 'id_customer',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMessage' => $this->empty_message,
			'pagingMessage' => $this->paging_message
		);
        $currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

        $this->getData();  		    
        $id_customer = Tools::getValue('id_customer');
        
		if (Tools::getValue('export')) {
		  if (empty($id_customer)) {
		      $this->_csv = $this->l('id_customer').';'
                .$this->l('Last Name').';'
                .$this->l('First Name').';'
                .$this->l('Email').';'
                .$this->l('Country').';'
                .$this->l('BPs Accured').';'
                .$this->l('Credits Accured').';'
                .$this->l('Commissions Accrued').' ('.Tools::safeOutput($currency->iso_code).')'.';'
                .$this->l('Points Exp. within 30 days').';'
                .$this->l('Points Exp. within 60 days').';'
                .$this->l('Points Exp. within 90 days').';';
		  }
          else {}
          $this->_csv .= "\n";
          
	      foreach ($this->_values as $val) {
            foreach ($val as $val1) {
                $this->_csv .= $val1;
                $this->_csv .= ';';
            }
            $this->_csv .= "\n";
          }
          $this->_displayCsv();
		}
			

		$this->html = '
		<div class="panel-heading">
			'.$this->displayName.'
		</div>
		<!--<h4>'.$this->l('Guide').'</h4>
			<div class="alert alert-warning">
				<h4>'.$this->l('Develop clients\' loyalty').'</h4>
				<div>
					'.$this->l('Keeping a client can be more profitable than gaining a new one. That is one of the many reasons it is necessary to cultivate customer loyalty.').' <br />
					'.$this->l('Word of mouth is also a means for getting new, satisfied clients. A dissatisfied customer can hurt your e-reputation and obstruct future sales goals.').'<br />
					'.$this->l('In order to achieve this goal, you can organize:').'
					<ul>
						<li>'.$this->l('Punctual operations: commercial rewards (personalized special offers, product or service offered), non commercial rewards (priority handling of an order or a product), pecuniary rewards (bonds, discount coupons, payback).').'</li>
						<li>'.$this->l('Sustainable operations: loyalty points or cards, which not only justify communication between merchant and client, but also offer advantages to clients (private offers, discounts).').'</li>
					</ul>
					'.$this->l('These operations encourage clients to buy products and visit your online store more regularly.').'
				</div>
			</div>-->';
        //echo '<pre>';
        if (empty($id_customer)) {
            $this->html .= '
				<div>
					<table class="table"><thead><tr>
    						<th>
								<span class="title_box  active">'.$this->l('Last Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('First Name').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Email').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Country').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('BPs Accured').'</span>
							</th>
							<th>
								<span class="title_box  active">'.$this->l('Credits Accured').'</span>
							</th>
							<th class="center">
								<span class="title_box  active">'.html_entity_decode($this->l('Commissions <br>Accured').' ('.Tools::safeOutput($currency->iso_code)).')'.'</span>
							</th>
							<th class="center">
								<span class="title_box active">'.html_entity_decode($this->l('Points Exp. <br> within 30 days')).'</span>
							</th>
							<th class="center">
								<span class="title_box active">'.html_entity_decode($this->l('Points Exp. <br> within 60 days')).'</span>
							</th>
							<th class="center">
								<span class="title_box active">'.html_entity_decode($this->l('Points Exp. <br> within 90 days')).'</span>
							</th>
                    </tr></thead><tbody>';
            
            foreach ($this->_values as $val) {
                $this->html .= '<tr>';
                $this->html .= '<td class="left">'.$val['lastname'].'</td>';        
                $this->html .= '<td class="left">'.$val['firstname'].'</td>';        
                $this->html .= '<td class="left">'.$val['email'].'</td>';        
                $this->html .= '<td class="left">'.$val['country'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalBPs'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalCredits'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalCommissions'].'</td>';  
                $this->html .= '<td class="center">'.$val['totalExp30'].'</td>';  
                $this->html .= '<td class="center">'.$val['totalExp60'].'</td>';        
                $this->html .= '<td class="center">'.$val['totalExp90'].'</td>';        
                $this->html .= '</tr>';                
            }
    		$this->html .= '</tbody></table>';
            $this->html .= '<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=').'1">
    			<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
    		</a>';
    		//echo 'done';die;
    		return $this->html;	
        }
        else {}	
	}    
    
	public function getData()
	{
	   $id_customer = Tools::getValue('id_customer');
       if (empty($id_customer)) {
    		$this->query = '
    		SELECT SQL_CALC_FOUND_ROWS c.`id_customer`, c.`lastname`, c.`firstname`, c.`email`, cl.name as country,    			
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0))* '.(int)Myconf::get('REWARDS_VIRTUAL_VALUE_1', null, 2).', 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.id_customer = c.id_customer
    				AND r.date_add BETWEEN '.$this->getDate().'
                    AND r.id_template = 8
    				AND r.virt = 1
    			), 0) as totalBPs,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0))* '.(int)Myconf::get('REWARDS_VIRTUAL_VALUE_1', null, 2).', 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.id_customer = c.id_customer
    				AND r.date_add BETWEEN '.$this->getDate().'
                    AND r.id_template != 8
    				AND r.virt = 1
    			), 0) as totalCredits,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(r.`credits`, 0)), 2)
    				FROM `'._DB_PREFIX_.'rewards` r
    				WHERE r.id_customer = c.id_customer
    				AND r.date_add BETWEEN '.$this->getDate().'
                    AND r.virt = 0
    			), 0) as totalCommissions,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(cr.`reduction_amount`, 0)), 2)
    				FROM `'._DB_PREFIX_.'cart_rule` cr
    				WHERE cr.id_customer = c.id_customer
    				AND cr.date_to BETWEEN NOW() AND ADDDATE(NOW(), INTERVAL 30 DAY)
    			), 0) as totalExp30,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(cr.`reduction_amount`, 0)), 2)
    				FROM `'._DB_PREFIX_.'cart_rule` cr
    				WHERE cr.id_customer = c.id_customer
    				AND cr.date_to BETWEEN ADDDATE(NOW(), INTERVAL 30 DAY) AND ADDDATE(NOW(), INTERVAL 60 DAY)
    			), 0) as totalExp60,
    			IFNULL((
    				SELECT ROUND(SUM(IFNULL(cr.`reduction_amount`, 0)), 2)
    				FROM `'._DB_PREFIX_.'cart_rule` cr
    				WHERE cr.id_customer = c.id_customer
    				AND cr.date_to BETWEEN ADDDATE(NOW(), INTERVAL 60 DAY) AND ADDDATE(NOW(), INTERVAL 90 DAY)
    			), 0) as totalExp90

     		FROM `'._DB_PREFIX_.'customer` c
            LEFT JOIN `'._DB_PREFIX_.'address` ad ON c.id_customer = ad.id_customer
    		LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON ad.id_country = cl.id_country
            LEFT JOIN `'._DB_PREFIX_.'rewards` rw ON rw.id_customer = c.id_customer
    		WHERE rw.date_add BETWEEN '.$this->getDate()
    			.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c').
    			'GROUP BY c.`id_customer`, c.`lastname`, c.`firstname`, c.`email`';
    
    		if (Validate::IsName($this->_sort))
    		{
    		      if (empty($this->_sort)) $this->_sort = 'lastname';
    			$this->query .= ' ORDER BY `'.bqSQL($this->_sort).'` DESC';
    			if (isset($this->_direction) && Validate::isSortDirection($this->_direction))
    				$this->query .= ' '.$this->_direction;
    		}
		   //echo $this->query; die;
    		if (($this->_start === 0 || Validate::IsUnsignedInt($this->_start)) && Validate::IsUnsignedInt($this->_limit))
    			$this->query .= ' LIMIT '.(int)$this->_start.', '.(int)$this->_limit;
            $this->_values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
    		$this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
        }
        else {}
	}
}
