<?php

require_once _PS_MODULE_DIR_ .
    'belvg_discounts/classes/BelvgDiscountAbstract.php';

/**
 * BelvgDiscountCustomer
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class BelvgDiscountCustomer extends BelvgDiscountAbstract
{
    protected $_id_customer = NULL;

    /**
     * BelvgDiscountCustomer::__construct()
     * 
     * @param mixed $id_customer
     * @return
     */
    public function __construct($id_customer)
    {
        $this->_id_customer = (int)$id_customer;
    }

    /**
     * BelvgDiscountCustomer::setData()
     * 
     * @param mixed $points
     * @return bool
     */
    public function setData($points)
    {
        return Db::getInstance()->Execute('
            REPLACE INTO `' . $this->getModule()->getDbPrefix() .
            'customer_points`
            SET `id_customer` = ' . $this->_id_customer . ', `points` = ' . (int)
            $points);
    }

    /**
     * BelvgDiscountCustomer::getPoints()
     * 
     * @return int
     */
    public function getPoints()
    {
        return (int)Db::getInstance()->getValue('
            SELECT `points` FROM `' . $this->getModule()->getDbPrefix() .
            'customer_points`
            WHERE `id_customer` = ' . $this->_id_customer);
    }
}
