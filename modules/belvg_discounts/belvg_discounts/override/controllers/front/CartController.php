<?php

/**
 * CartController
 *
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class CartController extends CartControllerCore
{
    /**
     * CartController::init()
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        if (!Module::isEnabled('belvg_discounts')) {
            return;
        }

        require_once _PS_MODULE_DIR_ . 'belvg_discounts/classes/BelvgDiscountCart.php';
        require_once _PS_MODULE_DIR_ .
            'belvg_discounts/classes/BelvgDiscountProduct.php';
        require_once _PS_MODULE_DIR_ .
            'belvg_discounts/classes/BelvgDiscountCustomer.php';
    }

    /**
     * CartController::processChangeProductInCart()
     *
     * @return void
     */
    protected function processChangeProductInCart()
    {
        $shop_id = 1;
        if (!is_null(Shop::getContextShopID())) {
            $shop_id = Shop::getContextShopID();
        }

        if (!Module::isEnabled('belvg_discounts')) {
            return parent::processChangeProductInCart();
        }

        $id_product = (int)Tools::getValue('product_id');
        if (Tools::getIsset('belvg_add_to_cart')) {
            $addons = Tools::getValue('addon_ids');
            if ($id_product and is_array($addons) and count($addons)) {
                $_addons = array();
                foreach ($addons as $addon) {
                    $_product = new Product((int)$addon);
                    $this->id_product = $_product->id;
                    $this->id_product_attribute = $_product->getDefaultIdProductAttribute();
                    $this->errors = FALSE;
                    parent::processChangeProductInCart();
                    if (!$this->errors) {
                        $_addons[] = $_product->id . '_' . $this->id_product_attribute;
                    }
                }

                if (count($_addons)) {
                    $product = new Product($id_product);
                    $belvgCart = new BelvgDiscountCart($this->context->cart->id, $product->id, $product->
                        getDefaultIdProductAttribute(), $shop_id);
                    $belvgCart->addData(array('ids_addons' => $_addons));
                }
            }
        } else {
            if (Tools::getIsset('add_point') || Tools::getIsset('delete_point')) {
                if ($this->context->customer->isLogged()) {
                    $belvgCustomer = new BelvgDiscountCustomer($this->context->customer->id);
                    $points = (int)$belvgCustomer->getPoints();
                    $belvgProduct = new BelvgDiscountProduct($this->id_product, $shop_id);
                    $data = $belvgProduct->getData();
                    $buyPoints = (int)$data['buy_points'];
                    $belvgCart = new BelvgDiscountCart($this->context->cart->id, $this->id_product,
                        $this->id_product_attribute, $shop_id);

                    if (Tools::getIsset('add_point')) {
                        if ($points >= $buyPoints) {
                            $qty = (int)$belvgCart->getQtyByPoints();
                            $belvgCart->setQtyByPoints($qty + 1);
                        }
                    }

                    if (Tools::getIsset('delete_point')) {
                        $qty = (int)$belvgCart->getQtyByPoints();
                        if ($qty > 0) {
                            $belvgCart->setQtyByPoints(--$qty);
                        }
                    }
                }
            }

            parent::processChangeProductInCart();
        }
    }

    protected function processDeleteProductInCart()
    {
        parent::processDeleteProductInCart();
        $belvgCart = new BelvgDiscountCart($this->context->cart->id, $this->id_product,
            $this->id_product_attribute, $shop_id);
        $belvgCart->removeData();
    }
}
