<?php

/**
 * AdminCustomersController
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class AdminCustomersController extends AdminCustomersControllerCore
{
    /**
     * AdminCustomersController::ajaxProcessUpdateBelvgCustomerPoints()
     * 
     * @return void
     */
    public function ajaxProcessUpdateBelvgCustomerPoints()
    {
        if ($this->tabAccess['edit'] === '1') {
            $points = abs((int)trim(Tools::getValue('points')));
            $customer = new Customer((int)Tools::getValue('id_customer'));
            if (!Validate::isLoadedObject($customer)) {
                die('error');
            }

            require_once _PS_MODULE_DIR_ .
                'belvg_discounts/classes/BelvgDiscountCustomer.php';
            $belvg_customer = new BelvgDiscountCustomer($customer->id);
            if ($belvg_customer->setData($points)) {
                die((string)$points);
            }
        }
    }
}
