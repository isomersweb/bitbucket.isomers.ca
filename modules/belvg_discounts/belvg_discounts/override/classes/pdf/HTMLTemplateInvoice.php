<?php

/**
 * HTMLTemplateInvoice
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class HTMLTemplateInvoice extends HTMLTemplateInvoiceCore
{
    /**
     * HTMLTemplateInvoice::getBelvgDiscountsTplDir()
     * 
     * @return string
     */
    public function getBelvgDiscountsTplDir()
    {
        return _PS_MODULE_DIR_ . 'belvg_discounts/tpl/front/';
    }

    /**
     * HTMLTemplateInvoice::getContent()
     * 
     * @return string
     */
    public function getContent()
    {
        if (!Module::isEnabled('belvg_discounts')) {
            return parent::getContent();
        }

        parent::getContent();
        return $this->smarty->fetch($this->getBelvgDiscountsTplDir() . 'invoice.tpl');
    }
}
