(function($){
	$(document).ready(function(){
		
		//Catalog Page:
		var catalogButtons = $('a.button.ajax_add_to_cart_button.exclusive');
		if (catalogButtons.length) {
			var productIds = [];
			catalogButtons.each(function(){
				productIds.push($(this).attr('rel').replace('ajax_id_product_', ''));
			});
			
			$.ajax({
				url: belvg_discounts_url + '&action=getPoints',
				data: {ids: productIds},
				success: function(response){
					var json = $.parseJSON(response);
					for (var id in json) {
						var catalog_li = $('a.button.ajax_add_to_cart_button.exclusive[rel="ajax_id_product_'+id+'"]').parents('li:first');
						if (json[id].points) {
							catalog_li.find('a.button.ajax_add_to_cart_button.exclusive')
								.append(json[id].points);
						}
						
						if (json[id].buy_points) {
							catalog_li.find('.price').append(json[id].buy_points);
						}
					}
				}
			});
		}
		
		//Product Page:
		if (belvg_discounts_id_product > 0) {
			$.ajax({
				url: belvg_discounts_url + '&action=getProductPoints',
				data: {id: belvg_discounts_id_product},
				success: function(response){
					var json = $.parseJSON(response);
					if (json.link) {
						$('#add_to_cart').before('<div class="belvg_discounts_product">'+json.link+'</div><br>');
					}
					
					if (json.buy) {
						$('.our_price_display:first').append(json.buy);
					}
				}
			});

			$('#belvg_add_to_cart').click(function(){
				if ($('#belvg_addons .addon_item .input:checked').length > 1) {
					$(this).parents('form:first').submit();
				} else {
					$('#add_to_cart input').click();
				}
			});
		}

	});
})(jQuery);