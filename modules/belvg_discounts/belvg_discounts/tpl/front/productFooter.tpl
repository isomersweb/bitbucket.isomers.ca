<div id="belvg_addons" class="clear">
	<form action="{$link->getPageLink('cart')}" method="POST">
		<div class="hidden">
			<input type="hidden" name="product_id" value="{$belvg_id_product}"/>
			<input type="hidden" name="belvg_add_to_cart" value="1"/>
			<input type="hidden" name="add" value="1" />
			<input type="hidden" name="token" value="{$static_token}" />
		</div>
		<ul class="idTabs idTabsShort clearfix">
			<li><a href="#idBelvgTab1" class="selected">{l s='Frequently Bought Together' mod='belvg_discounts'}</a></li>										
		</ul>
		<div class="sheets align_justify">
			<div id="idBelvgTab1" class="rte">
				<ul>
					{foreach from=$belvg_addons item=addon name=addons_list}
						<li class="addon_item" data-points="{$addon.points}" data-price="{$addon.product->getPrice() - $addon.discount}" data-discount="{$addon.discount}">
							<input class="input check" type="checkbox" checked="checked" {if $smarty.foreach.addons_list.first}style="width:0px;opacity:0;"{/if} name="addon_ids[]" value="{$addon.id_addon}" />
							<a href="{$belvg_context->link->getProductLink($addon.id_addon)|escape:'htmlall':'UTF-8'}">
								{if $addon.product_image.id_image}
									<img title="{$addon.product_name|escape:'htmlall':'UTF-8'}" width="60" src="{$belvg_context->link->getImageLink($addon.product->link_rewrite, $addon.product_image.id_image, 'home_default')}" />								
								{else}
									<img title="{$addon.product_name|escape:'htmlall':'UTF-8'}" width="60" src="{$belvg_context->link->getImageLink($addon.product->link_rewrite, Context::getContext()->language->iso_code|cat:'-default', 'home_default')}" />								
								{/if}
							</a>	
								
							<div class="price">								
								<p class="our_price_display" style="line-height: 12px;">
									{if $addon.product->getPrice() > ($addon.product->getPrice() - $addon.discount)}
										<span style="font-size: 15px;">{convertPrice price=$addon.product->getPrice() - $addon.discount}</span>
										<br><strike><span style="font-size: 12px;">{convertPrice price=$addon.product->getPrice()}</span></strike>
									{else}
										<span style="font-size: 15px;">{convertPrice price=$addon.product->getPrice()}</span>
									{/if}
									<br><span class="belvg_discounts_catalog_buy_points">+{$addon.points} {l s='points' mod='belvg_discounts'}</span>
								</p>
							</div>
							
						</li>
						{if $smarty.foreach.addons_list.last}
							<li class="math end">=</li>
						{else}
							<li class="math">+</li>
						{/if}
					{/foreach}
					<li class="summary end">
						<div class="price">								
							<p class="our_price_display" style="line-height: 12px;">
								<span id="belvgResPrice" style="font-size: 19px;">$66.05</span>
								<br><span class="belvg_discounts_catalog_buy_points" id="belvgResPoints">+200 point(s)</span>
								<br><span class="belvg_discounts_catalog_buy_points" id="belvgResDiscount">$130 discount!</span>
								<p class="buttons_bottom_block" style="padding-top:0px">
									<span></span>
									<input id="belvg_add_to_cart" type="button" name="Submit" value="{l s='Add to cart' mod='belvg_discounts'}" class="exclusive">
								</p>
							</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</form>
</div>
<br><br>
<script>
{literal}
	(function($){
		
		function resetPrice()
		{
			var currency_symbol = '{/literal}{$belvg_context->currency->getSign()}{literal}';
			var price = 0;
			var points = 0;
			var discount = 0;
			$('#idBelvgTab1 .addon_item').each(function(){
				if ($(this).find('.input').attr('checked')) {
					var val = parseFloat($(this).attr('data-price'));
					if (!isNaN(val)) {
						price += val;
					}

					var val = parseFloat($(this).attr('data-points'));
					if (!isNaN(val)) {
						points += val;
					}

					var val = parseFloat($(this).attr('data-discount'));
					if (!isNaN(val)) {
						discount += val;
					}
				}
			});		
			
			{/literal}
				$('#belvgResPrice').text(formatCurrency(price, '{$belvg_context->currency->format}', '{$belvg_context->currency->getSign()}', {$belvg_context->currency->blank|intval}));
			{literal}
			
			if (points > 0) {
				$('#belvgResPoints').show().text('+'+points+' {/literal}{l s='point(s)' mod='belvg_discounts'}{literal}');
			} else {
				$('#belvgResPoints').hide()
			}
			
			if (parseFloat(discount).toFixed(2) > 0) {
				$('#belvgResDiscount').show();
				{/literal}
					$('#belvgResDiscount').text(
						'{l s='Discount:' mod='belvg_discounts'} '
						+ formatCurrency(discount, '{$belvg_context->currency->format}', '{$belvg_context->currency->getSign()}', {$belvg_context->currency->blank|intval})
						+ '!'
					);
				{literal}				
			} else {
				$('#belvgResDiscount').hide()
			}
		}
	
		$('#idBelvgTab1 .addon_item .input').click(function(){
			if ($(this).attr('checked')) {
				//$(this).show();
				$(this).parents('.addon_item').css({opacity: '1'}).prevAll('.math:first').css({color: 'black'});
			} else {
				//$(this).hide();
				$(this).parents('.addon_item').css({opacity: '0.3'}).prevAll('.math:first').css({color: 'white'});
			}
			
			if ($('#idBelvgTab1 .addon_item .input:checked').length) {
				$('#idBelvgTab1 .end').show();
			} else {
				$('#idBelvgTab1 .end').hide();
			}
			
			resetPrice();
		});
		
		$('#idBelvgTab1 .addon_item').hover(
			function(){
				$(this).css({opacity: '1'});//.find('.input').show();
			},
			function(){
				if ($(this).find('.input').attr('checked')) {
					$(this).css({opacity: '1'});//.find('.input').show();
				} else {
					$(this).css({opacity: '0.3'});//.find('.input').hide();
				}
			}
		);
		
		resetPrice();
	})(jQuery);
{/literal}
</script>