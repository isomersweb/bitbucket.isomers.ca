<div class="clear"></div>
<div class="separation"></div>
<h2>{l s='Points' mod='belvg_discounts'}</h2>
<script type="text/javascript">
	function saveBelvgCustomerPoints()
	{
		$('#belvg_points_feedback').html('<img src="../img/loader.gif" alt="" />').show();
		var points = encodeURIComponent($('#belvg_points').val());

		$.ajax({
			type: "POST",
			url: "index.php",
			data: "token={getAdminToken tab='AdminCustomers'}&tab=AdminCustomers&ajax=1&action=updateBelvgCustomerPoints&id_customer={$belvg_customer}&points="+points,
			async : true,
			success: function(r) {
				$('#belvg_points_feedback').html('').hide();
				if (r != 'error') {
					$('#belvg_points_feedback').html("<b style='color:green'>{l s='Your points has been saved' mod='belvg_discounts'}</b>").fadeIn(400);
					$('#belvg_points').val(r);
				} else {
					$('#belvg_points_feedback').html("<b style='color:red'>{l s='Error: cannot save your points' mod='belvg_discounts'}</b>").fadeIn(400);
				}

				$('#belvg_points_feedback').fadeOut(3000);
			}
		});
	}
</script>
<input size="25" maxlength="14" id="belvg_points" type="text" value="{$belvg_discounts}">
<span id="belvg_points_feedback"></span>
<br><br>
<a class="button bt-icon add-product" href="javascript:saveBelvgCustomerPoints()">
	<span>{l s='Save Points' mod='belvg_discounts'}</span>
</a>
<div class="separation"></div>
<div class="clear"></div>