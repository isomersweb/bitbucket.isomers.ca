<?php

require_once _PS_MODULE_DIR_ .
    'belvg_discounts/classes/BelvgDiscountAbstract.php';

/**
 * BelvgDiscountProduct
 *
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class BelvgDiscountProduct extends BelvgDiscountAbstract
{
    protected $_id_product = NULL;
    protected $_id_shop = NULL;

    /**
     * BelvgDiscountProduct::__construct()
     *
     * @param mixed $id_product
     * @return
     */
    public function __construct($id_product, $id_shop = 1)
    {
        $this->_id_product = (int)$id_product;
        $this->_id_shop = (int)$id_shop;
    }

    /**
     * BelvgDiscountProduct::getProductData()
     *
     * @param bool $adminTab
     * @return array
     */
    public function getProductData($adminTab = FALSE)
    {
        return array(
            'addons' => $this->getProductAddons($adminTab),
            'points' => $this->getData(),
            );
    }

    /**
     * BelvgDiscountProduct::save()
     *
     * @param mixed $params
     * @return NULL
     */
    public function save($params = array())
    {
        $points = abs((int)trim(Tools::getValue('belvg_product_points')));
        $buy = abs((int)trim(Tools::getValue('belvg_product_buy')));
        $this->setData($points, $buy);

        if ((int)Tools::getValue('belvg_is_add_new_product')) {
            $addon_id = (int)Tools::getValue('belvg_add_id_product');
            if ($addon_id) {
                $addon_points = abs((int)trim(Tools::getValue('belvg_add_new_points')));
                $addon_discount = abs((float)trim(Tools::getValue('belvg_add_new_discount')));
                $this->addProductAddon($addon_id, $addon_discount, $addon_points);
            }
        }

        if ($belvg_delete = (int)Tools::getValue('belvg_delete')) {
            $this->removeProductAddon($belvg_delete);
        }
    }

    /**
     * BelvgDiscountProduct::setData()
     *
     * @param mixed $points
     * @param mixed $buy
     * @return bool
     */
    public function setData($points, $buy)
    {
        return Db::getInstance()->Execute('
            REPLACE INTO `' . $this->getModule()->getDbPrefix() .
            'product_points`
            SET `id_product` = ' . $this->_id_product . ', `points` = ' . (int)
            $points . ', `id_shop` = ' . $this->_id_shop . ',
            `buy_points` = ' . (int)$buy);
    }

    /**
     * BelvgDiscountProduct::getData()
     *
     * @return array
     */
    public function getData()
    {
        return Db::getInstance()->getRow('
            SELECT `points`, `buy_points`, `id_shop` FROM `' . $this->getModule()->
            getDbPrefix() . 'product_points`
            WHERE `id_product` = ' . $this->_id_product . ' AND `id_shop` = ' . $this->_id_shop);
    }

    /**
     * BelvgDiscountProduct::addProductAddon()
     *
     * @param mixed $id_addon
     * @param mixed $discount
     * @param mixed $points
     * @return bool
     */
    public function addProductAddon($id_addon, $discount, $points)
    {
        $_product = new Product((int)$id_addon);
        $price = $_product->getPublicPrice(FALSE, $_product->
            getDefaultIdProductAttribute());
        if ($discount > $price) {
            $discount = $price;
        }

        return Db::getInstance()->Execute('
            REPLACE INTO `' . $this->getModule()->getDbPrefix() . 'product` SET
             `id_product` = ' . $this->_id_product . ',
             `id_addon` = ' . (int)$id_addon . ',
             `discount` = ' . (float)$discount . ',
             `id_shop` = ' . $this->_id_shop . ',
             `points` = ' . (int)$points);
    }

    /**
     * BelvgDiscountProduct::getProductAddons()
     *
     * @param bool $adminTab
     * @return array
     */
    public function getProductAddons($adminTab = FALSE)
    {
        $addons = Db::getInstance()->ExecuteS('
            SELECT * FROM `' . $this->getModule()->getDbPrefix() . 'product`
            WHERE `id_product` = ' . $this->_id_product . ' AND `id_shop` = ' . $this->_id_shop);

        $data = $this->getData();

        if (!$adminTab) {
            array_unshift($addons, array(
                'id_addon' => $this->_id_product,
                'discount' => 0,
                'points' => $data['points'],
                ));
        }

        $rate = Context::getContext()->currency->id;

        foreach ($addons as &$addon) {
        	$_product = new Product($addon['id_addon']);
            $addon['product'] = $_product;
            $addon['product_image'] = $addon['product']->getCover($addon['id_addon']);
            $addon['product_name'] = $addon['product']->name[Context::getContext()->cookie->
                id_lang];
            $addon['discount'] = Tools::convertPrice($addon['discount'], $rate);
            $addon['groups'] = $this->assignAttributesGroups($_product);
        }

        return $addons;
    }

    /**
     * BelvgDiscountProduct::removeProductAddon()
     *
     * @param mixed $id_addon
     * @return int
     */
    public function removeProductAddon($id_addon)
    {
        return (int)Db::getInstance()->getValue('
            DELETE FROM `' . $this->getModule()->getDbPrefix() . 'product`
            WHERE `id_product` = ' . $this->_id_product . ' AND `id_addon` = ' .
            (int)$id_addon . ' AND `id_shop` = ' . $this->_id_shop);
    }

	protected function assignAttributesGroups($_product)
	{
		$colors = array();
		$groups = array();

		// @todo (RM) should only get groups and not all declination ?
		$attributes_groups = $_product->getAttributesGroups($this->context->language->id);
		if (is_array($attributes_groups) && $attributes_groups)
		{
			$combination_images = $_product->getCombinationImages($this->context->language->id);
			$combination_prices_set = array();
			foreach ($attributes_groups as $k => $row)
			{
				// Color management
				if ((isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_.$row['id_attribute'].'.jpg')))
				{
					$colors[$row['id_attribute']]['value'] = $row['attribute_color'];
					$colors[$row['id_attribute']]['name'] = $row['attribute_name'];
					if (!isset($colors[$row['id_attribute']]['attributes_quantity']))
						$colors[$row['id_attribute']]['attributes_quantity'] = 0;
					$colors[$row['id_attribute']]['attributes_quantity'] += (int)$row['quantity'];
				}
				if (!isset($groups[$row['id_attribute_group']]))
					$groups[$row['id_attribute_group']] = array(
						'name' => $row['public_group_name'],
						'group_type' => $row['group_type'],
						'default' => -1,
					);

				$groups[$row['id_attribute_group']]['attributes'][$row['id_attribute']] = $row['attribute_name'];
				if ($row['default_on'] && $groups[$row['id_attribute_group']]['default'] == -1)
					$groups[$row['id_attribute_group']]['default'] = (int)$row['id_attribute'];
				if (!isset($groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']]))
					$groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] = 0;
				$groups[$row['id_attribute_group']]['attributes_quantity'][$row['id_attribute']] += (int)$row['quantity'];


				$combinations[$row['id_product_attribute']]['attributes_values'][$row['id_attribute_group']] = $row['attribute_name'];
				$combinations[$row['id_product_attribute']]['attributes'][] = (int)$row['id_attribute'];
				$combinations[$row['id_product_attribute']]['price'] = (float)$row['price'];

				// Call getPriceStatic in order to set $combination_specific_price
				if (!isset($combination_prices_set[(int)$row['id_product_attribute']]))
				{
					Product::getPriceStatic((int)$_product->id, false, $row['id_product_attribute'], 6, null, false, true, 1, false, null, null, null, $combination_specific_price);
					$combination_prices_set[(int)$row['id_product_attribute']] = true;
					$combinations[$row['id_product_attribute']]['specific_price'] = $combination_specific_price;
				}
				$combinations[$row['id_product_attribute']]['ecotax'] = (float)$row['ecotax'];
				$combinations[$row['id_product_attribute']]['weight'] = (float)$row['weight'];
				$combinations[$row['id_product_attribute']]['quantity'] = (int)$row['quantity'];
				$combinations[$row['id_product_attribute']]['reference'] = $row['reference'];
				$combinations[$row['id_product_attribute']]['unit_impact'] = $row['unit_price_impact'];
				$combinations[$row['id_product_attribute']]['minimal_quantity'] = $row['minimal_quantity'];
				if ($row['available_date'] != '0000-00-00')
					$combinations[$row['id_product_attribute']]['available_date'] = $row['available_date'];
				else
					$combinations[$row['id_product_attribute']]['available_date'] = '';

				if (isset($combination_images[$row['id_product_attribute']][0]['id_image']))
					$combinations[$row['id_product_attribute']]['id_image'] = $combination_images[$row['id_product_attribute']][0]['id_image'];
				else
					$combinations[$row['id_product_attribute']]['id_image'] = -1;
			}
			// wash attributes list (if some attributes are unavailables and if allowed to wash it)
			if (!Product::isAvailableWhenOutOfStock($_product->out_of_stock) && Configuration::get('PS_DISP_UNAVAILABLE_ATTR') == 0)
			{
				foreach ($groups as &$group)
					foreach ($group['attributes_quantity'] as $key => &$quantity)
						if (!$quantity)
							unset($group['attributes'][$key]);

				foreach ($colors as $key => $color)
					if (!$color['attributes_quantity'])
						unset($colors[$key]);
			}
			foreach ($combinations as $id_product_attribute => $comb)
			{
				$attribute_list = '';
				foreach ($comb['attributes'] as $id_attribute)
					$attribute_list .= '\''.(int)$id_attribute.'\',';
				$attribute_list = rtrim($attribute_list, ',');
				$combinations[$id_product_attribute]['list'] = $attribute_list;
			}

			return array(
				'groups' => $groups,
				'combinations' => $combinations,
				'colors' => (count($colors)) ? $colors : false,
				'combinationImages' => $combination_images
			);
		}
		
		return array();
	}
}
