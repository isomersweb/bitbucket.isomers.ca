<?php

/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
* @category   Belvg
* @package    Belvg_Discounts
* @author     Dzianis Yurevich (dzianis.yurevich@gmail.com)
* @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
* @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ .
    'belvg_discounts/classes/BelvgDiscountProduct.php';
require_once _PS_MODULE_DIR_ .
    'belvg_discounts/classes/BelvgDiscountCustomer.php';

/**
 * belvg_discounts
 *
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class belvg_discounts extends Module
{
    protected $_hooks = array(
        'displayAdminProductsExtra',
        'actionProductUpdate',
        'displayAdminCustomers',
        'displayHeader',
        'displayFooterProduct',
        'actionOrderStatusUpdate',
        'displayMyAccountBlock',
        'displayCustomerAccount',
        'actionProductDelete',
        'actionValidateOrder'
        );

    protected $_templates = array(
        'productAdminTab' => 'tpl/admin/productAdminTab.tpl',
        'customerAdminTab' => 'tpl/admin/customerAdminTab.tpl',
        'moduleConfig' => 'tpl/admin/moduleConfig.tpl',
        'frontJs' => 'tpl/front/frontJs.tpl',
        'productFooter' => 'tpl/front/productFooter.tpl',
        'adminOrder' => 'tpl/admin/adminOrder.tpl',
        'myAccountBlock' => 'tpl/front/myAccountBlock.tpl',
        );

    protected $_configs = array('desc_link' => 'Link to description point system',
            'order_state' => 'Order state by defalut');

    /**
     * belvg_discounts::__construct()
     *
     * @return void
     */
    public function __construct()
    {
        $this->name = 'belvg_discounts';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.0';
        $this->author = 'BelVG';
        $this->need_instance = 0;
        $this->module_key = '';

        parent::__construct();

        $this->displayName = $this->l('Discounts');
        $this->description = $this->l('Discounts');
    }

    /**
     * belvg_discounts::getDbPrefix()
     *
     * @return string
     */
    public function getDbPrefix()
    {
        return _DB_PREFIX_ . $this->name . '_';
    }

    /**
     * belvg_discounts::getConfigPrefix()
     *
     * @param mixed $key
     * @return string
     */
    public function getConfigPrefix($key)
    {
        return $this->name . '_' . $key;
    }

    /**
     * belvg_discounts::install()
     *
     * @return bool
     */
    public function install()
    {
        $sql = array();

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . $this->getDbPrefix() . 'product` (
              `id_product` int(10) unsigned NOT NULL,
              `id_addon` int(10) NOT NULL,
              `discount` decimal(20,6) NOT NULL,
              `points` decimal(20) NOT NULL,
              `id_shop` int(10) NOT NULL,
              PRIMARY KEY  (`id_product`, `id_addon`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . $this->getDbPrefix() .
            'product_points` (
              `id_product` int(10) unsigned NOT NULL,
              `points` decimal(20) NOT NULL,
              `buy_points` decimal(20) NOT NULL,
              `id_shop` int(10) NOT NULL,
              PRIMARY KEY (`id_product`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . $this->getDbPrefix() .
            'customer_points` (
              `id_customer` int(10) unsigned NOT NULL,
              `points` decimal(20) NOT NULL,
              PRIMARY KEY (`id_customer`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . $this->getDbPrefix() . 'cart` (
              `id_cart` int(10) unsigned NOT NULL,
              `id_product` int(10) unsigned NOT NULL,
              `id_product_attribute` int(10) unsigned NOT NULL,
              `addons` text NOT NULL,
              `qty_by_points` int(10) NOT NULL,
              PRIMARY KEY  (`id_cart`, `id_product`, `id_product_attribute`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . $this->getDbPrefix() . 'status_updater` (
              `id_cart` int(10) unsigned NOT NULL,
              `is_status_updated` int(10) NOT NULL,
              PRIMARY KEY  (`id_cart`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        $states = OrderState::getOrderStates(Context::getContext()->language->id);
        $this->setConfig('order_state', $states[0]['id_order_state']);

        $flagInstall = parent::install();

        foreach ($this->_hooks as $hook) {
            $this->registerHook($hook);
        }

        return $flagInstall;
    }

    /**
     * belvg_discounts::uninstall()
     *
     * @return bool
     */
    public function uninstall()
    {
        $sql = array();

        $sql[] = 'DROP TABLE IF EXISTS `' . $this->getDbPrefix() . 'product`';
        $sql[] = 'DROP TABLE IF EXISTS `' . $this->getDbPrefix() . 'product_points`';
        $sql[] = 'DROP TABLE IF EXISTS `' . $this->getDbPrefix() . 'customer_points`';
        $sql[] = 'DROP TABLE IF EXISTS `' . $this->getDbPrefix() . 'cart`';
        $sql[] = 'DROP TABLE IF EXISTS `' . $this->getDbPrefix() . 'status_updater`';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        foreach ($this->_configs as $key) {
            Configuration::deleteByName($this->getConfigPrefix($key));
        }

        foreach ($this->_hooks as $hook) {
            $this->unregisterHook($hook);
        }

        return parent::uninstall();
    }

    /**
     * belvg_discounts::getTemplate()
     *
     * @param mixed $id
     * @return bool
     */
    public function getTemplate($id)
    {
        if (isset($this->_templates[$id])) {
            return $this->_templates[$id];
        }

        return FALSE;
    }

    /**
     * belvg_discounts::setConfig()
     *
     * @param mixed $key
     * @param mixed $value
     * @return bool
     */
    public function setConfig($key, $value)
    {
        if (array_key_exists($key, $this->_configs)) {
            return Configuration::updateValue($this->getConfigPrefix($key), $value);
        }

        return FALSE;
    }

    /**
     * belvg_discounts::getConfig()
     *
     * @param mixed $key
     * @return bool | string
     */
    public function getConfig($key)
    {
        if (array_key_exists($key, $this->_configs)) {
            return Configuration::get($this->getConfigPrefix($key));
        }

        return FALSE;
    }

    /**
     * belvg_discounts::hookDisplayAdminProductsExtra()
     *
     * @param mixed $params
     * @return string
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $id_product = (int)Tools::getValue('id_product');
        if ($id_product) {
            $stores = Shop::getContextListShopID();
            if (count($stores) > 1) {
                return $this->l('Please select shop!');
            }

            $_storeId = (isset($stores[0]) ? $stores[0] : 1);
            $belvg_product = new BelvgDiscountProduct($id_product, $_storeId);
            $product_data = $belvg_product->getProductData(TRUE);
            $this->smarty->assign(
                array(
                    $this->name => $product_data,
                    'storeId' => $_storeId,
                )
            );
            return $this->display(__file__, $this->getTemplate('productAdminTab'));
        } else {
            return $this->l('Please save this product!');
        }
    }

    /**
     * belvg_discounts::hookActionProductUpdate()
     *
     * @param mixed $params
     * @return void
     */
    public function hookActionProductUpdate($params)
    {
        if (Tools::isSubmit($this->name)) {
            $id_product = (int)Tools::getValue('id_product');
            $id_shop = (int)Tools::getValue('belvg_id_shop');
            if ($id_product) {
                $belvg_product = new BelvgDiscountProduct($id_product, $id_shop);
                $belvg_product->save($params);
            }
        }
    }

    /**
     * belvg_discounts::hookDisplayAdminCustomers()
     *
     * @param mixed $params
     * @return string
     */
    public function hookDisplayAdminCustomers($params)
    {
        $id_customer = (int)$params['id_customer'];
        $belvg_customers = new BelvgDiscountCustomer($id_customer);
        $this->smarty->assign(array($this->name => $belvg_customers->getPoints(),
                'belvg_customer' => $id_customer));

        return $this->display(__file__, $this->getTemplate('customerAdminTab'));
    }

    /**
     * belvg_discounts::hookDisplayHeader()
     *
     * @return string
     */
    public function hookDisplayHeader()
    {
        $this->context->controller->addJS($this->_path . 'js/front.js');
        $this->context->controller->addJS($this->_path . 'js/jquery.price_format.1.7.js');
        $this->context->controller->addCss($this->_path . 'css/front.css', 'all');
        return $this->display(__file__, $this->getTemplate('frontJs'));
    }

    /**
     * belvg_discounts::hookDisplayFooterProduct()
     *
     * @param mixed $params
     * @return string
     */
    public function hookDisplayFooterProduct($params)
    {
        $id_product = (int)Tools::getValue('id_product');
        $shop_id = 1;
        if (!is_null(Shop::getContextShopID())) {
            $shop_id = Shop::getContextShopID();
        }

        $belvgProduct = new BelvgDiscountProduct($id_product, $shop_id);
        $addons = $belvgProduct->getProductAddons(); //print_r($addons); die;
        $_addons = array();
        foreach ($addons as $addon) {
            if ($addon['product']->checkQty(1) and $addon['product']->active) {
                $_addons[] = $addon;
            } elseif ($addon['product']->id == $id_product) {
                return NULL;
            }
        }

        if (count($addons) > 1) {
            $this->context->smarty->assign(array(
                'belvg_addons' => $_addons,
                'belvg_context' => $this->context,
                'belvg_img_dir' => $this->_path . 'img/',
                'belvg_id_product' => $id_product,
                ));

            return $this->display(__file__, $this->getTemplate('productFooter'));
        }
    }



    public function hookActionValidateOrder($params)
    {
        $_cart = new Cart($params['cart']->id);
        if ($params['customer']->id) {
            $belvgCustomer = new BelvgDiscountCustomer($params['customer']->id);
            $points = (int)$belvgCustomer->getPoints() - (int)$_cart->getTotalPoints();
            $belvgCustomer->setData($points);
        }
    }

    /**
     * belvg_discounts::hookActionOrderStatusUpdate()
     *
     * @param mixed $params
     * @return void
     */
    public function hookActionOrderStatusUpdate($params)
    {
        if ($params['newOrderStatus']->id == $this->getConfig('order_state')) {
            $_cart = new Cart($params['cart']->id);
            if ($params['cart']->id_customer) {
                $is_updated = (int)Db::getInstance()
                    ->getValue('SELECT `is_status_updated` FROM `' . $this->getDbPrefix() .
                    'status_updater` WHERE `id_cart` = ' . (int)$params['cart']->id);

                if (!$is_updated) {
                    $belvgCustomer = new BelvgDiscountCustomer($params['cart']->id_customer);
                    $points = (int)$belvgCustomer->getPoints() + (int)$_cart->getYouGetPoints(TRUE);
                    $belvgCustomer->setData($points);

                    Db::getInstance()
                        ->Execute('INSERT INTO `' . $this->getDbPrefix() . 'status_updater` VALUES (' . (int)$params['cart']->id . ', 1)');
                }
            }
        }
    }

    /**
     * belvg_discounts::hookDisplayMyAccountBlock()
     *
     * @param mixed $params
     * @return string
     *
    public function hookDisplayMyAccountBlock($params)
    {
        $this->context->smarty->assign('in_footer', TRUE);
        $this->context->smarty->assign('belvg_discounts_dir', $this->_path);
        return $this->context->smarty->fetch($this->getTemplatePath($this->getTemplate('myAccountBlock')));
    }*/

    /**
     * belvg_discounts::hookDisplayCustomerAccount()
     *
     * @param mixed $params
     * @return string
     */
    public function hookDisplayCustomerAccount($params)
    {
        $this->context->smarty->assign('in_footer', FALSE);
        $this->context->smarty->assign('belvg_discounts_dir', $this->_path);
        return $this->context->smarty->fetch($this->getTemplatePath($this->getTemplate('myAccountBlock')));
    }

    public function hookActionProductDelete($params)
    {
        $id = (int)$params['product']->id;
        if ($id) {
            $sql = array();

            $sql[] = 'DELETE FROM `' . $this->getDbPrefix() . 'product`
                        WHERE `id_product` = ' . $id . ' OR `id_addon` = ' . $id . ';';

            $sql[] = 'DELETE FROM `' . $this->getDbPrefix() . 'product_points`
                        WHERE `id_product` = ' . $id . ';';

            $sql[] = 'DELETE FROM `' . $this->getDbPrefix() . 'cart`
                        WHERE `id_product` = ' . $id . ';';

            foreach ($sql as $_sql) {
                Db::getInstance()->Execute($_sql);
            }
        }
    }

    /**
     * belvg_discounts::_postProcess()
     *
     * @return void
     */
    protected function _postProcess()
    {
        $this->_configureUri = 'index.php?tab=AdminModules&configure=' . $this->name .
            '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName
            ('AdminModules')) . (int)$this->context->employee->id);

        $this->_redirectUri = $this->_configureUri . '&conf=4';

        if (Tools::isSubmit('submitUpdateDiscount')) {
            foreach ($_POST as $config => $data) {
                if ($config == 'desc_link') {
                    $data = htmlentities($data);
                    if (!file_get_contents($data)) {
                        $data = Context::getContext()->link->getPageLink('404.php');
                    }
                }

                $this->setConfig($config, $data);
            }

            header('Location: ' . $this->_redirectUri);
            exit;
        }
    }

    /**
     * belvg_discounts::getContent()
     *
     * @return string
     */
    public function getContent()
    {
        $this->_postProcess();

        $this->smarty->assign(array(
            'submitUri' => $this->_configureUri,
            'belvg_discounts' => new self,
            ));

        return $this->display(__file__, $this->getTemplate('moduleConfig'));
    }
}
