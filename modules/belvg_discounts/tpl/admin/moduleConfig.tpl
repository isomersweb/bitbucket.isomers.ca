<div class="toolbarBox toolbarHead" style="clear: both;">
	<ul class="cc_button">
		<li>
			<a id="desc-belvg-save" class="toolbar_btn" href="javascript:$('.defaultForm.belvg').submit()">
				<span class="process-icon-save"></span>
				<div>{l s='Save Configuration' mod='belvg_discounts'}</div>
			</a>
		</li>		
	</ul>
	<div class="pageTitle">
		<h3>
			<span id="current_obj" style="font-weight: normal;">
				<span class="breadcrumb item-0 ">
					{l s='Modules' mod='belvg_discounts'}
					<img style="margin-right:5px" src="../img/admin/separator_breadcrumb.png">
				</span>
				<span class="breadcrumb item-1">
					{l s='BelVG Discount System' mod='belvg_discounts'}
				</span>
			</span>
		</h3>
	</div>
</div>
<form class="defaultForm belvg" action="{$submitUri}" method="POST">
	<input type="hidden" name="submitUpdateDiscount" value="1" />
	<input type="hidden" name="action" value="save" />
	<fieldset class="filter-module"  style="clear: both;">
		<legend>
			{l s='Discount System' mod='belvg_discounts'}
		</legend>
		<div class="bl-child" style="display: block" id="belvg_settings">
			<label>{l s='Link to description point system' mod='belvg_discounts'}</label>
			<div class="margin-form">
				<input type="text" name="desc_link" value="{$belvg_discounts->getConfig('desc_link')}" />
			</div>
			<div class="clear"></div>
		</div>
		<div class="bl-child" style="display: block" id="belvg_settings">
			<label>{l s='After what the status of an order to use points?' mod='belvg_discounts'}</label>
			<div class="margin-form">
				<select name="order_state">
					{foreach from=OrderState::getOrderStates(Context::getContext()->language->id) item=state}
						<option {if $belvg_discounts->getConfig('order_state') == $state.id_order_state}selected{/if} value="{$state.id_order_state}">{$state.name}</option>
					{/foreach}
				</select>
			</div>
			<div class="clear"></div>
		</div>
	</fieldset>
</form>