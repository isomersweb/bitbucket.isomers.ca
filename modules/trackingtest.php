<?php

require_once('../config/config.inc.php');
 

$shipping_id = ''; 
$status_shipped = Configuration::get('PS_OS_SHIPPING');
$status_delivered = Configuration::get('PS_OS_DELIVERED');
$order_ids = Order::getOrderIdsByStatus($status_shipped);

if (!defined('_PS_VERSION_'))
	exit;

if (!extension_loaded('soap'))
	return false;
    
$cp_dirpath = _PS_MODULE_DIR_ . '/canadapostrates/';

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $cp_dirpath . 'wsdl/track.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/vis/soap/track';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => $cp_dirpath . 'cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$cp_client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$cp_client->__setSoapHeaders($header); 

$fd_dirpath = _PS_ROOT_DIR_ . '/modules/fedexcarrier/';

require_once($fd_dirpath . 'fedex-common.php5');
$fd_wsdl = $fd_dirpath . "TrackService_v14.wsdl";
ini_set("soap.wsdl_cache_enabled", "0");
$fd_client = new SoapClient($fd_wsdl, array('trace' => 1)); 

$fd_request['WebAuthenticationDetail'] = array(
	/*'ParentCredential' => array(
		'Key' => getProperty('parentkey'), 
		'Password' => getProperty('parentpassword')
	),*/
	'UserCredential' => array(
		'Key' => Configuration::get('FEDEX_CARRIER_API_KEY'), 
        'Password' => Configuration::get('FEDEX_CARRIER_PASSWORD')
	)
);

$fd_request['ClientDetail'] = array(
	'AccountNumber' => Configuration::get('FEDEX_CARRIER_ACCOUNT'), 
    'MeterNumber' => Configuration::get('FEDEX_CARRIER_METER')
);
$fd_request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');
$fd_request['Version'] = array(
	'ServiceId' => 'trck', 
	'Major' => '14', 
	'Intermediate' => '0', 
	'Minor' => '0'
);
$fd_request['SelectionDetails'] = array(
	'PackageIdentifier' => array(
		'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
	)
);

foreach($order_ids as $order_id) {
    $order = new Order($order_id);
    if ($shipping_id = $order->getWsShippingNumber()) {
        $carrier = new Carrier($order->id_carrier);
//        p($order_id ."#". $shipping_id);
        if ($carrier->external_module_name == 'canadapostrates') {
            $tracking_status = getCPStatus($cp_client, $shipping_id);
        }
        else if ($carrier->external_module_name == 'fedexcarrier') {
            //$shipping_id = '9261299998825462485198';
            $fd_request['SelectionDetails']['PackageIdentifier']['Value'] = $shipping_id;
            $tracking_status = getFedexStatus($fd_client, $fd_request);
        }

            //p($order_id ."#". $shipping_id);
            $order->setCurrentState($status_delivered);             
        
    }
}
d('OK');

function getFedexStatus($client, $request) {
    //d($request);
    try {
    	if(setEndpoint('changeEndpoint')){
    		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
    	}
    	
    	$response = $client ->track($request);
    
        if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
    		/*if($response->HighestSeverity != 'SUCCESS'){
    			echo '<table border="1">';
    			echo '<tr><th>Track Reply</th><th>&nbsp;</th></tr>';
    			trackDetails($response->Notifications, '');
    			echo '</table>';
    		}else{
    	    	if ($response->CompletedTrackDetails->HighestSeverity != 'SUCCESS'){
    				echo '<table border="1">';
    			    echo '<tr><th>Shipment Level Tracking Details</th><th>&nbsp;</th></tr>';
    			    trackDetails($response->CompletedTrackDetails, '');
    				echo '</table>';
    			}else{
    				echo '<table border="1">';
    			    echo '<tr><th>Package Level Tracking Details</th><th>&nbsp;</th></tr>';
    			    trackDetails($response->CompletedTrackDetails->TrackDetails, '');
    				echo '</table>';
    			}
    		}*/
            //printSuccess($client, $response);
            //p($response->CompletedTrackDetails->TrackDetails->StatusDetail->Code);
            if ($response->CompletedTrackDetails->TrackDetails->StatusDetail->Code == 'DL') {                
                return true;                
            }
            return false;
        }else{
            //printError($client, $response);
            return false;
        } 
    
    //writeToLog($client);    // Write to log file   
    } catch (SoapFault $exception) {
        //printFault($exception, $client);
        return false;
    }
    return false;
}

function getCPStatus($client, $shipping_id) {
    try {
    	// Execute Request
    	$result = $client->__soapCall('GetTrackingSummary', array(
    	    'get-tracking-summary-request' => array(
    			'locale'	=> 'EN',
    			// PIN, DNC or reference choice
    			'pin'		=> $shipping_id
    			// 'dnc'		=> '315052413796541'	
    			/* 'reference-criteria' => array(
    			 *	  'reference-number' 			=> 'DIA101',
    			 *	  'destination-postal-code' 	=> 'K2H7X3',
    			 *	  'mailing-date-from' 			=> '2012-02-01',
    			 *	  'mailing-date-to' 			=> '2013-06-25'
    			 * )
    			 */
    		)
    	), NULL, NULL);
    	
    	// Parse Response
    	if ( isset($result->{'tracking-summary'}) ) {
    		foreach ( $result->{'tracking-summary'}->{'pin-summary'} as $pinSummary ) {
/*
    			echo 'PIN Number: ' . $pinSummary->{'pin'} . "\n";
    			echo 'Mailed On Date: ' . $pinSummary->{'mailed-on-date'} . "\n";
    			echo 'Event Description: ' . $pinSummary->{'event-description'} . "\n\n";
*/
                if ($pinSummary->{'event-type'} == 'DELIVERED') {
                    return true;
                }
                return false;
    		}
    	} else {
    		foreach ( $result->{'messages'}->{'message'} as $message ) {
//    			echo 'Error Code: ' . $message->code . "\n";
//    			echo 'Error Msg: ' . $message->description . "\n\n";
    		  return false;
    		}
    	}
    } catch (SoapFault $exception) {
//    	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
//    	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
        return false;
    }    
    return false;
}
