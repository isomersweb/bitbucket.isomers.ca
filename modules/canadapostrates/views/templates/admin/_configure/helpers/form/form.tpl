{*
*  2014 Zack Hussain
*
*  @author      Zack Hussain <me@zackhussain.ca>
*  @copyright   2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}
{extends file="helpers/form/form.tpl"}
{block name="field"}
    {if $input.type == 'package'}
        <select class="{$pl|escape:'htmlall':'UTF-8'}package" name="{$p|escape:'htmlall':'UTF-8'}PACKAGE">
            {foreach $input.boxes AS $box}
            <option
                data-id="{$box.id|escape:'htmlall':'UTF-8'}"
                data-length="{$box.length|escape:'htmlall':'UTF-8'}"
                data-width="{$box.width|escape:'htmlall':'UTF-8'}"
                data-height="{$box.height|escape:'htmlall':'UTF-8'}"
                data-weight="{$box.weight|escape:'htmlall':'UTF-8'}"
                {if $fields_value[$p|cat:'PACKAGE'] == $box.id}selected{/if}>{$box.name|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
        </select>
    {elseif $input.type == 'cp_buttons'}
        {foreach $input.buttons as $btn}
            <button type="{if isset($btn.type)}{$btn.type|escape:'htmlall':'UTF-8'}{else}button{/if}" class="btn btn-default{if isset($btn.class)} {$btn.class|escape:'htmlall':'UTF-8'}{/if}" name="{if isset($btn.name)}{$btn.name|escape:'htmlall':'UTF-8'}{else}submitOptions{/if}">{$btn.title|escape:'htmlall':'UTF-8'}</button>
        {/foreach}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}
{block name="other_fieldsets"}
	{if !empty($smarty.get.message)}
		<p class="alert alert-danger">{$smarty.get.message|htmlentities}</p>
	{/if}
    {if $input.type == 'dest'}
        <div data-path="{$path|escape:'htmlall':'UTF-8'}" class="{$pl|cat:'shipment_settings'} col-lg-12">

                <div class="{if $input.shipment.country !== 'CA'}col-lg-4{else}{/if}">
                    <h4>Customer Address</h4>
                    <div class="col-lg-6">
                    <label for="{$p|cat:'NAME'}">Name</label>
                    <input type="text" value="{$input.shipment.name}" name="{$p|cat:'NAME'}">
                    <br>

                    <label for="{$p|cat:'COMPANY'}">Company (optional)</label>
                    <input type="text" value=" {if isset($input.shipment.company)}{$input.shipment.company}{/if}" name="{$p|cat:'COMPANY'}">
                    <br>

                    <label for="{$p|cat:'ADDRESS1'}">Address Line 1</label>
                    <input type="text" value="{$input.shipment.address1}" name="{$p|cat:'ADDRESS1'}">
                    <br>

                    <label for="{$p|cat:'ADDRESS2'}">Address Line 2 (optional)</label>
                    <input type="text" value="{if isset($input.shipment.address2)}{$input.shipment.address2}{/if}" name="{$p|cat:'ADDRESS2'}">
                    <br>

                    <label for="{$p|cat:'PHONE'}">Phone Number</label>
                    <input type="text" value="{$input.shipment.phone}" name="{$p|cat:'PHONE'}">
                    </div>

                    <div class="col-lg-6">
                    <label for="{$p|cat:'MOBILE'}">Mobile Phone (optional)</label>
                    <input type="text" value="{if isset($input.shipment.mobile)}{$input.shipment.mobile}{/if}" name="{$p|cat:'MOBILE'}">
                    <br>

                    <label for="{$p|cat:'CITY'}">City</label>
                    <input type="text" value="{$input.shipment.city}" name="{$p|cat:'CITY'}">
                    <br>

                    <label for="{$p|cat:'PROVINCE'}">Prov/State Code (e.g. QC)</label>
                    <input type="text" value="{$input.shipment.province}" name="{$p|cat:'PROVINCE'}">
                    <br>

                    <label for="{$p|cat:'COUNTRY'}">Country Code (e.g. CA)</label>
                    <input type="text" value="{$input.shipment.country}" name="{$p|cat:'COUNTRY'}">
                    <br>

                    <label for="{$p|cat:'POSTAL'}">Postal/Zip Code</label>
                    <input type="text" value="{$input.shipment.postal}" name="{$p|cat:'POSTAL'}">
                    <br>
                    </div>

                    <input type="hidden" value="{$input.shipment.orderid}" name="{$p|cat:'ORDERID'}">
                    <input type="hidden" value="{$input.shipment.employee}" name="{$p|cat:'EMPLOYEE'}">

                </div>
                {if $input.shipment.country !== 'CA'}
                <div class="col-lg-6 {$pl|escape:'htmlall':'UTF-8'}international">
                    <h4>International Customs Options</h4>
                        <label for="{$p|cat:'CURRENCY'}">Currency ISO (e.g. USD)</label>
                        <input type="text" class="fixed-width-xs" value="{$input.shipment.currency}" name="{$p|cat:'CURRENCY'}">
                        <br>
                        <label for="{$p|cat:'REASON'}">Reason for Export</label>
                        <select class="fixed-width-md" name="{$p|cat:'REASON'}">
                            <option value="SOG" selected>Sale of Goods</option>
                            <option value="DOC">Document</option>
                            <option value="SAM">Commercial Sample</option>
                            <option value="REP">Repair or Warranty</option>
                            <option value="GIF">Gift</option>
                        </select>
                        <br>
                        <hr>
                    {assign var=i value=1}
                    {foreach $input.shipment.products as $product}
                    <div class="col-lg-3">
                        <label for="{$p|cat:'PRODUCT'}[{$i}][NAME]">Product Name {$i}</label>
                        <input type="text" value="{$product.product_name|regex_replace:'/[^a-zA-Z0-9\s]/':''|substr:0:44}" name="{$p|cat:'PRODUCT'}[{$i}][NAME]">
                        <label for="{$p|cat:'PRODUCT'}[{$i}][WEIGHT]">Product Weight {$i}</label>
                        <div class="input-group">
                        	<input type="text" value="{$product.product_weight|string_format:'%.3f'}" name="{$p|cat:'PRODUCT'}[{$i}][WEIGHT]">
	                        <span class="input-group-addon">kg</span>
						</div>
                        <label for="{$p|cat:'PRODUCT'}[{$i}][CUSTOMS_DESC]">Product Customs Description {$i}</label>
                        <input type="text" value="{$product.product_name|regex_replace:'/[^a-zA-Z0-9\s]/':''|substr:0:44}" name="{$p|cat:'PRODUCT'}[{$i}][CUSTOMS_DESC]">
                        <label for="{$p|cat:'PRODUCT'}[{$i}][QUANTITY]">Product Quantity {$i}</label>
                        <input type="text" value="{$product.product_quantity}" name="{$p|cat:'PRODUCT'}[{$i}][QUANTITY]">
                        <label for="{$p|cat:'PRODUCT'}[{$i}][PRICE]">Product Price {$i}</label>
                        <input type="text" value="{$product.product_price|string_format:'%.2f'}" name="{$p|cat:'PRODUCT'}[{$i}][PRICE]">
                        <hr>
                    </div>
                        {$i = $i + 1}
                    {/foreach}
                </div>
                {/if}
            <br>
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}
{block name="after"}
    {if isset($shipment)}
        <div id="{$pl|escape:'htmlall':'UTF-8'}response">
            <img class="{$pl|escape:'htmlall':'UTF-8'}loader" src="{$path}img/AjaxLoader.gif">
            <div class="{$pl|escape:'htmlall':'UTF-8'}rates"></div>
            <div class="{$pl|escape:'htmlall':'UTF-8'}shipment"></div>
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}