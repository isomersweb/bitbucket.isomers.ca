{*
*  2014 Zack Hussain
*
*  @author      Zack Hussain <me@zackhussain.ca>
*  @copyright   2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}

<div class="alert alert-info">
    <button class="btn btn-default {$pl|escape:'htmlall':'UTF-8'}instructions_btn">Show Instructions</button>
    <div id="{$pl|escape:'htmlall':'UTF-8'}instructions">
        <h2>What you need to know right now</h2>
        <ul>
            <li><b>Connect: </b>{l s='You first need to connect your Canada Post account to this module by clicking "Sign in with Canada Post" if you haven\'t already.' mod='canadapostrates'}</li>
            <li><b>Rates: </b>{l s='To display real-time rates on the cart page, select each method you want below under Rates Preferences.' mod='canadapostrates'}</li>
        </ul>
        <hr>
        <h2>Helpful Information</h2>
        <h4>FAQ</h4>
        <ul>
            <li>
                <b>{l s='Q: Why are the rates not showing up?' mod='canadapostrates'} </b><br>
                {l s='Check the products\' Shipping settings to see if the carriers are enabled/disabled in Catalog > Products > (edit a product) > Shipping.' mod='canadapostrates'} <br>
                {l s='Make sure the carriers\' zones are all enabled and at $0.00, even the ones you don\'t use, in Shipping > Carriers.' mod='canadapostrates'} <br>
                {l s='Make sure you aren\'t exceeding the 30kg weight limit or dimension limit. If you think you are, enable Split Products in the module\'s configuration below.' mod='canadapostrates'} <br>
            </li>
        </ul>
        <h4>Rates</h4>
        <ul>
            <li><b>Boxes: </b>{l s='You MUST add at least one box under the box settings below for rates to show up on the cart page.' mod='canadapostrates'}</li>
            <li><b>Back-Office Carriers: </b>{l s='Selecting shipping methods below will install custom carriers in your Shipping > Carriers menu. It\'s not advisable to edit these (unless it\'s just the logo or Delay message). To deactivate a carrier, simply uncheck one or more carriers below under "Rates Preferences" instead of doing it from the Carriers page.' mod='canadapostrates'}</li>
            <li><b>Invalid Methods: </b>{l s='Some shipping methods will not show up if they are invalid for the given parcel. e.g. Worldwide Envelope Int’l will not show up if the current order needs a large box.' mod='canadapostrates'}</li>
            <li><b>Invalid Addresses: </b>{l s='If you or the customer has an invalid address, the rates will not show up.' mod='canadapostrates'}</li>
            <li><b>Product Attributes: </b>{l s='It\'s a good idea to set product shipping attributes (lenth, width, height and weight) for each product, as the module will go through all your box sizes and determine the smallest box that will fit all the products in the cart, and display the rates accordingly.' mod='canadapostrates'}</li>
        </ul>
        <h4>Boxes</h4>
        <ul>
            <li><b>Accurate Rates: </b>{l s='The more boxes you add, the more accurate the rates will be.' mod='canadapostrates'}</li>
            <li><b>Measurements: </b>{l s='If you change your store measurement preferences, e.g. IN to CM, you must edit your box sizes to reflect that change as they will still be in inches.' mod='canadapostrates'}</li>
            <li><b>Box Weight: </b>{l s='The weight of the box will be added to the total weight of the current cart behind the scenes for more accurate rates.' mod='canadapostrates'}</li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $('#cpr_instructions').hide();
    $('.cpr_instructions_btn').click(function(){
        $(this).text(function(i, text){
            return text === "Hide Instructions" ? "Show Instructions" : "Hide Instructions";
        })
        $('#cpr_instructions').slideToggle(200);
    });
</script>