<?php
 /**
 * Sample code for the GetAuthreturnServiceInfo Canada Post service.
 * 
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/serviceinfo.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/serviceinfo/v2';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	// Execute Request
	$result = $client->__soapCall('GetShipmentServiceInfo', array(
	    'get-shipment-service-info-request' => array(
			'locale'			=> 'EN',
			'message-type'	=> 'SO'
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'info-messages'}) ) {

		if(isset($result->{'info-messages'}->{'info-message'})){
			foreach ( $result->{'info-messages'}->{'info-message'} as $info_message ) {
				echo  "\n" . 'Message Text is :' . $info_message->{'message-text'} ."\n\n";
				echo  "\n" . 'Message Type is :' . $info_message->{'message-type'} ."\n\n";
				echo  "\n" . 'Message start time is :' . $info_message->{'from-datetime'} ."\n\n";
				echo  "\n" . 'Message end time is :' . $info_message->{'to-datetime'} ."\n\n";
			}
		}
		
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

