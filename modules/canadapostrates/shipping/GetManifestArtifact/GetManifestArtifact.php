<?php
 /**
 * Sample code for the GetManifestArtifact Canada Post service.
 * 
 * The GetManifestArtifact service is used to retrieve the rendered manifest document(s). 
 * May be called more than once to perform a reprint.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/GetManifestArtifact/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/artifact.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/artifact';

// SSL Options
$opts = array('ssl' =>
    array(
        'verify_peer'=> true,
        'cafile' => $dirpath . '/../../cert/cacert.pem',
        'peer_name' => $hostName
    ),
    'http' => array(
        'protocol_version' => 1.0,
    ),
);

//ini_set('default_socket_timeout', 600);
$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array(
        'location' => $location, 
        'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 
        'stream_context' => $ctx,
));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

//$cp_label_id = Db::getInstance()->ExecuteS('SELECT cp_label_id FROM '._DB_PREFIX_.'order_carrier WHERE id_order_carrier = '. pSQL(Tools::getValue('id_order_carrier')));
	   

//d($client->__getLastRequest());

try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
	$result = $client->__soapCall('GetArtifact', array(
	    'get-artifact-request' => array(
			'locale'			=> 'EN',
            'mailed-by'         => $mailedBy,
			'artifact-id'		=> $_GET['cp_label_id'],
		)
	), NULL, NULL);

	// Parse Response
	if ( isset($result->{'artifact-data'}) ) {
        unlink(_PS_ROOT_DIR_. '/orders/labels/' . 'CP-'.$order->id.'ma.pdf');
		$fileLoc = _PS_ROOT_DIR_. '/orders/labels/' . 'CP-'.$order->id.'ma.pdf';		
        //echo 'Decoding to' . $fileLoc . "\n";
		$fp = fopen($fileLoc, 'w');
		stream_filter_append($fp, 'convert.base64-decode');
		fwrite($fp, $result->{'artifact-data'}->{'image'});
		fclose($fp);
        return true;
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
            $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n<br>"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
//    d($exception);
}

?>