<?php
 /**
 * Sample code for the GetCustomerInformation Canada Post service.
 * 
 * The GetCustomerInformation service is used to retrieve details on allowed
 * payers, methods of payment and other information about a customer to determine
 * the proper customer number for charging shipping costs.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/customerinfo.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/customerinfo';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
	$result = $client->__soapCall('GetCustomerInformation', array(
	    'get-customer-information-request' => array(
			'locale'			=> 'FR',
			'customer-number'	=> $mailedBy
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'customer'}) ) {
		echo "\n" . 'Customer Number: ' . $result->{'customer'}->{'customer-number'} . "\n\n";
		echo 'Contract Ids:' . "\n";
		foreach ( $result->{'customer'}->{'contracts'}->{'contract-id'} as $contractId ) {
			echo '- ' . $contractId . "\n";							
		}
		echo "\n" . 'Payers:' . "\n";
		foreach ( $result->{'customer'}->{'authorized-payers'}->{'payer'} as $payer ) {
			echo '- Customer Number: ' . $payer->{'payer-number'} . "\n";
			$i = 0;
			foreach ( $payer->{'methods-of-payment'}->{'method-of-payment'} as $methodOfPayment ) {
				if ( $i == 0 ) {
					echo '  Payment Methods:' . "\n";						
				}
				echo '  - ' . $methodOfPayment . "\n";
				$i++;
			}
		}	
		echo "\n";	
		if ( isset($result->{'customer'}->{'mobos'}) ) {
			foreach ( $result->{'customer'}->{'mobos'}->{'mobo'} as $mobo ) {  
				echo 'Mobo: ' .$mobo . "\n";
			}
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

