<?php
 /**
 * Sample code for the GetManifests Canada Post service.
 * 
 * The GetManifests service is used to retrieve a list of manifests (within 
 * a given date range) that were previously transmitted.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/
define('PRESTASHOP_INTEGRATION_VERSION', true);
define ('PS_DIR', __DIR__ . '/../../../../');
//define('_PS_MODE_DEV_', true);
require_once PS_DIR .'/config/config.inc.php';

if (!defined('_PS_VERSION_'))
	exit;

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/GetManifests/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/manifest.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/manifest/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => $dirpath . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
p($mailedBy);
	// Execute Request	
	$result = $client->__soapCall('GetManifests', array(
	    'get-manifests-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'start'				=> '2015-01-01',
			'end'				=> '2019-01-01',
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'manifests'}) ) {
		if ( isset($result->{'manifests'}->{'manifest-id'}) ) {
            p($result->{'manifests'});		  
			/*
            foreach ( $result->{'manifests'}->{'manifest-id'} as $manifestId ) {  
				echo 'Manifest Id: ' . $manifestId . "\n";
			}
            */
		} else {
			echo 'No manifests returned.' . "\n";
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

