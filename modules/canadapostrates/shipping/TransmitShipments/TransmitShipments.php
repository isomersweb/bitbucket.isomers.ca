<?php
 /**
 * Sample code for the TransmitShipments Canada Post service.
 * 
 * The TransmitShipments service is used to specify shipments to be included in a manifest. 
 * Inclusion in a manifest is specified by group. Specific shipments may be excluded if 
 * desired.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/TransmitShipments/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/manifest.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/manifest/v8';

// SSL Options
$opts = array('ssl' =>
    array(
        'verify_peer'=> true,
        'cafile' => $dirpath . '/../../cert/cacert.pem',
        'peer_name' => $hostName
    ),
    'http' => array(
        'protocol_version' => 1.0,
    ),
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	$groupId = $order->id;
	$requestedShippingPoint = Configuration::get('CPR_POSTAL_CODE');

	// Execute Request	
	$result = $client->__soapCall('TransmitShipments', array(
	    'transmit-shipments-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'transmit-set' 	=> array(
				'group-ids' 	=> array(
					'group-id'		=> $groupId
				),
				'requested-shipping-point'	=> $requestedShippingPoint,
				'cpc-pickup-indicator'	=> 'true',
				'detailed-manifests'		=> true,	
				'method-of-payment'			=> Tools::getValue('cp_payment'),	
				'manifest-address'	=> array(
					'manifest-company'	=> Configuration::get('CPR_COMPANY'),
					'phone-number'		=> Configuration::get('CPR_PHONE'),
					'address-details'	=> array(
						'address-line-1'	=> Configuration::get('CPR_ADDRESS1'),	
						'city'				=> Configuration::get('CPR_CITY'),	
						'prov-state'		=> Configuration::get('CPR_PROVINCE'),	
						'country-code'		=> 'CA',	
						'postal-zip-code'	=> Configuration::get('CPR_POSTAL_CODE')		
					)
				)
			)
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'manifests'}) ) {
		/*
        foreach ( $result->{'manifests'}->{'manifest-id'} as $manifestId ) {  
			echo 'Manifest Id: ' . $manifestId . "\n";
		}
        */
        return json_decode(json_encode($result->{'manifests'}), 1);
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
            $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
//            d();
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
//    d();
}

?>

