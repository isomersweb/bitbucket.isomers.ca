<?php
 /**
 * Sample code for the GetShipmentArtifact Canada Post service.
 * 
 * The GetShipmentArtifact service is used to retrieve the rendered label(s) for a 
 * shipment created by a prior "create shipment" call. It can be called more than 
 * once to reprint a spoiled label.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/GetShipmentArtifact/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/artifact.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/artifact';

// SSL Options
$opts = array('ssl' =>
	array(
    		'verify_peer'=> true,
    		'cafile' => $dirpath . '/../../cert/cacert.pem',
    		'peer_name' => $hostName
        ),
        'http' => array(
            'protocol_version' => 1.0,
        ),
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

$cp_label_id = Db::getInstance()->ExecuteS('SELECT cp_label_id FROM '._DB_PREFIX_.'order_carrier WHERE id_order_carrier = '. pSQL(Tools::getValue('id_order_carrier')));

try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
    $result = $client->__soapCall('GetArtifact', array(
	    'get-artifact-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'artifact-id'		=> $cp_label_id[0]['cp_label_id'], 
			'page-index'		=> '0'
		)
	), NULL, NULL);
	
    // Parse Response
	if ( isset($result->{'artifact-data'}) ) {
		//echo 'base64 Encoded: ' . $result->{'artifact-data'}->{'image'} . "\n";
		//echo 'Mime type: ' . $result->{'artifact-data'}->{'mime-type'} . "\n";
		// Decoding base64 certificate to a file
        unlink(_PS_ROOT_DIR_. '/orders/labels/' . 'CP-'.$order->id.'.zpl');
        unlink(_PS_ROOT_DIR_. '/orders/labels/' . 'CP-'.$order->id.'.pdf');

		if ( strpos($result->{'artifact-data'}->{'mime-type'}, 'application/pdf' ) !== FALSE ) {
			$fileLoc = _PS_ROOT_DIR_. '/orders/labels/' . DIRECTORY_SEPARATOR . 'CP-'.$order->id.'.pdf';
		} else {
			$fileLoc = _PS_ROOT_DIR_. '/orders/labels/' . DIRECTORY_SEPARATOR . 'CP-'.$order->id.'.zpl';
		}
		
//        echo 'Decoding to' . $fileLoc . "\n";
		$fp = fopen($fileLoc, 'w');
		stream_filter_append($fp, 'convert.base64-decode');
		fwrite($fp, $result->{'artifact-data'}->{'image'});
		fclose($fp);
        
        /*
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($fileLoc).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileLoc));
        readfile($fileLoc);
        */
        return true;
                
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
            $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
}




?>