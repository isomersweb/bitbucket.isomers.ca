<?php
 /**
 * Sample code for the CreateShipment Canada Post service.
 * 
 * The CreateShipment service is used to initiate generation of a shipping label
 * by providing shipment details. Use of this service indicates an intention to 
 * pay for shipment of an item.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

/*
 Need to override SoapClient because the abstract element 'groupIdOrTransmitShipment' is expected to be in the request in order for validation to pass.

So, we give it what it expects, but in __doRequest we modify the request by removing the abstract element and add the correct element.

*/
class MySoapClient extends SoapClient {

	function __construct($wsdl, $options = null) {
		parent::__construct($wsdl, $options);
	}

	function __doRequest($request, $location, $action, $version, $one_way = NULL) {
		$dom = new DOMDocument('1.0');
		$dom->loadXML($request);

		//get element name and values of group-id or transmit-shipment.
		$groupIdOrTransmitShipment =  $dom->getElementsByTagName("groupIdOrTransmitShipment")->item(0);
		$element = $groupIdOrTransmitShipment->firstChild->firstChild->nodeValue;
		$value = $groupIdOrTransmitShipment->firstChild->firstChild->nextSibling->firstChild->nodeValue;

		//remove bad element
		$newDom = $groupIdOrTransmitShipment->parentNode->removeChild($groupIdOrTransmitShipment);

		//append correct element with namespace
		$body =  $dom->getElementsByTagName("shipment")->item(0);
		$newElement = $dom->createElement($element, $value);
		$body->appendChild($newElement);

		//save $dom to string
		$request = $dom->saveXML();

		//echo $request;

		//doRequest
		return parent::__doRequest($request, $location, $action, $version);
	}
}

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/CreateShipment/';

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/shipment.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/shipment/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => $dirpath . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new MySoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 
$addressDelivery = new Address($order->id_address_delivery, $this->context->language->id);
$iso_state = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '. $addressDelivery->id_state);
$iso_country = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '. $addressDelivery->id_country);
$receiver_country = $iso_country;

$mailedBy = $userProperties['customerNumber'];
$groupId = $order->id;
$requestedShippingPoint = Configuration::get('CPR_POSTAL_CODE');
$mailingDate = date('Y-m-d', strtotime(Tools::getValue('cp_date')));
$contractId = $userProperties['contractId'];

$service_code = Db::getInstance()->getValue('SELECT code FROM '._DB_PREFIX_.'cpr_methods WHERE id_carrier = '. pSQL(Tools::getValue('cp_method')));
//d($service_code); 

$encoding = 'PDF';
if (Tools::getValue('cp_label') == '4x6') {
    $encoding = 'ZPL';
}  
$weight = Tools::getValue('cp_box_weight') + Tools::getValue('cp_weight');

$request = array(
    'create-shipment-request' => array(
		'locale'			=> 'EN',
//        'platform-id'		=> '0008186599', //Configuration::get('CPR_PLATFORM_ID'),			
		'mailed-by'			=> $mailedBy,
		'shipment' 				=> array(
			//The validation expects this structure. However, this element will be removed and replaced only with ns1:group-id or ns1:transmit-shipment. 
			'groupIdOrTransmitShipment' => array(
				'ns1:group-id'	=> $groupId
				//'ns1:transmit-shipment' => 'true'
			),
			'requested-shipping-point'	=> $requestedShippingPoint,
			'cpc-pickup-indicator'	=> 'true',
			'expected-mailing-date'		=> $mailingDate,	
			'delivery-spec'		=> array(
				'service-code'		=> $service_code,
				'sender'			=> array(
					'name'				=> '',	
					'company'			=> Configuration::get('CPR_COMPANY'),	
					'contact-phone'		=> Configuration::get('CPR_PHONE'),	
					'address-details'	=> array(
						'address-line-1'	=> Configuration::get('CPR_ADDRESS1'),	
						'city'				=> Configuration::get('CPR_CITY'),	
						'prov-state'		=> Configuration::get('CPR_PROVINCE'),	
						'country-code'		=> 'CA',	
						'postal-zip-code'	=> Configuration::get('CPR_POSTAL_CODE')		
					)
				),
				'destination'			=> array(
					'name'				=> $addressDelivery->firstname.' '.$addressDelivery->lastname,	
					'company'			=> $addressDelivery->company,	
                    'client-voice-number' => empty($addressDelivery->phone) ? '000000000' : preg_replace('/[^0-9\-\+]/', '', $addressDelivery->phone),
					'address-details'	=> array(
						'address-line-1'	=> $addressDelivery->address1,	
						'address-line-2'	=> $addressDelivery->address2,	
						'city'				=> $addressDelivery->city,	
						'prov-state'		=> $iso_state,	
						'country-code'		=>$receiver_country,	
						'postal-zip-code'	=> $addressDelivery->postcode		
					)					
				),
				'options' => array(
					'option' => array(
						'option-code' => 'LAD',
					)
				),
				'parcel-characteristics'	=> array(
					'weight'		=> $weight,
					'dimensions'	=> array(
						'length'		=> Tools::getValue('cp_box_length'),
						'width'			=> Tools::getValue('cp_box_width'),
						'height'		=> Tools::getValue('cp_box_height')
					),
					'unpackaged'	=> false,
					'mailing-tube'	=> false
				),
				'notification' 	=> array(
					'email'			=> Configuration::get('PS_SHOP_EMAIL'),
					'on-shipment'	=> true,
					'on-exception'	=> false,
					'on-delivery'	=> true
				),
				'print-preferences' => array(
					'output-format'		=> Tools::getValue('cp_label'),
                    //'output-format'		=> '4x6',
                    'encoding' => $encoding
				),
				'preferences' 	=> array(
					'show-packing-instructions'	=> true,
					'show-postage-rate'			=> false,
					'show-insured-value'		=> true
				),													
				'settlement-info' => array(
					'contract-id'					=> $contractId,
					'intended-method-of-payment'	=> Tools::getValue('cp_payment')
				),
				'references' 	=> array(
					'cost-centre'	=> '',
					'customer-ref-1'	=> 'ORDER ID: '.Tools::getValue('id_order')
//						,'customer-ref-2'	=> 'custref2'
				)
			)
		)
	)
);
if ($receiver_country != 'CA') {
    $currency = Currency::getCurrency($order->id_currency);
    $currency = $currency['iso_code'];

    $request['create-shipment-request']['shipment']['delivery-spec']['customs'] = array (
        'currency' => 'CAD',
        'conversion-from-cad' => '1',
        'reason-for-export' => 'OTH',
        'other-reason' => 'GIFT',
    );
    if ($service_code == 'USA.XP') {  
        $request['create-shipment-request']['shipment']['delivery-spec']['options']['option']['option-code'] = 'RASE';        
    }
    else if ($order->total_paid_tax_incl > 50 || $service_code == 'USA.TP') {  
        $request['create-shipment-request']['shipment']['delivery-spec']['options']['option']['option-code'] = 'RTS';        
    }
    else {
        $request['create-shipment-request']['shipment']['delivery-spec']['options']['option']['option-code'] = 'ABAN';        
    }
    $order_details = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'order_detail` WHERE `id_order` = '.$order->id);
    foreach ($order_details  as $val) {
        $sku = Db::getInstance()->ExecuteS('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '. $val['product_id']);
        $request['create-shipment-request']['shipment']['delivery-spec']['customs']['sku-list'][] = array(
    		'customs-number-of-units' => $val['product_quantity'],
            'unit-weight' => $val['product_weight'],
            'sku' => empty($sku)? 'ProductID_'.$val['product_id'] : $sku[0]['reference'],
            'customs-description' => 'Cosmetic skin care products. NOT FOR RESALE.',
            'customs-value-per-unit' => $val['product_quantity']*2.5,
            'country-of-origin' => 'CA',
            'province-of-origin' => 'ON',
            'hs-tariff-code' => '3304.99.50'
    	);
    }    
}
//d($request);

try { 
//d($client);
    // Execute Request
	$result = $client->__soapCall('CreateShipment', $request, NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'shipment-info'}) ) {
	   return json_decode(json_encode($result->{'shipment-info'}), 1);      
       
	    echo  'Shipment Id: ' . $result->{'shipment-info'}->{'shipment-id'} . "\n";                 
		echo  'Tracking Pin: ' . $result->{'shipment-info'}->{'tracking-pin'} . "\n";                 
		foreach ( $result->{'shipment-info'}->{'artifacts'}->{'artifact'} as $artifact ) {  
			echo 'Artifact Id: ' . $artifact->{'artifact-id'} . "\n";
			echo 'Page Index: ' . $artifact->{'page-index'} . "\n\n";
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
            $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
//            d();
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
//    d();
}

return;



?>

