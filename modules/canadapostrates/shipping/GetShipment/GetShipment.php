<?php
 /**
 * Sample code for the GetShipment Canada Post service.
 * 
 * The GetShipment service is used to retrieve information related to a previously
 * created shipment. This may be useful for recovery from a communication or other
 * error and/or reporting on all outstanding shipments. 
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
if (empty($order_carrier->shipping_id)) return;
$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/GetShipment/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');
if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/shipment.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/shipment/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => $dirpath . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 


try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
	$result = $client->__soapCall('GetShipment', array(
	    'get-shipment-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
//            'platform-id'		=> Configuration::get('CPR_PLATFORM_ID'),
			'shipment-id'		=> $order_carrier->shipping_id
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'shipment-info'}) ) {
	   return json_decode(json_encode($result->{'shipment-info'}), 1);      
       
	    echo  'Shipment Id: ' . $result->{'shipment-info'}->{'shipment-id'} . "\n";                 
		echo  'Tracking Pin: ' . $result->{'shipment-info'}->{'tracking-pin'} . "\n";                 
		foreach ( $result->{'shipment-info'}->{'artifacts'}->{'artifact'} as $artifact ) {  
			echo 'Artifact Id: ' . $artifact->{'artifact-id'} . "\n";
			echo 'Page Index: ' . $artifact->{'page-index'} . "\n\n";
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
		  //return $message->code . " - " . $message->description;
                    
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
        $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
}

?>

