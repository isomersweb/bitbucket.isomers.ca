<?php
/**
*  Zack Hussain
*
*  @author      Zack Hussain <me@zackmedia.ca>
*  @copyright   2015 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*/

class CPTools
{
	/* Convert weight to KG */
	public static function toKg($unit)
	{
		if (Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT')) == "kg" ||
			Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT')) == "kgs")
			return $unit;
		if (Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT')) == "lbs" ||
			Tools::strtolower(Configuration::get('PS_WEIGHT_UNIT')) == "lb")
			return number_format($unit/2.2, 3, '.', '');
		else
			return "Invalid unit of measurement.";
	}

	/* Convert dimensions to CM */
	public static function toCm($unit)
	{
		if (Tools::strtolower(Configuration::get('PS_DIMENSION_UNIT')) == "cm" ||
			Tools::strtolower(Configuration::get('PS_DIMENSION_UNIT')) == "cms")
			return $unit;
		if (Tools::strtolower(Configuration::get('PS_DIMENSION_UNIT')) == "in" ||
			Tools::strtolower(Configuration::get('PS_DIMENSION_UNIT')) == "ins" ||
			Tools::strtolower(Configuration::get('PS_DIMENSION_UNIT')) == "inc")
			return number_format($unit*2.54, 1, '.', '');
		else
			return "Invalid unit of measurement.";
	}

	/* Return the conversion rate from CAD */
	public static function getConversionRateFromCad($currency)
	{
		$id_cad = Db::getInstance()->getValue('SELECT `id_currency` FROM `'._DB_PREFIX_.'currency` WHERE `iso_code` = "CAD"');

		$conversionRate = 1;
		$currencyOrigin = new Currency((int)$id_cad);
		$conversionRate /= $currencyOrigin->conversion_rate;
		$conversionRate *= $currency->conversion_rate;
		return number_format($conversionRate, 3, '.', '');
	}

	/* Return the conversion rate from CAD */
	public static function getCartCurrencyRateFromCad($id_cart)
	{
		$id_cad = Db::getInstance()->getValue('SELECT `id_currency` FROM `'._DB_PREFIX_.'currency` WHERE `iso_code` = "CAD"');

		$conversionRate = 1;
		$cart = new Cart($id_cart);
		if ($cart->id_currency != $id_cad)
		{
			$currencyOrigin = new Currency((int)$id_cad);
			$origin = $currencyOrigin->conversion_rate;
			if (!$origin || $origin <= 0)
				$origin = 1;
			$conversionRate /= $origin;
			$currencySelect = new Currency((int)$cart->id_currency);
			$conversionRate *= $currencySelect->conversion_rate;
		}
		return number_format($conversionRate, 3, '.', '');
	}

	/* Parses Rates array and creates 'service-code' => 'price' array */
	public static function putRatesInArray($array)
	{
		$rates = false;
		if (array_key_exists('price-quote', $array)) {
			$rates = array();
			if (CPTools::countArray($array['price-quote']) > 1)
			{
				foreach ($array['price-quote'] as $q)
				{
					$rates[$q['service-code']]['rate'] = $q['price-details']['due'];
					if (array_key_exists('expected-transit-time', $q['service-standard']))
						$rates[$q['service-code']]['delay'] = $q['service-standard']['expected-transit-time'];
				}
			}
			else if (CPTools::countArray($array['price-quote']) == 1)
			{
				$rates[$array['price-quote']['service-code']]['rate'] = $array['price-quote']['price-details']['due'];
				$rates[$array['price-quote']['service-code']]['delay'] = $array['price-quote']['service-standard']['expected-transit-time'];
			}
		}
		return $rates;
	}

	/* Parses Services array and puts option codes in PHP array */
	public static function putOptionsInArray($services)
	{
		$options = array();
		if (array_key_exists(0, $services['options']['option'])) {
			foreach ($services['options']['option'] as $o) {
				$options[] = $o['option-code'];
			}
		} else {
			$options[] = $services['options']['option']['option-code'];
		}
		return $options;
	}

	/* If array keys are numeric, it means there's more than one of the same key. Return count of a key */
	public static function countArray($array)
	{
		$count = 0;
		foreach($array as $k => $v)
		{
			if (is_numeric($k))
				$count++;
			else
				return $count += 1;
		}
		return $count;
	}

	/*
		Post request
	*/
	public static function call($url, $postfields)
	{
		try
		{
			$handle = curl_init();

			curl_setopt($handle, CURLOPT_URL, $url);
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $postfields);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);

			$curl = curl_exec($handle);
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

			curl_close ($handle);

			if ($httpCode != 200) {
				throw new Exception($httpCode);
			}

		}
		catch (Exception $e)
		{
			die("Connection to Zack Media failed. " . $curl);
		}

		return Tools::jsonDecode($curl);
	}

	public static function verify($prefix, $email, $serial)
	{
		$postfields = array(
			"email" => Tools::safeOutput($email),
			"serial" => Tools::safeOutput($serial),
			"download" => Configuration::get($prefix.'DOWNLOAD_ID'),
			);

		$url = 'http://zackhussain.ca/drm/verify.php';

		$verify = CPTools::call($url, $postfields);

		$response = array();
		if (!$verify->error)
		{
			$response['status'] = 1;
			$response['message'] = $verify->success;
			Configuration::updateValue($prefix.'VS', $serial);
			Configuration::updateValue($prefix.'VE', $email);
		}
		else
		{
			$response['status'] = 0;
			$response['message'] = $verify->error;
		}
		return $response;
	}

	public static function update($download, $version)
	{
		$postfields = array(
			"module_id" => Tools::safeOutput($download),
			"version" => Tools::safeOutput($version),
			);

		$url = 'http://zackhussain.ca/drm/update.php';

		$update = CPTools::call($url, $postfields);

		$response = array();
		if (!$update->error)
		{
			if ($update->update)
				return $update->update;
			return false;
		}
		else
		{
			return $update->error;
		}
		return false;
	}

	/* Get Token used to connect with CP */
	public static function getToken($prefix)
	{
		$postfields = array(
			Configuration::get($prefix.'TOKEN_REQUEST') => "1",
			"email" => Configuration::get($prefix.'VE'),
			"serial" => Configuration::get($prefix.'VS'),
			"mode" => "1",
			);

		/* Connect to platform owner to authenticate with Canada Post */
		$url = 'http://zackhussain.ca/cpl-api/cpapi.php';

		$token = CPTools::call($url, $postfields);

		$date = new DateTime();

		/**
		 *  Store time that token was generated, it expires in 30 minutes
		 *  so we will keep track of the time before we use it later
		 */
		$response = array();
		if ($token->{'success'})
		{
			$response['token'] = $token->{'token'}->{0};
			Configuration::updateValue($prefix.'TOKEN_TIME', $date->format('Y-m-d H:i:s'));
			Configuration::updateValue($prefix.'PLATFORM_ID', $token->{'platform_id'});
		}
		else
		{
			$response['error'] = $token->{'error'};
		}
		return $response;
	}

	/**
	 * Save customer API info
	 */
	public static function getCustomerInfo($prefix)
	{
		$postfields = array(
			"customer_request" => "1",
			"token_id" => Configuration::get($prefix.'TOKEN'),
			"mode" => "1",
			);

		$url = 'http://zackhussain.ca/cpl-api/cpapi.php';

		$customer = CPTools::call($url, $postfields);

		if (property_exists($customer, 'success'))
		{
			$response = $customer->{'merchant_info'};

			if ($customer->{'mode'} == "1")
			{
				if (property_exists($customer->{'merchant_info'}, 'contract-number'))
					Configuration::updateValue($prefix.'CONTRACT', $customer->{'merchant_info'}->{'contract-number'});

				Configuration::updateValue($prefix.'CUSTOMER_NUMBER', $customer->{'merchant_info'}->{'customer-number'});
				Configuration::updateValue($prefix.'PROD_API_USER', $customer->{'merchant_info'}->{'merchant-username'});
				Configuration::updateValue($prefix.'PROD_API_PASS', $customer->{'merchant_info'}->{'merchant-password'});
				Configuration::updateValue($prefix.'CREDIT', $customer->{'merchant_info'}->{'has-default-credit-card'});
			}

		} else {
			return false;
		}

		return $response;
	}

	/**
	 * @return if array of links contains commercial invoice
	 */
	public static function hasInvoice($array)
	{
		foreach ($array['links']['link'] as $a)
			if ($a['@attributes']['rel'] == 'commercialInvoice')
				return true;
			return false;
	}

	public static function seed($object)
	{
		if ( !Configuration::updateValue($object->_prefix.'PROD_API_USER', '1') ||
			!Configuration::updateValue($object->_prefix.'PROD_API_PASS', '1') ||
			!Configuration::updateValue($object->_prefix.'CUSTOMER_NUMBER', '1') ||
			!Configuration::updateValue($object->_prefix.'VS', true) ||
			!Configuration::updateValue($object->_prefix.'VE', true) ||
			!Configuration::updateValue($object->_prefix.'TOKEN_REQUEST', 'token_request_ps') ||
			!$object->installMethodsDb() ||
			!$object->installMethods() ||
			!$object->installGroupsDb() ||
			!Db::getInstance()->insert($object->_prefix_low.'groups', array('name' => pSQL('Default')), false, true, Db::INSERT_IGNORE) ||
			!$object->installBoxesDb())
			return false;
		return true;
	}

	/* Install DB table with Canada Post service methods */
	public static function installMethodsDb($object)
	{
		return Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$object->_prefix_low.'methods` (
				`id_method` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_carrier` int(10) NULL,
				`id_carrier_history` text NULL,
				`name` varchar(255) NOT NULL,
				`code` varchar(16) NOT NULL,
				`group` varchar(16) NOT NULL,
				`active` tinyint(1) NOT NULL,
				UNIQUE(`name`, `code`),
				PRIMARY KEY (`id_method`))
		ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
	}

	/* Populate Methods DB with Canada Post service methods */
	public static function installMethods($object)
	{
		if (!$object->getMethods()) {
			foreach ($object->shipping_methods as $type => $methods) {
				foreach ($methods as $k => $v) {
					if (!Db::getInstance()->insert($object->_prefix_low.'methods', array(
						'name'      => pSQL($v),
						'code'      => pSQL($k),
						'group'      => pSQL($type),
						'active'      => 0,
						)))
						return false;
				}
			}
			return true;
		}
		return true;
	}

	public static function installBoxesDb($object)
	{
		return Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$object->_prefix_low.'boxes` (
				`id_box` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`name` varchar(32) NOT NULL,
				`width` decimal(10,1) NOT NULL,
				`height` decimal(10,1) NOT NULL,
				`length` decimal(10,1) NOT NULL,
				`weight` decimal(10,3) NOT NULL,
				`cube` decimal(30,3) NOT NULL,
				PRIMARY KEY (`id_box`))
		ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
	}

	public static function installBox($object)
	{
		if (!$object->getBoxes()) {
			$box = array(
				'name'      => 'Default Box',
				'width'      => '10.0',
				'height'      => '10.0',
				'length'      => '10.0',
				'weight'      => '0.100',
				'cube'      => '1000.0',
				);
			Db::getInstance()->insert($object->_prefix_low.'boxes', $box);
			return true;
		}
		return true;
	}

	public static function installGroupsDb($object)
	{
		return Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$object->_prefix_low.'groups` (
				`id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`name` varchar(32) NOT NULL,
				PRIMARY KEY (`id_group`),
				UNIQUE KEY (`name`)) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
	}
}