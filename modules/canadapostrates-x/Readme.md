# Canada Post Rates

A Prestashop module that displays rates from Canada Post.

- Connect: You first need to connect your Canada Post account to this module by clicking 'Sign in with Canada Post' if you haven't already.
- Rates: To display real-time rates on the cart page, select each method you want below under Rates Preferences.

## Helpful Information

### Rates

- Boxes: You MUST add at least one box under the box settings below for rates to show up on the cart page.
- Back-Office Carriers: Selecting shipping methods below will install custom carriers in your Shipping > Carriers menu. It's not advisable to edit these (unless it's just the logo). To deactivate a carrier, simply uncheck one or more carriers below under 'Rates Preferences' instead of doing it from the Carriers page.
- Invalid Methods: Some shipping methods will not show up if they are invalid for the given parcel. e.g. Worldwide Envelope Int’l will not show up if the current order needs a large box.
- Invalid Addresses: If you or the customer has an invalid address, the rates will not show up.
- Product Attributes: It's a good idea to set product shipping attributes (lenth, width, height and weight) for each product, as the module will go through all your box sizes and determine the smallest box that will fit all the products in the cart, and display the rates accordingly.

### Boxes

- Accurate Rates: The more boxes you add, the more accurate the rates will be.
- Measurements: If you change your store measurement preferences, e.g. IN to CM, you must edit your box sizes to reflect that change as they will still be in inches.
- Changing Order Dimensions: On an admin order page, you can enter a custom length/width/height and the box size will be ignored.
- Box Weight: Depending on your preferences for this module, the weight of the box may be added to the total weight of the shipment behind the scenes.

Changelog

- 1.2.1
    - FIX: Weight calculation on split shipments
    - FIX: Conversion edge case when CAD is disabled on a separate multishop site
    - FIX: SQL sanitzation bug with magic quotes

- 1.2
    - NEW: Split Products option
    - NEW: Delivery Delay setting
    - NEW: Edit boxes
    - FIX: Multiply each product by its quantity
    - FIX: Make box form larger
    - DEBUG: Refactor value deletion
    - DEBUG: Add seed debug tools

- 1.0.7
    - Fixed rates with handling fees
    - Fixed CPTools class typo