<?php
/**
 * Sample code for the CreateNonContractShipment Canada Post service.
 * 
 * The CreateNonContractShipment service is used to generate and pay for a 
 * shipping label, including validation and payment authorization. 
 * 
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 */

// Your username and password are imported from the following file
// CPCWS_SOAP_NCShipping_PHP_Samples\SOAP\ncshipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../wsdl/ncshipment.wsdl';




$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/ncshipment/v4';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 


try {
	$mailedBy = $userProperties['customerNumber'];
	$requestedShippingPoint = 'H2B1A0';
	$contractId = '0040604014';

	// Execute Request
	$result = $client->__soapCall('CreateNCShipment', array(
	    'create-non-contract-shipment-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'non-contract-shipment' 		=> array(
				'requested-shipping-point'		=> 'H2B1A0',
				'delivery-spec'		=> array(
					'service-code'		=> 'DOM.EP',
					'sender'			=> array(
						'company'			=> 'company',	
						'contact-phone'		=> 'contact-phone',	
						'address-details'	=> array(
							'address-line-1'	=> '502 MAIN ST N',	
							'city'				=> 'MONTREAL',	
							'prov-state'		=> 'QC',	
							'postal-zip-code'	=> 'H2B1A0'		
						)
					),
					'destination'			=> array(
						'name'				=> 'John Doe',	
						'company'			=> 'ACME Corp',	
						'address-details'	=> array(
							'address-line-1'	=> '123 Postal Drive',	
							'city'				=> 'Ottawa',	
							'prov-state'		=> 'ON',	
							'country-code'		=> 'CA',	
							'postal-zip-code'	=> 'K1P5Z9'		
						)					
					),
					'options' 			=> array(
						'option' 				=> array(
							'option-code'			=> 'DC'
						)
					),
					'parcel-characteristics'	=> array(
						'weight'		=> 15,
						'dimensions'	=> array(
							'length'		=> 6,
							'width'			=> 12,
							'height'		=> 9
						)
					),
					'preferences' 	=> array(
						'show-packing-instructions'	=> true,
					),
					'settlement-info' => array(
						'contract-id'					=> $contractId,
						'intended-method-of-payment'	=> 'Account'
					),
					'references' 	=> array(
						'cost-centre'	=> 'ccent',
						'customer-ref-1'	=> 'custref1',
						'customer-ref-2'	=> 'custref2'
					)													
				)
			)
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'non-contract-shipment-info'}) ) {
	    echo  'Shipment Id: ' . $result->{'non-contract-shipment-info'}->{'shipment-id'} . "\n";                 
		foreach ( $result->{'non-contract-shipment-info'}->{'artifacts'}->{'artifact'} as $artifact ) {  
			echo 'Artifact Id: ' . $artifact->{'artifact-id'} . "\n";
			echo 'Page Index: ' . $artifact->{'page-index'} . "\n\n";
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

echo "<pre>";
print_r($result);
echo "</pre>";

?>

