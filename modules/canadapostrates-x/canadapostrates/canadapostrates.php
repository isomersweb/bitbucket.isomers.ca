<?php
/**
*  2014 Zack Hussain
*
*  @author 		Zack Hussain <me@zackhussain.ca>
*  @copyright  	2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*/
if (!defined('_PS_VERSION_'))
	exit;

include_once(dirname(__FILE__).'/classes/CprApi.php');

if (!class_exists('CPTools'))
	include_once(dirname(__FILE__).'/classes/CPTools.php');

class CanadaPostRates extends CarrierModule {

	const PREFIX = 'CPR_';
	const PREFIX_LOW = 'cpr_';

	public $_prefix = 'CPR_';
	public $_prefix_low = 'cpr_';

	/* Variable that the cart loads with carrier ID */
	public $id_carrier;

	/* Store rate data to avoid calling multiple times */
	public $rate = false;

	private $_postErrors = array();
	private $_dimensionUnit = '';
	private $_weightUnit = '';
	private $_dimensionUnitList = array('CM' => 'CM', 'IN' => 'IN', 'CMS' => 'CM', 'INC' => 'IN');
	private $_weightUnitList = array('KG' => 'KGS', 'KGS' => 'KGS', 'LBS' => 'LBS', 'LB' => 'LBS');
	public $_provinces = array('AB' => 'Alberta', 'BC' => 'British Columbia', 'MB' => 'Manitoba', 'NB' => 'New Brunswick', 'NL' => 'Newfoundland and Labrador',
		'NT' => 'Northwest Territories', 'NS' => 'Nova Scotia', 'NU' => 'Nunavut', 'ON' => 'Ontario',
		'PE' => 'Prince Edward Island', 'QC' => 'Quebec', 'SK' => 'Saskatchewan', 'YT' => 'Yukon');
	public $shipping_methods = array(
		'DOM' => array(
			'DOM.RP' => 'Regular',
			'DOM.EP' => 'Expedited',
			'DOM.XP' => 'Xpresspost',
			'DOM.PC' => 'Priority',
		),
		'USA' => array(
			'USA.EP' => 'Expedited Parcel USA',
			'USA.XP' => 'Xpresspost USA',
			'USA.PW.ENV' => 'Priority Worldwide Envelope USA',
			'USA.PW.PAK' => 'Priority Worldwide pak USA',
			'USA.SP.AIR' => 'Small Packet USA Air (less than 1kg)',
			'USA.TP' => 'Tracked Packet USA',
		),
		'INT' => array(
			'INT.PW.ENV' => 'Priority Worldwide Envelope Int’l',
			'INT.PW.PAK' => 'Priority Worldwide pak Int’l',
			'INT.PW.PARCEL' => 'Priority Worldwide parcel Int’l',
			'INT.XP' => 'Xpresspost International',
			'INT.SP.AIR' => 'Small Packet Int’l Air (less than 2kg)',
			'INT.SP.SURF' => 'Small Packet Int’l Surface (less than 2kg)',
			'INT.TP' => 'Tracked Packet – Int’l',
		),
	);

	public function __construct() {

        $this->name = 'canadapostrates';
		$this->version = '1.2.1';
		$this->author = 'ZH Media';
		$this->tab = 'shipping_logistics';
		$this->bootstrap = true;
		$this->limited_countries = array('ca');
		$this->module_key = 'a69ddac172a07dcfddf665284d90eacd';
		$this->displayName = $this->l('Canada Post Rates');
		$this->description = $this->l('Display real-time dynamic rates to customers.');

		if (Configuration::get(self::PREFIX.'SAVE_INFO') == false)
			$this->confirmUninstall = $this->l('Are you sure you want to remove all your settings?');

        parent::__construct();

        /* Check if cURL is enabled */
		if (!is_callable('curl_exec'))
		{
			$this->warning = $this->l('cURL extension must be enabled on your server to use this module.');

        	if ($update = $this->checkForUpdate())
        		$this->warning .= $update;
		}

		// Add false values for debugging
        // CPTools::seed($this);

		if ($this->isConnected())
			if (!$this->getBoxes())
				$this->warning = $this->l('You must add at least 1 box in order for the module to function correctly.');

			$this->_dimensionUnit = isset($this->_dimensionUnitList[ Tools::strtoupper(Configuration::get('PS_DIMENSION_UNIT'))]) ? $this->_dimensionUnitList[ Tools::strtoupper(Configuration::get('PS_DIMENSION_UNIT'))] : false;
			$this->_weightUnit = isset($this->_weightUnitList[ Tools::strtoupper(Configuration::get('PS_WEIGHT_UNIT'))]) ? $this->_weightUnitList[ Tools::strtoupper(Configuration::get('PS_WEIGHT_UNIT'))] : false;
			if (!$this->_weightUnit || !$this->_weightUnitList[$this->_weightUnit])
				$this->warning = $this->l('\'Invalid Weight Unit, must be LB, LBS or KG.\'').' ';
			if (!$this->_dimensionUnit || !$this->_dimensionUnitList[$this->_dimensionUnit])
				$this->warning = $this->l('\'Invalid Dimension Unit, must be CM or IN.\'').' ';

	}

	public function install() {

        if (!parent::install() ||
        	!Configuration::updateValue(self::PREFIX.'MODE', '1') ||
        	!Configuration::updateValue(self::PREFIX.'DEFAULT_WEIGHT', '1.000') ||
        	// !Configuration::updateValue(self::PREFIX.'SAVE_INFO', false) ||
        	!Configuration::updateValue(self::PREFIX.'DELAY', '0') ||
        	!Configuration::updateValue(self::PREFIX.'TOKEN_REQUEST', 'token_request') ||
        	!Configuration::updateGlobalValue(self::PREFIX.'DOWNLOAD_ID', '874') ||
        	!Configuration::updateValue(self::PREFIX.'VS', true) ||	!Configuration::updateValue(self::PREFIX.'VE', true) ||!Configuration::updateValue(self::PREFIX.'TOKEN_REQUEST', 'token_request_ps') ||
			!$this->registerHook('updateCarrier') ||
			!$this->registerHook('actionCartSave') ||
    		!$this->registerHook('displayBeforeCarrier') ||
			!CPTools::installMethodsDb($this) ||
			!CPTools::installMethods($this) ||
			!CPTools::installBoxesDb($this) ||
			!CPTools::installBox($this))
				return false;
			return true;
	}

	public function checkForUpdate()
	{
		if ($update = CPTools::update(Configuration::get(self::PREFIX.'DOWNLOAD_ID'), $this->version))
			return $update;
		return false;
	}

	public function uninstall() {
		if (Configuration::get(self::PREFIX.'SAVE_INFO') == false)
			if (!$this->uninstallCarriers() ||
				!$this->deleteConfig() ||
				!Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::PREFIX_LOW.'boxes`') ||
				!Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::PREFIX_LOW.'methods`'))
				return false;
		if (!Configuration::deleteByName(self::PREFIX.'TOKEN') ||
			!Configuration::deleteByName(self::PREFIX.'TOKEN_TIME') ||
			!Configuration::deleteByName(self::PREFIX.'TOKEN_REQUEST') ||
			!parent::uninstall())
			return false;
		return true;
    }

    /**
     * Delete module configurations
     * @return boolean
     */
    private function deleteConfig()
    {
    	if ($values = $this->getConfigFieldsValues())
    		foreach ($values as $k => $v)
    			if (!Configuration::deleteByName(str_replace('.', '_', $k)))
    				return false;
		return true;
    }

	/* Check if a Canada Post account is connected */
	public function isConnected()
	{
		if (Configuration::get(self::PREFIX.'PROD_API_PASS') &&  Configuration::get(self::PREFIX.'PROD_API_USER') && Configuration::get(self::PREFIX.'CUSTOMER_NUMBER'))
			return true;
		return false;
	}

	public function isVerified()
	{
		if (Configuration::get(self::PREFIX.'VS') && Configuration::get(self::PREFIX.'VE'))
			return true;
		return false;
	}

    /* Get token from Canada Post. It is used to authenticate this module */
    public function getToken()
    {
		/* Canada Post account connect validations */
		if (Tools::getValue('registration-status') == "SUCCESS")
		{
			CPTools::getCustomerInfo(self::PREFIX);
			Configuration::deleteByName(self::PREFIX.'TOKEN');
		}
		elseif (Tools::getValue('registration-status') == "CANCELLED")
			$this->_postErrors[] = $this->l('This module will be unable to submit requests on your behalf until you accept the terms and conditions, but your relationship with Canada Post remains in effect. You may try again.');
		elseif (Tools::getValue('registration-status') && Tools::getValue('registration-status') == "SERVICE_UNAVAILABLE")
			$this->_postErrors[] = $this->l('The Canada Post service is currently unavailable, please try again later.');
		elseif (Tools::getValue('registration-status') && Tools::getValue('registration-status') !== "SUCCESS" && Tools::getValue('registration-status') !== "CANCELLED")
			$this->_postErrors[] = $this->l('Failure to connect with Canada Post. Your token might have expired, please refresh the page to generate a new one. Your account might also have already been connected. Please try again later.');

		if ($this->isVerified()) {
			/* Generate a token. The token is used by canada post to authorize this app */
			if (!$this->isConnected()) {

		    	/* Get the time that the token was created at, and the difference between now and then */
				$start_date = new DateTime(Configuration::get(self::PREFIX.'TOKEN_TIME'));
				$since_start = $start_date->diff(new DateTime());

				/* If 30 minutes has passed, delete expired token */
				if ((int)$since_start->i >= 30) {
					Configuration::deleteByName(self::PREFIX.'TOKEN');
					Configuration::deleteByName(self::PREFIX.'TOKEN_TIME');
				}

				$token = CPTools::getToken(self::PREFIX);

				if (!array_key_exists('error', $token) && $token['token'] != NULL) {
					Configuration::updateValue(self::PREFIX.'TOKEN', $token['token']);
				} else {
					if (is_object($token['error']))
						foreach ($token['error'] as $e)
							$this->_postErrors[] = $e;

					$this->_postErrors[] = $this->l('Error retrieving token. Please contact the module creator at me@zackmedia.ca and give him this error message.');
				}
			}
		}
    }

	/* Show config form */
	public function getContent()
	{
		$this->getToken();

		$output = '';

		if ($this->isConnected())
			if (!$this->getBoxes())
				$output .= $this->displayError($this->l('You must add at least 1 box in order for the module to function correctly.'));

	    $this->_postValidation();
			if (!count($this->_postErrors))
				$output .= $this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$output .= $this->displayError($err);

		$output .= $this->_displayInfos();
		if (!$this->isVerified())
			$output .= $this->renderVerify();
		elseif (!$this->isConnected()) {
			if (Configuration::get(self::PREFIX.'TOKEN'))
				$output .= $this->renderConnect();
		}
		else
		{
			$output .= $this->renderDisconnect();
			$output .= $this->renderPrefs();
			$output .= $this->renderAddress();
			$output .= $this->renderRates();
			$output .= $this->renderBoxes();
		}

	    return $output;
	}

	private function _displayInfos()
	{
		$this->context->smarty->assign(array(
			'pl' => self::PREFIX_LOW,
			));
		return $this->display(__FILE__, 'infos.tpl');
	}

	private function _postValidation()
	{
		if (Tools::isSubmit('submitVerify'))
		{
			if (!Tools::getValue(self::PREFIX.'VE'))
				$this->_postErrors[] = $this->l('Your email is required.');
			if (!Tools::getValue(self::PREFIX.'VS'))
				$this->_postErrors[] = $this->l('Your serial is required.');
		}

		if (Tools::isSubmit('submitPrefs'))
		{
			if ((Tools::getValue(self::PREFIX.'MODE') == 0) && (!Tools::getValue(self::PREFIX.'DEV_API_USER') || !Tools::getValue(self::PREFIX.'DEV_API_PASS')))
				$this->_postErrors[] = $this->l('You must enter your Test API keys to use Test mode.');
		}

		if (Tools::isSubmit('submitAddress'))
		{
			if (!Tools::getValue(self::PREFIX.'CITY'))
				$this->_postErrors[] = $this->l('Your city is required.');
			if (!Tools::getValue(self::PREFIX.'ADDRESS1'))
				$this->_postErrors[] = $this->l('Your address is required.');
			if (!Tools::getValue(self::PREFIX.'POSTAL_CODE'))
				$this->_postErrors[] = $this->l('Your postal code is required.');
			if (!Tools::getValue(self::PREFIX.'PHONE'))
				$this->_postErrors[] = $this->l('Your phone number is required.');
		}

		if (Tools::isSubmit('submitUpdateMethods'))
		{
			if (!is_numeric(Tools::getValue(self::PREFIX.'DELAY')))
				$this->_postErrors[] = $this->l('Delivery Delay must be a number.');
		}

		if (Tools::isSubmit('submitBox'))
		{
			if (!Tools::getValue(self::PREFIX.'BOX_NAME'))
				$this->_postErrors[] = $this->l('You must enter a name.');
			if (!Tools::getValue(self::PREFIX.'BOX_L') || !Tools::getValue(self::PREFIX.'BOX_W') || !Tools::getValue(self::PREFIX.'BOX_H') || !Tools::getValue(self::PREFIX.'BOX_WEIGHT'))
				$this->_postErrors[] = $this->l('You must enter the dimensions.');
		}

		if (Tools::isSubmit('submitBoxUpdate'))
		{
			if (!Tools::getValue(self::PREFIX.'BOXES'))
				$this->_postErrors[] = $this->l('You must select a box first.');
			if (!Tools::getValue(self::PREFIX.'BOX_NAME'))
				$this->_postErrors[] = $this->l('You must enter a name.');
			if (!Tools::getValue(self::PREFIX.'BOX_L') || !Tools::getValue(self::PREFIX.'BOX_W') || !Tools::getValue(self::PREFIX.'BOX_H') || !Tools::getValue(self::PREFIX.'BOX_WEIGHT'))
				$this->_postErrors[] = $this->l('You must enter the dimensions.');
		}

		if (Tools::isSubmit('submitBoxDelete'))
		{
			if (!Tools::getValue(self::PREFIX.'BOXES'))
				$this->_postErrors[] = $this->l('You must select a box first.');
		}
		if (Tools::isSubmit('submitBoxDefault'))
		{
			if (!Tools::getValue(self::PREFIX.'BOXES'))
				$this->_postErrors[] = $this->l('You must select a box first.');
		}
	}

	/* Process settings submission */
	private function _postProcess()
	{

		if (Tools::isSubmit('submitVerify'))
		{
			if (!sizeof($this->_postErrors))
			{
				$verify = CPTools::verify(self::PREFIX, Tools::getValue(self::PREFIX.'VE'), Tools::getValue(self::PREFIX.'VS'));

				if ($verify['status'] == 1)
					Tools::redirect($_SERVER['HTTP_REFERER']);
				elseif ($verify['status'] == 0)
					return $this->displayError($verify['message']);
				else
					return false;
			}
			else
				$this->displayErrors();
		}

		/* Disconnect account */
		if (Tools::isSubmit('submitDisconnect')) {

			Configuration::updateValue(self::PREFIX.'MODE', "1");
			Configuration::deleteByName(self::PREFIX.'CUSTOMER_NUMBER');
			Configuration::deleteByName(self::PREFIX.'CONTRACT');
			Configuration::deleteByName(self::PREFIX.'PROD_API_USER');
			Configuration::deleteByName(self::PREFIX.'PROD_API_PASS');
			Configuration::deleteByName(self::PREFIX.'PLATFORM_ID');

			Tools::redirect($_SERVER['HTTP_REFERER']);

		}
		/* Save preferences */
		if (Tools::isSubmit('submitPrefs'))
		{
			Configuration::updateValue(self::PREFIX.'MODE', Tools::getValue(self::PREFIX.'MODE'));
			Configuration::updateValue(self::PREFIX.'SAVE_INFO', Tools::getValue(self::PREFIX.'SAVE_INFO'));
			Configuration::updateValue(self::PREFIX.'SIGNATURE', Tools::getValue(self::PREFIX.'SIGNATURE'));
			Configuration::updateValue(self::PREFIX.'DEV_API_USER', Tools::getValue(self::PREFIX.'DEV_API_USER'));
			Configuration::updateValue(self::PREFIX.'DEV_API_PASS', Tools::getValue(self::PREFIX.'DEV_API_PASS'));
			Configuration::updateValue(self::PREFIX.'CUSTOMER_NUMBER', Tools::getValue(self::PREFIX.'CUSTOMER_NUMBER'));
			Configuration::updateValue(self::PREFIX.'CONTRACT', Tools::getValue(self::PREFIX.'CONTRACT'));

			return $this->displayConfirmation('Settings updated.');
		}
		/* Save address info */
		if (Tools::isSubmit('submitAddress'))
		{
			$postcode = str_replace(" ", "", Tools::getValue(self::PREFIX.'POSTAL_CODE'));
			$postcode = Tools::strtoupper($postcode);

			Configuration::updateValue(self::PREFIX.'PROVINCE', Tools::getValue(self::PREFIX.'PROVINCE'));
			Configuration::updateValue(self::PREFIX.'CITY', Tools::getValue(self::PREFIX.'CITY'));
			Configuration::updateValue(self::PREFIX.'ADDRESS1', Tools::getValue(self::PREFIX.'ADDRESS1'));
			Configuration::updateValue(self::PREFIX.'ADDRESS2', Tools::getValue(self::PREFIX.'ADDRESS2'));
			Configuration::updateValue(self::PREFIX.'POSTAL_CODE', $postcode);
			Configuration::updateValue(self::PREFIX.'COMPANY', Tools::getValue(self::PREFIX.'COMPANY'));
			Configuration::updateValue(self::PREFIX.'PHONE', Tools::getValue(self::PREFIX.'PHONE'));

			return $this->displayConfirmation('Settings updated.');
		}
		/* Save rates methods and install carriers */
		if (Tools::isSubmit('submitUpdateMethods'))
		{
			Configuration::updateValue(self::PREFIX.'RATE_SIGNATURE', Tools::getValue(self::PREFIX.'RATE_SIGNATURE'));
			Configuration::updateValue(self::PREFIX.'CARRIER_IMAGE', Tools::getValue(self::PREFIX.'CARRIER_IMAGE'));
			Configuration::updateValue(self::PREFIX.'DEFAULT_WEIGHT', number_format(Tools::getValue(self::PREFIX.'DEFAULT_WEIGHT'),3,'.',''));
			Configuration::updateValue(self::PREFIX.'SPLIT', Tools::getValue(self::PREFIX.'SPLIT'));
			Configuration::updateValue(self::PREFIX.'DELAY', (int)Tools::getValue(self::PREFIX.'DELAY'));
			if ($methods = $this->getMethods()) {
		    	foreach ($methods as $m) {
		    		/*
		    			Form $_POST changes our "." to an "_" (e.g. DOM.RP to DOM_RP),
		    			so we must change ours to correctly reference it.
		    		*/
		    		$code = str_replace(".", "_", $m['code']);

		    		$where = '`code` = "'.pSQL($m['code']).'"';

		    		if (Tools::getIsset(self::PREFIX.'METHODS_'.$code)) {

		    			Db::getInstance()->update(self::PREFIX_LOW.'methods', array('active' => 1), $where);

		    			/* If the back-office carrier hasn't been installed yet, create a carrier for the selected method */
		    			if (!$m['id_carrier']) {
		    				$this->installCarrier($m);
		    			} else {
		    				$carrier = new Carrier((int)$m['id_carrier']);
		    				if (!Configuration::get(self::PREFIX.'CARRIER_IMAGE'))
		    					unlink(_PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
		    				elseif (!file_exists(_PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
		    					Tools::copy(dirname(__FILE__).'/views/img/cp_logo.png', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
		    				/* If the carrier was deleted from BO carrier page, install it again. Otherwise activate it. */
		    				if ($carrier->deleted == 1) {
		    					$this->installCarrier($m);
		    				} else {
								$carrier->active = true;
								$carrier->update();
							}
		    			}
		    		} else {

		    			Db::getInstance()->update(self::PREFIX_LOW.'methods', array('active' => 0), $where);

		    			/* If the carrier exists, disable it */
		    			if ($m['id_carrier']) {
		    				$carrier = new Carrier((int)$m['id_carrier']);
		    				if (!Configuration::get(self::PREFIX.'CARRIER_IMAGE'))
		    					unlink(_PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
		    				elseif (!file_exists(_PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
		    					Tools::copy(dirname(__FILE__).'/views/img/cp_logo.png', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
							$carrier->active = false;
							$carrier->update();
		    			}
		    		}
		    	}
	    	}
	    	return $this->displayConfirmation('Settings updated.');
		}
		/* Insert custom box in db */
		if (Tools::isSubmit('submitBox') || Tools::isSubmit('submitBoxUpdate'))
		{
			$cube = Tools::getValue(self::PREFIX.'BOX_L') * Tools::getValue(self::PREFIX.'BOX_W') * Tools::getValue(self::PREFIX.'BOX_H');
			$box = array(
				'name'      => pSQL(Tools::getValue(self::PREFIX.'BOX_NAME')),
				'width'      => (float)number_format(Tools::getValue(self::PREFIX.'BOX_W'), 1,'.',''),
				'height'      => (float)number_format(Tools::getValue(self::PREFIX.'BOX_H'), 1,'.',''),
				'length'      => (float)number_format(Tools::getValue(self::PREFIX.'BOX_L'), 1,'.',''),
				'weight'      => (float)number_format(Tools::getValue(self::PREFIX.'BOX_WEIGHT'), 3,'.',''),
				'cube'      => (float)$cube,
				);
			if (Tools::isSubmit('submitBox'))
				Db::getInstance()->insert(self::PREFIX_LOW.'boxes', $box);
			elseif (Tools::isSubmit('submitBoxUpdate'))
				Db::getInstance()->update(self::PREFIX_LOW.'boxes', $box, 'id_box = '.Tools::getValue(self::PREFIX.'BOXES'));

			Tools::redirect($_SERVER['HTTP_REFERER']);
		}
		/* Delete selected box from db */
		if (Tools::isSubmit('submitBoxDelete'))
		{
			$where = 'id_box = ' . pSQL(Tools::getValue(self::PREFIX.'BOXES'));
			Db::getInstance()->delete(self::PREFIX_LOW.'boxes', $where, 1);

			return $this->displayConfirmation('Box deleted.');
		}
        if (Tools::isSubmit('submitBoxDefault'))
		{
		  Configuration::updateValue(self::PREFIX.'BOX_DEFAULT', (int)Tools::getValue(self::PREFIX.'BOXES'));
		}
		/* Get box */
		if (Tools::isSubmit('submitAjaxBox'))
		{
			if ($box = $this->getBox(Tools::getValue('id'))) {
				echo Tools::jsonEncode($box);
				exit;
			}
			return false;
		}
	}

	/* Display configuration in admin panel */
	private function renderVerify()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Product Activation'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Enter your Email'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'VE',
						'desc' => $this->l('This must be the email that you purchased the module with.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Enter your Serial Number'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'VS',
						'desc' => $this->l('The serial number was emailed to the address you used to purchase the module with. If you never received one, please contact me@zackmedia.ca'),
					),
				),
				'submit' => array(
					'title' => $this->l('Activate'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitVerify';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}


	private function renderConnect()
	{
		$this->context->smarty->assign(array(
			'return_url' => Tools::getShopDomainSsl(true).$_SERVER['REQUEST_URI'].'&request=1',
			'token_id' => Configuration::get(self::PREFIX.'TOKEN'),
			'platform_id' => Configuration::get(self::PREFIX.'PLATFORM_ID'),
			));
		return $this->display(__FILE__, 'connect.tpl');
	}

	/* Display Disconnect button */
	private function renderDisconnect()
	{


		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Disconnect Canada Post Account'),
					'icon' => 'icon-cogs',
				),
				'description' => $this->l('Your account number '.Configuration::get(self::PREFIX.'CUSTOMER_NUMBER').' has been connected. '),
				'input' => array(),
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => $this->l('Disconnect Account'),
						'name' => 'submitDisconnect',
						'icon' => 'icon-unlink',
					),
				),
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}
	/* Display preferences */
	private function renderPrefs()
	{

		version_compare(_PS_VERSION_, '1.6', '<') ? $switch = "radio" : $switch = "switch";

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Preferences'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => $switch,
						'label' => $this->l('Live Mode'),
						'name' => self::PREFIX.'MODE',
						'desc' => $this->l('"Yes" to display real rates, "No" for testing purposes only. Testing require Test API keys below.'),
						'class' => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Test API user key'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'DEV_API_USER',
						'desc' => 'Login to <a target="_blank" href="https://www.canadapost.ca/webservices">developer program</a> and paste the first half of your Dev API key (excl. colon).',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Test API pass key'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'DEV_API_PASS',
						'autocomplete' => false,
						'desc' => 'Login to <a target="_blank" href="https://www.canadapost.ca/webservices">developer program</a> and paste the second half of your Dev API key (excl. colon).',
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Сustomer Number'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'CUSTOMER_NUMBER',
						'autocomplete' => false,
						'desc' => '',
					),
                    array(
						'type' => 'text',
						'label' => $this->l('Contract ID'),
						'class' => 'fixed-width-xxl',
						'name' => self::PREFIX.'CONTRACT',
						'autocomplete' => false,
						'desc' => '',
					),
					array(
						'type' => $switch,
						'label' => $this->l('Save info on uninstall'),
						'name' => self::PREFIX.'SAVE_INFO',
						'desc' => $this->l('Saves your settings. If on, your address, carriers and boxes will still be here when you reinstall.'),
						'class' => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitPrefs';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	/* Display Address in admin panel */
	private function renderAddress()
	{

		/* Get list of provinces to fill in Selector */
		$options = array();
		foreach ($this->_provinces as $k => $v) {
            $options[] = array(
		    		'id_option' => $k,
		    		'name' => $v,
	    	);
        }

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Your Address'),
					'icon' => 'icon-user'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Company'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'COMPANY',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Phone'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'PHONE',
						'required' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Address Line 1'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'ADDRESS1',
						'required' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Address Line 2'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'ADDRESS2',
					),
					array(
						'type' => 'text',
						'label' => $this->l('City'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'CITY',
						'required' => true,
					),
					array(
						'type' => 'select',
						'label' => $this->l('Province'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'PROVINCE',
						'options' => array(
							'query' => $options,
							'id' => 'id_option',
   							'name' => 'name' ,
							),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Postal Code'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'POSTAL_CODE',
						'required' => true,
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitAddress';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	/* Display Boxes in admin panel */
	private function renderBoxes()
	{

		/* Get list of boxes to fill Selector with */
        $options = array();
        if ($boxes = $this->getBoxes()) {
            foreach ($boxes as $row) {
                $options[] = array(
                        'id_option' => $row['id_box'],
                        'name' => ((Configuration::get($this->_prefix.'BOX_DEFAULT') == $row['id_box']) ? "(default) " : "") . $row['name']. " - ". $row['length'].Configuration::get('PS_DIMENSION_UNIT')." x ".$row['width'].Configuration::get('PS_DIMENSION_UNIT')." x ".$row['height'] . Configuration::get('PS_DIMENSION_UNIT')." - " . $row['weight'] . Configuration::get('PS_WEIGHT_UNIT'),
                );
            }
        }

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Your Boxes'),
					'icon' => 'icon-envelope'
				),
				'description' => $this->l('Fill out the fields to add a new Box size. Select a box from the list to Delete/Set as Default.'),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Boxes'),
						'class' => self::PREFIX_LOW.'box_select',
						'name' => self::PREFIX.'BOXES',
						'desc' => $this->l('A list of your box sizes. Select one to Delete it or Set As Default.'),
						'multiple' => true,
						'options' => array(
							'query' => $options,
							'id' => 'id_option',
   							'name' => 'name' ,
							),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Name'),
						'class' => 'fixed-width-lg',
						'name' => self::PREFIX.'BOX_NAME',
						'hint' => 'Name of your box, e.g. Small Box',
						'placeholder' => 'Small Box',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Weight'),
						'class' => 'fixed-width-sm',
						'name' => self::PREFIX.'BOX_WEIGHT',
						'prefix' => Configuration::get('PS_WEIGHT_UNIT'),
						'hint' => 'Weight of your box, e.g. 0.010',
						'placeholder' => '0.000',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Length'),
						'class' => 'fixed-width-xs',
						'name' => self::PREFIX.'BOX_L',
						'prefix' => Configuration::get('PS_DIMENSION_UNIT'),
						'hint' => 'Length of your box, e.g. 15.0',
						'placeholder' => '0.0',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Width'),
						'class' => 'fixed-width-xs',
						'name' => self::PREFIX.'BOX_W',
						'prefix' => Configuration::get('PS_DIMENSION_UNIT'),
						'hint' => 'Width of your box, e.g. 15.0',
						'placeholder' => '0.0',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Height'),
						'class' => 'fixed-width-xs',
						'name' => self::PREFIX.'BOX_H',
						'prefix' => Configuration::get('PS_DIMENSION_UNIT'),
						'hint' => 'Height of your box, e.g. 15.0',
						'placeholder' => '0.0',
					),
				),
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => $this->l('Add Box'),
						'name' => 'submitBox',
						'icon' => 'icon-plus',
					),
                    array(
                        'type' => 'submit',
                        'title' => $this->l('Update Box'),
                        'name' => 'submitBoxUpdate',
                        'icon' => 'icon-refresh',
                    ),
					array(
						'type' => 'submit',
						'title' => $this->l('Delete Box'),
						'name' => 'submitBoxDelete',
						'icon' => 'icon-trash-o',
					),
					array(
						'type' => 'submit',
						'title' => $this->l('Set As Default'),
						'name' => 'submitBoxDefault',
					),
				),
			),
		);

		/* Add buttons for 1.5 */
		if (version_compare(_PS_VERSION_, '1.6', '<')) {
			$count = count($fields_form['form']['input']);
			for ($i = 0; $i <= $count; $i++) {
					unset($fields_form['form']['input'][$i]['hint']);
			}
			$fields_form['form']['input'][] = array(
				'type' => 'cp_buttons',
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => $this->l('Add Box'),
						'name' => 'submitBox',
					),
                    array(
                        'type' => 'submit',
                        'title' => $this->l('Update Box'),
                        'name' => 'submitBoxUpdate',
                        'icon' => 'icon-refresh',
                    ),
					array(
						'type' => 'submit',
						'title' => $this->l('Delete Box'),
						'name' => 'submitBoxDelete',
					),
					array(
						'type' => 'submit',
						'title' => $this->l('Set As Default'),
						'name' => 'submitBoxDefault',
					),
				),
			);
			/* If a default box is set, show unset button */
			if (Configuration::get(self::PREFIX.'BOX_DEFAULT')) {
				$fields_form['form']['input'][] = array(
					'type' => 'cp_buttons',
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => $this->l('Unset As Default'),
							'name' => 'submitBoxUnsetDefault',
						),
					),
				);
			}
		}

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}


	/* Values for configuration form */
	public function getConfigFieldsValues()
	{
		$values = array(
			self::PREFIX.'MODE' => Tools::getValue(self::PREFIX.'MODE', Configuration::get(self::PREFIX.'MODE')),
			self::PREFIX.'SAVE_INFO' => Tools::getValue(self::PREFIX.'SAVE_INFO', Configuration::get(self::PREFIX.'SAVE_INFO')),
			self::PREFIX.'SIGNATURE' => Tools::getValue(self::PREFIX.'SIGNATURE', Configuration::get(self::PREFIX.'SIGNATURE')),
			self::PREFIX.'ADDWEIGHT' => Tools::getValue(self::PREFIX.'ADDWEIGHT', Configuration::get(self::PREFIX.'ADDWEIGHT')),
			self::PREFIX.'DEV_API_USER' => Tools::getValue(self::PREFIX.'DEV_API_USER', Configuration::get(self::PREFIX.'DEV_API_USER')),
			self::PREFIX.'DEV_API_PASS' => Tools::getValue(self::PREFIX.'DEV_API_PASS', Configuration::get(self::PREFIX.'DEV_API_PASS')),

			self::PREFIX.'PROVINCE' => Tools::getValue(self::PREFIX.'PROVINCE', Configuration::get(self::PREFIX.'PROVINCE')),
			self::PREFIX.'CITY' => Tools::getValue(self::PREFIX.'CITY', Configuration::get(self::PREFIX.'CITY')),
			self::PREFIX.'ADDRESS1' => Tools::getValue(self::PREFIX.'ADDRESS1', Configuration::get(self::PREFIX.'ADDRESS1')),
			self::PREFIX.'ADDRESS2' => Tools::getValue(self::PREFIX.'ADDRESS2', Configuration::get(self::PREFIX.'ADDRESS2')),
			self::PREFIX.'POSTAL_CODE' => Tools::getValue(self::PREFIX.'POSTAL_CODE', Configuration::get(self::PREFIX.'POSTAL_CODE')),
			self::PREFIX.'COMPANY' => Tools::getValue(self::PREFIX.'COMPANY', Configuration::get(self::PREFIX.'COMPANY')),
			self::PREFIX.'PHONE' => Tools::getValue(self::PREFIX.'PHONE', Configuration::get(self::PREFIX.'PHONE')),

			self::PREFIX.'BOX_DEFAULT' => Tools::getValue(self::PREFIX.'BOX_DEFAULT', Configuration::get(self::PREFIX.'BOX_DEFAULT')),

			self::PREFIX.'CUSTOMER_NUMBER' => Tools::getValue(self::PREFIX.'CUSTOMER_NUMBER', Configuration::get(self::PREFIX.'CUSTOMER_NUMBER')),
			self::PREFIX.'CONTRACT' => Tools::getValue(self::PREFIX.'CONTRACT', Configuration::get(self::PREFIX.'CONTRACT')),
			self::PREFIX.'PROD_API_USER' => Tools::getValue(self::PREFIX.'PROD_API_USER', Configuration::get(self::PREFIX.'PROD_API_USER')),
			self::PREFIX.'PROD_API_PASS' => Tools::getValue(self::PREFIX.'PROD_API_PASS', Configuration::get(self::PREFIX.'PROD_API_PASS')),
			self::PREFIX.'PLATFORM_ID' => Tools::getValue(self::PREFIX.'PLATFORM_ID', Configuration::get(self::PREFIX.'PLATFORM_ID')),
			self::PREFIX.'DOWNLOAD_ID' => Tools::getValue(self::PREFIX.'DOWNLOAD_ID', Configuration::get(self::PREFIX.'DOWNLOAD_ID')),
			self::PREFIX.'TOKEN' => Tools::getValue(self::PREFIX.'TOKEN', Configuration::get(self::PREFIX.'TOKEN')),
			self::PREFIX.'TOKEN_TIME' => Tools::getValue(self::PREFIX.'TOKEN_TIME', Configuration::get(self::PREFIX.'TOKEN_TIME')),
			self::PREFIX.'TOKEN_REQUEST' => Tools::getValue(self::PREFIX.'TOKEN_REQUEST', Configuration::get(self::PREFIX.'TOKEN_REQUEST')),
			self::PREFIX.'VS' => Tools::getValue(self::PREFIX.'VS', Configuration::get(self::PREFIX.'VS')),
			self::PREFIX.'VE' => Tools::getValue(self::PREFIX.'VE', Configuration::get(self::PREFIX.'VE')),

			self::PREFIX.'RATE_SIGNATURE' => Tools::getValue(self::PREFIX.'RATE_SIGNATURE', Configuration::get(self::PREFIX.'RATE_SIGNATURE')),
			self::PREFIX.'SPLIT' => Tools::getValue(self::PREFIX.'SPLIT', Configuration::get(self::PREFIX.'SPLIT')),
			self::PREFIX.'DELAY' => Tools::getValue(self::PREFIX.'DELAY', Configuration::get(self::PREFIX.'DELAY')),
			self::PREFIX.'CARRIER_IMAGE' => Tools::getValue(self::PREFIX.'CARRIER_IMAGE', Configuration::get(self::PREFIX.'CARRIER_IMAGE')),
			self::PREFIX.'DEFAULT_WEIGHT' => Tools::getValue(self::PREFIX.'DEFAULT_WEIGHT', Configuration::get(self::PREFIX.'DEFAULT_WEIGHT')),

			self::PREFIX.'BOXES' => Tools::getValue(self::PREFIX.'BOXES'),
			self::PREFIX.'BOX_NAME' => '',
			self::PREFIX.'BOX_L' => '',
			self::PREFIX.'BOX_W' => '',
			self::PREFIX.'BOX_H' => '',
			self::PREFIX.'BOX_WEIGHT' => '',
		);

		if (Configuration::get(self::PREFIX.'BOX_DEFAULT'))
			$values[self::PREFIX.'BOXES'] = Tools::getValue(self::PREFIX.'BOX_DEFAULT', Configuration::get(self::PREFIX.'BOX_DEFAULT'));

		if ($methods = $this->getMethods()) {
			foreach ($methods as $m) {
				$values[self::PREFIX.'METHODS_'.$m['code']] = Tools::getValue(self::PREFIX.'METHODS_'.$m['code'], $m['active']);
			}
		}

		return $values;
	}

	/* Return array of boxes with dimensions */
	public function getBoxes() {
		$sql = 'SELECT * FROM '._DB_PREFIX_.self::PREFIX_LOW.'boxes';
		if ($results = Db::getInstance()->ExecuteS($sql))
		    return $results;
		else
			return false;
	}
	/* Return one box with dimensions */
	public function getBox($id) {
		$sql = 'SELECT * FROM '._DB_PREFIX_.self::PREFIX_LOW.'boxes WHERE `id_box` = '.$id;
		if ($results = Db::getInstance()->ExecuteS($sql))
		    return $results[0];
		else
			return false;
	}

	/*
		Front Office Rates
	*/

	/* Display Rates Prerences */
	private function renderRates()
	{
		version_compare(_PS_VERSION_, '1.6', '<') ? $switch = "radio" : $switch = "switch";

		$dom = array();
		$usa = array();
		$int = array();

		foreach ($this->shipping_methods['DOM'] as $k => $v) {
			$dom[] = array(
				'id_option' => $k,
				'name' => $v,
			);
		}
		foreach ($this->shipping_methods['USA'] as $k => $v) {
			$usa[] = array(
				'id_option' => $k,
				'name' => $v,
			);
		}
		foreach ($this->shipping_methods['INT'] as $k => $v) {
			$int[] = array(
				'id_option' => $k,
				'name' => $v,
			);
		}

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Rates Preferences'),
					'icon' => 'icon-cogs',
				),
				'input' => array(
					array(
						'type' => 'checkbox',
						'label' => $this->l('Canada Methods'),
						'name' => self::PREFIX.'METHODS',
						'values' => array(
							'query' => $dom,
							'id' => 'id_option',
							'name' => 'name',
						),
					),
					array(
						'type' => 'checkbox',
						'label' => $this->l('USA Methods'),
						'name' => self::PREFIX.'METHODS',
						'values' => array(
							'query' => $usa,
							'id' => 'id_option',
							'name' => 'name',
						),
					),
					array(
						'type' => 'checkbox',
						'label' => $this->l('International Methods'),
						'name' => self::PREFIX.'METHODS',
						'values' => array(
							'query' => $int,
							'id' => 'id_option',
							'name' => 'name',
						),
					),
					array(
						'type' => $switch,
						'label' => $this->l('Add Signature cost to checkout rates'),
						'desc' => $this->l('If set to Yes, checkout rates will be calculated with $1.50 added for signature fee. Some methods, like Priority, already include this fee and will be unaffected by this setting.'),
						'name' => self::PREFIX.'RATE_SIGNATURE',
						'class' => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => $switch,
						'label' => $this->l('Show Carrier Logo'),
						'desc' => $this->l('Display the Canada Post logo next to each Canada Post carrier on the cart page.'),
						'name' => self::PREFIX.'CARRIER_IMAGE',
						'class' => 't',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('No')
							)
						),
					),
                    array(
                        'type' => $switch,
                        'label' => $this->l('Split products into separate shipments'),
                        'desc' => $this->l('Splits the products into multiple shipments if the dimensions of the ordered products exceed the dimensions of the module\'s boxes. The rate will be multipled by how many of your largest box it takes to fit all the products. The weight limit per parcel is 30kg and if the products still exceed the weight after splitting, the module will split the products into even more boxes.'),
                        'name' => $this->_prefix.'SPLIT',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
					array(
						'type' => 'text',
						'label' => $this->l('Default Weight'),
						'desc' => $this->l('This weight will be used to calculate rates at checkout if your products and boxes do not have their weights set. If no default weight, product weight and box weight is set, the rates will not show up.'),
						'class' => 'fixed-width-xs',
						'name' => self::PREFIX.'DEFAULT_WEIGHT',
						'prefix' => Configuration::get('PS_WEIGHT_UNIT'),
						'hint' => $this->l('Format: 0.000'),
						'placeholder' => '0.000',
					),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Delivery Delay'),
                        'desc' => $this->l('Amount of days to add to the delivery time estimate. Leave as 0 to use the value returned by Canada Post.'),
                        'class' => 'fixed-width-xs',
                        'name' => $this->_prefix.'DELAY',
                        'placeholder' => '0',
                    ),
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'name' => 'submitUpdateMethods',
				),
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}
	/*
		When a user edits a carrier in the back-office,
		the carrier gets deleted and a new one replaces it with a new ID.
		Make sure that our DB gets updated with the new ID.
	*/
	public function hookUpdateCarrier($params)
	{
		if ((int)($params['id_carrier']) != (int)($params['carrier']->id))
		{
			$methods = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.self::PREFIX_LOW.'methods` WHERE `id_carrier` = '.(int)$params['id_carrier']);

			$where = '`id_carrier` = '.(int)$params['id_carrier'];
			$history = (int)$methods['id_carrier_history'].'|'.(int)$params['carrier']->id;
			Db::getInstance()->update(self::PREFIX_LOW.'methods', array('id_carrier' => (int)$params['carrier']->id, 'id_carrier_history' => $history), $where);
		}
	}

	/* When user updates cart, deleted old rates information */
	public function hookActionCartSave($params)
	{
		$this->rate = false;
		$this->context->cookie->unsetFamily('cp_rate');
		$this->context->cookie->__unset('cp_error');
	}

	/* Change carrier delay messages and display error before carrier list */
	public function hookDisplayBeforeCarrier($params)
	{
		/* Show rate error if any */
		if ($this->context->cookie->exists('cp_error') && $this->context->cookie->cp_error != false) {
			$this->context->smarty->assign(array(
				'cp_error' => $this->context->cookie->cp_error,
				));
		}

		if ((!$this->context->cookie->exists('cp_error') || $this->context->cookie->cp_error == false) && array_key_exists('delivery_option_list', $params))
		{
			$delivery 		= new Address($this->context->cart->id_address_delivery);
			$country 		= new Country($delivery->id_country);

			if (!Validate::isLoadedObject($country))
				return false;

			/* Declare which group the customer belongs to (Domestic-Canada, USA or International) */
			if ($country->iso_code == 'CA')
				$group = 'DOM';
			elseif ($country->iso_code == 'US')
				$group = 'USA';
			elseif ($country->iso_code != 'CA' && $country->iso_code != 'US')
				$group = 'INT';

			if ($methods = $this->getMethods('`group` = "'.pSQL($group).'" AND `active` = 1')) {

				/* Array cp_rate_CODE => (int)delay */
				$delays = $this->context->cookie->getFamily('cp_delay');
				/* Store active service codes in array */
				$codes = array();
				foreach ($methods as $m)
				{
					if (array_key_exists('cp_delay_'.$m['code'], $delays))
					{
						$codes[$m['id_carrier']] = array(
							'code' => $m['code'],
							'delay' => $delays['cp_delay_'.$m['code']],
							);
					}
				}

				/* Replace delay message with message from CP */
				$option_list = $params['delivery_option_list'];
				foreach($option_list as $id_address => $carrier_list_raw)
				{
					foreach($carrier_list_raw as $key => $carrier_list)
					{
						foreach($carrier_list['carrier_list'] as $id_carrier => $carrier)
						{
							if(array_key_exists($id_carrier, $codes))
							{
								$delay = &$option_list[$id_address][$key]['carrier_list'][$id_carrier]['instance']->delay;
								$days = $codes[$id_carrier]['delay'];

								if ($days != 0)
									foreach ($delay as $k => $v)
										$delay[$k] = $days.' '.($days > 1 ? $this->l('Business days.') : $this->l('Business day.'));
							}
							else
							{
								continue;
							}
						}
					}
				}
				$this->context->smarty->assign('delivery_option_list', $option_list);
			}
		}

		return $this->display(__FILE__, 'carrier_error.tpl');
	}

	/* Return array of boxes with dimensions */
	public function getMethods($where = false) {

		if (!$where)
			$sql = 'SELECT * FROM '._DB_PREFIX_.self::PREFIX_LOW.'methods';
		else
			$sql = 'SELECT * FROM '._DB_PREFIX_.self::PREFIX_LOW.'methods WHERE '.$where;

		if ($results = Db::getInstance()->ExecuteS($sql))
		    return $results;
		else
			return false;
	}

	/* Register a carrier in the back-office */
	public function installCarrier($m)
	{
		$config = array(
			'name' => 'Canada Post ('.$m['name'].')',
			'id_tax_rules_group' => 0,
			'active' => true,
			'url' => 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=@',
			'deleted' => 0,
			'shipping_handling' => false,
			'range_behavior' => 0,
			'delay' => array('fr' => $m['name'], 'en' => $m['name'], Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')) => $m['name']),
			'id_zone' => 1,
			'is_module' => true,
			'shipping_external' => true,
			'external_module_name' => $this->name,
			'need_range' => true
		);
		$id_carrier = $this->installExternalCarrier($config);
		$where = '`code` = "'.pSQL($m['code']).'"';
		Db::getInstance()->update(self::PREFIX_LOW.'methods', array('id_carrier' => (int)$id_carrier, 'id_carrier_history' => (int)$id_carrier), $where);
	}

	/* Register a carrier in the back-office */
	public static function installExternalCarrier($config)
	{
		$carrier = new Carrier();
		$carrier->name = $config['name'];
		$carrier->id_tax_rules_group = $config['id_tax_rules_group'];
		$carrier->id_zone = $config['id_zone'];
		$carrier->active = $config['active'];
		$carrier->url = $config['url'];
		$carrier->deleted = $config['deleted'];
		$carrier->delay = $config['delay'];
		$carrier->shipping_handling = $config['shipping_handling'];
		$carrier->range_behavior = $config['range_behavior'];
		$carrier->is_module = $config['is_module'];
		$carrier->shipping_external = $config['shipping_external'];
		$carrier->external_module_name = $config['external_module_name'];
		$carrier->need_range = $config['need_range'];

		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
		{
			if ($language['iso_code'] == 'fr')
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
			if ($language['iso_code'] == 'en')
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
			if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
				$carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
		}

		if ($carrier->add())
		{
			$groups = Group::getGroups(true);
			foreach ($groups as $group)
				Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])), 'INSERT');

			$rangePrice = new RangePrice();
			$rangePrice->id_carrier = $carrier->id;
			$rangePrice->delimiter1 = '0';
			$rangePrice->delimiter2 = '10000';
			$rangePrice->add();

			$rangeWeight = new RangeWeight();
			$rangeWeight->id_carrier = $carrier->id;
			$rangeWeight->delimiter1 = '0';
			$rangeWeight->delimiter2 = '10000';
			$rangeWeight->add();

			$zones = Zone::getZones(true);
			foreach ($zones as $zone)
			{
				Db::getInstance()->autoExecute(_DB_PREFIX_.'carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])), 'INSERT');
				Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
				Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
			}

			/* Copy Logo */
			if (Configuration::get(self::PREFIX.'CARRIER_IMAGE'))
				if (!Tools::copy(dirname(__FILE__).'/views/img/cp_logo.png', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
					return false;

			// Return ID Carrier
			return (int)($carrier->id);
		}

		return false;
	}

	/* Set all installed Canada Post carriers to deleted */
	public function uninstallCarriers()
	{
		if ($methods = $this->getMethods()) {
			foreach ($methods as $m) {
				if ($m['id_carrier']) {
					$carrier = new Carrier($m['id_carrier']);
					$carrier->deleted = 1;
					$carrier->update();
				}
			}
		}
		return true;
	}

	/* Send rate request to Canada Post and return a rate for all shipping methods ($codes) at checkout */
	public function getRates($params, $codes, $products)
	{
		$cp = new CprApi();
		$address 		= new Address($params->id_address_delivery);
		if (!Validate::isLoadedObject($address))
		{
			// If address is not loaded, we take data from shipping estimator module (if installed)
			$address->id_country = $this->context->cookie->id_country;
			$address->id_state = $this->context->cookie->id_state;
			$address->postcode = $this->context->cookie->postcode;
		}

		$country 		= new Country($address->id_country);

		/* Get order volume size */
		$cube_total = 0;
		$weight = 0;
		$rate_multiple = 1;
		$packages = array();

		foreach($products as $p) {
			if (!$p['is_virtual']) {
				for ($i = 0; $i < $p['quantity']; $i++)
				{
					array_push($packages, array(
						'length' => CPTools::toCm($p['depth']),
						'width' => CPTools::toCm($p['width']),
						'height' => CPTools::toCm($p['height']),
						));
				}
				$cube =  ($p['width']*$p['height']*$p['depth']) * $p['quantity'];
				$cube_total += CPTools::toCm($cube);
				$weight += $p['weight'] * $p['quantity'];
			}
		}

		/*
			Retreive all user boxes from DB

			Store all boxes that have enough cubic space to fit the order in an array,
			Select the smallest box which has enough cubic space to fit the order.

			If none fit, select the largest box and hope for the best.
		*/
		if ($boxes = $this->getBoxes()) {
				$valid_cubes = array();
				$cubes = array();
				$best_box = false;
				foreach ($boxes as $row) {
					if (CPTools::toCm($row['cube']) >= $cube_total)
						$valid_cubes[] = CPTools::toCm($row['cube']);
					$cubes[] = CPTools::toCm($row['cube']);
				}
			    if (!empty($valid_cubes)) {
				    foreach ($boxes as $row) {
				    	if (CPTools::toCm($row['cube']) == min($valid_cubes))
				    		$best_box = $row;
					}
				}
			    if (!$best_box) { // if no suitable box
			    	foreach ($boxes as $row) {
				    	if (CPTools::toCm($row['cube']) == max($cubes))
				    		$best_box = $row;
				    }
				    if (Configuration::get(self::PREFIX.'SPLIT'))
				    	$rate_multiple = (int)ceil($cube_total / CPTools::toCm($best_box['cube']));
				}

		}
		/* Add box weight */
		$weight = number_format($weight + $best_box['weight'], 3, '.', '');
		$weight = CPTools::toKg($weight);

		// add 1 to rate multiple until the quotient of weight/multiple <= 30
		if (Configuration::get(self::PREFIX.'SPLIT')) {
			if ($weight/$rate_multiple > 30)
				for ($i = 1; $weight/$rate_multiple > 30; $i++)
					$rate_multiple++;
			$weight = number_format($weight / $rate_multiple, 3, '.', '');
		}

		/* Use default rate weight if product and box weight is empty */
		if ($weight == 0)
			$weight = CPTools::toKg(Configuration::get(self::PREFIX.'DEFAULT_WEIGHT'));

		$postcode = str_replace(" ","", $address->postcode);
		$postcode = Tools::strtoupper($postcode);

		$parcel = array(
					'customer-number' => Configuration::get(self::PREFIX.'CUSTOMER_NUMBER'),
					'parcel-characteristics' => array(
						'weight' => $weight,
						'dimensions' => array('length' => CPTools::toCm($best_box['length']),'width' => CPTools::toCm($best_box['width']),'height' => CPTools::toCm($best_box['height']))),
					'origin-postal-code' => Configuration::get(self::PREFIX.'POSTAL_CODE'),
					'destination' =>
						array('domestic' => array('postal-code' => $postcode)),
					);

		if (Configuration::get(self::PREFIX.'CONTRACT'))
			$parcel['contract-id'] = Configuration::get(self::PREFIX.'CONTRACT');

		/* Add signature option */
		if (Configuration::get(self::PREFIX.'RATE_SIGNATURE'))
			$parcel['options'] = array(array('option' => array('option-code' => 'DC')), array('option' => array('option-code' => 'SO')));
		else
			$parcel['options'] = array('option' => array('option-code' => 'DC'));

		/* Format for domestic/US/international */
		if ($country->iso_code == "CA") {
			$parcel['destination'] = array('domestic' => array('postal-code' => $postcode));
		} else if ($country->iso_code == "US") {
			$parcel['destination'] = array('united-states' => array('zip-code' => $postcode));
		} else {
			$parcel['destination'] = array('international' => array('zip-code' => $postcode));
			$parcel['destination'] = array('international' => array('postal-code' => $postcode));
			$parcel['destination'] = array('international' => array('country-code' => $country->iso_code));
			/* International does not support delivery confirmation */
			unset($parcel['options']);
		}

		foreach ($codes as $code)
			$parcel['services'][] = array('service-code' => $code);
//p($parcel);
//d($cp->getRate($parcel));
		/* Store rates and delays in cookie variable so we don't have to create it multiple times */
		if ($rates = $cp->getRate($parcel))
		{
			if (!$cp->getErrors($rates))
			{
				if ($this->rate = CPTools::putRatesInArray($rates))
				{
					foreach ($this->rate as $code => $rate)
					{
						$this->context->cookie->__set('cp_rate_'.$code, ($rate['rate']*$rate_multiple));
						if (array_key_exists('delay', $rate))
							$this->context->cookie->__set('cp_delay_'.$code, ((int)$rate['delay'] + (int)Configuration::get(self::PREFIX.'DELAY')));
						$this->context->cookie->__set('cp_error', false);
					}
					return $this->context->cookie->getFamily('cp_rate');
				}
			}
			else
			{
				$this->context->cookie->__set('cp_error', $cp->getErrors($rates));
			}
		}
		return false;
	}

	/* Show checkout rates */
	public function getPackageShippingCost($params, $shipping_cost, $products)
	{
		if ($this->context->cookie->exists('cp_error') && $this->context->cookie->cp_error != false)
			return false;

		$rate 			= $this->context->cookie->getFamily('cp_rate');
		$delivery 		= new Address($params->id_address_delivery);
		$country 		= new Country($delivery->id_country);

		if (!Validate::isLoadedObject($country))
			return false;


		/* Declare which group the customer belongs to (Domestic-Canada, USA or International) */
		$cost = 0;
		if ($country->iso_code == 'CA')
			$group = 'DOM';
		elseif ($country->iso_code == 'US')
			$group = 'USA';
		elseif ($country->iso_code != 'CA' && $country->iso_code != 'US')
			$group = 'INT';

		if ($methods = $this->getMethods('`group` = "'.$group.'" AND `active` = 1')) {
			/* Store active service codes in array */
			$codes = array();
			foreach ($methods as $m)
				$codes[] = $m['code'];

			/*
				Because the Canada Post Rates API can retrieve all method prices at once,
				we make sure that we only call the API once.
			*/
			if (count($rate) < 1)
				$rate = $this->getRates($params, $codes, $products);

			if ($rate) {
				foreach ($methods as $m) {
					if ($this->id_carrier == $m['id_carrier']) {
						/* If currency != CAD, convert the price from CAD to the cart currency */
						$conversion = CPTools::getCartCurrencyRateFromCad($params->id);
						if (array_key_exists('cp_rate_'.$m['code'], $rate))
							$cost = $rate['cp_rate_'.$m['code']] * $conversion;
						/* Add handling fees */
						if ($cost > 0 && is_numeric($cost))
						{
							$cost += $shipping_cost;
							if ($cost > 0 && is_numeric($cost))
								return $cost;
						} else {
							return false;
						}
					}
				}
			}
		}
		return false;
	}

	public function getOrderShippingCost($params, $shipping_cost)
	{
		return $this->getPackageShippingCost($params, $shipping_cost);
	}

	public function getOrderShippingCostExternal($params)
	{
		return $this->getPackageShippingCost($params, 23);
	}
}

?>
