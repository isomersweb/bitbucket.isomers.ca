<?php
 /**
 * Sample code for the GetManifestArtifactId Canada Post service.
 * 
 * The GetManifestArtifactId service is used to retrieve the artifact-id number 
 * of the artifact (label) for a given manifest. 
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

$dirpath = _PS_ROOT_DIR_ . '/modules/canadapostrates/shipping/GetManifestArtifactId/';

//$userProperties = parse_ini_file($dirpath . '/../../user.ini');

if (Configuration::get('CPR_MODE') == 0) $mode = 'DEV'; else $mode = 'PROD';
$userProperties = array(
    'username' => Configuration::get("CPR_{$mode}_API_USER"),
    'password' => Configuration::get("CPR_{$mode}_API_PASS"),
    'customerNumber' => Configuration::get('CPR_CUSTOMER_NUMBER'),
    'contractId' => Configuration::get('CPR_CONTRACT')
);

$wsdl = $dirpath . '/../../wsdl/manifest.wsdl';

if (Configuration::get('CPR_MODE') == 0)
    $hostName = 'ct.soa-gw.canadapost.ca';
else
    $hostName = 'soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/manifest/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => $dirpath . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);


$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];

	// Execute Request	
	$result = $client->__soapCall('GetManifestArtifactId', array(
	    'get-manifest-artifact-id-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'manifest-id'		=>  $_GET['manifest']
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'manifest'}) ) {
		echo 'PO Number: ' . $result->{'manifest'}->{'po-number'}  . "\n";
        echo 'Artifact Id: ' . $result->{'manifest'}->{'artifact-id'} . "\n\n";
        return $result->{'manifest'}->{'artifact-id'};
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
            $this->errors[] = Tools::displayError($message->code . " - " .$message->description);
//            d();
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
    $this->errors[] = Tools::displayError(trim($exception->faultcode) . " - " .trim($exception->getMessage()));
//    d();
}

?>

