<?php
 /**
 * Sample code for the GetShipmentDetails Canada Post service.
 * 
 * The GetShipmentDetails service is used to retrieve the shipment details in XML 
 * format for a shipment created by a prior "create shipment" call.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini

$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/shipment.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/shipment/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx, 'trace' => 1));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
	$result = $client->__soapCall('GetShipmentDetails', array(
	    'get-shipment-details-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'shipment-id'		=> '158011483210344687'
		)
	), NULL, NULL);
	
	/* Because group-id, transmit-shipment have a namespace prefix, these elements are missing from $result.
	 *
	* Get the actual respons, remove the ':', and parse.
	*
	*/
	$fullResponse = $client->__getLastResponse();
	$fullResponse = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $fullResponse);
	$xml = simplexml_load_string($fullResponse);
	$body = $xml->{'soapenvBody'}->{'tnsget-shipment-details-response'};
	
	// Parse Response
	if ( isset($result->{'shipment-details'}) ) {
		echo 'Tracking Pin: ' . $body->{'shipment-details'}->{'tracking-pin'} . "\n";
		echo 'Group Id: ' . $body->{'shipment-details'}->{'shipment-detail'}->{'tnsgroup-id'} . "\n";
		echo 'Sender Postal Code: ' . $body->{'shipment-details'}->{'shipment-detail'}->{'delivery-spec'}->{'sender'}->{'address-details'}->{'postal-zip-code'} . "\n";
		echo 'Destination Postal Code: ' . $body->{'shipment-details'}->{'shipment-detail'}->{'delivery-spec'}->{'destination'}->{'address-details'}->{'postal-zip-code'} . "\n";
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}
echo "<pre>";
print_r($result);
echo "</pre>";

?>

