<?php

/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* *****************************************************
* @category   Belvg
* @package    belvg_testimonials
* @author     Dzianis Yurevich (dzianis.yurevich@gmail.com)
* @site       http://module-presta.com
* @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
* @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*/

require_once _PS_MODULE_DIR_ . 'belvg_testimonials/includer.php';

class AdminBelvgTestimonials extends AdminController
{
    protected $position_identifier = 'id_belvg_testimonials';

    /*protected $_js = "
        <script type='text/javascript' src='../modules/belvg_testimonials/js/admin.js'></script>
    ";*/

    protected $_module = NULL;

    public function getModule()
    {
        if (is_NULL($this->_module)) {
            $this->_module = new belvg_testimonials;
        }

        return $this->_module;
    }

    protected function l($string, $class = 'AdminBelvgTestimonials', $addslashes = FALSE, $htmlentities = TRUE)
    {
        return $this->getModule()->l($string, $class, $addslashes, $htmlentities);
    }

    public function __construct()
    {
    	$this->bootstrap = true;
        $this->table = belvg_testimonials::PREFIX . 'testimonials';
        $this->identifier = 'id_belvg_testimonials';
        $this->className = 'BelvgTestimonials';
        $this->lang = TRUE;
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->fields_list = array(
            'id_belvg_testimonials' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 30),
            'name' => array('title' => $this->l('Sender Name'), 'width' => 70),
            'location' => array('title' => $this->l('Location'), 'width' => 70),
            'email' => array('title' => $this->l('Email'), 'width' => 70),
            'date_add' => array('title' => $this->l('Date Add'), 'width' => 70),
            'date_upd' => array('title' => $this->l('Date Last Update'), 'width' => 50),
            'status' => array(
                'title' => $this->l('Status'),
                'width' => 25,
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => FALSE,
                ));

        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_ALL) {
            $this->_where .= ' AND a.' . $this->identifier . ' IN (
                SELECT sa.' . $this->identifier . '
                FROM `' . _DB_PREFIX_ . $this->table . '_shop` sa
                WHERE sa.id_shop IN (' . implode(', ', Shop::
                getContextListShopID()) . ')
            )';
        }

        $this->identifiersDnd = array('id_belvg_testimonials' => 'id_sslide_to_move');

        parent::__construct();
    }

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();
        if (!$obj = $this->loadObject(TRUE)) {
            return;
        }

        $this->fields_form = array(
            'tinymce' => TRUE,
            'legend' => array('title' => $this->l('Testimonials'), 'image' =>
                    '../img/admin/tab-categories.gif'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Sender Name:'),
                    'name' => 'name',
                    'id' => 'name',
                    'required' => TRUE,
                    'size' => 50,
                    'maxlength' => 50,
                    ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Email:'),
                    'name' => 'email',
                    'id' => 'email',
                    'required' => TRUE,
                    'size' => 50,
                    'maxlength' => 50,
                    ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Site:'),
                    'name' => 'site',
                    'id' => 'site',
                    'size' => 50,
                    'maxlength' => 50,
                    ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Location:'),
                    'name' => 'location',
                    'id' => 'location',
                    'size' => 50,
                    'maxlength' => 50,
                    'lang' => TRUE,
                    'required' => TRUE
                    ),
                array(
                    'type' => 'date',
                    'label' => $this->l('Date:'),
                    'name' => 'date_add',
                    'id' => 'date_add',
                    'size' => 50,
                    'maxlength' => 50,
                    'required' => TRUE
                    ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Status:'),
                    'name' => 'status',
                    'required' => FALSE,
                    'class' => 't',
                    'is_bool' => TRUE,
                    'values' => array(array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')), array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'))),
                    ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Message:'),
                    'name' => 'message',
                    'id' => 'message',
                    'required' => TRUE,
                    'lang' => TRUE,
                    'autoload_rte' => TRUE
                    ),
            ),
            'submit' => array('title' => $this->l('   Save   '), 'class' => 'button'));

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
                );
        }

        return parent::renderForm();// . $this->_js;
    }

    public function validateRules($class_name = FALSE)
    {
      
    }

    protected function updateAssoShop($id_object)
    {
        
    }

    protected function getSelectedAssoShop($table)
    {
        
    }
}
