<?php

/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* *****************************************************
* @category   Belvg
* @package    belvg_testimonials
* @author     Dzianis Yurevich (dzianis.yurevich@gmail.com)
* @site       http://module-presta.com
* @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
* @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*/

if (!class_exists('belvg_testimonials')) {
    return;
}

$prefix = belvg_testimonials::getDbPrefix();
return array(
    '
    CREATE TABLE IF NOT EXISTS `' . $prefix . 'testimonials` (
      `id_belvg_testimonials` int(10) unsigned NOT NULL auto_increment,
      `name` varchar(255) NOT NULL,
      `email` varchar(255) NOT NULL,
      `site` varchar(255) NOT NULL,
      `status` int(10) NOT NULL default "0",
      `date_add` datetime NOT NULL,
      `date_upd` datetime NOT NULL,
      PRIMARY KEY  (`id_belvg_testimonials`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
    '
    CREATE TABLE IF NOT EXISTS `' . $prefix . 'testimonials_shop` (
      `id_belvg_testimonials` int(10) unsigned NOT NULL auto_increment,
      `id_shop` int(10) unsigned NOT NULL,
      PRIMARY KEY (`id_belvg_testimonials`, `id_shop`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8',
    '
    CREATE TABLE IF NOT EXISTS `' . $prefix . 'testimonials_lang` (
      `id_belvg_testimonials` int(10) unsigned NOT NULL,
      `id_lang` int(10) unsigned NOT NULL,
      `message` text NOT NULL,
      `location` text NOT NULL,
      PRIMARY KEY (`id_belvg_testimonials`,`id_lang`)
    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8');
