(function($){
	$(document).ready(function(){
		$('.belvg_topproduct .product_image img').hover(
			function(){
				$(this).parent().parent().find('.product_desc').show();
			},
			function(){
				$(this).parent().parent().find('.product_desc').hide();
			}
		);
		$('.belvg_topproduct .product_desc').hover(
			function(){
				$(this).show();
			},
			function(){
				$(this).hide();
			}
		);
	});
})(jQuery);