<form action="{Tools::safeOutput($smarty.server.REQUEST_URI)}" method="post" >
	<h2>{l s='Belvg Testimonials' mod='belvg_testimonials'}</h2>
	{$belvg_output}
	<fieldset>
		<label>{l s='Count of the testimonials in the block' mod='belvg_testimonials'}</label>
		<input style="width:48px;" maxlength="3" type="text" name="{$belvg_testimonials->name}[count]" value="{$belvg_testimonials->getConfig('count')}">
		<br class="clear"><br class="clear">
		<label>{l s='Testimonials block column' mod='belvg_testimonials'}</label>
		<select name="{$belvg_testimonials->name}[column]">
			<option {if $belvg_testimonials->getConfig('column') == 'left'}selected="selected"{/if} value="left">{l s='Left' mod='belvg_testimonials'}</option>
			<option {if $belvg_testimonials->getConfig('column') == 'right'}selected="selected"{/if} value="right">{l s='Right' mod='belvg_testimonials'}</option>
		</select>
		<br class="clear"><br class="clear">
		<br>
	</fieldset>
	<br>
	<input class="button" type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;{l s='Save' mod='belvg_testimonials'}&nbsp;&nbsp;&nbsp;&nbsp;">
</form>