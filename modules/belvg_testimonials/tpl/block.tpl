<div id="belvg_testimonials" class="block blocksupplier">
	<p class="title_block">
		<a href="{$link->getModuleLink('belvg_testimonials', 'view', [])}" title="{l s='Testimonials' mod='belvg_testimonials'}">
			{l s='Testimonials' mod='belvg_testimonials'}
		</a>
	</p>
	<div class="block_content">
		<ul class="bullet">
		{foreach from=$belvg_testimonials item=testimonial name=testimonials}
			<li class="{if $smarty.foreach.testimonials.first}first_item{elseif $smarty.foreach.testimonials.last}last_item{else}item{/if}">
				<p>{$testimonial->message|strip_tags|truncate:250:'...'}</p>
				<div class="author">{$testimonial->name}, {$testimonial->location}</div>
			</li>
		{/foreach}
		</ul>
	</div>
</div>