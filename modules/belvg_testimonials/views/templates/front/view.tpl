{*{if $testimonials}
	<div class="content_sortPagiBar">
		<div class="sortPagiBar clearfix">
			{include file="$testimonials_tpl_dir/nbr-page.tpl"}

		</div>
	</div>
{/if}*}
{include file="$testimonials_tpl_dir/new-testimonials-form.tpl"}
{include file="$testimonials_tpl_dir/testimonials-list.tpl" testimonials=$testimonials}
    
{if $testimonials}
		{include file="$testimonials_tpl_dir/pagination.tpl"}
	{else}
	<p class="warning">{l s='There are no testimonials.' mod='belvg_testimonials'}</p>
{/if}    

{literal}
<script>
//when document is loaded...
$(document).ready(function(){
	{/literal}
	var str1 = "{l s='Add new testimonials' mod='belvg_testimonials'}";
	var str2 = "{l s='Close form' mod='belvg_testimonials'}";
	{literal}
    $("#belvg_testimonials_nql").click(function(){
        $('#belvg_testimonials_cnq').stop().toggle(0, function() {
            if ($("#belvg_testimonials_nql").text() == str1) {
                $("#belvg_testimonials_nql").text(str2);
            } else {
                $("#belvg_testimonials_nql").text(str1);
            }
        });
        return false;
    });
})
</script>
{/literal}
