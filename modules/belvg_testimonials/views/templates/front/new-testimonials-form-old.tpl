<div class="new_question_link btn black btn-add" id="belvg_testimonials_nql" {if isset($submit_error)}style="display: none;"{/if}>{l s='Add new testimonials' mod='belvg_testimonials'}</div>

<div class="belvg_testimonials_err message {if isset($submit_error)} error alert alert-danger {/if} {if isset($submit_success)} alert alert-success {/if}">
    {if isset($submit_success)} {l s='Thank you, testimonials will display after moderation' mod='belvg_testimonials'} {/if}
    {if isset($submit_error)} {l s='Something went wrong, please check your data and try again' mod='belvg_testimonials'} {/if}
    {if isset($submit_error) && !empty($validate)}
        <ul class="validate">
        {foreach from=$validate item=issue}
            <li>{$issue}</li>
        {/foreach}
        </ul>
    {/if}
</div>

<div class="content_new_question " id="belvg_testimonials_cnq" {if isset($submit_error)}style="display: block;"{/if}>
    <form id="testimonialsNewForm" action="{$request|escape:'htmlall':'UTF-8'}" method="post" class="ajax_form">
        <div class="row">
        <div class="form-group">
            <label for="name">{l s='Your name:' mod='belvg_testimonials'}*</label>
            <input class="form-control" name="name" id="name" type="text" value="{if isset($smarty.post.name)}{$smarty.post.name|strip_tags|escape:'htmlall':'UTF-8'|stripslashes}{else}{$customerName}{/if}">
        </div>                          
        <div class="form-group">
            <label for="email">{l s='Your email:' mod='belvg_testimonials'}*</label>
            <input class="form-control" name="email" id="email" type="text" value="{if isset($smarty.post.email)}{$smarty.post.email|strip_tags|escape:'htmlall':'UTF-8'|stripslashes}{else}{$customerEmail}{/if}">
        </div>                          
        <div class="form-group">
            <label for="location">{l s='Your location:' mod='belvg_testimonials'}*</label>
            <input class="form-control" name="location" id="location" type="text" value="{if isset($smarty.post.location)}{$smarty.post.location|strip_tags|escape:'htmlall':'UTF-8'|stripslashes}{/if}">
        </div> 
        <div class="form-group">
            <label for="location">{l s='Testimonials:' mod='belvg_testimonials'}*</label>
            <textarea class="form-control" name="message" id="message" cols="40" rows="10">{if isset($smarty.post.message)}{$smarty.post.message|strip_tags|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
        </div>    
        </div>    
        <div class="form-group">
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <div class="g-recaptcha" data-sitekey="6LdLLw0TAAAAAFxlLaCJXwofqf7h86R4QMScbEuH"></div>                 
        </div>        
        <div class="form-group">
            <input type="submit" name="submit_new_question" class="btn black btn-submit" id="submitMessage" value="{l s='Send' mod='belvg_testimonials'}" />
        </div>        
    </form>
</div>
