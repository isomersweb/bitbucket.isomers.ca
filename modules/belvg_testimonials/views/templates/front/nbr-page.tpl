{if isset($p) AND $p}
	<!-- nbr product/page -->
	{if $nb_products > $products_per_page}
		<form action="{$requestPage|escape:'htmlall':'UTF-8'}" method="get" class="nbrItemPage pagination">
            <input type="hidden" name="fc" value="module" />
            <input type="hidden" name="module" value="belvg_testimonials" />
            <input type="hidden" name="controller" value="view" />
			<p>
				{if isset($search_query) AND $search_query}<input type="hidden" name="search_query" value="{$search_query|escape:'htmlall':'UTF-8'}" />{/if}
				{if isset($tag) AND $tag AND !is_array($tag)}<input type="hidden" name="tag" value="{$tag|escape:'htmlall':'UTF-8'}" />{/if}
				<label for="nb_item">{l s='Show' mod='belvg_testimonials'}</label>
				{if isset($requestNb) && is_array($requestNb)}
					{foreach from=$requestNb item=requestValue key=requestKey}
						{if $requestKey != 'requestUrl'}
							<input type="hidden" name="{$requestKey|escape:'htmlall':'UTF-8'}" value="{$requestValue|escape:'htmlall':'UTF-8'}" />
						{/if}
					{/foreach}
				{/if}
				<select name="n" id="nb_item" onchange="$(this).parents('form:first').submit();">
				{assign var="lastnValue" value="0"}
				{foreach from=$nArray item=nValue}
					{if $lastnValue <= $nb_products}
						<option value="{$nValue|escape:'htmlall':'UTF-8'}" {if $n == $nValue}selected="selected"{/if}>{$nValue|escape:'htmlall':'UTF-8'}</option>
					{/if}
					{assign var="lastnValue" value=$nValue}
				{/foreach}
				</select>
				<span>{l s='Testimonials by page' mod='belvg_testimonials'}</span>
			</p>
		</form>
	{/if}
	<!-- /nbr product/page -->
{/if}
