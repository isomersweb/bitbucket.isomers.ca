^XA
^CI13
^MMT
^PW812
^LL1218
^LS0
^FO0,15^A0N,23,18^FB812.0,,,C^FDCOMMERCIAL INVOICE / CN23    FACTURE COMMERCIALE / CN23^FS
^FO22,60^FT22,60^A0N,18,14^FH|^FDSender / Exp|82diteur^FS
^FO0,61^A0N,18,14^FB426.30002,,,R^FD416 787 2465^FS
^FO22,91^FT22,91^A0N,18,14^TBN,416,18^FDIsomers^FS
^FO22,109^FT22,109^A0N,18,14^TBN,416,18^FD105 Tycos Drive^FS
^FO22,127^FT22,127^A0N,18,14^TBN,416,18^FDToronto ON M6B 1W3^FS
^FO22,145^FT22,145^A0N,18,14^TBN,416,18^FDCANADA^FS
^FO22,172^FT22,172^A0N,18,14^FDAddressee / Destinataire^FS
^FO0,172^A0N,18,14^FB426.30002,,,R^FD416-787-2465^FS
^FO22,203^FT22,203^A0N,18,14^TBN,416,18^FD^FS
^FO22,221^FT22,221^A0N,18,14^TBN,416,18^FDFRIEND SEVEN^FS
^FO22,239^FT22,239^A0N,18,14^TBN,416,18^FD8035 PENNSYLVANIA ROAD^FS
^FO22,257^FT22,257^A0N,18,14^TBN,416,18^FDBLOOMINGTON MN 55438^FS
^FO22,275^FT22,275^A0N,18,14^TBN,416,18^FDUNITED STATES^FS
^FO22,293^FT22,293^A0N,18,14^TBN,416,18^FD^FS
^FO22,314^FT22,314^A0N,18,14^FDTax Id / Enrg.^FS
^FO123,314^FT123,314^A0N,18,14^FD^FS
^FO438,60^FT438,60^A0N,18,14^FDItem ID^FS
^FO438,78^FT438,78^A0N,18,14^FH|^FDN d'article^FS
^FO0,50^A0N,18,14^FB789.67,,,R^FDEM 070 774 974 CA^FS
^FO438,101^FT438,101^A0N,18,14^FDDate^FS
^FO0,91^A0N,18,14^FB789.67,,,R^FD2017 05 10^FS
^FO438,131^FT438,131^A0N,18,14^FDReference No.^FS
^FO438,149^FT438,149^A0N,18,14^FH|^FDN de r|82f|82rence^FS
^FO789.67,121,1^A0N,18,14^FB789.67,,,R^TBN,203,18^FDORDER ID: 162^FS
^FO438,172^FT438,172^A0N,18,14^FDReason for Export^FS
^FO438,190^FT438,190^A0N,18,14^FDRaison de l'exportation^FS
^FO789.67,162,1^A0N,18,14^FB789.67,,,R^TBN,203,18^FDGIFT^FS
^FO0,178^A0N,18,14^FB789.67,,,R^FD^FS
^FO0,195^A0N,18,14^FB789.67,,,R^FD^FS
^FO438,233^FT438,233^A0N,18,14^FDGross Weight^FS
^FO438,251^FT438,251^A0N,18,14^FDPoids brut^FS
^FO0,223^A0N,18,14^FB789.67,,,R^FD0.135 kg^FS
^FO22,345^GB761,508,1^FS
^FO22,385^GB757,,1^FS
^FO22,811^GB757,,1^FS
^FO89,345^GB,467,0^FS
^FO323,345^GB,467,0^FS
^FO444,345^GB,467,0^FS
^FO560,345^GB,467,0^FS
^FO672,345^GB,467,0^FS
^FO24,365^FT24,365^A0N,15,12^FDQuantity^FS
^FO24,380^FT24,380^A0N,15,12^FH|^FDQuantit|82^FS
^FO133,365^FT133,365^A0N,15,12^FDDescription of Contents^FS
^FO133,380^FT133,380^A0N,15,12^FDDescription du contenu^FS
^FO336,365^FT336,365^A0N,15,12^FDHS Tariff Code ^FS
^FO336,380^FT336,380^A0N,15,12^FDCode tarif SH^FS
^FO448,365^FT448,365^A0N,15,12^FDCountry of Origin^FS
^FO458,379^FT458,379^A0N,15,12^FDPays d'origine^FS
^FO566,365^FT566,365^A0N,15,12^FDNet Weight (kg)^FS
^FO574,379^FT574,379^A0N,15,12^FDPoids net (kg)^FS
^FO682,365^FT682,365^A0N,15,12^FDTotal Value^FS
^FO682,380^FT682,380^A0N,15,12^FDValeur totale^FS
^FO0,394^A0N,18,14^FB87,,,R^FD1^FS
^FO0,412^A0N,18,14^FB87,,,R^FD^FS
^FO0,430^A0N,18,14^FB87,,,R^FD^FS
^FO0,448^A0N,18,14^FB87,,,R^FD^FS
^FO0,466^A0N,18,14^FB87,,,R^FD^FS
^FO0,484^A0N,18,14^FB87,,,R^FD^FS
^FO0,502^A0N,18,14^FB87,,,R^FD^FS
^FO0,520^A0N,18,14^FB87,,,R^FD^FS
^FO0,538^A0N,18,14^FB87,,,R^FD^FS
^FO0,556^A0N,18,14^FB87,,,R^FD^FS
^FO0,574^A0N,18,14^FB87,,,R^FD^FS
^FO0,592^A0N,18,14^FB87,,,R^FD^FS
^FO0,610^A0N,18,14^FB87,,,R^FD^FS
^FO0,628^A0N,18,14^FB87,,,R^FD^FS
^FO0,646^A0N,18,14^FB87,,,R^FD^FS
^FO0,664^A0N,18,14^FB87,,,R^FD^FS
^FO0,682^A0N,18,14^FB87,,,R^FD^FS
^FO0,700^A0N,18,14^FB87,,,R^FD^FS
^FO0,718^A0N,18,14^FB87,,,R^FD^FS
^FO0,736^A0N,18,14^FB87,,,R^FD^FS
^FO0,754^A0N,18,14^FB87,,,R^FD^FS
^FO0,772^A0N,18,14^FB87,,,R^FD^FS
^FO0,790^A0N,18,14^FB87,,,R^FD^FS
^FO97,406^FT97,406^A0N,18,14^FDCosmetic skin care products. NOT^FS
^FO97,424^FT97,424^A0N,18,14^FDFOR RESALE. (1579)^FS
^FO97,442^FT97,442^A0N,18,14^FD^FS
^FO97,460^FT97,460^A0N,18,14^FD^FS
^FO97,478^FT97,478^A0N,18,14^FD^FS
^FO97,496^FT97,496^A0N,18,14^FD^FS
^FO97,514^FT97,514^A0N,18,14^FD^FS
^FO97,532^FT97,532^A0N,18,14^FD^FS
^FO97,550^FT97,550^A0N,18,14^FD^FS
^FO97,568^FT97,568^A0N,18,14^FD^FS
^FO97,586^FT97,586^A0N,18,14^FD^FS
^FO97,604^FT97,604^A0N,18,14^FD^FS
^FO97,622^FT97,622^A0N,18,14^FD^FS
^FO97,640^FT97,640^A0N,18,14^FD^FS
^FO97,658^FT97,658^A0N,18,14^FD^FS
^FO97,676^FT97,676^A0N,18,14^FD^FS
^FO97,694^FT97,694^A0N,18,14^FD^FS
^FO97,712^FT97,712^A0N,18,14^FD^FS
^FO97,730^FT97,730^A0N,18,14^FD^FS
^FO97,748^FT97,748^A0N,18,14^FD^FS
^FO97,766^FT97,766^A0N,18,14^FD^FS
^FO97,784^FT97,784^A0N,18,14^FD^FS
^FO97,802^FT97,802^A0N,18,14^FD^FS
^FO0,394^A0N,18,14^FB442,,,R^FD3304.99.50^FS
^FO0,412^A0N,18,14^FB442,,,R^FD^FS
^FO0,430^A0N,18,14^FB442,,,R^FD^FS
^FO0,448^A0N,18,14^FB442,,,R^FD^FS
^FO0,466^A0N,18,14^FB442,,,R^FD^FS
^FO0,484^A0N,18,14^FB442,,,R^FD^FS
^FO0,502^A0N,18,14^FB442,,,R^FD^FS
^FO0,520^A0N,18,14^FB442,,,R^FD^FS
^FO0,538^A0N,18,14^FB442,,,R^FD^FS
^FO0,556^A0N,18,14^FB442,,,R^FD^FS
^FO0,574^A0N,18,14^FB442,,,R^FD^FS
^FO0,592^A0N,18,14^FB442,,,R^FD^FS
^FO0,610^A0N,18,14^FB442,,,R^FD^FS
^FO0,628^A0N,18,14^FB442,,,R^FD^FS
^FO0,646^A0N,18,14^FB442,,,R^FD^FS
^FO0,664^A0N,18,14^FB442,,,R^FD^FS
^FO0,682^A0N,18,14^FB442,,,R^FD^FS
^FO0,700^A0N,18,14^FB442,,,R^FD^FS
^FO0,718^A0N,18,14^FB442,,,R^FD^FS
^FO0,736^A0N,18,14^FB442,,,R^FD^FS
^FO0,754^A0N,18,14^FB442,,,R^FD^FS
^FO0,772^A0N,18,14^FB442,,,R^FD^FS
^FO0,790^A0N,18,14^FB442,,,R^FD^FS
^FO470,406^FT470,406^A0N,18,14^FDCA/ON^FS
^FO470,424^FT470,424^A0N,18,14^FD^FS
^FO470,442^FT470,442^A0N,18,14^FD^FS
^FO470,460^FT470,460^A0N,18,14^FD^FS
^FO470,478^FT470,478^A0N,18,14^FD^FS
^FO470,496^FT470,496^A0N,18,14^FD^FS
^FO470,514^FT470,514^A0N,18,14^FD^FS
^FO470,532^FT470,532^A0N,18,14^FD^FS
^FO470,550^FT470,550^A0N,18,14^FD^FS
^FO470,568^FT470,568^A0N,18,14^FD^FS
^FO470,586^FT470,586^A0N,18,14^FD^FS
^FO470,604^FT470,604^A0N,18,14^FD^FS
^FO470,622^FT470,622^A0N,18,14^FD^FS
^FO470,640^FT470,640^A0N,18,14^FD^FS
^FO470,658^FT470,658^A0N,18,14^FD^FS
^FO470,676^FT470,676^A0N,18,14^FD^FS
^FO470,694^FT470,694^A0N,18,14^FD^FS
^FO470,712^FT470,712^A0N,18,14^FD^FS
^FO470,730^FT470,730^A0N,18,14^FD^FS
^FO470,748^FT470,748^A0N,18,14^FD^FS
^FO470,766^FT470,766^A0N,18,14^FD^FS
^FO470,784^FT470,784^A0N,18,14^FD^FS
^FO470,802^FT470,802^A0N,18,14^FD^FS
^FO0,394^A0N,18,14^FB669,,,R^FD0.072^FS
^FO0,412^A0N,18,14^FB669,,,R^FD^FS
^FO0,430^A0N,18,14^FB669,,,R^FD^FS
^FO0,448^A0N,18,14^FB669,,,R^FD^FS
^FO0,466^A0N,18,14^FB669,,,R^FD^FS
^FO0,484^A0N,18,14^FB669,,,R^FD^FS
^FO0,502^A0N,18,14^FB669,,,R^FD^FS
^FO0,520^A0N,18,14^FB669,,,R^FD^FS
^FO0,538^A0N,18,14^FB669,,,R^FD^FS
^FO0,556^A0N,18,14^FB669,,,R^FD^FS
^FO0,574^A0N,18,14^FB669,,,R^FD^FS
^FO0,592^A0N,18,14^FB669,,,R^FD^FS
^FO0,610^A0N,18,14^FB669,,,R^FD^FS
^FO0,628^A0N,18,14^FB669,,,R^FD^FS
^FO0,646^A0N,18,14^FB669,,,R^FD^FS
^FO0,664^A0N,18,14^FB669,,,R^FD^FS
^FO0,682^A0N,18,14^FB669,,,R^FD^FS
^FO0,700^A0N,18,14^FB669,,,R^FD^FS
^FO0,718^A0N,18,14^FB669,,,R^FD^FS
^FO0,736^A0N,18,14^FB669,,,R^FD^FS
^FO0,754^A0N,18,14^FB669,,,R^FD^FS
^FO0,772^A0N,18,14^FB669,,,R^FD^FS
^FO0,790^A0N,18,14^FB669,,,R^FD^FS
^FO0,394^A0N,18,14^FB771,,,R^FD2.50^FS
^FO0,412^A0N,18,14^FB771,,,R^FD^FS
^FO0,430^A0N,18,14^FB771,,,R^FD^FS
^FO0,448^A0N,18,14^FB771,,,R^FD^FS
^FO0,466^A0N,18,14^FB771,,,R^FD^FS
^FO0,484^A0N,18,14^FB771,,,R^FD^FS
^FO0,502^A0N,18,14^FB771,,,R^FD^FS
^FO0,520^A0N,18,14^FB771,,,R^FD^FS
^FO0,538^A0N,18,14^FB771,,,R^FD^FS
^FO0,556^A0N,18,14^FB771,,,R^FD^FS
^FO0,574^A0N,18,14^FB771,,,R^FD^FS
^FO0,592^A0N,18,14^FB771,,,R^FD^FS
^FO0,610^A0N,18,14^FB771,,,R^FD^FS
^FO0,628^A0N,18,14^FB771,,,R^FD^FS
^FO0,646^A0N,18,14^FB771,,,R^FD^FS
^FO0,664^A0N,18,14^FB771,,,R^FD^FS
^FO0,682^A0N,18,14^FB771,,,R^FD^FS
^FO0,700^A0N,18,14^FB771,,,R^FD^FS
^FO0,718^A0N,18,14^FB771,,,R^FD^FS
^FO0,736^A0N,18,14^FB771,,,R^FD^FS
^FO0,754^A0N,18,14^FB771,,,R^FD^FS
^FO0,772^A0N,18,14^FB771,,,R^FD^FS
^FO0,790^A0N,18,14^FB771,,,R^FD^FS
^FO326,832^FT326,832^A0N,20,16^FDTOTAL^FS
^FO428,828^FT428,828^A0N,18,14^FDCurrency & Value^FS
^FO428,846^FT428,846^A0N,18,14^FDDevise et valeur^FS
^FO0,818^A0N,18,14^FB771.4,,,R^FDCAD^FS
^FO0,838^A0N,18,14^FB771.4,,,R^FD2.50^FS
^FO22,913^FT22,913^A0N,18,14^FH|^FDJe, soussign|82, l'exportateur des marchandises, atteste que l'information fournie sur la pr|82sente^FS
^FO22,931^FT22,931^A0N,18,14^FH|^FDd|82claration relative |85 la facture est v|82ridique et correcte et que le colis ne contient^FS
^FO22,949^FT22,949^A0N,18,14^FH|^FDpas de marchandises dangereuses ou prohib|82es, conform|82ment aux conditions g|82n|82rales de^FS
^FO22,967^FT22,967^A0N,18,14^FDtransport.^FS
^FO22,994^FT22,994^A0N,18,14^FDI, the undersigned exporter of goods, certify that the particulars given in this invoice^FS
^FO22,1012^FT22,1012^A0N,18,14^FDdeclaration are true and correct to the best of my knowledge and that the items do not^FS
^FO22,1030^FT22,1030^A0N,18,14^FDcontain any dangerous or prohibited articles as stated in the General Conditions of Carriage.^FS
^FO22,1065^FT22,1065^A0N,20,16^FDSIGNATURE:^FS
^FO144,1069^GB609,,1^FS
^FO22,1090^FT22,1090^A0N,18,14^FD^FH|^FDEST/O|90E V1704.1.189^FS
^FO330,1090^FT330,1090^A0N,18,14^FDPage^FS
^FO0,1080^A0N,18,14^FB399.91,,,R^FD1^FS
^FO406,1090^FT406,1090^A0N,18,14^FDof/de^FS
^FO0,1080^A0N,18,14^FB477.05002,,,R^FD1^FS
^FO0,1080^A0N,18,14^FB789.67,,,R^FDSPEC 3521 V3^FS
^XZ
