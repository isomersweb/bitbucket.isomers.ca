<div class="extra-btn-wrapper clearfix">
	<div id="add_to_cart"  class="{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE}unvisible{/if}">
		<button class="btn purple btn-cart-extra clearfix" type="submit" name="Submit">
			<span>{l s='Add to cart'}</span><i class="icon-extra"></i>
		</button>
	</div>
	<div class="wishlist">
		<a class="btn btn-wishlist-extra btn-extra purple wishlistProd_{$product->id|intval}" href="#" rel="{$product->id|intval}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product->id|intval}', false, 1); return false;">
			<span>{l s="Add to Wishlist" mod='blockwishlist'}</span><i class="icon-extra"></i>
		</a>
	</div>
	<div role="send friend">
		<a class="btn btn-send-extra btn-extra" href="#sendfriend" aria-controls="sendfriend" role="tab" data-toggle="tab" data-target="#sendfriend, #sendfriend2"><span>{l s='Email a Friend'}</span><i class="icon-extra"></i></a>
	</div>
	<div role="share">
		<a class="btn btn-share-extra btn-extra" href="#share" aria-controls="share" role="tab" data-toggle="tab" data-target="#share, #share2"><span>{l s='Share'}</span><i class="icon-extra"></i></a>
	</div>
</div>