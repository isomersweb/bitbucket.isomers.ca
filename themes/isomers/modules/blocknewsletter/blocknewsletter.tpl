{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block Newsletter module-->
<div id="newsletter_block_left" class="block">
	<h3 class="title_block">{l s="Subscribe to our mailing list" mod="blocknewsletter"}</h3>
    <div class="newsletter_block_content">
        {if isset($msg) && $msg}
    		<p class="alert alert-danger">{$msg}</p>
    	{/if}
    	<form class="newsletter_form clearfix" action="{$smarty.server.REQUEST_URI}" method="post">
            <div class="form-group col-sm-6" >
                <label for="firstname">First Name</label>
                <input class="form-control grey newsletter-input" id="firstname" type="text" name="firstname" size="18" value="{if !empty($smarty.post.firstname)}{$smarty.post.firstname}{else}{$customer->firstname}{/if}" {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="form-group col-sm-6" >
                <label for="lastname">Last Name</label>
                <input class="form-control grey newsletter-input" id="lastname" type="text" name="lastname" size="18" value="{if !empty($smarty.post.lastname)}{$smarty.post.lastname}{else}{$customer->lastname}{/if}"  {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="form-group col-sm-6" >
                <label for="newsletter-input">Email*</label>
                <input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="{if !empty($smarty.post.email)}{$smarty.post.email}{else}{$customer->email}{/if}" {if !empty($customer->id)} readonly="readonly"{/if} />
            </div>
           <div class="form-group col-sm-6" >
                <label for="address">Address</label>
                <input class="form-control grey newsletter-input" id="address" type="text" name="address" size="18" value="{if !empty($smarty.post.address)}{$smarty.post.address}{else}{$address.address1}{/if}"  {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="form-group col-sm-6" >
                <label for="city">City</label>
                <input class="form-control grey newsletter-input" id="city" type="text" name="city" size="18" value="{if !empty($smarty.post.city)}{$smarty.post.city}{else}{$address.city}{/if}"  {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="form-group col-sm-6" >
                <label for="zip">Postal Code/ZIP Code </label>
                <input class="form-control grey newsletter-input" id="zip" type="text" name="zip" size="18" value="{if !empty($smarty.post.zip)}{$smarty.post.zip}{else}{$address.postcode}{/if}"  {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="form-group col-sm-6" >
                <label for="country">Country</label>
                <select class="selectpicker" id="country" name="country"  {if !empty($customer->id)} readonly="readonly"{/if}>
                    {$countries}
                </select>

            </div>
            <div class="form-group col-sm-6" >
                <label for="phone">Phone</label>
                <input class="form-control grey newsletter-input" id="phone" type="text" name="phone" size="18" value="{if !empty($smarty.post.phone)}{$smarty.post.phone}{else}{$address.phone}{/if}"  {if !empty($customer->id)} readonly="readonly"{/if}/>
            </div>
            <div class="clearfix"></div>

            <div class="form-group" >
                <input type="submit" class="btn btn-submit black" value="Subscribe" name="submitNewsletter" >
    			<input type="hidden" name="action" value="0" />
    		</div>
    	</form>
    </div>
</div>
{*<!-- /Block Newsletter module-->
<script type="text/javascript">
    var placeholder = "{l s='Email' mod='blocknewsletter' js=1}";
    {literal}
        $(document).ready(function() {
            $('#newsletter-input').on({
                focus: function() {
                    if ($(this).val() == placeholder) {
                        $(this).val('');
                    }
                },
                blur: function() {
                    if ($(this).val() == '') {
                        $(this).val(placeholder);
                    }
                }
            });
        });
    {/literal}
</script>

<div id="newsletter_block_left" class="block">
	<h4 class="title_block">{l s='Newsletter' mod='blocknewsletter'}</h4>
	<div class="block_content">
	{if isset($msg) && $msg}
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg}</p>
	{/if}
		<form action="{$link->getPageLink('index', true)|escape:'html'}" method="post">
			<p>
				<input class="inputNew" id="newsletter-input" type="text" name="email" size="18" value="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='blocknewsletter'}{/if}" />
				<input type="submit" value="ok" class="button_mini" name="submitNewsletter" />
				<input type="hidden" name="action" value="0" />
			</p>
		</form>
	</div>
</div>
<!-- /Block Newsletter module-->

<script type="text/javascript">
    var placeholder = "{l s='your e-mail' mod='blocknewsletter' js=1}";
    {literal}
        $(document).ready(function() {
            $('#newsletter-input').on({
                focus: function() {
                    if ($(this).val() == placeholder) {
                        $(this).val('');
                    }
                },
                blur: function() {
                    if ($(this).val() == '') {
                        $(this).val(placeholder);
                    }
                }
            });
        });
    {/literal}
</script>*}