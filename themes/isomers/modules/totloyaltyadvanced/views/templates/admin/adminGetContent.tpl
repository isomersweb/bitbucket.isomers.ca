{*
* @version 1.0
* @author 202-ecommerce
* @copyright 2014-2015 202-ecommerce
* @license ?
*}
{if $isLoyaltyModuleInstalled}
	<fieldset>
		<legend><img src="{$_path|escape:'html':'UTF-8'}logo.gif" alt="">{l s='Help' mod='totloyaltyadvanced'}</legend>
		<p>"{$DisplayName|escape:'html':'UTF-8'}" {l s='works with Prestashop native loyalty module, please click' mod='totloyaltyadvanced'} <a href="{$url|escape:'html':'UTF-8'}">{l s='here' mod='totloyaltyadvanced'}</a> {l s='to change configuration' mod='totloyaltyadvanced'}.</p>
		<h3>{l s='List and change customers loyalty points' mod='totloyaltyadvanced'}</h3>
		{l s='Please refer to Customers > Loyalty points list' mod='totloyaltyadvanced'}.
		<h3>{l s='Define product specific reward points amount' mod='totloyaltyadvanced'}</h3>
		{l s='Please edit product, then go to' mod='totloyaltyadvanced'} {$DisplayName|escape:'html':'UTF-8'} {l s='and set amount, either as fixed amount or multiplier' mod='totloyaltyadvanced'}.
	</fieldset>
{else}
	<div class="warn">
	{$DisplayName|escape:'html':'UTF-8'} {l s='requires Prestashop native loyalty module to operate, please click' mod='totloyaltyadvanced'} <a href="{$url|escape:'html':'UTF-8'}">{l s='here' mod='totloyaltyadvanced'}</a> {l s='to install this module' mod='totloyaltyadvanced'}.
	</div>
{/if}
