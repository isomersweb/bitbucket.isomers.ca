<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{totloyaltyadvanced}isomers>totloyaltyadvanced_94c2a3734a95577d173f702aa67a4788'] = 'Beauty Points (%d points)';
$_MODULE['<{totloyaltyadvanced}isomers>totloyaltyadvanced_89ef6b8a7c03cc14c2f29e6dc6f85dc6'] = 'Add Beauty Points';
$_MODULE['<{totloyaltyadvanced}isomers>loyalty_c540093e64d84440025b2d8201f04336'] = 'My Beauty Points';
$_MODULE['<{totloyaltyadvanced}isomers>loyalty_b39cba8836db01a04888aef6ba386420'] = 'My vouchers from Beauty Points';
$_MODULE['<{totloyaltyadvanced}isomers>shopping-cart_971a5c5c8cebfa1ea0f20dde33f9325c'] = 'Add some products to your shopping cart to collect some Beauty Points.';
