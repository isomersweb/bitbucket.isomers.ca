<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_58ef962a87e6fbbea6027c17a954a18d'] = 'Empty recordset returned.';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_f5c493141bb4b2508c5938fd9353291a'] = 'Displaying %1$s of %2$s';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_ca0dbad92a874b2f69b549293387925e'] = 'Code';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_49ee3087348e8d44e1feda1917443987'] = 'Name';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_b769cee333527b8dc6f3f67882e35a0b'] = 'Best vouchers';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_d32edaf4608c91c5795eceaa1948aea7'] = 'Adds a list of the best vouchers to the Stats dashboard.';
$_MODULE['<{statsbestvouchers}isomers>statsbestvouchers_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
