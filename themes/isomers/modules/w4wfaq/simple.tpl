<div class="w4wfaq-wrapper">
	<div class="w4wfaq-cat">
<div class="faq-content-wrapper">
    {foreach from=$faq_data item=faq_cat name=leftloop}
         <h3 class="uppercase">{$faq_cat.name}</h3>
         <div class="faq_wrapper">            
             {foreach from=$faq_cat.entries item=entry name=entryloop}
                <div class="faq_item" id="tab-pane-{$faq_cat.id}-panel-{$smarty.foreach.entryloop.iteration}">
                    <div class="faq_question">
                        <a  href="#{$faq_cat.id}-collapse{$entry.id}" aria-lable="Show Faq Item">{$entry.title} <span></span></a>
                    </div>
                    
                    <div class="faq_answer"  id="{$faq_cat.id}-collapse{$entry.id}">
                        {$entry.content}
                        {if $entry.has_controls}
							<a type="button" tabindex="0" data-trigger="focus" class="btn black btn-submit" data-container="div.w4wfaq-wrapper" data-toggle="popover" data-placement="top">{l s='Did we answer your question ?' mod='w4wfaq'}</a>
						{/if}
                    </div>
                </div>
            {/foreach}            
         </div>         
    {/foreach}
</div>
</div>
</div>
<div id="faq_popover_content" style="display:none">
	<p>{l s='You can contact us by mail' mod='w4wfaq'} <a href="{$faq_contact_link}" class="contact_link">{l s='here' mod='w4wfaq'}</a></p>
	{if $faq_show_phone}
	<p>{l s='or by phone using this number : ' mod='w4wfaq'} {$faq_phone_number}</p>
	{/if}
</div>
{literal}
<script>
	$(function(){
		if(typeof $().popover == 'function')
		{
			$("a[data-toggle=\"popover\"]").popover({html:true,content:function(){return $("#faq_popover_content").html();}});
		}

		if(typeof $().tooltip == 'function')
		{
			$("a[data-toggle=\"popover\"]").tooltip();
		}
	});
</script>
{/literal}           

{*
<div class="w4wfaq-wrapper">
	<div class="col-md-4 span4 w4wfaq-cat">
		<!-- Cat menu -->
		<h4>{l s='Category' mod='w4wfaq'}</h4>
		<hr>
		<div>
			<ul id="w4wfaq-tab" class="nav nav-tabs" role="tablist">
			{foreach from=$faq_data item=faq_cat name=leftloop}
				<li class="{if $smarty.foreach.leftloop.first}active{/if}"><a href="#tab-pane-{$faq_cat.id}" role="tab" data-toggle="tab">{if $faq_cat.icon!=''}<i class="fa {$faq_cat.icon}"></i>{/if}{$faq_cat.name}</a></li>
			{/foreach}
			</ul>
		</div>
	</div>
	<!-- faq list -->
	<div class="col-md-8 span8 w4wfaq-list">
		<h4>{l s='FAQ' mod='w4wfaq'}</h4>
		<hr>
		<div id="w4wfaq-tab-content" class="tab-content">
			{foreach from=$faq_data item=faq_cat name=rightloop}
				<div class="tab-pane fade{if $smarty.foreach.rightloop.first} active in{/if}" id="tab-pane-{$faq_cat.id}">
					<div class="panel-group accordion" id="accordion-{$faq_cat.id}">
						{foreach from=$faq_cat.entries item=entry name=entryloop}
							<div class="panel panel-default accordion-group" id="tab-pane-{$faq_cat.id}-panel-{$smarty.foreach.entryloop.iteration}">
								<div class="panel-heading accordion-heading">
									<h4 class="panel-title accordion-toggle">
										{if $faq_mode == 'accordion'}<a data-toggle="collapse" data-parent="#accordion-{$faq_cat.id}" href="#{$faq_cat.id}-collapse{$entry.id}" class="{if $faq_mode == 'accordion'}collapsed{/if}">{else}<span>{/if}{if $entry.icon!=''}<i class="fa {$entry.icon}"></i>{/if}{$entry.title}{if $faq_mode == 'accordion'}</a>{else}</span>{/if}
									</h4>
								</div>
								<div id="{$faq_cat.id}-collapse{$entry.id}" class="panel-collapse accordion-body {if $faq_mode == 'accordion'}collapse{else}in{/if}">
									<div class="panel-body accordion-inner">
										<p>{$entry.content}</p>
										{if $entry.has_controls}
											<a type="button" tabindex="0" data-trigger="focus" class="btn btn-danger" data-container="div.w4wfaq-wrapper" data-toggle="popover" data-placement="top">{l s='Did we answer your question ?' mod='w4wfaq'}</a>
										{/if}
									</div>
								</div>
							</div>
						{/foreach}
					</div>
				</div>
			{/foreach}
		</div>
	</div>
</div>
<div id="faq_popover_content" style="display:none">
	<p>{l s='You can contact us by mail' mod='w4wfaq'} <a href="{$faq_contact_link}" class="contact_link">{l s='here' mod='w4wfaq'}</a></p>
	{if $faq_show_phone}
	<p>{l s='or by phone using this number : ' mod='w4wfaq'} {$faq_phone_number}</p>
	{/if}
</div>
{literal}
<script>
	$(function(){
		//If bootstrap's popover feature is available, enable it for controls button
		if(typeof $().popover == 'function')
		{
			$("a[data-toggle=\"popover\"]").popover({html:true,content:function(){return $("#faq_popover_content").html();}});
		}

		//If bootstrap's tootlip feature is available, enable it for controls button
		if(typeof $().tooltip == 'function')
		{
			$("a[data-toggle=\"popover\"]").tooltip();
		}
	});
</script>
{/literal}*}