
<!-- Pagination -->
{if $start!=$stop}
	<ul class="pagination">
	{if $pages_nb > 1 AND $p != $pages_nb}
		{assign var='p_next' value=$p+1}
		<li id="pagination_next" class="pagination_next"><a href="{SimpleBlogPost::getPageLink($p_next, $type, $rewrite)|escape:'html':'UTF-8'}" rel="next">{l s='Older posts' mod='ph_simpleblog'}</a></li>
	{else}
		<li id="pagination_next" class="disabled pagination_next">{l s='Older posts' mod='ph_simpleblog'}</li>
	{/if}
	{if $p != 1}
		{assign var='p_previous' value=$p-1}
		<li id="pagination_previous" class="pagination_previous"><a href="{SimpleBlogPost::getPageLink($p_previous, $type, $rewrite)|escape:'html':'UTF-8'}" rel="prev">{l s='Newer posts' mod='ph_simpleblog'}</a></li>
	{else}
		<li id="pagination_previous" class="disabled pagination_previous">{l s='Newer posts' mod='ph_simpleblog'}</li>
	{/if}
	</ul>
{/if}
<!-- /Pagination -->		