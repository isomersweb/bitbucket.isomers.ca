{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}

{assign var=currentpagelink value=$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}

{if $sponsorDelete > 0}
    <p class="alert alert-warning">
        {l s='The Sponsor was deleted!' mod='allinone_rewards'}
    </p>
{/if}
{if $emailpass}
    <p class="alert alert-warning">
        {l s='Please change your default password to avoid hacking your account!'}
    </p>
{/if}
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="{$currentpagelink}#info" aria-controls="info" role="tab" data-toggle="tab">{l s='Account Info'}</a></li>
    <li role="presentation"><a href="{$currentpagelink}#addresses" aria-controls="addresses" role="tab" data-toggle="tab">{l s='My Addresses'}</a></li>
    <li role="presentation"><a href="{$currentpagelink}#orders" aria-controls="orders" role="tab" data-toggle="tab">{l s='Recent Orders'}</a></li>
    <li role="presentation"><a href="{$currentpagelink}#points" aria-controls="points" role="tab" data-toggle="tab">{l s='Rewards Program'}</a></li>
    <li role="presentation"><a href="{$currentpagelink}#return" data-url="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" aria-controls="returns" role="tab" data-toggle="tab">{l s='Returns'}</a></li>
</ul>


<div class="tab-content">
    <a href="{$currentpagelink}#info" aria-controls="info" role="tab" data-toggle="tab" class="active"> {l s='Account Info'}</a>
    <div role="tabpanel" class="tab-pane fade in active" id="info">

        <div class="info_view clearfix">
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">{l s='First name'}</div>
                <div class="col-xs-6 col-sm-8">{$firstname}</div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">{l s='Last name'}</div>
                <div class="col-xs-6 col-sm-8">{$lastname}</div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">Email</div>
                <div class="col-xs-6 col-sm-8">{$email}</div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">Date of birth</div>
                <div class="col-xs-6 col-sm-8">{$birthday|date_format:"%d/%m/%Y"}</div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">Gender</div>
                <div class="col-xs-6 col-sm-8">
                    {foreach from=$genders key=k item=gender}
                        {if $id_gender == $gender->id}{$gender->name}{/if}
                    {/foreach}        
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-4  uppercase">Password</div>
                <div class="col-xs-6 col-sm-8">&bull;&bull;&bull;&bull;&bull;&bull;<a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" class="btn black btn-edit">Edit</a></div>
                <HR />
            </div>
			<BR>To export your personal data click here:  
			<a href="{$base_dir}module/psgdpr/gdpr" class="btn btn-continue btn-purple">GDPR - Personal Data </a>
			<BR><BR>For any other request you might have regarding the rectification and/or erasure of your personal data, please contact us through our <a href="../contact-us">[ contact page ]</a>. We will review your request and reply as soon as possible.
                <HR />
            <div class="row">
<!-- START - Isomers - Kang added 3/6/2018 -->
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
{literal}
<style>
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
    #mc_embed_signup .indicates-required {text-align: left;}
    #mc_embed_signup .unsubscribe a {color: #60123c; text-decoration: underline; text-transform: uppercase;}
    #mc_embed_signup .unsubscribe a:hover {color: red;}
</style>
{/literal}
<div id="mc_embed_signup">
    <form action="https://isomers.us12.list-manage.com/subscribe/post?u=ba2141ffdcb5aa5b1beb7612b&amp;id=52919f1667" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
            <h2 class="text-uppercase">Subscribe to our mailing list</h2>
            <div class="indicates-required text-left"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                </label>
                <input type="email" value="{$email}" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME">First Name </label>
                <input type="text" value="{$firstname}" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME">Last Name </label>
                <input type="text" value="{$lastname}" name="LNAME" class="" id="mce-LNAME">
            </div>

            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ba2141ffdcb5aa5b1beb7612b_52919f1667" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
        </div>
    </form>

    <div class="unsubscribe">
        <h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To <a href=" https://isomers.us12.list-manage.com/unsubscribe?u=ba2141ffdcb5aa5b1beb7612b&id=52919f1667">unsubscribe</a> click here</h5><BR>
    </div>
</div>
<!-- END - Isomers - Kang added 3/6/2018 -->                
                
            </div>
 
        </div>
    </div>
    <a href="{$currentpagelink}#addresses" aria-controls="addresses" role="tab" data-toggle="tab">{l s='My Addresses'}</a>
    <div role="tabpanel" class="tab-pane fade" id="addresses">
        {if isset($multipleAddresses) && $multipleAddresses}
        	<div class="block_adresses row">
        	{foreach from=$multipleAddresses item=address name=myLoop}
                <div class="block_adress row">
                    <div class="col-sm-3  uppercase">{$address.object.alias}</div>
                    <div class="col-sm-9 address_wrapper">
                    {foreach from=$address.ordered name=adr_loop item=pattern}
                        {assign var=addressKey value=" "|explode:$pattern}
                        <p>
                        {foreach from=$addressKey item=key name="word_loop"}
                            <span {if isset($addresses_style[$key])} class="{$addresses_style[$key]}"{/if}>
                                {$address.formated[$key|replace:',':'']|escape:'html':'UTF-8'}
                            </span>
                        {/foreach}
                        </p>
                    {/foreach}
                    </div>
                    <div class="address_update text-right">
                        <a class="btn btn-edit" href="{$link->getPageLink('address', true, null, "id_address={$address.object.id|intval}")|escape:'html':'UTF-8'}" title="{l s='Edit'}">{l s='Edit'}</a>
                        <a class="btn black btn-delete" href="#" data-id="addresses_confirm" title="{l s='Delete'}" data-toggle="modal" data-target="#deleteAddress" data-link="{$link->getPageLink('address', true, null, "id_address={$address.object.id|intval}&delete")|escape:'html':'UTF-8'}">{l s='Delete'}</a>
                    </div>
                </div>
        	{if $smarty.foreach.myLoop.index % 2 && !$smarty.foreach.myLoop.last}
        	</div>
        	<div class="block_adresses row">
        	{/if}
        	{/foreach}
        	</div>
        {else}
        	<p class="alert alert-warning">{l s='No addresses are available.'}</p>
        {/if}
        <div class="clearfix main-page-indent text-right">
        	<a href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add an address'}" class="btn black btn-add">{l s='ADD'}</a>
        </div>
    </div>
    <a href="{$currentpagelink}#orders" aria-controls="orders" role="tab" data-toggle="tab">{l s='Recent Orders'}</a>
    <div role="tabpanel" class="tab-pane fade" id="orders">
        {include file="./my-orders.tpl"}
    </div>
    <a href="{$currentpagelink}#points" aria-controls="points" role="tab" data-toggle="tab">{l s='Rewards Program'}</a>
    <div role="tabpanel" class="tab-pane fade" id="points">
        <div class="col-sm-8">
            <ul id="block-points">
                <li>
                    <a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}#id_default_group1" title="{l s='REWARDS GROUP'}" class="btn btn-continue btn-purple">{l s='REWARDS GROUP'}</a>
                </li>
                {$HOOK_CUSTOMER_ACCOUNT}
                <li><a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}"  title="{l s='MY REDEMPTIONS'}" class="btn btn-continue btn-purple">{l s="MY REDEMPTIONS"}</a></li>
            </ul>

        </div>
        <div class="col-sm-4">
            {if isset($id_sponsorship) && !empty($id_sponsorship)}
                <form id="sponsor" method="post" action="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
                    <input type="hidden" name="id_sponsor" value="{$sponsor->id}">
                    <input type="hidden" name="id_sponsorship" value="{$id_sponsorship}">
                    Sponsor: <strong>{$sponsor->firstname} {$sponsor->lastname}</strong><br><br>
                    {*<input class="btn btn-delete btn-purple" name="deleteSponsor" id="deleteSponsor" value="Delete Sponsor" type="submit">*}
                </form>
            {/if}
        </div>
    </div>
    <a href="{$currentpagelink}#returns" aria-controls="returns" role="tab" data-toggle="tab">{l s='Returns'}</a>
    <div role="tabpanel" class="tab-pane fade" id="returns">
        {$cmspage->content}
    </div>
</div>
<div class="modal fade" id="deleteAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Address</h4>
      </div>
      <div class="modal-body">
            Are you sure want to delete this address?
      </div>
      <div class="modal-footer">
        <a href="#" class="btn black btn-submit">Yes</a>
        <button type="button" class="btn btn-continue" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

{literal}
<script>
    $(function() {
        $('.btn-delete').on('click', function() {
            $('.modal .btn-submit').attr('href', $(this).attr('data-link'));
        });
    });
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var url = $(e.target).data('url');
        if ((typeof url !== "undefined")) {
            e.preventDefault();
            window.location = url;
        }
    });
</script>
{/literal}
{*
<h1 class="page-heading">{l s='My account'}</h1>
{if isset($account_created)}
	<p class="alert alert-success">
		{l s='Your account has been created.'}
	</p>
{/if}
<p class="info-account">{l s='Welcome to your account. Here you can manage all of your personal information and orders.'}</p>
<div class="row addresses-lists">
	<div class="col-xs-12 col-sm-6 col-lg-4">
		<ul class="myaccount-link-list">
            {if $has_customer_an_address}
            <li><a href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add my first address'}"><i class="icon-building"></i><span>{l s='Add my first address'}</span></a></li>
            {/if}
            <li><a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders'}"><i class="icon-list-ol"></i><span>{l s='Order history and details'}</span></a></li>
            {if $returnAllowed}
                <li><a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}"><i class="icon-refresh"></i><span>{l s='My merchandise returns'}</span></a></li>
            {/if}
            <li><a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='Credit slips'}"><i class="icon-file-o"></i><span>{l s='My credit slips'}</span></a></li>
            <li><a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}"><i class="icon-building"></i><span>{l s='My addresses'}</span></a></li>
            <li><a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information'}"><i class="icon-user"></i><span>{l s='My personal information'}</span></a></li>
        </ul>
	</div>
{if $voucherAllowed || isset($HOOK_CUSTOMER_ACCOUNT) && $HOOK_CUSTOMER_ACCOUNT !=''}
	<div class="col-xs-12 col-sm-6 col-lg-4">
        <ul class="myaccount-link-list">
            {if $voucherAllowed}
                <li><a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}"><i class="icon-barcode"></i><span>{l s='My vouchers'}</span></a></li>
            {/if}
            {$HOOK_CUSTOMER_ACCOUNT}
        </ul>
    </div>
{/if}
</div>
<ul class="footer_links clearfix">
<li><a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{l s='Home'}"><span><i class="icon-chevron-left"></i> {l s='Home'}</span></a></li>
</ul>*}