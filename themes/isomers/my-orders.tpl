{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account'}
	</a>
{/capture}
{include file="$tpl_dir./errors.tpl"}
<link href="{$css_dir}history.css" rel="stylesheet" type="text/css" media="{$media}" />
<link href="{$css_dir}addresses.css" rel="stylesheet" type="text/css" media="{$media}" />
<div class="block-center row" id="block-history">
	{if $orders && count($orders)}
				{foreach from=$orders item=order name=myLoop}
                    <div class="history-item {if $smarty.foreach.myLoop.first}first_item col-xs-12 clearfix{elseif $smarty.foreach.myLoop.last}last_item col-sm-6{else}item col-sm-6{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4  uppercase">{l s='Order No.'}</div>
                            <div class="col-xs-6 col-sm-8">
                                <a class="color-myaccount" href="javascript:showOrder(1, {$order.id_order|intval}, '{$link->getPageLink('order-detail', true)|escape:'html':'UTF-8'}');">
    								{Order::getUniqReferenceOf($order.id_order)}
    							</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4  uppercase">{l s='Order Date'}</div>
                            <div class="col-xs-6 col-sm-8">{dateFormat date=$order.date_add full=0}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4  uppercase">{l s='Total Amount'}</div>
                            <div class="col-xs-6 col-sm-8">{displayPrice price=$order.total_paid currency=$order.id_currency no_utf8=false convert=false}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4  uppercase">{l s='Status'}</div>
                            <div class="col-xs-6 col-sm-8">{$order.order_state|escape:'html':'UTF-8'}</div>
                        </div>
                        {*<div class="row">
                            <div class="col-xs-6 col-sm-4  uppercase">{l s='Invoice'}</div>
                            <div class="col-xs-6 col-sm-8">
                                {if (isset($order.invoice) && $order.invoice && isset($order.invoice_number) && $order.invoice_number) && isset($invoiceAllowed) && $invoiceAllowed == true}
    								<a class="link-button" href="{$link->getPageLink('pdf-invoice', true, NULL, "id_order={$order.id_order}")|escape:'html':'UTF-8'}" title="{l s='Invoice'}" target="_blank"><i class="icon-file-text large"></i>{l s='PDF'}</a>
    							{else}
    								-
    							{/if}                            
                            </div>
                        </div>
                        {if !empty($order.shipping_number)}
                            <div class="row">
                                <div class="col-xs-6 col-sm-4  uppercase">{l s='Tracking No.'}</div>
                                <div class="col-xs-6 col-sm-8">{$order.shipping_number}</div>
                            </div>
                        {/if}*}
                        <div class="text-right">
                            <a class="btn black btn-viewcart" href="javascript:showOrder(1, {$order.id_order|intval}, '{$link->getPageLink('order-detail', true)|escape:'html':'UTF-8'}');">{l s='View Order'}<i class="icon-chevron-right right"></i></a>
                        </div>
                    </div>
           {/foreach}
	{else}
		<p class="alert alert-warning">{l s='You have not placed any orders.'}</p>
	{/if}
</div>
<div id="block-order-detail" class="">&nbsp;</div>
                    





{*
                    
					<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
						<td class="history_link bold">
							{if isset($order.invoice) && $order.invoice && isset($order.virtual) && $order.virtual}
								<img class="icon" src="{$img_dir}icon/download_product.gif"	alt="{l s='Products to download'}" title="{l s='Products to download'}" />
							{/if}
							<a class="color-myaccount" href="javascript:showOrder(1, {$order.id_order|intval}, '{$link->getPageLink('order-detail', true)|escape:'html':'UTF-8'}');">
								{Order::getUniqReferenceOf($order.id_order)}
							</a>
						</td>
						<td data-value="{$order.date_add|regex_replace:"/[\-\:\ ]/":""}" class="history_date bold">
							{dateFormat date=$order.date_add full=0}
						</td>
						<td class="history_price" data-value="{$order.total_paid}">
							<span class="price">
								{displayPrice price=$order.total_paid currency=$order.id_currency no_utf8=false convert=false}
							</span>
						</td>
						<td class="history_method">{$order.payment|escape:'html':'UTF-8'}</td>
						<td{if isset($order.order_state)} data-value="{$order.id_order_state}"{/if} class="history_state">
							{if isset($order.order_state)}
								<span class="label{if isset($order.order_state_color) && Tools::getBrightness($order.order_state_color) > 128} dark{/if}"{if isset($order.order_state_color) && $order.order_state_color} style="background-color:{$order.order_state_color|escape:'html':'UTF-8'}; border-color:{$order.order_state_color|escape:'html':'UTF-8'};"{/if}>
									{$order.order_state|escape:'html':'UTF-8'}
								</span>
							{/if}
						</td>
						<td class="history_invoice">
							{if (isset($order.invoice) && $order.invoice && isset($order.invoice_number) && $order.invoice_number) && isset($invoiceAllowed) && $invoiceAllowed == true}
								<a class="link-button" href="{$link->getPageLink('pdf-invoice', true, NULL, "id_order={$order.id_order}")|escape:'html':'UTF-8'}" title="{l s='Invoice'}" target="_blank">
									<i class="icon-file-text large"></i>{l s='PDF'}
								</a>
							{else}
								-
							{/if}
						</td>
						<td class="history_detail">
							<a class="btn btn-default button button-small" href="javascript:showOrder(1, {$order.id_order|intval}, '{$link->getPageLink('order-detail', true)|escape:'html':'UTF-8'}');">
								<span>
									{l s='Details'}<i class="icon-chevron-right right"></i>
								</span>
							</a>
							{if isset($opc) && $opc}
								<a class="link-button" href="{$link->getPageLink('order-opc', true, NULL, "submitReorder&id_order={$order.id_order|intval}")|escape:'html':'UTF-8'}" title="{l s='Reorder'}">
							{else}
								<a class="link-button" href="{$link->getPageLink('order', true, NULL, "submitReorder&id_order={$order.id_order|intval}")|escape:'html':'UTF-8'}" title="{l s='Reorder'}">
							{/if}
								{if isset($reorderingAllowed) && $reorderingAllowed}
									<i class="icon-refresh"></i>{l s='Reorder'}
								{/if}
							</a>
						</td>
					</tr>
				{/foreach}
		</div>
		<div id="block-order-detail" class="unvisible">&nbsp;</div>
	{else}
		<p class="alert alert-warning">{l s='You have not placed any orders.'}</p>
	{/if}
</div>

*}