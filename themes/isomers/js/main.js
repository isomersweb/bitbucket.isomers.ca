$(document).ready(function() {
    $('.navbar-toggle').on('click', function(e) {
        e.preventDefault();

        var toggleRight = $(".sidebar").css('margin-left') == '0px' ? "-300px" : "0px";
        $('.sidebar').animate({"margin-left": toggleRight}, 800, 'linear', function() {
            $('.sidebar').toggleClass('open');
        });
    });
    //**************** HOVER NAVIGATION *************************
    /*$('#navbar .dropdown').hover(function(e) {
        //e.preventDefault();
        $(this).find('.dropdown-menu').stop(true, true).slideDown();
        $(this).addClass('open');
    }, function(e){
        $(this).find('.dropdown-menu').stop(true, true).delay(1000).slideUp();
        $(this).removeClass('open');
    });*/



    $('.sidebar .sidebar-btn a').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('data-target');
        $('.cart-wrapper').addClass('active');
        $('.sidebar .sidebar-btn.active').removeClass('active');
        $(this).parent('.sidebar-btn').addClass('active');
        $('.sidebar .block_content.open').removeClass('open').stop(true, true).slideUp('slow');
        $('#' + href).addClass('open').stop(true, true).slideDown('slow');
    });
    $('.sidebar .btn-close').on('click', function(e) {
        e.preventDefault();
        $('.cart-wrapper').removeClass('active');
        $('.sidebar .sidebar-btn.active').removeClass('active');
        $(this).parents('.block_content').removeClass('open').slideUp('slow');
    });
    $('.tags-wrapper .introtext a').on('click', function(e) {
        e.preventDefault();
        $('.tags-wrapper').toggleClass('open').find('.tags-items-wrapper').slideToggle();
    });
    $('.tab-content > a').on('click', function(e) {
        e.preventDefault();
        $('.tags-wrapper').toggleClass('open').find('.tags-items-wrapper').slideToggle();
    });

    /*$(document).off('click', '.ajax_add_to_cart_button').on('click', '.ajax_add_to_cart_button', function(e){
        //e.preventDefault();
        var idProduct =  parseInt($(this).data('id-product'));
        var idProductAttribute =  parseInt($(this).data('id-product-attribute'));
        var minimalQuantity =  parseInt($(this).data('minimal_quantity'));
        if ($(this).is('.add-to-cart-in-wl')) {
            quan = $(this).closest('.product_item').find('.product_quantity').val();
            if (quan != minimalQuantity)
                minimalQuantity = quan;
        }
        if (!minimalQuantity)
            minimalQuantity = 1;
        if ($(this).prop('disabled') != 'disabled')
            ajaxCart.add(idProduct, idProductAttribute, false, this, minimalQuantity);
    });*/
    /*$('#info .btn-submit').on('click', function(e) {
        e.preventDefault();
        if ($('.info_view').is(":visible")) {
            var $info_first = $('.info_view');
            var $info_sec =  $('.info_edit');
        }
        else {
            var $info_first =  $('.info_edit');
            var $info_sec = $('.info_view');
        }
        $info_first.fadeOut(function() {
            $info_sec.fadeIn();
        });

    });*/
    $('.video-item a').on('click', function(e) {
        e.preventDefault();
        $('#video .modal-body').html('<iframe width="100%" height="475" src="'+$(this).attr('href')+'?autoplay=1" allowfullscreen  allowtransparency="true" frameborder="0"></iframe>');
        $('#video .modal-title').html($(this).attr('data-title'));
        $('#video').modal();
        console.log($(this).attr('data-title'));
    });
    $('#video').on('shown.bs.modal', function () {
        //$('#video .modal-title').html($(this).attr('data-title'));
    });
    $('#video').on('hidden.bs.modal', function () {
        $('#video .modal-body').html('');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        win_resize();
        var $attr = $(this).attr('href');
        $('.nav-tabs li.active, .tab-content > a.active, .btn-extra.active, .tab-descr a.active').removeClass('active');
        $('a[href="' + $attr + '"]').addClass('active').parent('li').addClass('active');
    });

    $('.faq_question a').on('click', function(e) {
        e.preventDefault();
        $(this).parents('.faq_item').toggleClass('open').find('.faq_answer').stop(true, true).slideToggle();
    });
    win_resize();
    $(window).resize(function() {
        win_resize();
    });
    $('body.cms-12 .video-item .time').each(function() {
        getYouTubeVideoDuration($(this), $(this).attr('data-video'));
    });

});

function win_resize() {
        var $windowHeight = $(window).height();
        var $contentHeight = $('.content').outerHeight();
        var $navbarHeight = $('#navbar').outerHeight();
        if ($('.sidebar').height() <= $contentHeight) {
            $('#navbar').outerHeight($contentHeight);
        }
        else {
            if ($windowHeight > $navbarHeight) {
                $('#navbar').outerHeight($windowHeight-80);
            }
            else {
                $('#navbar').outerHeight('auto');
            }
        }
        $('.sidebar .navbar-header').outerHeight($windowHeight);
}
function getYouTubeVideoDuration($this, $id) {
    var url = "https://www.googleapis.com/youtube/v3/videos?id=" + $id + "&part=snippet,contentDetails&key=AIzaSyDYwPzLevXauI-kTSVXTLroLyHEONuF9Rw";

    $.ajax({
        async: false,
        type: 'GET',
        url: url,
        success: function (data) {
            if (data.items.length > 0) {
                //console.log(convert_time(data.items[0].contentDetails.duration));
                $this.text(convert_time(data.items[0].contentDetails.duration));
            }
        }
    });
}
function convert_time(duration) {
    var a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
        a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
        a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
        a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length == 3) {
        duration = duration + parseInt(a[0]) * 3600;
        duration = duration + parseInt(a[1]) * 60;
        duration = duration + parseInt(a[2]);
    }

    if (a.length == 2) {
        duration = duration + parseInt(a[0]) * 60;
        duration = duration + parseInt(a[1]);
    }

    if (a.length == 1) {
        duration = duration + parseInt(a[0]);
    }
    var h = Math.floor(duration / 3600);
    var m = Math.floor(duration % 3600 / 60);
    var s = Math.floor(duration % 3600 % 60);
    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
}