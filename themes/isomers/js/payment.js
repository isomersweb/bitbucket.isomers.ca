$(document).ready(function() {
    checkPayment($('#HOOK_PAYMENT .payment_method:checked'));

    $('#HOOK_PAYMENT .payment_method').on('click', function() {
        $('#HOOK_PAYMENT .payment_method:checked').removeAttr('checked');
        $(this).attr('checked', 'checked');
        checkPayment($(this));
    });

    $('#confirm-order').on('click', function(e) {
        e.preventDefault();
        var error = false;
        $('#cc-form .alert').html('');
        if($('#payment_method').val() == 'cc_payment') {
            $('#cc-form #cc_num').validateCreditCard(function (result) {
                if (result.valid == false) {
                    //$('#cc-form .alert').html('Card type: ' + (result.card_type == null ? '-' : result.card_type.name));
                    if (result.card_type == null) {
                        $('#cc-form .alert').append('The Credit Card type is not recognized. Check the number again.<br>');
                    }
                    if (result.length_valid == false) {
                        $('#cc-form .alert').append('The length of the Credit Card is invalid<br>');
                    }
                    if (result.luhn_valid == false) {
                        $('#cc-form .alert').append('The Credit Card Number is wrong<br>');
                    }
                    $('#cc_num').addClass('error');
                    error = true;
                }
                else $('#cc-form #cc_num').removeClass('error');
            });
            if ($('#cc-form #cc_owner').val() == '') {
                $('#cc-form .alert').append('The Credit Card Holder Name is empty<br>');
                $('#cc-form #cc_owner').addClass('error');
                error = true;
            }
            else $('#cc-form #cc_owner').removeClass('error');

            var reg = new RegExp('^\\d{3}$');

            if ($('#cc-form #cc_cvd').val() == '' || reg.test($('#cc-form #cc_cvd').val()) == false) {
                $('#cc-form .alert').append('The Credit Card Verification Code is wrong or empty<br>');
                $('#cc-form #cc_cvd').addClass('error');
                error = true;
            }
            else $('#cc-form #cc_cvd').removeClass('error');
        }

        if (error == false) {
            $('#cc-form input').removeClass('error');
            $('#cc-form .alert').html('').slideUp();
        }
        else {
            $('#cc-form .alert').slideDown();
            return false;
        }
        if (typeof msg_order_carrier != 'undefined' && $('#cgv').length && !$('input#cgv:checked').length)
        {
            $.notify({message: msg_order_carrier}, {offset: 20});
            return false;
        }
        $('#cc-form').submit();

    });
    $('#place-order').on('click', function(e) {
        e.preventDefault();
        if (payment_method == 'paypal') {
            $('.paypal-form, #paypal-express-checkout-form').submit();
        }
        else if (payment_method == 'cc_payment') {
            $('#monerishosted_form  ').submit();
        }
    });
});

function checkPayment($this) {
    $('#payment_method').val($this.attr('name'));
    if($this.attr('name') == 'cc_payment') {
        $('#payment_method').val($this.attr('name'));
        $('#cc-form').slideDown();
    }
    else {
        $('#cc-form').slideUp();
    }
}