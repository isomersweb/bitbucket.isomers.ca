{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
                    {*{if $page_name != 'index'}</div>{/if}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->*}
				</div><!-- #columns -->
			</div><!-- .columns-container -->
		</div>
{if $page_name != 'index'}
	</div>
{/if}
{if Tools::getValue('controller') == 'contact'}
        <div class="gmap">
			<div id="map-canvas" style="width:100%; height:350px;"></div>

            {literal}

            <script>
			
			  var map;
			  function initMap() {
			  

			  	{/literal}
			  	var lat = {(float)Configuration::get('PS_STORES_CENTER_LAT')};
				var long = {(float)Configuration::get('PS_STORES_CENTER_LONG')};
				var contentString = "{Configuration::get('PS_SHOP_ADDR1')}<br>{Configuration::get('PS_SHOP_CITY')}, {substr(Configuration::get('PS_SHOP_STATE'),0,2)}, {Configuration::get('PS_SHOP_CODE')}<br>{Configuration::get('PS_SHOP_COUNTRY')}";
                 var content = "<h4>{$shop_name}</h4><p>"+contentString+"</p>";
                 content = content.replace(/(<([^>]+)>)/ig,"");
				 
			    {literal}
				var uluru = {lat: lat, lng: long};
				var infowindow = new google.maps.InfoWindow({
					content: content
				});
				map = new google.maps.Map(document.getElementById('map-canvas'), {
				   zoom: 12,
				  center: uluru,
				 
				});
				var marker = new google.maps.Marker({position: uluru, map: map});
				marker.addListener('click', function() {
					infowindow.open(map, marker);
				});

			  }
			   {/literal}
               /* var map;
                var geocoder = new google.maps.Geocoder();
               
                var $LatLong = new google.maps.LatLng({(float)Configuration::get('PS_STORES_CENTER_LAT')}, {(float)Configuration::get('PS_STORES_CENTER_LONG')});
                var $address = "{Configuration::get('PS_SHOP_ADDR1')}<br>{Configuration::get('PS_SHOP_CITY')}, {substr(Configuration::get('PS_SHOP_STATE'),0,2)}, {Configuration::get('PS_SHOP_CODE')}<br>{Configuration::get('PS_SHOP_COUNTRY')}";
                 var content = "<h4>{$shop_name}</h4><p>"+$address+"</p>";
                 $address = $address.replace(/(<([^>]+)>)/ig,"");
                {literal}
               function initialize() {
        var mapOptions = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
        setLocation();
    }

    function setLocation() {
        geocoder.geocode({
            'address': $address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //var position = results[0].geometry.location;
                marker = new google.maps.Marker({
					zoom: 12, 
                    map: map,
                    position: $LatLong,
					title: 'Uluru (Ayers Rock)'

                });
                map.setCenter(marker.getPosition());

               
                infoWindow = new google.maps.InfoWindow({
                    content: content
                });

  
                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open(map, marker);
                });

                google.maps.event.addListenerOnce(map, 'idle', function() {
                    setTimeout(function() {
                google.maps.event.trigger(marker, 'click'); 
                    },2000);
            });
            } 
        });
    }

                
                google.maps.event.addDomListener(window, 'load', initialize);*/
            </script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzr29rVqkvUSOgrwrh55awc9OVgFsj93o&callback=initMap" async defer></script>

            {/literal}
        </div>
        {/if}  
    </div>
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html> 