{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Your shopping cart'}{/capture}


{if isset($account_created)}
	<p class="alert alert-success">
		{l s='Your account has been created.'}
	</p>
{/if}

{assign var='current_step' value='summary'}
{assign var='confirm_order' value='confirm'}
{*{include file="$tpl_dir./order-steps.tpl"}*}
{include file="$tpl_dir./errors.tpl"}

{if $smarty.get.monerror == 1}
<div class="alert alert-warning"><h3 class="uppercase">{l s='The payment is not completed:' mod='monerishosted'}</h3>{$smarty.get.message|urldecode}</div>
{/if}

{if isset($empty)}
	<p class="alert alert-warning">{l s='Your shopping cart is empty.'}</p>
{elseif $PS_CATALOG_MODE}
	<p class="alert alert-warning">{l s='This store has not accepted your new order.'}</p>
{else}
	<p id="emptyCartWarning" class="alert alert-warning unvisible">{l s='Your shopping cart is empty.'}</p>

	{if $use_taxes && $priceDisplay == 0}
		{assign var='total_discounts_negative' value=$total_discounts * -1}
	{else}
		{assign var='total_discounts_negative' value=$total_discounts_tax_exc * -1}
	{/if}

	{assign var='total_discounts_num' value="{if $total_discounts != 0}1{else}0{/if}"}
	{assign var='use_show_taxes' value="{if $use_taxes && $show_taxes}2{else}0{/if}"}
	{assign var='total_wrapping_taxes_num' value="{if $total_wrapping != 0}1{else}0{/if}"}
	{* eu-legal *}
	{hook h="displayBeforeShoppingCartBlock"}
    <div id="order-detail-content" class="order_block">
        {foreach $products as $product}
			{if $product.is_virtual == 0}
				{assign var='have_non_virtual_products' value=true}
			{/if}
			{assign var='productId' value=$product.id_product}
			{assign var='productAttributeId' value=$product.id_product_attribute}
			{assign var='quantityDisplayed' value=0}
			{assign var='odd' value=($odd+1)%2}
			{assign var='ignoreProductLast' value=isset($customizedDatas.$productId.$productAttributeId) || count($gift_products)}
			{assign var='available_country' value={Product::getProductAvailableCountry($productId)}}
			{* Display the product line *}
			{if $dlv_address->id_country != $available_country && $available_country > 0}
				<p class="alert">{l s='You can not order this product for your country. Please choose another shipping address, ot remove it.'}</p>
				{assign var='available_country_error' value=1}
			{/if}
			{include file="$tpl_dir./shopping-cart-product-line.tpl" productLast=$product@last productFirst=$product@first}
			{* Then the customized datas ones*}			
		{/foreach}
		{assign var='last_was_odd' value=$product@iteration%2}


		<div id="cart_voucher" class="cart_voucher box row">
			{if $voucherAllowed}
				{if isset($errors_discount) && $errors_discount}
					<ul class="alert alert-danger">
						{foreach $errors_discount as $k=>$error}
							<li>{$error|escape:'html':'UTF-8'}</li>
						{/foreach}
					</ul>
				{/if}
				<div class="col-sm-3">
					<h5 class="uppercase">{l s="Promo Code"}</h5>
				</div>

				<div class="col-sm-7">
					{if sizeof($discounts)}
						<div class="row">
						{foreach $discounts as $discount}
							{if ((float)$discount.value_real == 0 && $discount.free_shipping != 1) || ((float)$discount.value_real == 0 && $discount.code == '')}
								{continue}
							{/if}
								<div class="col-xs-8 cart_discount_name">
									{$discount.code}
								</div>
								<div class="col-xs-4 cart_discount_amount">
									{displayPrice price=$discount.reduction_amount}
								</div>
						{/foreach}
						</div>
					{/if}
				</div>
				{*
				<div class="col-sm-2 price-discount price text-left" id="total_discount">
					{if $use_taxes && $priceDisplay == 0}
						{assign var='total_discounts_negative' value=$total_discounts * -1}
					{else}
						{assign var='total_discounts_negative' value=$total_discounts_tax_exc * -1}
					{/if}
					{displayPrice price=$total_discounts_negative}
				</div>
				*}
			{/if}
		</div>


		{if $total_discounts_negative != 0}
			<div class="cart_total_item row">
				<div class="text-label col-xs-6">
					<h5 class="uppercase">{l s='Discount'}</h5>
				</div>
				<div class="value col-xs-6 price text-right" id="total_discounts_negative">
					{displayPrice price=$total_discounts_negative}
				</div>
			</div>
		{/if}
		<div class="cart_total_item row">
            <div class="text-label col-xs-6">
                <h5 class="uppercase">{l s='Subtotal'}</h5>
            </div>
            <div class="value col-xs-6 price text-right" id="total_product">
                {displayPrice price=($total_price_without_tax-$total_shipping_tax_exc)}
            </div>
        </div>

		{if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}
            <div class="row cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">
                <div class="text-label col-xs-6">
                    <h5 class="uppercase">{l s='Shipping'} <span>({$carrier|print_r})</span></h5>
                </div>
                <div class="value col-xs-6 price text-right" id="total_shipping">
                    {l s='Free shipping!'}
                </div>
            </div>
		{else}
            <div class="row cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">
                <div class="text-label col-xs-6">
                    <h5><span class="uppercase">{if $display_tax_label}{l s='Shipping (tax excl.)'}{else}{l s='Shipping'}{/if}</span> via {$carrier->name}</h5>
                </div>
                <div class="value col-xs-6 price text-right" id="total_shipping">
                    {displayPrice price=$total_shipping_tax_exc}
                </div>
            </div>
		{/if}
                 
        {if $use_taxes && $show_taxes && $total_tax != 0 }
            <div class="cart_total_item cart_total_tax row">
                <div class="text-label col-xs-6">
                    <h5 class="uppercase">{l s='Tax'}</h5>
                </div>
                <div class="value col-xs-6 price text-right" id="total_tax">
                    {displayPrice price=$total_tax}
                </div>
            </div>
		{/if}
        
        <div class="cart_total_item row">
            <div class="text-label col-xs-6 col-md-9">
                <h5 class="uppercase">{if $display_tax_label}{l s='Total (tax excl.)'}{else}{l s='Total'}{/if}</h5>
            </div>
            <div class="value col-xs-6 col-md-3 price text-right">
                <h4 id="total_price">{if $use_taxes}{displayPrice price=$total_price}{else}{displayPrice price=$total_price_without_tax}{/if}</h4>
            </div>
        </div>

		<div class="cart_total_item row">
            <div class="text-label col-xs-6">
                <h5 class="uppercase">{l s='Address'}</h5>
            </div>
            <div class="value col-xs-6 address-block text-right">
				{$dlv_address->address1}<br>
				{$dlv_address->city}, {if !empty($state)}{$state}, {/if}{$dlv_address->country} <br>
				{$dlv_address->postcode}<br>
            </div>
        </div>

		<div class="cart_total_item row">
			<div class="text-label col-xs-6">
				<h5 class="uppercase">{l s='Payment Method'}</h5>
			</div>
			<div class="value col-xs-6 price text-right">
				<h5 class="uppercase">{if $smarty.session.payment.payment_method == 'cc_payment'}{l s='Credit Card '} {$smarty.session.payment.cc_num|substr:0:4} **** **** {$smarty.session.payment.cc_num|substr:-4}{else}{$smarty.session.payment.payment_method}{/if}</h5>
			</div>
		</div>

	</div>

    <div id="HOOK_SHOPPING_CART">{$HOOK_SHOPPING_CART}</div>
	<div class="cart_navigation clearfix text-right">
		<a href="{$link->getPageLink('order', false, NULL, 'step=3')|escape:'html':'UTF-8'}" title="{l s='Change Payment Method'}" id="change-payment" class="btn btn-pay ">
			<i class="icon-chevron-left"></i>
			{l s='Change Payment Method'}
		</a>
		<a href="{if $back}{$link->getPageLink('order', true, NULL)|escape:'html':'UTF-8'}{else}{$link->getPageLink('order', true, NULL, 'step=4')|escape:'html':'UTF-8'}{/if}" id="place-order" class="btn btn-checkout purple {if $available_country_error == 1}disabled{/if}" title="{l s='Proceed to checkout'}">
			<i class="icon-chevron-left"></i>{l s='Confirm and place order'}
		</a>
	</div>
	<div class="clear"></div>
	<div class="cart_navigation_extra">
		<div id="HOOK_SHOPPING_CART_EXTRA">{if isset($HOOK_SHOPPING_CART_EXTRA)}{$HOOK_SHOPPING_CART_EXTRA}{/if}</div>
	</div>

	<div style="display: none !important;">
		{Hook::exec('displayPayment')}
	</div>
{/if}
<script src="{$js_dir}payment.js"></script>

{strip}
{addJsDef deliveryAddress=$cart->id_address_delivery|intval}
{addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
{addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{addJsDefL name=payment_method}{$smarty.session.payment.payment_method}{/addJsDefL}
{/strip}

    
