
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<link href="/modules/pscookiebanner/views/css/jquery-eu-cookie-law-popup.css" rel="stylesheet" type="text/less" media="all" />
		{if isset($css_files)}
			{*{foreach from=$css_files key=css_uri item=media}
				<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
			{/foreach}*}
            <link rel="stylesheet" href="/modules/custompopup/css/popup.css" type="text/css" media="all" />
            <link rel="stylesheet" href="{$css_dir}modules/blocksearch/blocksearch.css" type="text/css" media="all" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
            <link href="{$css_dir}bootstrap-select.css" rel="stylesheet" type="text/css" media="all" />
            <link href="{$css_dir}jcarousel.responsive.css" rel="stylesheet" type="text/css" media="all" />
            <link href="{$css_dir}global_isomers.css" rel="stylesheet" type="text/css" media="all" />
            <link href="{$css_dir}animate.css" rel="stylesheet" type="text/css" media="all" />
            <link href="{$css_dir}style.less" rel="stylesheet" type="text/less" media="all" />
		{/if}
        
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
            <script src="{$css_dir}less.min.js"></script>
			{$js_def}
                    
			{foreach from=$js_files item=js_uri}
			     <script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
                        
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
            <script src="{$js_dir}bootstrap-select.min.js"></script>
            <script src="{$js_dir}jquery.jcarousel.min.js"></script>
           <script src="{$js_dir}bootstrap-notify.min.js"></script>
            <script src="{$js_dir}main.js"></script>
            <script src="{$js_dir}jcarousel.js"></script>
		{/if}
		{$HOOK_HEADER}
		<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso} {if $page_name != 'index'}inner{/if}">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
    <div class="container-fluid">
        {if $page_name != 'index'}<div class="header-bg"></div>{/if}
        <div class="container">
             <div class="row">
                <div class="sidebar">
                    <nav class="navbar">
                        <div class="navbar-header collapsed">
                            <button type="button" class="navbar-toggle" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="cart-wrapper cart-wrapper-small clearfix">
                                <div class="sidebar-btn sidebar-member">
                            		<a href="{$smarty.server.REQUEST_URI}#myaccount_block" rel="nofollow" title="{l s='My account' mod='blockuserinfo'}" data-target="myaccount_block"></a>
                                 </div>
                                <div class="sidebar-btn sidebar-compare">
                                    <a href="{$smarty.server.REQUEST_URI}#compare_block" data-target="compare_block"  title="{l s='Compare Products' mod='blockuserinfo'}">
                                        <span class="ajax_compare_quantity">{count($compared_products)}</span>
                                    </a>
                                </div>
                                <div class="sidebar-btn sidebar-wishlist">
                                    <a href="{$smarty.server.REQUEST_URI}#wishlist_block_list" data-target="wishlist_block_list" title="{l s='Wishlist' mod='blockuserinfo'}">
                                        <span class="ajax_wishlist_quantity">{$cookie->wishlist_qty}</span>
                                    </a>
                                </div>
                                <div class="sidebar-btn sidebar-cart">
                                    <a href="{$smarty.server.REQUEST_URI}#cart_block_list" data-target="cart_block_list" title="{l s='View Cart' mod='blockuserinfo'}">
                                        <span class="ajax_cart_quantity">{$cart_qties}</span>
                                    </a>
                                </div>
                            </div>
                            <a href="#" class="menu_collapse_btn navbar-toggle" aria-controls="navbar"></a>
                        </div><!--/.nav-header -->
                        <div id="navbar" class="navbar-collapse">
                            <div class="cart-wrapper clearfix">
                                <div class="sidebar-btn sidebar-member">
                            		<a href="{$smarty.server.REQUEST_URI}#myaccount_block" rel="nofollow" title="{l s='My account' mod='blockuserinfo'}" data-target="myaccount_block"></a>                                   
                                 </div>
                                <div class="sidebar-btn sidebar-compare">
                                    <a href="{$smarty.server.REQUEST_URI}#compare_block" data-target="compare_block" title="{l s='Compare Products' mod='blockuserinfo'}">
                                        <span class="ajax_compare_quantity">{count($compared_products)}</span>
                                    </a>
                                </div>
                                <div class="sidebar-btn sidebar-wishlist">
                                    <a href="{$smarty.server.REQUEST_URI}#wishlist_block_list" data-target="wishlist_block_list" title="{l s='Wishlist' mod='blockuserinfo'}">
                                        <span class="ajax_wishlist_quantity">{$cookie->wishlist_qty}</span>
                                    </a>
                                </div>
                                <div class="sidebar-btn sidebar-cart">
                                    <a href="{$smarty.server.REQUEST_URI}#cart_block_list" data-target="cart_block_list" title="{l s='View Cart' mod='blockuserinfo'}">
                                        <span class="ajax_cart_quantity">{$cart_qties}</span>
                                    </a>
                                </div>
                            </div>
                            
                            {include file="$tpl_dir./product-compare.tpl"}
                            
                            {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
                            <div class="clearfix"></div>
                            {if isset($HOOK_FOOTER)}<div class="navbar-footer">{$HOOK_FOOTER}</div>{/if}
                            
                        </div>
                    </nav>
                </div> 
                       
			{*<div class="header-container">
				<header id="header">
					{capture name='displayBanner'}{hook h='displayBanner'}{/capture}
					{if $smarty.capture.displayBanner}
						<div class="banner">
							<div class="container">
								<div class="row">
									{$smarty.capture.displayBanner}
								</div>
							</div>
						</div>
					{/if}
					{capture name='displayNav'}{hook h='displayNav'}{/capture}
					{if $smarty.capture.displayNav}
						<div class="nav">
							<div class="container">
								<div class="row">
									<nav>{$smarty.capture.displayNav}</nav>
								</div>
							</div>
						</div>
					{/if}
					<div>
						<div class="container">
							<div class="row">
								<div id="header_logo">
									<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
									</a>
								</div>
								{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
							</div>
						</div>
					</div>
				</header>
			</div>*}
			<div class="content" role="main">
                    <div class="logo-wrapper">
                        <a href="{$link->getPageLink('index', true)|escape:'html':'UTF-8'}"><img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/></a>
                        {if $page_name == 'index'}<span>It’s About Time</span>{/if}
                    </div>
                     
                     {if $page_name != 'index'}                   
    					{if $page_name == 'product' || $page_name == 'module-ph_simpleblog-single'}
                            <div class="content-header no-print">
    						  {include file="$tpl_dir./breadcrumb.tpl"}
                            </div>
                        {else}
                            <div class="content-header" {*style="background-image: url('{$img_dir}header_bg_grey.png');"*}>
                                {assign var='page_title' value=Meta::getMetaByPage($page_name, $cookie->id_lang)}
                                <h1>
                                    {if ($page_name == 'authentication' && isset($back) && preg_match("/^http/", $back)) || ($page_name == 'order' && !empty($addresses)) || ($page_name == 'order' && !empty($delivery_option_list)) || ($page_name == 'order' && $HOOK_PAYMENT)  || ($page_name == 'module-bankwire-payment')  || ($page_name == 'module-cheque-payment')  || ($page_name == 'module-cashondelivery-validation') || ($page_name == 'module-paypalusa-expresscheckout')|| in_array('orderconfirm',$body_classes)}
                                        {l s="Checkout"}
                                    {elseif !empty($page_title['longtitle'])}
                                        {$page_title['longtitle']}
                                    {elseif !empty($page_title['title'])}
                                        {$page_title['title']}
                                    {elseif $page_name == 'module-ph_simpleblog-list'}
                                        <p>{$meta_title}</p>
                                    {elseif $page_name == 'cms' && !empty($cms->longtitle)}
                                        <span class="text-light">{$cms->longtitle}</span>  
                                    {elseif $page_name == 'category' && !empty($category->longtitle)}
                                        <span class="text-light">{$category->longtitle}</span>  
                                    {elseif $page_name == 'module-allinone_rewards-rewards'} 
                                        {l s='My rewards account' mod='module-allinone_rewards-rewards'}
                                    {elseif $page_name == 'module-allinone_rewards-sponsorship'} 
                                        {l s='Sponsorship program' mod='module-allinone_rewards-sponsorship'}
                                    {elseif $page_name == 'order-follow'}
                                        {l s='Return Merchandise Authorization (RMA)'}
                                    {else}
                                        {substr($meta_title, 0, strrpos($meta_title, '-'))}
                                    {/if}
                                </h1>
                                {if $page_name == 'authentication' && (isset($back) && preg_match("/^http/", $back))}
                                    <h2 class="text-light">{l s="1. Sign In" mod="order"}</h2>
                                {elseif $page_name == 'order' && !empty($addresses)}
                                        <h2 class="text-light">{l s="2. Shipping & Billing" mod="order"}</h2>
                                {elseif $page_name == 'order' && !empty($HOOK_PAYMENT)}
                                        <h2 class="text-light">{l s="4. Payment" mod="order"}</h2>
                                {elseif $page_name == 'order' && !empty($delivery_option_list)}
                                        <h2 class="text-light">{l s="3. Shipping Options" mod="order"}</h2>
                                {elseif ($page_name == 'module-bankwire-payment')  || ($page_name == 'module-cheque-payment')  || ($page_name == 'module-cashondelivery-validation') || in_array('orderconfirm',$body_classes)}
                                        <h2 class="text-light">{l s="5. Confirm Order" mod="order"}</h2>
                                {elseif $page_name == 'order-confirmation'}
                                    <h2 class="text-light">{$page_title['description']}</h2>                                                                        
                                {/if}
                                <div class="introtext">
                                    {if ($page_name == 'authentication' && (isset($back) && preg_match("/^http/", $back))) || ($page_name == 'order' && !empty($addresses)) || ($page_name == 'order' && !empty($delivery_option_list)) || ($page_name == 'order' && !empty($delivery_option_list)) || ($page_name == 'order' && $HOOK_PAYMENT) || ($page_name == 'module-bankwire-payment')  || ($page_name == 'module-cheque-payment')  || ($page_name == 'module-cashondelivery-validation')|| ($page_name == 'module-paypalusa-expresscheckout')|| in_array('orderconfirm',$body_classes)}
                                        <a class="btn-back-order" href="{$link->getPageLink('order', true, NULL)}" {*onclick="window.history.back();return false;"*}>{l s="Return to shopping Cart" mod="authentication"}</a>
                                {elseif $page_name == 'pagenotfound'}
                                    {if !empty($page_title['longdescription'])}{$page_title['longdescription']}{else}{$meta_description}{/if}
                                    <form role="Search Form" method="GET" action="{$smarty.server.REQUEST_URI}" class="search_form clearfix">
                                        <div class="form-group">
                                            <input type="text" placeholder="Search" value="" id="search_query_center" name="search_query">
                                            <input type="submit" value="" name="submit_search">
                                        </div>
                                    </form>                                         
                                {elseif !empty($page_title['longdescription'])}
                                    {$page_title['longdescription']}
                                {elseif (!empty($cms->header))} 
                                    {$cms->header} 
                                {elseif $page_name == 'category'}
                                    {$category->description}
                                {elseif $page_name == 'search'}
                                    We found {if $nbProducts == 1}{l s='%d product' sprintf=$nbProducts|intval}{else}{l s='%d products' sprintf=$nbProducts|intval}{/if} matching your search for "{if isset($search_query) && $search_query}{$search_query|escape:'html':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'html':'UTF-8'}{elseif $ref}{$ref|escape:'html':'UTF-8'}{/if}"
                                    <form role="Search Form" method="GET" action="{$smarty.server.REQUEST_URI}" class="search_form clearfix">
                                        <div class="form-group">
                                            <input type="text" placeholder="Search" value="" id="search_query_center" name="search_query">
                                            <input type="submit" value="" name="submit_search">
                                        </div>
                                    </form>   
                                {elseif ($page_name == 'my-account') || ($page_name == 'history') || ($page_name == 'addresses') || ($page_name == 'identity') || ($page_name == 'module-allinone_rewards-rewards') || ($page_name == 'module-allinone_rewards-sponsorship')}
                                     <p class="fontsize18">{l s='Welcome back, '}{$cookie->customer_firstname}!</p>                       
                                {elseif ($page_name == 'address')}
                                    <p class="fontsize18">
                                        {if isset($id_address) && (isset($smarty.post.alias) || isset($address->alias))}
                                			{l s='Modify address'}
                                			{if isset($smarty.post.alias)}
                                				"{$smarty.post.alias}"
                                			{else}
                                				{if isset($address->alias)}"{$address->alias|escape:'html':'UTF-8'}"{/if}
                                			{/if}
                                		{else}
                                			{l s='To add a new address, please fill out the form below.'}
                                		{/if} 
                                    </p>
                                    
                                {elseif $page_name == 'module-ph_simpleblog-list' || $page_name == 'module-ph_simpleblog-category'}
                                    {$meta_description}
                                    <form role="Search Form Blog" method="GET" action="{$smarty.server.SCRIPT_URI}" class="search_form clearfix">
                                        <div class="form-group">
                                            <input type="text" placeholder="Search Blog" value="{$smarty.get.search}" name="search">
                                            <input type="submit" value="">
                                        </div>
                                    </form>   
                                {elseif !empty($page_title['description'])}
                                    {if $page_name != 'order-confirmation'}
                                        <p class="fontsize18">{$page_title['description']}</p>
                                    {/if}
                                {else}
                                    {$meta_description}
                                {/if}
                                </div>
                                {if $page_name == 'module-belvg_testimonials-view'} {include file="$testimonials_tpl_dir/testimonials-sort.tpl"}{/if}
                            </div>                        
    					{/if}
                        <div class="content-wrapper">
                    {/if}
					{*
                    {$HOOK_LEFT_COLUMN}
                    <div id="slider_row" class="row">
						{capture name='displayTopColumn'}{hook h='displayTopColumn'}{/capture}
						{if $smarty.capture.displayTopColumn}
							<div id="top_column" class="center_column col-xs-12 col-sm-12">{$smarty.capture.displayTopColumn}</div>
						{/if}
					</div>
					<div class="row">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">.content-wrapper > *}
	{/if}
