{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='My account'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe}
    </span>
    <span class="navigation_page">
        {l s='Your personal information'}
    </span>
{/capture}
{*<div class="box">
    <h1 class="page-subheading">
        {l s='Your personal information'}
    </h1>*}

    {include file="$tpl_dir./errors.tpl"}
    {if isset($confirmation) && $confirmation}
        <p class="alert alert-success">
            {l s='Your personal information has been successfully updated.'}
            {if isset($pwd_changed)}<br />{l s='Your password has been sent to your email:'} {$email}{/if}
        </p>
    {/if}
        {*<p class="info-title">
            {l s='Please be sure to update your personal information if it has changed.'}
        </p>*}
    <div class="info_edit">
        <form action="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" method="post" class="info-form contact-form">
            <div class="col-sm-6">
                <div class="required form-group">
                    <label for="fname">{l s='First name'}<sup>*</sup></label>
                    <input class="is_required validate form-control {if !empty($errors.firstname)}error{/if}" data-validate="isName" type="text" id="firstname" name="firstname" value="{$smarty.post.firstname}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="{$errors.firstname|strip_tags}" />
                </div>
                <div class="required form-group">
                    <label for="lastname">{l s='Last name'}<sup>*</sup></label>
                    <input class="is_required validate form-control {if !empty($errors.lastname)}error{/if}" type="text" id="lastname" name="lastname" value="{$smarty.post.lastname}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="{$errors.lastname|strip_tags}">
                </div>
                <div class="required form-group">
                    <label for="email">{l s='Email'}<sup>*</sup></label>
                    <input class="is_required validate form-control {if !empty($errors.email)}error{/if}" data-validate="isEmail" type="text" name="email" id="email" value="{$smarty.post.email}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="{$errors.email|strip_tags}" />
                </div>
                <div class="form-group birthday-group col-sm-8">
                    <label>Date of Birth</label>
                    <select name="days" id="days" class="selectpicker" data-size="5">
                        <option value="">-</option>
                        {foreach from=$days item=v}
                            <option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                        {/foreach}
                    </select>
                    <select id="months" name="months" class="selectpicker" data-size="5">
                        <option value="">-</option>
                        {foreach from=$months key=k item=v}
                            <option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v}&nbsp;</option>
                        {/foreach}
                    </select>
                    <select id="years" name="years" class="selectpicker" data-size="5">
                        <option value="">-</option>
                        {foreach from=$years item=v}
                            <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group col-sm-4 gender-group">
                    <label>Gender</label>
                    <select name="id_gender" class="selectpicker" data-size="5">
                        {foreach from=$genders key=k item=gender}
                            <option value="{$gender->id|intval}" {if isset($smarty.post.id_gender) && $smarty.post.id_gender == $gender->id}selected="selected"{/if}>{$gender->name}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-6">
                <div class="required form-group right">
                    <label for="old_passwd" class="required">
                        {l s='Current Password'}<sup>*</sup>
                    </label>
                    <input class="is_required validate form-control {if !empty($errors.old_passwd)}error{/if}" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd"  data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="{$errors.old_passwd|strip_tags}" autocomplete="off" />
                </div>
                <div class="form-group right">
                    <label for="passwd">
                        {l s='New Password'}
                    </label>
                    <input class="is_required validate form-control {if !empty($errors.passwd)}error{/if}" type="password" data-validate="isPasswd" name="passwd" id="passwd"  data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="{$errors.passwd|strip_tags}" autocomplete="off" />
                </div>
                <div class="form-group right">
                    <label for="confirmation">
                        {l s='Confirm Password'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="confirmation" id="confirmation" autocomplete="off" />
                </div>
{*
                {if isset($newsletter) && $newsletter}
                    <div class="checkbox">
                        <input type="checkbox" id="newsletter" name="newsletter" value="1" {if isset($smarty.post.newsletter) && $smarty.post.newsletter == 1} checked="checked"{/if}/>
                        <label for="newsletter" class="lbl">
                            {l s='Sign up for our newsletter!'}
                            {if isset($required_fields) && array_key_exists('newsletter', $field_required)}
                              <sup>*</sup>
                            {/if}
                        </label>
                    </div>
                {/if}
*}
            </div>
            <div class="cradio_wrapper clearfix col-xs-12">
                <h3 class="uppercase">{l s="Select Customer Group"}</h3>
                <div class="form-group сradio">
                    <input class="is_required validate form-control" type="radio" name="id_default_group" id="id_default_group1" value="3" {if ($smarty.post.id_default_group == 3)}checked="checked"{/if}/>
                    <label for="id_default_group1" class="lbl">{l s='House Account'}</label>
                </div>
                    {if (int)$smarty.post.date_st_expire == 0}
                    <div class="form-group сradio">
                        <input class="is_required validate form-control" type="radio" name="id_default_group" id="id_default_group2" value="5" {if ($smarty.post.id_default_group == 5)}checked="checked"{/if}/>
                        <label for="id_default_group2" class="lbl">{l s='Sales Team'}</label>
                    </div>
                    {/if}
                    <div class="form-group сradio">
                        <input class="is_required validate form-control" type="radio" name="id_default_group" id="id_default_group3" value="7" {if ($smarty.post.id_default_group == 7)}checked="checked"{/if}/>
                        <label for="id_default_group3" class="lbl">{l s='Refer a Friend'}</label>
                        <p class="error">{l s="*NOTE: Moving from Sales Team or Refer a Friend to “House Account” will remove all your tree members."}</p>
                        {*{l s='Rewards Program : Account Change'}*}
                    </div>
            </div>
            <div class="clearfix"></div>
            {if isset($HOOK_CUSTOMER_IDENTITY_FORM)}
		         {$HOOK_CUSTOMER_IDENTITY_FORM}
            {/if}
            <div class="form-group col-sm-4">
                <a class="btn btn-continue" href="{$link->getPageLink('my-account', true)}">
                    {l s='Back to your account'}
                </a>
            </div>
            <div class="form-group col-sm-4 col-sm-offset-4 text-right">
                <input type="submit" name="submitIdentity" class="btn black btn-save" value="{l s='Save'}">
            </div>     
            <div class="clearfix"></div>      
        </form>
    </div>

<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
{literal}
<style>
    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
    #mc_embed_signup .indicates-required {text-align: left;}
    #mc_embed_signup .unsubscribe a {color: #60123c; text-decoration: underline; text-transform: uppercase;}
    #mc_embed_signup .unsubscribe a:hover {color: red;}
</style>
{/literal}
<div id="mc_embed_signup">
    <form action="https://isomers.us12.list-manage.com/subscribe/post?u=ba2141ffdcb5aa5b1beb7612b&amp;id=52919f1667" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
            <h2 class="text-uppercase">Subscribe to our mailing list</h2>
            <div class="indicates-required text-left"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME">First Name </label>
                <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME">Last Name </label>
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->

            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ba2141ffdcb5aa5b1beb7612b_52919f1667" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
        </div>
    </form>

    <div class="unsubscribe">
        <h5>To <a href=" https://isomers.us12.list-manage.com/unsubscribe?u=ba2141ffdcb5aa5b1beb7612b&id=52919f1667">unsubscribe</a> click here</h5>
    </div>
</div>





<!--End mc_embed_signup-->


{*<p class="required">
    <sup>*</sup>{l s='Required field'}
</p>
<form action="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" method="post" class="std">
    <fieldset>
        <div class="clearfix">
            <label>{l s='Social title'}</label>
            <br />
            {foreach from=$genders key=k item=gender}
                <div class="radio-inline">
                    <label for="id_gender{$gender->id}" class="top">
                    <input type="radio" name="id_gender" id="id_gender{$gender->id}" value="{$gender->id|intval}" {if isset($smarty.post.id_gender) && $smarty.post.id_gender == $gender->id}checked="checked"{/if} />
                    {$gender->name}</label>
                </div>
            {/foreach}
        </div>
        <div class="required form-group">
            <label for="firstname" class="required">
                {l s='First name'}
            </label>
            <input class="is_required validate form-control" data-validate="isName" type="text" id="firstname" name="firstname" value="{$smarty.post.firstname}" />
        </div>
        <div class="required form-group">
            <label for="lastname" class="required">
                {l s='Last name'}
            </label>
            <input class="is_required validate form-control" data-validate="isName" type="text" name="lastname" id="lastname" value="{$smarty.post.lastname}" />
        </div>
        <div class="required form-group">
            <label for="email" class="required">
                {l s='E-mail address'}
            </label>
            <input class="is_required validate form-control" data-validate="isEmail" type="email" name="email" id="email" value="{$smarty.post.email}" />
        </div>
        <div class="form-group">
            <label>
                {l s='Date of Birth'}
            </label>
            <div class="row">
                <div class="col-xs-4">
                    <select name="days" id="days" class="form-control">
                        <option value="">-</option>
                        {foreach from=$days item=v}
                            <option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-xs-4">
                    {*
                        {l s='January'}
                        {l s='February'}
                        {l s='March'}
                        {l s='April'}
                        {l s='May'}
                        {l s='June'}
                        {l s='July'}
                        {l s='August'}
                        {l s='September'}
                        {l s='October'}
                        {l s='November'}
                        {l s='December'}
                    *}
{*                            <select id="months" name="months" class="form-control">
                                <option value="">-</option>
                                {foreach from=$months key=k item=v}
                                    <option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v}&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <select id="years" name="years" class="form-control">
                                <option value="">-</option>
                                {foreach from=$years item=v}
                                    <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="required form-group">
                    <label for="old_passwd" class="required">
                        {l s='Current Password'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd" />
                </div>
                <div class="password form-group">
                    <label for="passwd">
                        {l s='New Password'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="passwd" id="passwd" />
                </div>
                <div class="password form-group">
                    <label for="confirmation">
                        {l s='Confirmation'}
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="confirmation" id="confirmation" />
                </div>
                {if isset($newsletter) && $newsletter}
                    <div class="checkbox">
                        <label for="newsletter">
                            <input type="checkbox" id="newsletter" name="newsletter" value="1" {if isset($smarty.post.newsletter) && $smarty.post.newsletter == 1} checked="checked"{/if}/>
                            {l s='Sign up for our newsletter!'}
                            {if isset($required_fields) && array_key_exists('newsletter', $field_required)}
                              <sup> *</sup>
                            {/if}
                        </label>
                    </div>
                {/if}
                {if isset($optin) && $optin}
                    <div class="checkbox">
                        <label for="optin">
                            <input type="checkbox" name="optin" id="optin" value="1" {if isset($smarty.post.optin) && $smarty.post.optin == 1} checked="checked"{/if}/>
                            {l s='Receive special offers from our partners!'}
                            {if isset($required_fields) && array_key_exists('optin', $field_required)}
                              <sup> *</sup>
                            {/if}
                        </label>
                    </div>
                {/if}
			{if $b2b_enable}
				<h1 class="page-subheading">
					{l s='Your company information'}
				</h1>
				<div class="form-group">
					<label for="">{l s='Company'}</label>
					<input type="text" class="form-control" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{/if}" />
				</div>
				<div class="form-group">
					<label for="siret">{l s='SIRET'}</label>
					<input type="text" class="form-control" id="siret" name="siret" value="{if isset($smarty.post.siret)}{$smarty.post.siret}{/if}" />
				</div>
				<div class="form-group">
					<label for="ape">{l s='APE'}</label>
					<input type="text" class="form-control" id="ape" name="ape" value="{if isset($smarty.post.ape)}{$smarty.post.ape}{/if}" />
				</div>
				<div class="form-group">
					<label for="website">{l s='Website'}</label>
					<input type="text" class="form-control" id="website" name="website" value="{if isset($smarty.post.website)}{$smarty.post.website}{/if}" />
				</div>
			{/if}
                {if isset($HOOK_CUSTOMER_IDENTITY_FORM)}
			{$HOOK_CUSTOMER_IDENTITY_FORM}
		{/if}
                <div class="form-group">
                    <button type="submit" name="submitIdentity" class="btn btn-default button button-medium">
                        <span>{l s='Save'}<i class="icon-chevron-right right"></i></span>
                    </button>
                </div>
            </fieldset>
        </form> <!-- .std -->
    {/if}
</div>
<ul class="footer_links clearfix">
	<li>
        <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)}">
            <span>
                <i class="icon-chevron-left"></i>{l s='Back to your account'}
            </span>
        </a>
    </li>
	<li>
        <a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
            <span>
                <i class="icon-chevron-left"></i>{l s='Home'}
            </span>
        </a>
    </li>
</ul>*}
