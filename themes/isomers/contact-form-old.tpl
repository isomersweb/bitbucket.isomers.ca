{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Contact'}{/capture}
{if isset($confirmation)}
	<p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
{elseif isset($alreadySent)}
	<p class="alert alert-warning">{l s='Your message has already been sent.'}</p>
{else}
	{include file="$tpl_dir./errors.tpl"}
    <form class="ajax_form contact-form" method="post" action="{$request_uri}" role="Contact">
       <input type="hidden" name="id_contact" value="{$contacts.0.id_contact|intval}" />
        <h3 class="uppercase">Send Us a message</h3>
        <div class="form-group col-sm-6">
            <label for="fname">First Name*</label>
            <input type="text" id="fname" class="form-control is_required validate" name="fname" data-validate="isName" value="{if isset($logged) AND $logged}{$cookie->customer_firstname|escape:'html':'UTF-8'}{else}{if isset($smarty.post.fname)}{$smarty.post.fname|escape:'htmlall':'UTF-8'}{/if}{/if}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="" title="First Name">
        </div>
        <div class="form-group col-sm-6 right">
            <label for="lname">Last Name*</label>
            <input type="text" id="lname" class="form-control is_required validate" name="lname" data-validate="isName" value="{if isset($logged) AND $logged}{$cookie->customer_lastname|escape:'html':'UTF-8'}{else}{if isset($smarty.post.lname)}{$smarty.post.lname|escape:'htmlall':'UTF-8'}{/if}{/if}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="" title="Last Name">
        </div>
        <div class="form-group col-sm-6">
            <label for="email">Email Address*</label>
            <input type="email" id="email" class="form-control is_required validate" name="email" data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="" title="Email Address">
        </div>
        <div class="form-group col-sm-6 right">
            <label for="phone">Phone Number</label>
            <input type="text" id="phone" class="form-control is_required validate" name="phone" data-validate="isPhoneNumber" value="{$phone|escape:'html':'UTF-8'}" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="" title="Phone Number">
        </div>
        <div class="form-group col-sm-6">
            <label for="country">Country</label>
            <select name="country" id="country" class="selectpicker" data-size="5">
                {include file="$tpl_dir/countries_list.tpl" }
            </select>
        </div>
        <div class="form-group col-sm-6 right">
            <label for="region">Region</label>
            <input type="text" id="region" class="form-control is_required" name="region" value="" data-placement="bottom" data-toggle="tooltip" placeholder="" data-original-title="" title="Region">
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="message">Comments*</label>
            <textarea name="message" class="form-control is_required validate" id="message" placeholder="" data-original-title="" data-toggle="tooltip" data-placement="bottom" title="Message">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
        </div>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<div class="g-recaptcha pull-left" data-sitekey="6LdLLw0TAAAAAFxlLaCJXwofqf7h86R4QMScbEuH"></div>
		<div class="form-group text-right">
            <input type="submit" value="Submit" class="btn black btn-submit" name="submitMessage" id="submitMessage" data-original-title="" title="">
        </div>
		<div class="clearfix"></div>
    </form>
    <link href="{$css_dir}jquery.jgrowl.css" rel="stylesheet" type="text/css" media="{$media}" />
    <script src="{$js_dir}jquery.jgrowl.js"></script>
    <script src="{$js_dir}jquery.form.min.js"></script>
    <script src="{$js_dir}form.validate.js"></script>
    
{/if}  


	{*<form action="{$request_uri}" method="post" class="contact-form-box" enctype="multipart/form-data">
		<fieldset>
			<h3 class="page-subheading">{l s='send a message'}</h3>
			<div class="clearfix">
				<div class="col-xs-12 col-md-3">
					<div class="form-group selector1">
						<label for="id_contact">{l s='Subject Heading'}</label>
					{if isset($customerThread.id_contact) && $customerThread.id_contact && $contacts|count}
							{assign var=flag value=true}
							{foreach from=$contacts item=contact}
								{if $contact.id_contact == $customerThread.id_contact}
									<input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contact.name|escape:'html':'UTF-8'}" readonly="readonly" />
									<input type="hidden" name="id_contact" value="{$contact.id_contact|intval}" />
									{$flag=false}
								{/if}
							{/foreach}
							{if $flag && isset($contacts.0.id_contact)}
									<input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contacts.0.name|escape:'html':'UTF-8'}" readonly="readonly" />
									<input type="hidden" name="id_contact" value="{$contacts.0.id_contact|intval}" />
							{/if}
					</div>
					{else}
						<select id="id_contact" class="form-control" name="id_contact">
							<option value="0">{l s='-- Choose --'}</option>
							{foreach from=$contacts item=contact}
								<option value="{$contact.id_contact|intval}"{if isset($smarty.request.id_contact) && $smarty.request.id_contact == $contact.id_contact} selected="selected"{/if}>{$contact.name|escape:'html':'UTF-8'}</option>
							{/foreach}
						</select>
					</div>
						<p id="desc_contact0" class="desc_contact{if isset($smarty.request.id_contact)} unvisible{/if}">&nbsp;</p>
						{foreach from=$contacts item=contact}
							<p id="desc_contact{$contact.id_contact|intval}" class="desc_contact contact-title{if !isset($smarty.request.id_contact) || $smarty.request.id_contact|intval != $contact.id_contact|intval} unvisible{/if}">
								<i class="icon-comment-alt"></i>{$contact.description|escape:'html':'UTF-8'}
							</p>
						{/foreach}
					{/if}
					<p class="form-group">
						<label for="email">{l s='Email address'}</label>
						{if isset($customerThread.email)}
							<input class="form-control grey" type="text" id="email" name="from" value="{$customerThread.email|escape:'html':'UTF-8'}" readonly="readonly" />
						{else}
							<input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}" />
						{/if}
					</p>
					{if !$PS_CATALOG_MODE}
						{if (!isset($customerThread.id_order) || $customerThread.id_order > 0)}
							<div class="form-group selector1">
								<label>{l s='Order reference'}</label>
								{if !isset($customerThread.id_order) && isset($is_logged) && $is_logged}
									<select name="id_order" class="form-control">
										<option value="0">{l s='-- Choose --'}</option>
										{foreach from=$orderList item=order}
											<option value="{$order.value|intval}"{if $order.selected|intval} selected="selected"{/if}>{$order.label|escape:'html':'UTF-8'}</option>
										{/foreach}
									</select>
								{elseif !isset($customerThread.id_order) && empty($is_logged)}
									<input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.id_order) && $customerThread.id_order|intval > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order) && !empty($smarty.post.id_order)}{$smarty.post.id_order|escape:'html':'UTF-8'}{/if}{/if}" />
								{elseif $customerThread.id_order|intval > 0}
									<input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.reference) && $customerThread.reference}{$customerThread.reference|escape:'html':'UTF-8'}{else}{$customerThread.id_order|intval}{/if}" readonly="readonly" />
								{/if}
							</div>
						{/if}
						{if isset($is_logged) && $is_logged}
							<div class="form-group selector1">
								<label class="unvisible">{l s='Product'}</label>
								{if !isset($customerThread.id_product)}
									{foreach from=$orderedProductList key=id_order item=products name=products}
										<select name="id_product" id="{$id_order}_order_products" class="unvisible product_select form-control"{if !$smarty.foreach.products.first} style="display:none;"{/if}{if !$smarty.foreach.products.first} disabled="disabled"{/if}>
											<option value="0">{l s='-- Choose --'}</option>
											{foreach from=$products item=product}
												<option value="{$product.value|intval}">{$product.label|escape:'html':'UTF-8'}</option>
											{/foreach}
										</select>
									{/foreach}
								{elseif $customerThread.id_product > 0}
									<input  type="hidden" name="id_product" id="id_product" value="{$customerThread.id_product|intval}" readonly="readonly" />
								{/if}
							</div>
						{/if}
					{/if}
					{if $fileupload == 1}
						<p class="form-group">
							<label for="fileUpload">{l s='Attach File'}</label>
							<input type="hidden" name="MAX_FILE_SIZE" value="{if isset($max_upload_size) && $max_upload_size}{$max_upload_size|intval}{else}2000000{/if}" />
							<input type="file" name="fileUpload" id="fileUpload" class="form-control" />
						</p>
					{/if}
				</div>
				<div class="col-xs-12 col-md-9">
					<div class="form-group">
						<label for="message">{l s='Message'}</label>
						<textarea class="form-control" id="message" name="message">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
					</div>
				</div>
			</div>
			<div class="submit">
				<button type="submit" name="submitMessage" id="submitMessage" class="button btn btn-default button-medium"><span>{l s='Send'}<i class="icon-chevron-right right"></i></span></button>
			</div>
		</fieldset>
	</form>*}
{*{addJsDefL name='contact_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='contact_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}*}
