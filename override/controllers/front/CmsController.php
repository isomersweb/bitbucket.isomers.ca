<?php
Class CmsController extends CmsControllerCore
{
	/*
    * module: w4wfaq
    * date: 2018-08-31 12:06:35
    * version: 1.2
    */
    public function initContent()
	{
		parent::initContent();
		if(Module::isInstalled('w4wfaq')&&Module::isEnabled('w4wfaq'))
		{
			$mod = Module::getInstanceByName('w4wfaq');
			$extra_content = $mod->getCmsContent($this->cms->id,$this->context->language->id);
			$this->cms->content.=$extra_content;
			$this->context->smarty->assign(array('cms'=>$this->cms));
		}
	}
}