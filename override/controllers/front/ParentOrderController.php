<?php
class ParentOrderController extends ParentOrderControllerCore
{
    /*
    * module: currencybycountry
    * date: 2017-09-28 03:14:33
    * version: 1.0.0
    */
    protected function _processCarrier()
    {
        if (!Module::isInstalled('currencybycountry')) {
            return ParentOrderControllerCore::_processCarrier();
        }
        $cbc_module = Module::getInstanceByName('currencybycountry');
        if (!$cbc_module->active) {
            return ParentOrderControllerCore::_processCarrier();
        }
        $id_country = (int)Country::getByIso(Tools::strtoupper($cbc_module->getCookieGeoip()));
        if ($this->context->cart && (int)$this->context->cart->id_address_delivery > 0) {
            $delivery = new Address((int)$this->context->cart->id_address_delivery);
            if (Validate::isLoadedObject($delivery) && (int)$delivery->id_country > 0) {
                if ($products = $this->context->cart->getProducts()) {
                    $ids_products = array();
                    foreach ($products as &$product) {
                        if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                            SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`
                            WHERE
                                id_country = '.(int)$delivery->id_country.' AND
                                id_shop = '.(int)$this->context->shop->id.' AND
                                id_product='.(int)$product['id_product'])) {
                            foreach ($result as $row) {
                                if (!in_array($product['id_product'], $ids_products)) {
                                    $ids_products[] = $product['id_product'];
                                    $this->errors[] = Tools::displayError($product['name'].': '.$cbc_module->countryProductError(), !Tools::getValue('ajax'));
                                }
                            }
                        }
                    }
                    if (count($ids_products) > 0) {
                        return false;
                    }
                }
            }
        }
        return ParentOrderControllerCore::_processCarrier();
    }
    /*
    * module: currencybycountry
    * date: 2017-09-28 03:14:33
    * version: 1.0.0
    */
    protected function _assignPayment()
    {
        if (!Module::isInstalled('currencybycountry')) {
            return ParentOrderControllerCore::_assignPayment();
        }
        $cbc_module = Module::getInstanceByName('currencybycountry');
        if (!$cbc_module->active) {
            return ParentOrderControllerCore::_assignPayment();
        }
        $cbc_module = Module::getInstanceByName('currencybycountry');
        $id_country = (int)Country::getByIso(Tools::strtoupper($cbc_module->getCookieGeoip()));
        if ($this->context->cart && (int)$this->context->cart->id_address_delivery > 0) {
            $delivery = new Address((int)$this->context->cart->id_address_delivery);
            if (Validate::isLoadedObject($delivery) && (int)$delivery->id_country > 0) {
                if ($products = $this->context->cart->getProducts()) {
                    $result = '';
                    $ids_products = array();
                    foreach ($products as &$product) {
                        if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                            SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`
                            WHERE
                                id_country = '.(int)$delivery->id_country.' AND
                                id_shop = '.(int)$this->context->shop->id.' AND
                                id_product='.(int)$product['id_product'])) {
                            foreach ($result as $row) {
                                if (!in_array($product['id_product'], $ids_products)) {
                                    $ids_products[] = $product['id_product'];
                                    $result .= '<p class="warning">'.Tools::displayError($product['name'].': '.$cbc_module->countryProductError()).'</p>';
                                }
                            }
                        }
                    }
                    if (count($ids_products) > 0) {
                        $this->context->smarty->assign(array(
                            'HOOK_TOP_PAYMENT' => '',
                            'HOOK_PAYMENT' => $result,
                        ));
                        return;
                    }
                }
            }
        }
        return ParentOrderControllerCore::_assignPayment();
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public function init()
    {
        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            $quantityDiscount = new QuantityDiscountRule();
            $this->isLogged = $this->context->customer->id && Customer::customerIdExistsStatic((int)$this->context->cookie->id_customer);
            FrontController::init();
             
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            $this->nbProducts = $this->context->cart->nbProducts();
            if (!$this->context->customer->isLogged(true) && $this->useMobileTheme() && Tools::getValue('step')) {
                Tools::redirect($this->context->link->getPageLink('authentication', true, (int)$this->context->language->id));
            }
            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 0 && Dispatcher::getInstance()->getController() != 'order') {
                Tools::redirect('index.php?controller=order');
            }
            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1 && Dispatcher::getInstance()->getController() != 'orderopc') {
                if (Tools::getIsset('step') && Tools::getValue('step') == 3) {
                    Tools::redirect('index.php?controller=order-opc&isPaymentStep=true');
                }
                Tools::redirect('index.php?controller=order-opc');
            }
            if (Configuration::get('PS_CATALOG_MODE')) {
                $this->errors[] = Tools::displayError('This store has not accepted your new order.');
            }
            if (Tools::isSubmit('submitReorder') && $id_order = (int)Tools::getValue('id_order')) {
                $oldCart = new Cart(Order::getCartIdStatic($id_order, $this->context->customer->id));
                $duplication = $oldCart->duplicate();
                if (!$duplication || !Validate::isLoadedObject($duplication['cart'])) {
                    $this->errors[] = Tools::displayError('Sorry. We cannot renew your order.');
                } else if (!$duplication['success']) {
                    $this->errors[] = Tools::displayError('Some items are no longer available, and we are unable to renew your order.');
                } else {
                    $this->context->cookie->id_cart = $duplication['cart']->id;
                    $context = $this->context;
                    $context->cart = $duplication['cart'];
                    CartRule::autoAddToCart($context);
                    $this->context->cookie->write();
                    $quantityDiscount->createAndRemoveRules();
                    if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                        Tools::redirect('index.php?controller=order-opc');
                    }
                    Tools::redirect('index.php?controller=order');
                }
            }
            if ($this->nbProducts) {
                if (CartRule::isFeatureActive()) {
                    if (Tools::isSubmit('submitAddDiscount')) {
                        if (!($code = trim(Tools::getValue('discount_name')))) {
                            $this->errors[] = Tools::displayError('You must enter a voucher code.');
                        } elseif (!Validate::isCleanHtml($code)) {
                            $this->errors[] = Tools::displayError('The voucher code is invalid.');
                        } else {
                            if (($quantityDiscount = new quantityDiscountRule(QuantityDiscountRule::getQuantityDiscountRuleByCode($code))) && Validate::isLoadedObject($quantityDiscount)) {
                                if ($quantityDiscount->createAndRemoveRules($code)) {
                                    if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                                        Tools::redirect('index.php?controller=order-opc&addingCartRule=1');
                                    }
                                    Tools::redirect('index.php?controller=order&addingCartRule=1');
                                } else {
                                    $this->errors[] = Tools::displayError('You cannot use this voucher');
                                }
                            } elseif (($cartRule = new CartRule(CartRule::getIdByCode($code))) && Validate::isLoadedObject($cartRule)) {
                                if ($quantityDiscount->cartRuleGeneratedByAQuantityDiscountRuleCode($code)) {
                                    $this->errors[] = Tools::displayError('The voucher code is invalid.');
                                } elseif ($error = $cartRule->checkValidity($this->context, false, true)) {
                                    $this->errors[] = $error;
                                } else {
                                    $this->context->cart->addCartRule($cartRule->id);
                                    CartRule::autoAddToCart($this->context);
                                    if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                                        Tools::redirect('index.php?controller=order-opc&addingCartRule=1');
                                    }
                                    Tools::redirect('index.php?controller=order&addingCartRule=1');
                                }
                            } else {
                                $this->errors[] = Tools::displayError('This voucher does not exists.');
                            }
                        }
                        $this->context->smarty->assign(array(
                            'errors' => $this->errors,
                            'discount_name' => Tools::safeOutput($code)
                        ));
                    } elseif (($id_cart_rule = (int)Tools::getValue('deleteDiscount')) && Validate::isUnsignedId($id_cart_rule)) {
                        if (!QuantityDiscountRule::removeQuantityDiscountCartRule($id_cart_rule, (int)$this->context->cart->id)) {
                            $this->context->cart->removeCartRule($id_cart_rule);
                        }
                        CartRule::autoAddToCart($this->context);
                        Tools::redirect('index.php?controller=order-opc');
                    }
                }
                
                if ($this->context->cart->isVirtualCart()) {
                    $this->setNoCarrier();
                }
            }
            $this->context->smarty->assign('back', Tools::safeOutput(Tools::getValue('back')));
        } else {
            parent::init();
        }
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public function setMedia()
    {
        parent::setMedia();
        if (Module::isEnabled('quantitydiscountpro') && in_array((int)Tools::getValue('step'), array(0, 2, 3)) || Configuration::get('PS_ORDER_PROCESS_TYPE')) {
            $this->addJS(_PS_MODULE_DIR_.'quantitydiscountpro/views/js/qdp.js', 'all');
        }
    }
}
