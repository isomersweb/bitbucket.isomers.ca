<?php

class AdminProductsController extends AdminProductsControllerCore
{

    public function postProcess()
    {
        parent::postProcess();
        if (array_key_exists('product_recurringproduct', $_GET)) {
                    $this->processRecurring();
        }
    }

    public function processRecurring()
    {
        $this->loadObject(true);
        if (!Validate::isLoadedObject($this->object)) {
            return false;
        }
        if (!array_key_exists('product_recurring', $this->object)) {
            throw new PrestaShopException('property "product_recurring" is missing in object '.get_class($this));
        }

        // Update only active field
        $this->object->setFieldsToUpdate(array('product_recurring' => true));

        // Update active status on object
        $this->object->product_recurring = !(int)$this->object->product_recurring;

        //d($this->object->product_recurring);
        return $this->object->update(false);        
    }

/*    
    public function processStatus()
    {        
        $this->loadObject(true);
        if (!Validate::isLoadedObject($this->object)) {
            return false;
        }
        if (($error = $this->object->validateFields(false, true)) !== true) {
            $this->errors[] = $error;
        }
        if (($error = $this->object->validateFieldsLang(false, true)) !== true) {
            $this->errors[] = $error;
        }

        if (count($this->errors)) {
            return false;
        }

        $res = parent::processStatus();

        $query = trim(Tools::getValue('bo_query'));
        $searchType = (int)Tools::getValue('bo_search_type');

        if ($query) {
            $this->redirect_after = preg_replace('/[\?|&](bo_query|bo_search_type)=([^&]*)/i', '', $this->redirect_after);
            $this->redirect_after .= '&bo_query='.$query.'&bo_search_type='.$searchType;
        }

        return $res;
    }   
*/

    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);        
        
        if ($this->isTabSubmitted('Prices')) {
            //$object->available_country = (int)Tools::getValue('available_country');   
            if (isset($_POST['main_offer'])) {
                if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product SET main_offer = "0" WHERE main_offer = "1"'));
                $object->main_offer = (int)Tools::getValue('main_offer');
            }
        }
    } 
    
/*    public function processAdd()
    {
        parent::processAdd(); 
        
        if (empty($this->errors)) {
            if ($this->isTabSubmitted('Informations') && (int)Tools::getValue('type_product') == 1) {
                $languages = Language::getLanguages(false);
                foreach($languages as $language) {
                    $products_list = Pack::getItemTable((int)$this->object->id, $language['id_lang']);
                    foreach ($products_list as $product) {
                        $this->object->description[$language['id_lang']] .= $product['description'];
                        $this->object->description_short[$language['id_lang']] .= $product['description_short'];
                    }
                }
                $this->object->save();
            }            
        }
    } */
	
// Added below - 2018 11 22 by Kang from (https://premiumpresta.com/blog/export-products-for-csv-import-prestashop-1-6/)
public function processExport($text_delimiter = '"')
    {
        $this->_orderBy = 'id_product';
 
        // Reference is Reference #
        $this->_select .= ', a.`reference`, ';
        $this->fields_list['reference']['title'] = 'Reference #';
        
        // YOUR EXPORT FIELDS CODE HERE
		// Image URLs (x,y,z...)
		$this->fields_list['image'] = array(
			'title' => $this->l('Image URLs (x,y,z...)'),
			'callback' => 'exportAllImagesLink'
		);

		// Categories (x,y,z...)
		$this->fields_list['name_category'] = array(
			'title' => $this->l('Categories (x,y,z...)'),
			'callback' => 'exportAllProductCategories'
		);			
				
        parent::processExport($text_delimiter);
    }

public static function exportAllImagesLink($cover, $row, $delimiter = ',')
{
    if (empty($row) || empty($row['id_product']) || empty($row['id_image'])) {
        return;
    }
 
    $id_product = (int) $row['id_product'];
    $id_shop = Context::getContext()->shop->id;
 
    $query = new DbQuery();
    $query->select('i.id_image')->from('image', 'i');
    $query->leftJoin('image_shop', 'is', 'i.id_image = is.id_image AND is.id_shop = ' . $id_shop);
    $query->where('i.id_product = ' . $id_product)->orderBy('i.cover DESC, i.position ASC');
    $images = Db::getInstance()->executeS($query);
 
    foreach ($images as $image) {
        if (Configuration::get('PS_LEGACY_IMAGES')) {
            $links[] = Tools::getShopDomain(true) . _THEME_PROD_DIR_ . $id_product . '-' . $image['id_image'] . '.jpg';
        } else {
            $links[] = Tools::getShopDomain(true) . _THEME_PROD_DIR_ . Image::getImgFolderStatic($image['id_image']) . $image['id_image'] . '.jpg';
        }
    }
 
    return implode($delimiter, $links);
}	
	
public static function exportAllProductCategories($defaultCategory, $row, $delimiter = ',')
{
    if (empty($row) || empty($row['id_product'])) return;
 
    $id_product = (int) $row['id_product'];
    $id_lang = Context::getContext()->language->id;
    $id_shop = Context::getContext()->shop->id;
 
    $query = new DbQuery();
    $query->select('cl.name')->from('category_lang', 'cl');
    $query->leftJoin('category_shop', 'cs', 'cl.id_category = cs.id_category AND cs.id_shop = ' . $id_shop);
    $query->leftJoin('category_product', 'cp', 'cl.id_category = cp.id_category AND cp.id_product = ' . $id_product);
    $query->leftJoin('product', 'p', 'cp.id_product = p.id_product');
    $query->where('cl.id_lang = ' . $id_lang . ' AND p.id_category_default != cl.id_category');
 
    $categories = array($defaultCategory); // the first category is the default one
    foreach (Db::getInstance()->executeS($query) as $category) {
        $categories[] = $category['name'];
    }
 
    return implode($delimiter, $categories);
}	

public static function exportAllProductCategoriesId($defaultCategory, $row, $delimiter = ',')
{
    if (empty($row) || empty($row['id_product'])) return;
 
    $id_product = (int) $row['id_product'];
    $id_shop = Context::getContext()->shop->id;
 
    $query = new DbQuery();
    $query->select('c.id_category, p.id_category_default')->from('category', 'c');
    $query->leftJoin('category_shop', 'cs', 'c.id_category = cs.id_category AND cs.id_shop = ' . $id_shop);
    $query->leftJoin('category_product', 'cp', 'c.id_category = cp.id_category AND cp.id_product = ' . $id_product);
    $query->leftJoin('product', 'p', 'cp.id_product = p.id_product');
    $query->where('p.id_category_default != c.id_category');
 
    $categories = array();
    foreach (Db::getInstance()->executeS($query) as $category) {
        if (!count($categories)) {
            $categories[] = $category['id_category_default']; // the first category is the default one
        }
 
        $categories[] = $category['id_category'];
    }
 
    return implode($delimiter, $categories);
}	
	
	
}