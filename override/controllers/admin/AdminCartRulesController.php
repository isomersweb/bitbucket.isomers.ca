<?php
class AdminCartRulesController extends AdminCartRulesControllerCore
{
    public function displayAjaxSearchCartRuleVouchers()
    {
        $found = false;
        $id_customer = Tools::getValue('customer');
        $sql_base = 'SELECT cr.*, crl.*
						FROM '._DB_PREFIX_.'cart_rule cr
						LEFT JOIN '._DB_PREFIX_.'cart_rule_lang crl ON (cr.id_cart_rule = crl.id_cart_rule AND crl.id_lang = '.(int)$this->context->language->id.')
                        WHERE cr.id_customer = '. $id_customer . ' AND cr.quantity > 0  AND cr.date_to > NOW() ORDER BY cr.date_from DESC';
        if ($vouchers = Db::getInstance()->executeS($sql_base)) {
            $found = true;
        }
        echo Tools::jsonEncode(array('found' => $found, 'vouchers' => $vouchers));
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            foreach ($this->_list as $key => &$row) {
                if (QuantityDiscountRule::isQuantityDiscountRule($row['id_cart_rule'])) {
                    unset($this->_list[$key]);
                }
            }
            $this->_listTotal = count($this->_list);
        }
    }
}
