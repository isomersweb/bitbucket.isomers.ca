<?php
class AdminCartsController extends AdminCartsControllerCore
{
    public function displayAjaxSearchCarts()
    {
        $id_customer = (int)Tools::getValue('id_customer');
        $carts = Cart::getCustomerCarts((int)$id_customer, true, 20);
        $orders = Order::getCustomerOrders((int)$id_customer, false, null, 20);
        $customer = new Customer((int)$id_customer);
        if (count($carts)) {
            foreach ($carts as $key => &$cart) {
                $cart_obj = new Cart((int)$cart['id_cart']);
                if ($cart['id_cart'] == $this->context->cart->id || !Validate::isLoadedObject($cart_obj) || $cart_obj->OrderExists()) {
                    unset($carts[$key]);
                }
                $currency = new Currency((int)$cart['id_currency']);
                $cart['total_price'] = Tools::displayPrice($cart_obj->getOrderTotal(), $currency);
            }
        }
        if (count($orders)) {
            foreach ($orders as &$order) {
                $order['total_paid_real'] = Tools::displayPrice($order['total_paid_real'], $currency);
            }
        }
        if ($orders || $carts) {
            $to_return = array_merge($this->ajaxReturnVars(),
                                            array('carts' => $carts,
                                                     'orders' => $orders,
                                                     'found' => true));
        } else {
            $to_return = array_merge($this->ajaxReturnVars(), array('found' => false));
        }
        echo Tools::jsonEncode($to_return);
    }
    public function ajaxProcessupdateFreeShipping()
    {
        if ($this->tabAccess['edit'] === '1') {
            $cart = new Cart((int)$this->context->cart->id);
            $cart_rules = $cart->getCartRules();
            if (is_array($cart_rules)) {
                if (Tools::getValue('free_shipping') == 1) {
                    $free_shipping = true;
                } else {
                    $free_shipping = false;
                }
                foreach ($cart_rules as $cart_rule) {
                    if (strpos($cart_rule['code'], 'SHIP') !== false && $free_shipping == true) {
                    }
                }
            }
        }
        parent::ajaxProcessupdateFreeShipping();
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public function ajaxProcessUpdateDeliveryOption()
    {
        if (Module::isEnabled('quantitydiscountpro')) {
            $delivery_option = Tools::getValue('delivery_option');
            if ($delivery_option !== false) {
                $this->context->cart->setDeliveryOption(array($this->context->cart->id_address_delivery => $delivery_option));
            }
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            $quantityDiscount = new QuantityDiscountRule();
            $quantityDiscount->createAndRemoveRules();
        }
        echo parent::ajaxProcessUpdateDeliveryOption();
    }
}