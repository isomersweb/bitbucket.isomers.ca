<?php

class AdminProductsController extends AdminProductsControllerCore
{

    public function postProcess()
    {
        parent::postProcess();
        if (array_key_exists('product_recurringproduct', $_GET)) {
                    $this->processRecurring();
        }
    }

    public function processRecurring()
    {
        $this->loadObject(true);
        if (!Validate::isLoadedObject($this->object)) {
            return false;
        }
        if (!array_key_exists('product_recurring', $this->object)) {
            throw new PrestaShopException('property "product_recurring" is missing in object '.get_class($this));
        }

        // Update only active field
        $this->object->setFieldsToUpdate(array('product_recurring' => true));

        // Update active status on object
        $this->object->product_recurring = !(int)$this->object->product_recurring;

        //d($this->object->product_recurring);
        return $this->object->update(false);        
    }

/*    
    public function processStatus()
    {        
        $this->loadObject(true);
        if (!Validate::isLoadedObject($this->object)) {
            return false;
        }
        if (($error = $this->object->validateFields(false, true)) !== true) {
            $this->errors[] = $error;
        }
        if (($error = $this->object->validateFieldsLang(false, true)) !== true) {
            $this->errors[] = $error;
        }

        if (count($this->errors)) {
            return false;
        }

        $res = parent::processStatus();

        $query = trim(Tools::getValue('bo_query'));
        $searchType = (int)Tools::getValue('bo_search_type');

        if ($query) {
            $this->redirect_after = preg_replace('/[\?|&](bo_query|bo_search_type)=([^&]*)/i', '', $this->redirect_after);
            $this->redirect_after .= '&bo_query='.$query.'&bo_search_type='.$searchType;
        }

        return $res;
    }   
*/

    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);        
        
        if ($this->isTabSubmitted('Prices')) {
            //$object->available_country = (int)Tools::getValue('available_country');   
            if (isset($_POST['main_offer'])) {
                if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product SET main_offer = "0" WHERE main_offer = "1"'));
                $object->main_offer = (int)Tools::getValue('main_offer');
            }
        }
    } 
    
/*    public function processAdd()
    {
        parent::processAdd(); 
        
        if (empty($this->errors)) {
            if ($this->isTabSubmitted('Informations') && (int)Tools::getValue('type_product') == 1) {
                $languages = Language::getLanguages(false);
                foreach($languages as $language) {
                    $products_list = Pack::getItemTable((int)$this->object->id, $language['id_lang']);
                    foreach ($products_list as $product) {
                        $this->object->description[$language['id_lang']] .= $product['description'];
                        $this->object->description_short[$language['id_lang']] .= $product['description_short'];
                    }
                }
                $this->object->save();
            }            
        }
    } */
}