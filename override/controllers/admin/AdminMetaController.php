<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Meta $object
 */
class AdminMetaController extends AdminMetaControllerCore
{
    

    public function renderForm()
    {
        $files = Meta::getPages(true, ($this->object->page ? $this->object->page : false));

        $is_index = false;
        if (is_object($this->object) && is_array($this->object->url_rewrite) && count($this->object->url_rewrite)) {
            foreach ($this->object->url_rewrite as $rewrite) {
                if ($is_index != true) {
                    $is_index = ($this->object->page == 'index' && empty($rewrite)) ? true : false;
                }
            }
        }

        $pages = array(
            'common' => array(
                'name' => $this->l('Default pages'),
                'query' => array(),
            ),
            'module' => array(
                'name' => $this->l('Modules pages'),
                'query' => array(),
            ),
        );

        foreach ($files as $name => $file) {
            $k = (preg_match('#^module-#', $file)) ? 'module' : 'common';
            $pages[$k]['query'][] = array(
                'id' => $file,
                'page' => $name,
            );
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Meta tags'),
                'icon' => 'icon-tags'
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'id_meta',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Page'),
                    'name' => 'page',

                    'options' => array(
                        'optiongroup' => array(
                            'label' => 'name',
                            'query' => $pages,
                        ),
                        'options' => array(
                            'id' => 'id',
                            'name' => 'page',
                            'query' => 'query',
                        ),
                    ),
                    'hint' => $this->l('Name of the related page.'),
                    'required' => true,
                    'empty_message' => '<p>'.$this->l('There is no page available!').'</p>',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Page title'),
                    'name' => 'title',
                    'lang' => true,
                    'hint' => array(
                        $this->l('Title of this page.'),
                        $this->l('Invalid characters:').' &lt;&gt;;=#{}'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Long title'),
                    'name' => 'longtitle',
                    'lang' => true,
                    'hint' => array(
                        $this->l('Header title of this page.'),
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta description'),
                    'name' => 'description',
                    'lang' => true,
                    'hint' => array(
                        $this->l('A short description of your shop.'),
                        $this->l('Invalid characters:').' &lt;&gt;;=#{}'
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Extended description'),
                    'name' => 'longdescription',
                    'autoload_rte' => true,
                    'lang' => true,
                    'rows' => 5,
                    'cols' => 40,
                    'hint' => array(
                        $this->l('description of this page.'),
                    )
                ),
                array(
                    'type' => 'tags',
                    'label' => $this->l('Meta keywords'),
                    'name' => 'keywords',
                    'lang' => true,
                    'hint' =>  array(
                        $this->l('List of keywords for search engines.'),
                        $this->l('To add tags, click in the field, write something, and then press the "Enter" key.'),
                        $this->l('Invalid characters:').' &lt;&gt;;=#{}'
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Rewritten URL'),
                    'name' => 'url_rewrite',
                    'lang' => true,
                    'required' => true,
                    'disabled' => (bool)$is_index,
                    'hint' => array(
                        $this->l('For instance, "contacts" for http://example.com/shop/contacts to redirect to http://example.com/shop/contact-form.php'),
                        $this->l('Only letters and hyphens are allowed.'),
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
        return AdminController::renderForm();
    } 
}