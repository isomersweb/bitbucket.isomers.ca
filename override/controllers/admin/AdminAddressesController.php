<?php

class AdminAddressesController extends AdminAddressesControllerCore
{
    public function __construct()
    {
        parent::__construct();
        
        $this->fields_list['phone'] = array('title' => $this->l('Phone'), 'filter_key' => 'a!phone');   
        $this->fields_list['phone_mobile'] = array('title' => $this->l('Mobile'), 'filter_key' => 'a!phone_mobile');   
    }
}