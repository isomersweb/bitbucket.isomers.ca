<?php

class AdminRangeWeightController extends AdminRangeWeightControllerCore
{
    public function postProcess()
    {
        $id = (int)Tools::getValue('id_'.$this->table);
        
        if (Tools::getValue('submitAdd'.$this->table)) {
            if (Tools::getValue('delimiter1') >= Tools::getValue('delimiter2')) {
                $this->errors[] = Tools::displayError('Invalid range');
            } elseif (!$id && RangeWeight::rangeExist((int)Tools::getValue('id_carrier'), (float)Tools::getValue('delimiter1'), (float)Tools::getValue('delimiter2'))) {
                $this->errors[] = Tools::displayError('The range already exists');
            //} elseif (RangeWeight::isOverlapping((int)Tools::getValue('id_carrier'), (float)Tools::getValue('delimiter1'), (float)Tools::getValue('delimiter2'), ($id ? (int)$id : null))) {
              //  $this->errors[] = Tools::displayError('Error: Ranges are overlapping');
            } elseif (!count($this->errors)) {
                parent::postProcess();
            }
        } else {
            parent::postProcess();
        }
    }
}
