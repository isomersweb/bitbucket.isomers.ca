<?php

class Country extends CountryCore
{
    
    public function checkZipCode($zip_code)
    {
        if ($_REQUEST['country'] == 21) return true;    
        $zip_regexp = '/^'.$this->zip_code_format.'$/ui';
        $zip_regexp = str_replace(' ', '( |)', $zip_regexp);
        $zip_regexp = str_replace('-', '(-|)', $zip_regexp);
        $zip_regexp = str_replace('N', '[0-9]', $zip_regexp);
        $zip_regexp = str_replace('L', '[a-zA-Z]', $zip_regexp);
        $zip_regexp = str_replace('C', $this->iso_code, $zip_regexp);

        return (bool)preg_match($zip_regexp, $zip_code);
    }
    
    
}