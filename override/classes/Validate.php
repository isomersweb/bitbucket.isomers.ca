<?php
class Validate extends ValidateCore {
    
    public static function isPostCode($postcode)
    {
        if (Tools::getValue('country') == 21) return true;    
        return empty($postcode) || preg_match('/^[a-zA-Z 0-9-]+$/', $postcode);
    }

    /**
     * Check for zip code format validity
     *
     * @param string $zip_code zip code format to validate
     * @return bool Validity is ok or not
     */
    public static function isZipCodeFormat($zip_code)
    {
        if (Tools::getValue('country') == 21) return true;    
        if (!empty($zip_code)) {
            return preg_match('/^[NLCnlc 0-9-]+$/', $zip_code);
        }
        return true;
    }
    
}