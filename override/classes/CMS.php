<?php
class CMS extends CMSCore
{
    
    public $meta_title;
    public $meta_description;
    public $meta_keywords;
    public $content;
    public $link_rewrite;
    public $header;
    public $id_cms_category;
    public $position;
    public $indexation;
    public $active;
    public $featured;
    public $longtitle;
    
    public static $definition = array(
        'table' => 'cms',
        'primary' => 'id_cms',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'id_cms_category' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'position' =>            array('type' => self::TYPE_INT),
            'indexation' =>         array('type' => self::TYPE_BOOL),
            'active' =>            array('type' => self::TYPE_BOOL),
           'featured' =>            array('type' => self::TYPE_BOOL),
            
            'meta_description' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'header' =>        array('type' => self::TYPE_HTML, 'lang' => true, 'size' => 3999999999999),
            'meta_title' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 128),
            'longtitle' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 65000),
            'link_rewrite' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'required' => true, 'size' => 128),
            'content' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 3999999999999),
        ),
    );
	
	
	function uniteGalleryModify($text){
			
		if(is_array($text))
			return($text);
		
		if (strpos($text, 'unitegallery') === false)
			return $text;
		
		$regex = '/\[unitegallery\s+(.*?)\]/i';
		preg_match_all($regex, $text, $arrMatches, PREG_SET_ORDER);
				
		if(empty($arrMatches))
			return($text);
		
        if(!defined("_JEXEC"))
        	define("_JEXEC", true);
        
 		$filepathUG = _PS_MODULE_DIR_ ."unitegallery/includes.php";
        
    	require_once($filepathUG);
    	require_once _PS_MODULE_DIR_."unitegallery/inc_php/framework/provider/provider_main_file.php";
    	
		foreach($arrMatches as $match){
			if(!isset($match[1]))
				continue;
		
			$arguments = $match[1];
		
			$keywords = preg_split("/\s+/", $arguments);
			$galleryID = $keywords[0];
		
			$catID = null;
			if(count($keywords) > 1){
				$strcat = $keywords[1];
				$strcat = str_replace("catid=", "", $strcat);
				$strcat = str_replace("catid =", "", $strcat);
				$strcat = str_replace("catid = ", "", $strcat);
				$catID = trim($strcat);
			}
		
			$content = HelperUG::outputGallery($galleryID, $catID, "alias");
		
			$match = $match[0];
		
			$pos = strpos($text, $match);
			if($pos === false)
				continue;
			$text = substr_replace($text, $content, $pos, strlen($match));
		
		}
		
		
		return($text);
	}
	
	
	
    
    
}
