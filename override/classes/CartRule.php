<?php
class CartRule extends CartRuleCore
{
    /*
    * module: rpcarrierdiscount
    * date: 2017-06-08 16:07:27
    * version: 1.2.9
    */
    public $rpcarrierdiscount_cart = 0;
    /*
    * module: rpcarrierdiscount
    * date: 2017-06-08 16:07:27
    * version: 1.2.9
    */
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        self::$definition['fields']['rpcarrierdiscount_cart'] =
            array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1);
        parent::__construct($id, $id_lang, $id_shop);
    }
    
    public function checkValidity(Context $context, $alreadyInCart = false, $display_error = true, $check_carrier = true)
    {
        if (!CartRule::isFeatureActive()) {
            return false;
        }
        if (!$this->active) {
            return (!$display_error) ? false : Tools::displayError('This voucher is disabled');
        }
        if (!$this->quantity) {
            return (!$display_error) ? false : Tools::displayError('This voucher has already been used');
        }
        if (strtotime($this->date_from) > time()) {
            return (!$display_error) ? false : Tools::displayError('This voucher is not valid yet');
        }
        if (strtotime($this->date_to) < time()) {
            return (!$display_error) ? false : Tools::displayError('This voucher has expired');
        }
        if (!$context->cart->id_carrier) {
            $context->cart->id_carrier = (int)Configuration::get('PS_CARRIER_DEFAULT');
        }
        if ($context->cart->id_customer) {
            $quantityUsed = Db::getInstance()->getValue('
			SELECT count(*)
			FROM '._DB_PREFIX_.'orders o
			LEFT JOIN '._DB_PREFIX_.'order_cart_rule od ON o.id_order = od.id_order
			WHERE o.id_customer = '.$context->cart->id_customer.'
			AND od.id_cart_rule = '.(int)$this->id.'
			AND '.(int)Configuration::get('PS_OS_ERROR').' != o.current_state
			');
            if ($quantityUsed + 1 > $this->quantity_per_user) {
                return (!$display_error) ? false : Tools::displayError('You cannot use this voucher anymore (usage limit reached)');
            }
        }
        if ($this->group_restriction) {
            $id_cart_rule = (int)Db::getInstance()->getValue('
			SELECT crg.id_cart_rule
			FROM '._DB_PREFIX_.'cart_rule_group crg
			WHERE crg.id_cart_rule = '.(int)$this->id.'
			AND crg.id_group '.($context->cart->id_customer ? 'IN (SELECT cg.id_group FROM '._DB_PREFIX_.'customer_group cg WHERE cg.id_customer = '.(int)$context->cart->id_customer.')' : '= '.(int)Configuration::get('PS_UNIDENTIFIED_GROUP')));
            if (!$id_cart_rule) {
                return (!$display_error) ? false : Tools::displayError('You cannot use this voucher');
            }
        }
        if ($this->country_restriction) {
            if (!$context->cart->id_address_delivery) {
                return (!$display_error) ? false : Tools::displayError('You must choose a delivery address before applying this voucher to your order');
            }
            $id_cart_rule = (int)Db::getInstance()->getValue('
			SELECT crc.id_cart_rule
			FROM '._DB_PREFIX_.'cart_rule_country crc
			WHERE crc.id_cart_rule = '.(int)$this->id.'
			AND crc.id_country = (SELECT a.id_country FROM '._DB_PREFIX_.'address a WHERE a.id_address = '.(int)$context->cart->id_address_delivery.' LIMIT 1)');
            if (!$id_cart_rule) {
                return (!$display_error) ? false : Tools::displayError('You cannot use this voucher in your country of delivery');
            }
        }
        if ($this->carrier_restriction && $check_carrier) {
            if (!$context->cart->id_carrier) {
                return (!$display_error) ? false : Tools::displayError('You must choose a carrier before applying this voucher to your order');
            }
            $id_cart_rule = (int)Db::getInstance()->getValue('
			SELECT crc.id_cart_rule
			FROM '._DB_PREFIX_.'cart_rule_carrier crc
			INNER JOIN '._DB_PREFIX_.'carrier c ON (c.id_reference = crc.id_carrier AND c.deleted = 0)
			WHERE crc.id_cart_rule = '.(int)$this->id.'
			AND c.id_carrier = '.(int)$context->cart->id_carrier);
            if (!$id_cart_rule) {
                return (!$display_error) ? false : Tools::displayError('You cannot use this voucher with this carrier');
            }
        }
        if ($this->shop_restriction && $context->shop->id && Shop::isFeatureActive()) {
            $id_cart_rule = (int)Db::getInstance()->getValue('
			SELECT crs.id_cart_rule
			FROM '._DB_PREFIX_.'cart_rule_shop crs
			WHERE crs.id_cart_rule = '.(int)$this->id.'
			AND crs.id_shop = '.(int)$context->shop->id);
            if (!$id_cart_rule) {
                return (!$display_error) ? false : Tools::displayError('You cannot use this voucher');
            }
        }
        if ($this->product_restriction) {
            $r = $this->checkProductRestrictions($context, false, $display_error, $alreadyInCart);
            if ($r !== false && $display_error) {
                return $r;
            } elseif (!$r && !$display_error) {
                return false;
            }
        }
        if ($this->id_customer && $context->cart->id_customer != $this->id_customer) {
            if (!Context::getContext()->customer->isLogged()) {
                return (!$display_error) ? false : (Tools::displayError('You cannot use this voucher').' - '.Tools::displayError('Please log in first'));
            }
            return (!$display_error) ? false : Tools::displayError('You cannot use this voucher');
        }
        if ($this->minimum_amount && $check_carrier) {
            $minimum_amount = $this->minimum_amount;
            if ($this->minimum_amount_currency != Context::getContext()->currency->id) {
                $minimum_amount = Tools::convertPriceFull($minimum_amount, new Currency($this->minimum_amount_currency), Context::getContext()->currency);
            }
            $cartTotal = $context->cart->getOrderTotal($this->minimum_amount_tax, Cart::ONLY_PRODUCTS);
            if ($this->minimum_amount_shipping) {
                $cartTotal += $context->cart->getOrderTotal($this->minimum_amount_tax, Cart::ONLY_SHIPPING);
            }
            $products = $context->cart->getProducts();
            $cart_rules = $context->cart->getCartRules();
            foreach ($cart_rules as &$cart_rule) {
                if ($cart_rule['gift_product']) {
                    foreach ($products as $key => &$product) {
                        if (empty($product['gift']) && $product['id_product'] == $cart_rule['gift_product'] && $product['id_product_attribute'] == $cart_rule['gift_product_attribute']) {
                            $cartTotal = Tools::ps_round($cartTotal - $product[$this->minimum_amount_tax ? 'price_wt' : 'price'], (int)$context->currency->decimals * _PS_PRICE_COMPUTE_PRECISION_);
                        }
                    }
                }
            }
            if ($cartTotal < $minimum_amount) {
                return (!$display_error) ? false : Tools::displayError('You have not reached the minimum amount required to use this voucher');
            }
        }
        
        $nb_products = Cart::getNbProducts($context->cart->id);
        $otherCartRules = array();
        if ($check_carrier) {
            $otherCartRules = $context->cart->getCartRules();
        }
        if (count($otherCartRules)) {
            foreach ($otherCartRules as $otherCartRule) {
                if ($otherCartRule['id_cart_rule'] == $this->id && !$alreadyInCart) {
                    return (!$display_error) ? false : Tools::displayError('This voucher is already in your cart');
                }
                if ($otherCartRule['gift_product']) {
                    --$nb_products;
                }
                if ($this->cart_rule_restriction && $otherCartRule['cart_rule_restriction'] && $otherCartRule['id_cart_rule'] != $this->id) {
                    $combinable = Db::getInstance()->getValue('
					SELECT id_cart_rule_1
					FROM '._DB_PREFIX_.'cart_rule_combination
					WHERE (id_cart_rule_1 = '.(int)$this->id.' AND id_cart_rule_2 = '.(int)$otherCartRule['id_cart_rule'].')
					OR (id_cart_rule_2 = '.(int)$this->id.' AND id_cart_rule_1 = '.(int)$otherCartRule['id_cart_rule'].')');
                    if (!$combinable) {
                        $cart_rule = new CartRule((int)$otherCartRule['id_cart_rule'], $context->cart->id_lang);
                        if ($cart_rule->priority <= $this->priority) {
                            return (!$display_error) ? false : Tools::displayError('This voucher is not combinable with an other voucher already in your cart:').' '.$cart_rule->name;
                        }
                        else {
                            $context->cart->removeCartRule($cart_rule->id);
                        }
                    }
                }
            }
        }
        if (!$nb_products) {
            return (!$display_error) ? false : Tools::displayError('Cart is empty');
        }
        if (!$display_error) {
            return true;
        }
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public static function autoRemoveFromCart($context = null)
    {
        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            $quantityDiscount = new QuantityDiscountRule();
            $quantityDiscount->createAndRemoveRules();
        }
        parent::autoRemoveFromCart($context);
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public static function autoAddToCart(Context $context = null)
    {
        parent::autoAddToCart($context);
        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            $quantityDiscount = new QuantityDiscountRule();
            $quantityDiscount->createAndRemoveRules();
        }
    }
    /*
    * module: quantitydiscountpro
    * date: 2018-09-06 11:35:48
    * version: 2.1.13
    */
    public function delete()
    {
        $r = parent::delete();
        if (Module::isEnabled('quantitydiscountpro')) {
            include_once(_PS_MODULE_DIR_.'quantitydiscountpro/quantitydiscountpro.php');
            if ((bool)Configuration::get('PS_CART_RULE_FEATURE_ACTIVE') != (bool)QuantityDiscountRule::isCurrentlyUsed(null, true)
                || (bool)QuantityDiscountRule::isCurrentlyUsed(null, true)) {
                Configuration::updateGlobalValue('PS_CART_RULE_FEATURE_ACTIVE', true);
            }
        }
        return $r;
    }
}
