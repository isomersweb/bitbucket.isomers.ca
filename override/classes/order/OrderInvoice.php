<?php
class OrderInvoice extends OrderInvoiceCore 
{
    public function getProducts($products = false, $selected_products = false, $selected_qty = false)
    {
        if (!$products) {
            $products = $this->getProductsDetail();
        }

        $order = new Order($this->id_order);
        $customized_datas = Product::getAllCustomizedDatas($order->id_cart);

        $result_array = array();
        
        foreach ($products as $row) {
            // Change qty if selected
            $product_pack = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        		SELECT pl.name AS product_pack_name, pk.quantity AS product_pack_quantity, pk.id_product_item AS product_pack_item
        		FROM `'._DB_PREFIX_.'pack` pk
        		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = pk.id_product_item AND pl.id_lang='.Context::getContext()->language->id.')
        		WHERE pk.`id_product_pack` = '.(int)$row['id_product']);
        
            $row['product_pack'] = $product_pack;
            
            if ($selected_qty) {
                $row['product_quantity'] = 0;
                foreach ($selected_products as $key => $id_product) {
                    if ($row['id_order_detail'] == $id_product) {
                        $row['product_quantity'] = (int)$selected_qty[$key];
                    }
                }
                if (!$row['product_quantity']) {
                    continue;
                }
            }

            $this->setProductImageInformations($row);
            $this->setProductCurrentStock($row);
            $this->setProductCustomizedDatas($row, $customized_datas);

            // Add information for virtual product
            if ($row['download_hash'] && !empty($row['download_hash'])) {
                $row['filename'] = ProductDownload::getFilenameFromIdProduct((int)$row['product_id']);
                // Get the display filename
                $row['display_filename'] = ProductDownload::getFilenameFromFilename($row['filename']);
            }

            $row['id_address_delivery'] = $order->id_address_delivery;

            /* Ecotax */
            $round_mode = $order->round_mode;

            $row['ecotax_tax_excl'] = $row['ecotax']; // alias for coherence
            $row['ecotax_tax_incl'] = $row['ecotax'] * (100 + $row['ecotax_tax_rate']) / 100;
            $row['ecotax_tax'] = $row['ecotax_tax_incl'] - $row['ecotax_tax_excl'];

            if ($round_mode == Order::ROUND_ITEM) {
                $row['ecotax_tax_incl'] = Tools::ps_round($row['ecotax_tax_incl'], _PS_PRICE_COMPUTE_PRECISION_, $round_mode);
            }

            $row['total_ecotax_tax_excl'] = $row['ecotax_tax_excl'] * $row['product_quantity'];
            $row['total_ecotax_tax_incl'] = $row['ecotax_tax_incl'] * $row['product_quantity'];

            $row['total_ecotax_tax'] = $row['total_ecotax_tax_incl'] - $row['total_ecotax_tax_excl'];

            foreach (array(
                'ecotax_tax_excl',
                'ecotax_tax_incl',
                'ecotax_tax',
                'total_ecotax_tax_excl',
                'total_ecotax_tax_incl',
                'total_ecotax_tax'
            ) as $ecotax_field) {
                $row[$ecotax_field] = Tools::ps_round($row[$ecotax_field], _PS_PRICE_COMPUTE_PRECISION_, $round_mode);
            }

            // Aliases
            $row['unit_price_tax_excl_including_ecotax'] = $row['unit_price_tax_excl'];
            $row['unit_price_tax_incl_including_ecotax'] = $row['unit_price_tax_incl'];
            $row['total_price_tax_excl_including_ecotax'] = $row['total_price_tax_excl'];
            $row['total_price_tax_incl_including_ecotax'] = $row['total_price_tax_incl'];
            
            /* Stock product */
            $result_array[(int)$row['id_order_detail']] = $row;
        }

        if ($customized_datas) {
            Product::addCustomizationPrice($result_array, $customized_datas);
        }
        return $result_array;
    }
    
    public function getInvoiceNumberFormatted($id_lang, $id_shop = null)
    {
        $this->order = new Order((int)$this->id_order);
        $invoice_prefix = "";
        if ($this->order->source == '2') {
            $invoice_prefix = "p";
        }
        elseif ($this->order->source == '3') {
            $invoice_prefix = "w";
        }
        $invoice_formatted_number = Hook::exec('actionInvoiceNumberFormatted', array(
            get_class($this) => $this,
            'id_lang' => (int)$id_lang,
            'id_shop' => (int)$id_shop,
            'number' => (int)$this->number . $invoice_prefix
        ));


        if (!empty($invoice_formatted_number)) {
            return $invoice_formatted_number;
        }

        $format = '%1$s%2$06d';

        if (Configuration::get('PS_INVOICE_USE_YEAR')) {
            $format = Configuration::get('PS_INVOICE_YEAR_POS') ? '%1$s%3$s/%2$06d' : '%1$s%2$06d/%3$s';
        }
        return sprintf($format, Configuration::get('PS_INVOICE_PREFIX', (int)$id_lang, null, (int)$id_shop), $this->number, date('Y', strtotime($this->date_add))).$invoice_prefix;
    }
    
    public function getProductsDetail_merged()
    {
        $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'product` p
		ON p.id_product = od.product_id
		LEFT JOIN `'._DB_PREFIX_.'pack` pk
		ON pk.id_product_item = od.product_id
		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
		WHERE od.`id_order` = '.(int)$this->id_order.' ORDER BY od.`product_name`');
        
        return $this->getProducts($products); 
    }    
}