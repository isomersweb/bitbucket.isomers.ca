{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<table class="product" width="100%" cellpadding="0" cellspacing="0">
	
	<thead>
		<tr>
			<th class="product header small" width="25%">{l s='Reference' pdf='true'}</th>
			<th class="product header small" width="65%">{l s='Product' pdf='true'}</th>
			<th class="product header small" width="10%">{l s='Qty' pdf='true'}</th>
		</tr>
	</thead>

	<tbody>
		<!-- PRODUCTS -->
		{foreach $order_details as $order_detail}
			{cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_class}
			<tr class="product {$bgcolor_class}">
                <td colspan="3">
                <table style="width: 100%;">
                    <tr>                
        				<td width="25%" class="product center" style="line-height:6px; font-size: 11 pt;">
        					{if empty($order_detail.product_reference)}
        						---
        					{else}
        						{$order_detail.product_reference}
        					{/if}
        				</td>
        				<td width="65%" class="product left" style="line-height:6px; font-size: 11 pt;">
        					{if $display_product_images}
        						<table width="100%">
        							<tr>
        								<td width="15%">
        									{if isset($order_detail.image) && $order_detail.image->id}
        										{$order_detail.image_tag}
        									{/if}
        								</td>
        								<td width="5%">&nbsp;</td>
        								<td width="80%">
        									{$order_detail.product_name}
        								</td>
        							</tr>
        						</table>
        					{else}
        						{$order_detail.product_name}
        					{/if}
        				</td>
        				<td width="10%" class="product center" style="line-height:6px; font-size: 11 pt;">
        					{$order_detail.product_quantity-$order_detail.product_quantity_refunded}
        				</td>
                    </tr>
                </table>
                 {if $order_detail.product_pack}
                	<table style="width: 100%;" cellpadding="5">
                        {foreach $order_detail.product_pack as $product_pack}
                            <tr class="product_pack">
    							<td width="25%">&nbsp;</td>
    							<td width="45%">
                                    {$product_pack.product_pack_name}
                                </td>
    							<td width="30%">
    								{($product_pack.product_pack_quantity-$order_detail.product_quantity_refunded)}{l s='(Unit) x ' pdf='true'}{($order_detail.product_quantity-$order_detail.product_quantity_refunded)}{l s='(Kit Qty)=' pdf='true'}
									{($product_pack.product_pack_quantity-$order_detail.product_quantity_refunded)*($order_detail.product_quantity-$order_detail.product_quantity_refunded)}
    							</td>
    						</tr>
                        {/foreach}
    				</table>
                {/if}
               </td>				
			</tr>
			
			{foreach $order_detail.customizedDatas as $customizationPerAddress}
				{foreach $customizationPerAddress as $customizationId => $customization}
					<tr class="customization_data {$bgcolor_class}">
						<td class="center"> &nbsp;</td>

						<td>
							{if isset($customization.datas[Product::CUSTOMIZE_TEXTFIELD]) && count($customization.datas[Product::CUSTOMIZE_TEXTFIELD]) > 0}
								<table style="width: 100%;">
									{foreach $customization.datas[Product::CUSTOMIZE_TEXTFIELD] as $customization_infos}
										<tr>
											<td style="width: 30%;">
												{$customization_infos.name|string_format:{l s='%s:' pdf='true'}}
											</td>
											<td>{$customization_infos.value}</td>
										</tr>
									{/foreach}
								</table>
							{/if}

							{if isset($customization.datas[Product::CUSTOMIZE_FILE]) && count($customization.datas[Product::CUSTOMIZE_FILE]) > 0}
								<table style="width: 100%;">
									<tr>
										<td style="width: 30%;">{l s='image(s):' pdf='true'}</td>
										<td>{count($customization.datas[Product::CUSTOMIZE_FILE])}</td>
									</tr>
								</table>
							{/if}
						</td>

						<td class="center">
							({if $customization.quantity == 0}1{else}{$customization.quantity}{/if})
						</td>

					</tr>
				{/foreach}
			{/foreach}
			
			
			
		{/foreach}
		<!-- END PRODUCTS -->
{*	
    {assign var="shipping_discount_tax_incl" value="0"}
    {foreach from=$cart_rules item=cart_rule name="cart_rules_loop"}
		{if $smarty.foreach.cart_rules_loop.first}
		<tr class="discount">
			<th class="header" colspan="{$layout._colCount}">
				{l s='Discounts' pdf='true'}
			</th>
		</tr>
		{/if}
		<tr class="discount">
			<td class="white right" width="80%">
				{$cart_rule.name}
			</td>
			<td class="center white" width="20%">
				- {displayPrice currency=$order->id_currency price=$cart_rule.value_tax_excl}
			</td>
		</tr>
	{/foreach}
*}
	</tbody>

</table>
